/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/9/13
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class ServiceCost {
	int cost;

	int remainingCredit;

	public ServiceCost(int cost, int remainingCredit) {
		this.cost = cost;
		this.remainingCredit = remainingCredit;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getRemainingCredit() {
		return remainingCredit;
	}

	public void setRemainingCredit(int remainingCredit) {
		this.remainingCredit = remainingCredit;
	}

	@Override
	public String toString() {
		return "ServiceCost{" +
						"cost=" + cost +
						", remainingCredit=" + remainingCredit +
						'}';
	}

}
