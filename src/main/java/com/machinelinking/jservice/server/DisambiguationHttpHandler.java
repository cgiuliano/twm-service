/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */
package com.machinelinking.jservice.server;

import java.util.Locale;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.annotation.key.LanguageNotSupportedException;
import org.apache.log4j.Logger;

import eu.fbk.twm.utils.analysis.Token;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 12/22/12 ServiceTime: 10:07
 * AM To change this template use File | Settings | File Templates.
 * <p/>
 * <code>http://twm.apnetwork.it:9000/annotate?app_id=c3e5ca13&app_key=cc193754ccbfd7c7f85648585b4e00f3&text=Ulica+Francuska+%E2%80%93+warszawska+ulica+na+Saskiej+K%C4%99pie%2C+rozpoczynaj%C4%85ca+si%C4%99+przy+placu+Przymierza%2C+a+ko%C5%84cz%C4%85ca+przy+rondzie+Waszyngtona.+Znajduj%C4%85+si%C4%99+przy+niej+domy+mieszkalne+%28w+tym+zabytkowe+obiekty+z+okresu+dwudziestolecia+mi%C4%99dzywojennego%29+oraz+lokale+us%C5%82ugowe%2C+w+tym+gastronomiczne.+Wzd%C5%82u%C5%BC+ca%C5%82ej+d%C5%82ugo%C5%9Bci+ul.</code>
 */
public class DisambiguationHttpHandler extends AnnotateHttpHandler {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>DisambiguationHttpHandler</code>.
	 */
	static Logger logger = Logger.getLogger(DisambiguationHttpHandler.class.getName());

	@Override
	public void service(MLRequest request, MLResponse response) throws MissingRequiredParameterException, LanguageNotSupportedException, AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException {
		logger.info(Thread.currentThread().getName() + " handling annotation...");

		ServiceTime serviceTime = new ServiceTime();
		// todo: allow text downloading from url
		DisambiguationParameter disambiguationParameter = new DisambiguationParameter(request);

		StringBuilder text = new StringBuilder();
		text.append(disambiguationParameter.getLeft());
		text.append(disambiguationParameter.getTerm());
		text.append(disambiguationParameter.getRight());
		accessManagement.authorize(serviceTime, disambiguationParameter, ANNOTATE_CMD, text.length());

		serviceTime.executionBegin();

		// the tokenization is always done
		Token[] leftTokenArray = tokenizer.tokenArray(disambiguationParameter.getLeft());
		Token[] rightTokenArray = tokenizer.tokenArray(disambiguationParameter.getRight());
		Token[] termTokenArray = tokenizer.tokenArray(disambiguationParameter.getTerm());
		int i = leftTokenArray.length;
		int j = i + termTokenArray.length - 1;

		Token[] tokenArray = new Token[leftTokenArray.length + termTokenArray.length + rightTokenArray.length];
		System.arraycopy(leftTokenArray, 0, tokenArray, 0, leftTokenArray.length);
		System.arraycopy(termTokenArray, 0, tokenArray, i, termTokenArray.length);
		System.arraycopy(rightTokenArray, 0, tokenArray, j, rightTokenArray.length);
		Locale locale = disambiguationParameter.isLangSpecified() ? new Locale(disambiguationParameter.getLang()) : langIdentifier.detect(tokenArray);
		Keyword[] keywordArray = keywordExtractor.extract(tokenArray, i, j, text.toString(), locale);

		// apply filters
		normFilter.filter(keywordArray);
		if (disambiguationParameter.includesMinimumWeight()) {
			weightFilter.filter(keywordArray, locale, disambiguationParameter.getMinimumWeight());
		}
		else {
			weightFilter.filter(keywordArray, locale);
		}
		//weightSortFilter.filter(keywordArray);
		//overlappingFilter.filter(keywordArray);
		oneExamplePerSenseKeywordLinker.link(keywordArray, tokenArray, locale);

		if (disambiguationParameter.includesLink()) {
			linkEnhancer.enhance(keywordArray, locale);
		}
		if (disambiguationParameter.includesForm()) {
			formEnhancer.enhance(keywordArray, locale);
		}
		if (disambiguationParameter.includesCross()) {
			crossLangEnhancer.enhance(keywordArray, locale);
		}
		if (disambiguationParameter.includesType()) {
			typeEnhancer.enhance(keywordArray, locale);
		}


		serviceTime.executionEnd();
		logger.info(keywordArray.length + " annotations done in " + sf.format(serviceTime.executionNanoTime()) + " ns");
		String result = null;
		if (disambiguationParameter.getOutputFormat().equalsIgnoreCase("jsonld")) {
			result = toJSonLD(keywordArray, null, locale, disambiguationParameter, serviceTime, 0);
		}
		else {
			result = toJSon(keywordArray, null, locale, disambiguationParameter, serviceTime, 0);
		}

		response.setResult(result);

		logManagement.writeLog(disambiguationParameter, DISAMBIGUATION_CMD, serviceTime, disambiguationParameter.getText().length(), locale, request.getHeaders());
	}
}