/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/18/13
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class AbstractParameter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AbstractParameter</code>.
	 */
	static Logger logger = Logger.getLogger(AbstractParameter.class.getName());

	protected static final String TRUE = "1";

	protected static final String FALSE = "0";

	public static final String DEFAULT_OUTPUT_FORMAT = "json";

	protected String outputFormat;

	protected AbstractParameter(MLRequest request) {
		outputFormat = request.getParameter("output_format");
		if (outputFormat == null) {
			outputFormat = DEFAULT_OUTPUT_FORMAT;
		}
		//logger.debug(outputFormat);
	}

	public String getOutputFormat() {
		return outputFormat;
	}

	/*protected String callback;

	protected boolean includeCallback;

	protected AbstractParameter(MLRequest request) {
		callback = request.getParameter("callback");
		logger.debug(callback);
		includeCallback = callback != null;
		logger.debug(includeCallback);
	}

	public boolean includesCallback() {
		return includeCallback;
	}

	public String getCallback() {
		return callback;
	}   */

	/**
	 * Read the specified parameter. If the parameter is null or only blanks,
	 * then MissingRequiredParameterException is fired.
	 *
	 * @param request
	 * @param name
	 * @return the parameter only in case exists
	 * @throws java.io.UnsupportedEncodingException
	 *
	 * @throws MissingRequiredParameterException
	 *
	 */
	protected String getRequiredParameter(MLRequest request, String name) throws MissingRequiredParameterException {
		String val = request.getParameter(name);
		if (val == null || val.trim().length() == 0) {
			throw new MissingRequiredParameterException(name);
		}
		return val;
	}

	@Override
	public String toString() {
		return "AbstractParameter{" +
				"outputFormat='" + outputFormat + '\'' +
				'}';
	}
}
