/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */
package com.machinelinking.jservice.server;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

import com.machinelinking.jservice.analysis.HardSentenceBoundariesDetector;
import com.machinelinking.jservice.analysis.Sentence;
import com.machinelinking.jservice.analysis.SentenceBoundariesDetector;
import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.annotation.key.KeywordExtractor;
import com.machinelinking.jservice.annotation.key.LanguageNotSupportedException;
import com.machinelinking.jservice.annotation.key.filter.NormFilter;
import com.machinelinking.jservice.annotation.key.filter.OverlappingFilter;
import com.machinelinking.jservice.annotation.key.filter.WeightFilter;
import com.machinelinking.jservice.annotation.key.filter.WeightSortFilter;
import com.machinelinking.jservice.annotation.lang.LangIdentifier;
import com.machinelinking.jservice.summary.WeightedSentence;
import com.machinelinking.jservice.summary.TextSummarization;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import eu.fbk.twm.utils.analysis.Token;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 12/22/12 ServiceTime: 10:07
 * AM To change this template use File | Settings | File Templates.
 * <p/>
 * <code>http://twm.apnetwork.it:9000/annotate?app_id=c3e5ca13&app_key=cc193754ccbfd7c7f85648585b4e00f3&text=Ulica+Francuska+%E2%80%93+warszawska+ulica+na+Saskiej+K%C4%99pie%2C+rozpoczynaj%C4%85ca+si%C4%99+przy+placu+Przymierza%2C+a+ko%C5%84cz%C4%85ca+przy+rondzie+Waszyngtona.+Znajduj%C4%85+si%C4%99+przy+niej+domy+mieszkalne+%28w+tym+zabytkowe+obiekty+z+okresu+dwudziestolecia+mi%C4%99dzywojennego%29+oraz+lokale+us%C5%82ugowe%2C+w+tym+gastronomiczne.+Wzd%C5%82u%C5%BC+ca%C5%82ej+d%C5%82ugo%C5%9Bci+ul.</code>
 */
//todo: set the desired number of words
public class SummaryHttpHandler extends AbstractHttpHandler {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>SummaryHttpHandler</code>.
	 */
	static Logger logger = Logger.getLogger(SummaryHttpHandler.class.getName());

	LangIdentifier langIdentifier;

	KeywordExtractor keywordExtractor;

	//OneExamplePerSenseKeywordLinker oneExamplePerSenseKeywordLinker;

	WeightFilter weightFilter;

	//AdjacentFilter adjacentFilter;

	WeightSortFilter weightSortFilter;

	OverlappingFilter overlappingFilter;

	NormFilter normFilter;

	//StopFilter stopFilter;

	//TypeFilter typeFilter;

	//CrossLangEnhancer crossLangEnhancer;

	//TypeEnhancer typeEnhancer;

	LogManagement logManagement;

	//IncomingOutgoingFilter incomingOutgoingFilter;

	//ImageEnhancer imageEnhancer;

	//AirpediaEnhancer airpediaEnhancer;

	//PageAbstractEnhancer pageAbstractEnhancer;

	SentenceBoundariesDetector sentenceBoundariesDetector;

	TextSummarization textSummarization;
	private boolean authentication;

	public SummaryHttpHandler(boolean authentication) {
		super();
		this.authentication = authentication;
		langIdentifier = LangIdentifier.getInstance();
		keywordExtractor = KeywordExtractor.getInstance();
		//baselineKeywordLinker = BaselineKeywordLinker.getInstance();
		//oneExamplePerSenseKeywordLinker = OneExamplePerSenseKeywordLinker.getInstance();
		weightFilter = WeightFilter.getInstance();
		weightSortFilter = WeightSortFilter.getInstance();
		normFilter = NormFilter.getInstance();
		overlappingFilter = OverlappingFilter.getInstance();
		//incomingOutgoingFilter = IncomingOutgoingFilter.getInstance();
		//stopFilter = StopFilter.getInstance();
		//adjacentFilter = AdjacentFilter.getInstance();
		//vectorFilter = VectorFilter.getInstance();
		//personNameFilter = PersonNameFilter.getInstance();
		//typeFilter = TypeFilter.getInstance();

		sentenceBoundariesDetector = new HardSentenceBoundariesDetector();
		textSummarization = TextSummarization.getInstance();
		logManagement = LogManagement.getInstance();
	}

	@Override
	public void service(MLRequest request, MLResponse response) throws MissingRequiredParameterException, LanguageNotSupportedException, AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException {
		logger.debug(Thread.currentThread().getName() + " handling annotation...");

		ServiceTime serviceTime = new ServiceTime();
		// todo: allow text downloading from url
		SummaryParameter summaryParameter = new SummaryParameter(request);

		if (authentication){
			accessManagement.authorize(serviceTime, summaryParameter, SUMMARY_CMD, summaryParameter.getText().length());
		}

		serviceTime.executionBegin();

		// the tokenization is always done
		Token[] tokenArray = tokenizer.tokenArray(summaryParameter.getText());
		Sentence[] sentenceArray = sentenceBoundariesDetector.split(tokenArray, summaryParameter.getText());
		Locale locale = summaryParameter.isLangSpecified() ? new Locale(summaryParameter.getLang()) : langIdentifier.detect(tokenArray);

		Keyword[] keywordArray = keywordExtractor.extract(tokenArray, summaryParameter.getText(), locale);
		//print(keywordArray, "pure");

		// apply normalization filter
		normFilter.filter(keywordArray);
		//print(keywordArray, "norm");

		// apply weight filter on keywords
		if (summaryParameter.includesMinimumWeight()) {
			weightFilter.filter(keywordArray, locale, summaryParameter.getMinimumWeight());
		}
		else {
			weightFilter.filter(keywordArray, locale);
		}
		//print(keywordArray, "weight");

		weightSortFilter.filter(keywordArray);
		//print(keywordArray, "sort");

		overlappingFilter.filter(keywordArray);
		//print(keywordArray, "over");

		int scoreFunction = TextSummarization.SUM_SCORE;
		//logger.debug("scoreFunction = " + summaryParameter.getFunc());
		if (summaryParameter.isArgMax()) {
			scoreFunction = TextSummarization.ARG_MAX_SCORE;
			//logger.debug("scoreFunction = " + scoreFunction);
		} else if (summaryParameter.isAverage()) {
			scoreFunction = TextSummarization.AVERAGE_SCORE;
			//logger.debug("scoreFunction = " + scoreFunction);
		}

		WeightedSentence[] summary = textSummarization.summary(keywordArray, sentenceArray, summaryParameter.getCompressionRatio(), scoreFunction);;


		//logger.debug("summary");
		//for (int i = 0; i < summary.length; i++) {
		//	logger.debug(i + "\t" + summary[i]);
		//}
		serviceTime.executionEnd();
		//logger.info(summary.length + " sentences extracted in " + nf.format(serviceTime.executionNanoTime()) + " ns");
		String result = null;

		if (summaryParameter.getOutputFormat().equalsIgnoreCase("jsonld")) {
			result = toJSonLD(summary, locale, summaryParameter, serviceTime, 0);
		}
		else {
			result = toJSon(summary, locale, summaryParameter, serviceTime, 0);
		}

		response.setResult(result);

		logManagement.writeLog(summaryParameter, SUMMARY_CMD, serviceTime, summaryParameter.getText().length(), locale, request.getHeaders());
	}

	private void print(Token[] tokenArray, String label) {
		logger.debug(label);
		for (int i = 0; i < tokenArray.length; i++) {
			logger.debug(i + "\t" + tokenArray[i]);
		}
	}

	private void print(Keyword[] keywordArray, String label) {
		logger.debug(label);
		logger.debug("size\tidf\tlf\tnorm\tweight\ttype\tform\tsense");
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered()) {
				logger.debug(i + "\t" + keywordArray[i]);
			}

		}
	}

	String toJSon(WeightedSentence[] sentenceArray, Locale locale, SummaryParameter summaryParameter, ServiceTime serviceTime, double cost) {
		try {
			StringWriter w = new StringWriter();

			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			if (summaryParameter.includesText()) {
				g.writeStringField("text", summaryParameter.getText());
			}

			g.writeObjectFieldStart("annotation");
			// g.writeStringField("status", "OK");
			if (summaryParameter.includesId()) {
				g.writeStringField("id", summaryParameter.getId());
			}

			// Call common code!
			toJSonCommon(g, sentenceArray, locale, serviceTime, cost, summaryParameter);

			g.writeEndObject();
			g.close();

			return w.toString();
		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}

	}

	static void toJSonCommon(JsonGenerator g, WeightedSentence[] sentenceArray, Locale locale, ServiceTime serviceTime, double cost, SummaryParameter summaryParameter) throws JsonGenerationException, IOException {
		g.writeStringField("lang", locale.getLanguage());
		g.writeObjectFieldStart("time");
		g.writeNumberField("auth", serviceTime.authenticationTime());
		g.writeNumberField("exec", serviceTime.executionTime());
		g.writeEndObject();
		// g.writeNumberField("time", (double) time / NANO);

		g.writeNumberField("cost", cost);

		g.writeStringField("func", summaryParameter.getFunc());
		g.writeNumberField("compression-ratio", summaryParameter.getCompressionRatio());

		g.writeFieldName("summary");
		g.writeStartArray();
		for (int j = 0; j < sentenceArray.length; j++) {
			// logger.debug(j + "\t" + keywordArray[j]);
			//if (!keywordArray[j].isFiltered()) {
			WeightedSentence sentence = sentenceArray[j];
			g.writeStartObject();
			g.writeStringField("sentence", sentence.getSentence().getText());
			g.writeNumberField("weight", sentence.getWeight());
			g.writeNumberField("start", sentence.getSentence().getStart());
			g.writeNumberField("end", sentence.getSentence().getEnd());

			g.writeEndObject();
		}
		g.writeEndArray();

	}

	String toJSonLD(WeightedSentence[] sentenceArray, Locale locale, SummaryParameter summaryParameter, ServiceTime serviceTime, double cost) {
		try {
			StringWriter w = new StringWriter();

			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();

			g.writeObjectFieldStart("@context");
			g.writeStringField("@language", locale.getLanguage());
			g.writeStringField("ml", "http://machinelinking.com/terms#");
			g.writeEndObject();


			if (summaryParameter.includesText()) {
				g.writeStringField("ml:text", summaryParameter.getText());
			}

			g.writeObjectFieldStart("ml:annotation");
			// g.writeStringField("status", "OK");
			if (summaryParameter.includesId()) {
				g.writeStringField("ml:id", summaryParameter.getId());
			}

			// Call common code!
			toJSonLDCommon(g, sentenceArray, locale, serviceTime, cost);

			g.writeEndObject();
			g.close();

			return w.toString();
		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}

	}

	static void toJSonLDCommon(JsonGenerator g, WeightedSentence[] sentenceArray, Locale locale, ServiceTime serviceTime, double cost) throws JsonGenerationException, IOException {
		g.writeStringField("ml:lang", locale.getLanguage());
		g.writeObjectFieldStart("ml:time");
		g.writeNumberField("ml:auth", serviceTime.authenticationTime());
		g.writeNumberField("ml:exec", serviceTime.executionTime());
		g.writeEndObject();
		// g.writeNumberField("time", (double) time / NANO);
		g.writeNumberField("ml:cost", cost);


		g.writeFieldName("ml:summary");
		g.writeStartArray();
		for (int j = 0; j < sentenceArray.length; j++) {
			WeightedSentence sentence = sentenceArray[j];
			g.writeStartObject();
			g.writeStringField("ml:sentence", sentence.getSentence().getText());
			g.writeNumberField("ml:weight", sentence.getWeight());
			g.writeNumberField("ml:start", sentence.getSentence().getStart());
			g.writeNumberField("ml:end", sentence.getSentence().getEnd());

			g.writeEndObject();
		}
		g.writeEndArray();

	}

}