/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/5/13
 * ServiceTime: 8:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class ServiceTime {
	public static final double NANO = Math.pow(10, -9);

	public static final double DECI = Math.pow(10, -1);

	public static final double CENTI = Math.pow(10, -2);

	public static final double MILLI = Math.pow(10, -3);

	public static final double MICRO = Math.pow(10, -6);


	long authenticationBegin, authenticationEnd;

	long executionBegin, executionEnd;

	public void executionBegin() {
		executionBegin = System.nanoTime();
	}

	public void executionEnd() {
		executionEnd = System.nanoTime();
	}

	public void authenticationBegin() {
		authenticationBegin = System.nanoTime();
	}

	public void authenticationEnd() {
		authenticationEnd = System.nanoTime();
	}

	public long authenticationNanoTime() {
		return authenticationEnd - authenticationBegin;
	}

	public long executionNanoTime() {
		return executionEnd - executionBegin;
	}

	public double authenticationCentiTime() {
		return (double) (authenticationEnd - authenticationBegin) * CENTI;
	}

	public double executionCentiTime() {
		return (double) (executionEnd - executionBegin) * CENTI;
	}

	public double authenticationMilliTime() {
		return (double) (authenticationEnd - authenticationBegin) * MILLI;
	}

	public double executionMilliTime() {
		return (double) (executionEnd - executionBegin) * MILLI;
	}

	public double authenticationMicroTime() {
		return (double) (authenticationEnd - authenticationBegin) * MICRO;
	}

	public double executionMicroTime() {
		return (double) (executionEnd - executionBegin) * MICRO;
	}

	public double authenticationTime() {
		return (double) (authenticationEnd - authenticationBegin) * NANO;
	}

	public double executionTime() {
		return (double) (executionEnd - executionBegin) * NANO;
	}

	@Override
	public String toString() {
		return "ServiceTime{" +
						"authenticationBegin=" + authenticationBegin +
						", authenticationEnd=" + authenticationEnd +
						", executionBegin=" + executionBegin +
						", executionEnd=" + executionEnd +
						'}';
	}
}
