/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import net.threescale.api.cache.DefaultCacheImpl;
import net.threescale.api.v2.Api2;
import net.threescale.api.v2.Api2Impl;
import net.threescale.api.v2.HttpSender;
import net.threescale.api.v2.HttpSenderImpl;

/**
 * Rewrite of the Threescale Api Factory to access ExpirationInterval that
 * otherwise is private/hardcoded!
 *
 * @author d@vide.bz
 */
public class MLThreescaleApiFactory {
	public static Api2 createV2ApiWithLocalCache(String url, String provider_private_key) {
		HttpSender sender = new HttpSenderImpl();
		return createV2ApiWithLocalCache(url, provider_private_key, sender);
	}

	private static Api2 createV2ApiWithLocalCache(String url, String provider_private_key, net.threescale.api.v2.HttpSender sender) {
		DefaultCacheImpl defaultCache = new DefaultCacheImpl(url, provider_private_key, sender);
		/*
		 * !!! WARNING: USE THE SAME VALUE HERE AND IN THE src/main/resources / etc/default.xml !!!
		 * If not, the REPORT DOES NOT WORK AND CUSTOMERS ARE USING SERVICE FOR FREE!!!
		 * After ANY CHANGE OF THIS VALUES TEST IT!!!
		 */
		final int expirationInterval = 20000;
		defaultCache.setAuthorizeExpirationInterval(expirationInterval);
		defaultCache.setReportExpirationInterval(expirationInterval);
		/* !!! END OF WARNING */
		return new Api2Impl(url, provider_private_key, sender, defaultCache);
	}
}
