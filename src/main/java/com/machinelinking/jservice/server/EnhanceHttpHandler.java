/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.machinelinking.jservice.annotation.key.enhance.*;
import com.machinelinking.jservice.annotation.topic.Topic;
import org.apache.log4j.Logger;
import eu.fbk.twm.index.PageAirpediaClassSearcher;
import eu.fbk.twm.utils.ClassResource;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 12/22/12 ServiceTime: 10:07
 * AM To change this template use File | Settings | File Templates.
 */
public class EnhanceHttpHandler extends AbstractHttpHandler {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>EnhanceHttpHandler</code>.
	 */
	static Logger logger = Logger.getLogger(EnhanceHttpHandler.class.getName());

	LinkEnhancer linkEnhancer;

	FormEnhancer formEnhancer;

	CrossLangEnhancer crossLangEnhancer;


	//todo: replace with DBpediaEnhancer?
	TypeEnhancer typeEnhancer;

	ImageEnhancer imageEnhancer;

	PageAbstractEnhancer pageAbstractEnhancer;

	AirpediaEnhancer airpediaEnhancer;

	//CategoryEnhancer categoryEnhancer;

	CategoryHierarchyEnhancer categoryEnhancer;

	TopicEnhancer topicEnhancer;
	private boolean authentication;

	public EnhanceHttpHandler() {
		this(true);
	}

	public EnhanceHttpHandler(boolean authentication) {
		this.authentication = authentication;
		logger.debug("creating an enhancer handler...");
		accessManagement = AccessManagement.getInstance();

		linkEnhancer = LinkEnhancer.getInstance();
		formEnhancer = FormEnhancer.getInstance();
		crossLangEnhancer = CrossLangEnhancer.getInstance();
		typeEnhancer = TypeEnhancer.getInstance();
		imageEnhancer = ImageEnhancer.getInstance();
		airpediaEnhancer = AirpediaEnhancer.getInstance();
		pageAbstractEnhancer = PageAbstractEnhancer.getInstance();
		categoryEnhancer = CategoryHierarchyEnhancer.getInstance();
		logManagement = LogManagement.getInstance();
		topicEnhancer = TopicEnhancer.getInstance();
	}

	@Override
	public void service(MLRequest request, MLResponse response) throws MissingRequiredParameterException, AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException
	{
		logger.info(Thread.currentThread().getName() + " handling enhancement...");
		ServiceTime serviceTime = new ServiceTime();

		EnhanceParameter enhanceParameter = new EnhanceParameter(request);

		if (authentication){
			accessManagement.authorize(serviceTime, enhanceParameter, PAGE_ENHANCH_CMD, 1);
		}

		serviceTime.executionBegin();
		Locale locale = new Locale(enhanceParameter.getLang());
		List<Link> linkList = linkEnhancer.enhance(enhanceParameter.getPage(), locale);
		List<Form> formList = formEnhancer.enhance(enhanceParameter.getPage(), locale);
		Map<String, String> crossLangMap = crossLangEnhancer.enhance(enhanceParameter.getPage(), locale);

		//List<Type> typeList = new ArrayList<Type>();
		//typeList.add(typeEnhancer.enhance(enhanceParameter.getPage(), locale));
		//typeList.addAll(typeEnhancer.enhance(enhanceParameter.getPage(), locale));

		List<ClassResource> airpediaClassList = new ArrayList<ClassResource>();
		//airpediaClassList.add(airpediaEnhancer.enhance(enhanceParameter.getPage(), locale));

		Type type = typeEnhancer.enhance(enhanceParameter.getPage(), locale);
		airpediaClassList.add(new ClassResource(type.getLabel(), 3, type.getProb(), 3));

		List<ClassResource> airpediaClasses = airpediaEnhancer.enhance(enhanceParameter.getPage(), locale);
		if (airpediaClasses.size() > 0) {
			airpediaClassList.clear();
			airpediaClassList.addAll(airpediaClasses);
		}
//		airpediaClassList.addAll(airpediaEnhancer.enhance(enhanceParameter.getPage(), locale));

		String pageAbstract = pageAbstractEnhancer.enhance(enhanceParameter.getPage(), locale);
		List<Image> imageList = imageEnhancer.enhance(enhanceParameter.getPage(), locale);
		//todo: parametrize the category depth
		List<Category> categoryList = categoryEnhancer.enhance(enhanceParameter.getPage(), locale, 1);

		//todo: add topic
		//List<Topic> keywordTopicList = keywordArray.getTopic().topicList();


		List<Topic> topicList = topicEnhancer.enhance(enhanceParameter.getPage(), locale);

		serviceTime.executionEnd();
		logger.debug("Enhancement done in " + nf.format(serviceTime.executionNanoTime()) + " ns");

		String result = null;

		if (enhanceParameter.getOutputFormat().equalsIgnoreCase("jsonld")) {
			result = toJSonLD(enhanceParameter.getPage(), locale, linkList, formList, crossLangMap, airpediaClassList, imageList, pageAbstract, categoryList, topicList, serviceTime, 0);
		}
		else {
			result = toJSon(enhanceParameter.getPage(), locale, linkList, formList, crossLangMap, airpediaClassList, imageList, pageAbstract, categoryList, topicList, serviceTime, 0);
		}
		response.setResult(result);

		logManagement.writeLog(enhanceParameter, ENHANCEMENT_CMD, serviceTime, 0, locale, request.getHeaders());
	}

	String toJSon(String page, Locale locale, List<Link> linkList, List<Form> formList, Map<String, String> crossLangMap, List<ClassResource> airpediaClassList, List<Image> imageList, String pageAbstract, List<Category> categoryList, List<Topic> topicList, ServiceTime serviceTime, double cost) {

		try {
			StringWriter w = new StringWriter();
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			g.writeObjectFieldStart("enrichment");

			g.writeStringField("page", page);
			g.writeStringField("lang", locale.getLanguage());
			g.writeObjectFieldStart("time");
			g.writeNumberField("auth", serviceTime.authenticationTime());
			g.writeNumberField("exec", serviceTime.executionTime());
			g.writeEndObject();

			g.writeNumberField("cost", cost);
			//g.writeStringField("type", TypeEnhancer.UNKNOWN_TYPE);
			g.writeStringField("abstract", pageAbstract);

			//todo: add topic start new code for topic
			//start work in progress
			if (topicList != null && topicList.size() > 0) {
				Topic topic;
				g.writeFieldName("topic");
				g.writeStartArray();
				for (int i = 0; i < topicList.size(); i++) {
					topic = topicList.get(i);
					//logger.debug(i + "\t" + topic);
					g.writeStartObject();
					g.writeStringField("label", topic.getLabel());
					g.writeStringField("url", topic.getURL().toString());
					g.writeNumberField("prob", topic.getConfidence());
					g.writeEndObject();
				}
				g.writeEndArray();
			}
			//end work in progress

			//todo: end new code

			/*if (typeList.size() > 0) {
				g.writeFieldName("class");
				g.writeStartArray();
				Type type;
				for (int i = 0; i < typeList.size(); i++) {
					type = typeList.get(i);
					g.writeStartObject();
					g.writeStringField("label", type.getLabel());
					g.writeStringField("url", type.getURL().toString());
					g.writeStringField("resource", type.getResource());
					g.writeNumberField("prob", type.getProb());
					g.writeEndObject();
				}
				g.writeEndArray();
			}*/

			//todo: move NAMESPACES and RESOURCES to ClassResource class
			if (airpediaClassList.size() > 0) {
				g.writeFieldName("class");
				g.writeStartArray();

				ClassResource classResource;
				for (int i = 0; i < airpediaClassList.size(); i++) {
					classResource = airpediaClassList.get(i);
					g.writeStartObject();
					g.writeStringField("label", LabelBuilder.build(classResource.getLabel()));
					g.writeStringField("url", "http://www.airpedia.org/ontology/class/" + classResource.getLabel());
					g.writeStringField("resource", "airpedia");
					g.writeNumberField("prob", classResource.getConfidence());
					g.writeEndObject();
				}
				g.writeEndArray();
			}

			if (categoryList.size() > 0) {
				Category category;
				g.writeFieldName("category");
				g.writeStartArray();
				for (int i = 0; i < categoryList.size(); i++) {
					category = categoryList.get(i);
					g.writeStartObject();
					g.writeStringField("label", category.getLabel());
					g.writeStringField("url", category.getURL().toString());
					g.writeEndObject();
				}
				g.writeEndArray();
			}

			if (linkList.size() > 0) {
				Link link;
				g.writeFieldName("external");
				g.writeStartArray();
				for (int i = 0; i < linkList.size(); i++) {
					link = linkList.get(i);
					g.writeStartObject();
					g.writeStringField("label", link.getLabel());
					g.writeStringField("url", link.getURL().toString());
					g.writeStringField("resource", link.getResource());

					g.writeEndObject();
				}
				g.writeEndArray();

			}

			if (imageList.size() > 0) {
				Image image;
				g.writeFieldName("image");
				g.writeStartArray();
				for (int i = 0; i < imageList.size(); i++) {
					image = imageList.get(i);
					g.writeStartObject();
					g.writeStringField("image", image.getImage());
					g.writeStringField("thumb", image.getThumb());

					g.writeEndObject();
				}
				g.writeEndArray();
			}

			Form form;
			if (formList.size() > 0) {
				g.writeFieldName("alt");
				g.writeStartArray();
				for (int i = 0; i < formList.size(); i++) {
					form = formList.get(i);
					g.writeStartObject();
					g.writeStringField("form", form.getForm());
					g.writeNumberField("freq", form.getProbability());

					g.writeEndObject();
				}
				g.writeEndArray();

			}

			if (crossLangMap.size() > 0) {
				g.writeFieldName("cross");
				g.writeStartArray();
				String foreignLang;
				Iterator<String> it = crossLangMap.keySet().iterator();
				for (int i = 0; it.hasNext(); i++) {
					foreignLang = it.next();
					g.writeStartObject();
					// g.writeStringField(foreignLang,
					// crossLangMap.get(foreignLang));
					g.writeStringField("lang", foreignLang);
					g.writeStringField("page", crossLangMap.get(foreignLang));
					g.writeEndObject();
				}
				g.writeEndArray();

			}

			g.writeEndObject();
			g.close();

			return w.toString();
		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}
	}

	String toJSonLD(String page, Locale locale, List<Link> linkList, List<Form> formList, Map<String, String> crossLangMap, List<ClassResource> airpediaClassList, List<Image> imageList, String pageAbstract, List<Category> categoryList, List<Topic> topicList, ServiceTime serviceTime, double cost) {

		try {
			StringWriter w = new StringWriter();
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();

			g.writeObjectFieldStart("@context");
			g.writeStringField("@language", locale.getLanguage());
			g.writeStringField("ml", "http://machinelinking.com/terms#");
			g.writeEndObject();

			g.writeObjectFieldStart("enrichment");
			g.writeStringField("ml:page", page);
			g.writeStringField("ml:lang", locale.getLanguage());
			g.writeObjectFieldStart("ml:time");
			g.writeNumberField("ml:auth", serviceTime.authenticationTime());
			g.writeNumberField("ml:exec", serviceTime.executionTime());
			g.writeEndObject();

			g.writeNumberField("ml:cost", cost);
			g.writeStringField("ml:type", TypeEnhancer.UNKNOWN_TYPE);
			g.writeStringField("ml:abstract", pageAbstract);

			if (topicList != null && topicList.size() > 0) {
				Topic topic;
				g.writeFieldName("ml:topic");
				g.writeStartArray();
				for (int i = 0; i < topicList.size(); i++) {
					topic = topicList.get(i);
					//logger.debug(i + "\t" + topic);
					g.writeStartObject();
					g.writeStringField("ml:label", topic.getLabel());
					g.writeStringField("ml:url", topic.getURL().toString());
					g.writeNumberField("ml:prob", topic.getConfidence());
					g.writeEndObject();
				}
				g.writeEndArray();
			}

			/*if (typeList.size() > 0) {
				g.writeFieldName("ml:class");
				g.writeStartArray();
				Type type;
				for (int i = 0; i < typeList.size(); i++) {
					type = typeList.get(i);
					g.writeStartObject();
					g.writeStringField("ml:label", type.getLabel());
					g.writeStringField("ml:url", type.getURL().toString());
					g.writeStringField("ml:resource", type.getResource());
					g.writeNumberField("ml:prob", type.getProb());
					g.writeEndObject();
				}
				g.writeEndArray();
			}*/
			if (airpediaClassList.size() > 0) {
				g.writeFieldName("class");
				g.writeStartArray();

				ClassResource classResource;
				for (int i = 0; i < airpediaClassList.size(); i++) {
					classResource = airpediaClassList.get(i);
					g.writeStartObject();
					g.writeStringField("ml:label", LabelBuilder.build(classResource.getLabel()));
					g.writeStringField("ml:url", PageAirpediaClassSearcher.NAMESPACES[classResource.getNamespace()] + classResource.getLabel());
					g.writeStringField("ml:resource", PageAirpediaClassSearcher.RESOURCES[classResource.getResource()]);
					g.writeNumberField("ml:prob", classResource.getConfidence());
					g.writeEndObject();
				}
				g.writeEndArray();
			}
			Link link;
			if (linkList.size() > 0) {
				g.writeFieldName("ml:external");
				g.writeStartArray();
				for (int i = 0; i < linkList.size(); i++) {
					link = linkList.get(i);
					g.writeStartObject();
					g.writeStringField("ml:label", link.getLabel());
					g.writeStringField("ml:url", link.getURL().toString());
					g.writeStringField("ml:resource", link.getResource());

					g.writeEndObject();
				}
				g.writeEndArray();
			}

			if (categoryList.size() > 0) {
				Category category;
				g.writeFieldName("ml:category");
				g.writeStartArray();
				for (int i = 0; i < categoryList.size(); i++) {
					category = categoryList.get(i);
					g.writeStartObject();
					g.writeStringField("ml:label", category.getLabel());
					g.writeStringField("ml:url", category.getURL().toString());
					g.writeEndObject();
				}
				g.writeEndArray();
			}

			if (imageList.size() > 0) {
				Image image;
				g.writeFieldName("ml:image");
				g.writeStartArray();
				for (int i = 0; i < imageList.size(); i++) {
					image = imageList.get(i);
					g.writeStartObject();
					g.writeStringField("ml:image", image.getImage());
					g.writeStringField("ml:thumb", image.getThumb());

					g.writeEndObject();
				}
				g.writeEndArray();
			}

			Form form;
			if (formList.size() > 0) {
				g.writeFieldName("ml:alt");
				g.writeStartArray();
				for (int i = 0; i < formList.size(); i++) {
					form = formList.get(i);
					g.writeStartObject();
					g.writeStringField("ml:form", form.getForm());
					g.writeNumberField("ml:freq", form.getProbability());

					g.writeEndObject();
				}
				g.writeEndArray();

			}
			if (crossLangMap.size() > 0) {
				g.writeFieldName("ml:cross");
				g.writeStartArray();
				String foreignLang;
				Iterator<String> it = crossLangMap.keySet().iterator();
				for (int i = 0; it.hasNext(); i++) {
					foreignLang = it.next();
					g.writeStartObject();
					// g.writeStringField(foreignLang,
					// crossLangMap.get(foreignLang));
					g.writeStringField("ml:lang", foreignLang);
					g.writeStringField("ml:page", crossLangMap.get(foreignLang));
					g.writeEndObject();
				}
				g.writeEndArray();

			}

			g.writeEndObject();
			g.close();

			return w.toString();
		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}
	}

}