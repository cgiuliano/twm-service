/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import java.io.UnsupportedEncodingException;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 1/6/13 Time: 9:35 AM To
 * change this template use File | Settings | File Templates.
 */
class AuthenticationParameter extends AbstractParameter {
	private String appId;
	private String appKey;

	AuthenticationParameter(MLRequest request) throws MissingRequiredParameterException {
		super(request);
		appId = getRequiredParameter(request, "app_id");
		appKey = getRequiredParameter(request, "app_key");
	}

	public String getAppId() {
		return appId;
	}

	public String getAppKey() {
		return appKey;
	}

}
