/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import com.machinelinking.jservice.annotation.key.Keyword;
import org.apache.log4j.Logger;

import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/9/13
 * Time: 11:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class AnnotationResult {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AnnotationResult</code>.
	 */
	static Logger logger = Logger.getLogger(AnnotationResult.class.getName());

	Locale locale;

	Keyword[] keywords;

	public AnnotationResult(Locale locale, Keyword[] keywords) {
		this.locale = locale;
		this.keywords = keywords;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Keyword[] getKeywords() {
		return keywords;
	}

	public void setKeywords(Keyword[] keywords) {
		this.keywords = keywords;
	}
}
