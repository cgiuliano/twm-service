/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Locale;

import com.machinelinking.jservice.annotation.lang.LangIdentifier;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import eu.fbk.twm.utils.analysis.Token;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 12/22/12 ServiceTime: 10:07
 * AM To change this template use File | Settings | File Templates.
 * <p/>
 * <code>http://localhost:8080/lang?app_id=c3e5ca13&app_key=cc193754ccbfd7c7f85648585b4e00f3&text=%20карфагенский%20полководец.%20Считается%20одним%20из%20величайших%20полководцев%20и%20государственных%20деятелей%20древности.%20Был%20заклятым%20врагом%20Римской%20республики%20и%20последним%20значимым%20лидером%20Карфагена%20перед%20его%20падением%20в%20серии%20Пунических%20войн.</code>
 * <code>http://localhost:9000/lang?app_id=c3e5ca13&app_key=cc193754ccbfd7c7f85648585b4e00f3&text=sono andato a Roma di domenica a vedere la Lazio.</code>
 */
public class LangHttpHandler extends AbstractHttpHandler {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>LangHttpHandler</code>.
	 */
	static Logger logger = Logger.getLogger(LangHttpHandler.class.getName());

	LangIdentifier langIdentifier;

	LogManagement logManagement;
	private boolean authentication;

	public LangHttpHandler() {
		this(true);
	}

	public LangHttpHandler(boolean authentication) {
		super();
		this.authentication=authentication;
		langIdentifier = LangIdentifier.getInstance();
		logManagement = LogManagement.getInstance();
	}


	@Override
	public void service(MLRequest request, MLResponse response) throws MissingRequiredParameterException, AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException {
		logger.debug(Thread.currentThread().getName() + " handling language identification...");
		ServiceTime serviceTime = new ServiceTime();

		LangParameter langParameter = new LangParameter(request);

		if (authentication){
			accessManagement.authorize(serviceTime, langParameter, LANG_CMD, langParameter.getText().length());
		}


		Locale locale = langCalc(serviceTime, langParameter, response);

		logManagement.writeLog(langParameter, LANG_CMD, serviceTime, langParameter.getText().length(), locale, request.getHeaders());
	}

	Locale langCalc(ServiceTime serviceTime, LangParameter langParameter, MLResponse response) {
		serviceTime.executionBegin();

		// the tokenization is always done
		Token[] tokenArray = tokenizer.tokenArray(langParameter.getText());

		Locale locale = langIdentifier.detect(tokenArray);

		serviceTime.executionEnd();
		logger.debug(locale + " recognized in " + sf.format(serviceTime.executionNanoTime()) + " ns");

		String result = null;
		if (langParameter.getOutputFormat().equalsIgnoreCase("jsonld")) {
			result = toJSonLD(locale, langParameter, serviceTime, 0);
		}
		else {
			result = toJSon(locale, langParameter, serviceTime, 0);
		}

		response.setResult(result);
		return locale;
	}

	// String toJSon(Locale locale, String id, String text, ServiceTime
	// serviceTime)
	String toJSon(Locale locale, LangParameter langParameter, ServiceTime serviceTime, double cost) {

		try {
			StringWriter w = new StringWriter();
			//logger.debug(langParameter);
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			if (langParameter.includesText()) {
				g.writeStringField("text", langParameter.getText());
			}
			g.writeObjectFieldStart("annotation");
			// g.writeStringField("status", "OK");
			if (langParameter.includesId()) {
				g.writeStringField("id", langParameter.getId());
			}
			g.writeStringField("lang", locale.getLanguage());
			g.writeObjectFieldStart("time");
			g.writeNumberField("auth", serviceTime.authenticationTime());
			g.writeNumberField("exec", serviceTime.executionTime());
			g.writeEndObject();
			g.writeNumberField("cost", cost);

			g.writeEndObject();
			g.close();

			return w.toString();

		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}
	}

	String toJSonLD(Locale locale, LangParameter langParameter, ServiceTime serviceTime, double cost) {

		try {
			StringWriter w = new StringWriter();
			//logger.debug(langParameter);
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();

			g.writeObjectFieldStart("@context");
			g.writeStringField("@language", locale.getLanguage());
			g.writeStringField("ml", "http://machinelinking.com/terms#");
			g.writeEndObject();

			if (langParameter.includesText()) {
				g.writeStringField("ml:text", langParameter.getText());
			}
			g.writeObjectFieldStart("ml:annotation");
			// g.writeStringField("status", "OK");
			if (langParameter.includesId()) {
				g.writeStringField("ml:id", langParameter.getId());
			}
			g.writeStringField("ml:lang", locale.getLanguage());
			g.writeObjectFieldStart("ml:time");
			g.writeNumberField("ml:auth", serviceTime.authenticationTime());
			g.writeNumberField("ml:exec", serviceTime.executionTime());
			g.writeEndObject();
			g.writeNumberField("ml:cost", cost);

			g.writeEndObject();
			g.close();

			return w.toString();

		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}
	}

}