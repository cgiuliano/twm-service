package com.machinelinking.jservice.server;

import net.threescale.api.v2.ApiException;

public class AuthServiceException extends Exception {

	public static final int ERROR_CODE = 40102;

	private ApiException apiException;

	public AuthServiceException(ApiException apiException) {
		super();
		this.apiException = apiException;
	}

	public ApiException getApiException() {
		return apiException;
	}
}
