/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import java.util.Date;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 1/12/13 Time: 8:34 PM To
 * change this template use File | Settings | File Templates.
 */
public class PingHttpHandler extends AbstractHttpHandler {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>HelpHttpHandler</code>.
	 */
	static Logger logger = Logger.getLogger(HelpHttpHandler.class.getName());

	public PingHttpHandler() {
	}

	@Override
	public void service(MLRequest request, MLResponse response) {
		logger.info(Thread.currentThread().getName() + " handling help...");

		ServiceTime serviceTime = new ServiceTime();
		String lang = null, text = null, id = null;

		if (request.getParameter("lang") != null) {
			lang = request.getParameter("lang");
		}
		if (request.getParameter("text") != null) {
			text = request.getParameter("text");
		}
		if (request.getParameter("id") != null) {
			id = request.getParameter("id");
		}

		//String result = "OK\nlang = " + lang + "\ntext = " + text + "\nid = " + id + "\ndate = " + new Date();
		String result = "OK";
		logger.debug(result + " "+ new Date());
		response.setResult(result);
	}

}
