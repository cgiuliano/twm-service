package com.machinelinking.jservice.server;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 10/27/14
 * Time: 3:19 PM
 * <p/>
 * Used to change the log from the restful api
 */
public class LogHttpHandler extends HttpHandler {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>LogHttpHandler</code>.
	 */
	static Logger logger = Logger.getLogger(LogHttpHandler.class.getName());

	@Override
	public void service(Request request, Response response) {
		logger.info(Thread.currentThread().getName() + " handling log...");

		//Properties defaultProps = new Properties();
		String level = request.getParameter("level");
		Logger root = Logger.getRootLogger();
		if (level != null) {
			if (level.equals("trace")) {
				//defaultProps.setProperty("log4j.rootLogger", "trace,stdout");
				root.setLevel(Level.TRACE);
			}
			else if (level.equals("debug")) {
				//defaultProps.setProperty("log4j.rootLogger", "debug,stdout");
				root.setLevel(Level.DEBUG);
			}
			else if (level.equals("info")) {
				//defaultProps.setProperty("log4j.rootLogger", "info,stdout");
				root.setLevel(Level.INFO);
			}
		}
		logger.info("log level set to " + root.getLevel());
		//PropertyConfigurator.configure(defaultProps);
	}
}
