package com.machinelinking.jservice.server;

import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.CharacterTable;

import java.io.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/15/13
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class LogManagement {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>LogManagement</code>.
	 */
	static Logger logger = Logger.getLogger(LogManagement.class.getName());

	private static final DecimalFormat nf = new DecimalFormat("###,###,###");

	private static LogManagement ourInstance = new LogManagement();

	Configuration config;

	PrintWriter logWriter;

	AtomicInteger counter;

	public static final int DEFAULT_FLUSH_SIZE = 100;

	public static LogManagement getInstance() {
		if (ourInstance == null) {
			new LogManagement();
		}
		return ourInstance;
	}

	private LogManagement() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
		String logDirName = config.getString("log-dir");
		if (!logDirName.endsWith(File.separator)) {
			logDirName += File.separator;
		}

		if (!new File(logDirName).exists()){
			logger.info("created log dir (" + new File(logDirName).mkdir()+")");
		}
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String formattedDate = df.format(new Date());
		String logFileName = logDirName + "log" + CharacterTable.LOW_LINE + formattedDate;
		try {
			logWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logFileName, true), "UTF-8")));
		} catch (IOException e) {
			logger.error(e);
		}
		counter = new AtomicInteger();
	}

	public void close() {
			logWriter.close();
	}

	public void writeLog(AuthenticationParameter authenticationParameter, String method, ServiceTime serviceTime, int length, Locale locale, String headers) {
		StringBuilder sb = new StringBuilder();
		sb.append(counter.incrementAndGet());
		sb.append(CharacterTable.HORIZONTAL_TABULATION);
		sb.append(method);
		sb.append(CharacterTable.HORIZONTAL_TABULATION);
		sb.append(authenticationParameter.getAppId());
		sb.append(CharacterTable.HORIZONTAL_TABULATION);
		sb.append(authenticationParameter.getAppKey());
		sb.append(CharacterTable.HORIZONTAL_TABULATION);
		sb.append(nf.format(serviceTime.authenticationNanoTime()));
		sb.append(CharacterTable.HORIZONTAL_TABULATION);
		sb.append(nf.format(serviceTime.executionNanoTime()));
		sb.append(CharacterTable.HORIZONTAL_TABULATION);
		sb.append(nf.format(length));
		sb.append(CharacterTable.HORIZONTAL_TABULATION);
		sb.append(locale.getLanguage());
		sb.append(CharacterTable.HORIZONTAL_TABULATION);
		sb.append(headers);
		sb.append(CharacterTable.HORIZONTAL_TABULATION);

		sb.append(new Date());

		synchronized (this) {
			logWriter.println(sb.toString());
			logWriter.flush();
		}

		if ((counter.intValue() % DEFAULT_FLUSH_SIZE) == 0) {
			logger.debug("flushing loggers...");
			synchronized (this) {
				logWriter.flush();
			}
		}
	}

}
