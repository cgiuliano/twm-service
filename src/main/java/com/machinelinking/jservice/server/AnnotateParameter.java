/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 1/6/13 Time: 9:36 AM To
 * change this template use File | Settings | File Templates.
 */
class AnnotateParameter extends AuthenticationParameter {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>AnnotateParameter</code>.
	 */
	static Logger logger = Logger.getLogger(AnnotateParameter.class.getName());

	//public static final int DEFAULT_CATEGORY_DEPTH = 1;

	//public static final double DEFAULT_MIN_WEIGHT = 0.25;

	String text;

	boolean includeText;

	String id;

	boolean includeId;

	boolean includeNBestFilter;

	boolean includeOverlappingFilter;

	boolean includeDisambiguation;

	boolean includeForm;

	boolean includeAirpediaClass;

	boolean includeLink;

	boolean includeCross;

	boolean includeType;

	boolean includeTopic;

	boolean includeCategory;

	boolean includeAbstract;

	boolean includeImage;

	boolean includeClass;

	boolean includeTypeFilter;

	String lang;

	String targetLang;

	double minimumWeight;

	boolean includesMinimumWeight;

	boolean includePersonNameFilter;

	int categoryDepth;

	AnnotateParameter(MLRequest request) throws MissingRequiredParameterException {
		super(request);
		//decoding is already done
		//logger.warn(getRequiredParameter(request, "text"));
		//try {
		//	text = URLDecoder.decode(getRequiredParameter(request, "text"), CharEncoding.UTF_8);
		//} catch (UnsupportedEncodingException e) {
		//	logger.error(e);
		//}
		//logger.warn(text);
		text=getRequiredParameter(request, "text");

		// todo: check
		//logger.debug("text = " + text + " (" + text.length() + ")");

		String s = request.getParameter("include_text");
		includeText = s != null && s.equals(TRUE);
		// todo: decode
		id = request.getParameter("id");
		includeId = id != null;

		s = request.getParameter("disambiguation");
		//logger.debug("s: "+ s);
		includeDisambiguation = s != null && s.equals(TRUE);
		//logger.debug("includeDisambiguation: "+ includeDisambiguation);

		s = request.getParameter("nbest_filter");
		//logger.debug("s: "+ s);
		includeNBestFilter = s == null ? true : s.equals(TRUE);
		//logger.debug("includeNBestFilter: "+ includeNBestFilter);

		s = request.getParameter("type_filter");
		//logger.debug("s: "+ s);
		includeTypeFilter = s == null ? true : s.equals(TRUE);
		//logger.debug("includeDisambiguation: "+ includeDisambiguation);

		s = request.getParameter("overlapping_filter");
		//logger.debug("s: "+ s);
		includeOverlappingFilter = s == null ? true : s.equals(TRUE);
		//logger.debug("includeOverlappingFilter: "+ includeOverlappingFilter);

		s = request.getParameter("person_filter");
		includePersonNameFilter = s == null ? true : s.equals(TRUE);
		//logger.debug("includePersonNameFilter: "+ includePersonNameFilter);

		s = request.getParameter("form");
		includeForm = s != null && s.equals(TRUE);

		s = request.getParameter("topic");
		includeTopic = s != null && s.equals(TRUE);

		s = request.getParameter("link");
		includeLink = s != null && s.equals(TRUE);

		s = request.getParameter("cross");
		includeCross = s != null && s.equals(TRUE);

		s = request.getParameter("type");
		includeType = s != null && s.equals(TRUE);

		s = request.getParameter("category");
		includeCategory = s != null && s.equals(TRUE);
		if (s != null) {
			includeCategory = true;
			try {
				categoryDepth = Integer.parseInt(s);
			}
			catch (NumberFormatException e) {
				logger.error(e);
				//categoryDepth = DEFAULT_CATEGORY_DEPTH;
			}
		}
		else {
			includeCategory = false;
		}

		s = request.getParameter("abstract");
		includeAbstract = s != null && s.equals(TRUE);

		s = request.getParameter("image");
		includeImage = s != null && s.equals(TRUE);

		s = request.getParameter("dbpedia");
		includeAirpediaClass = s != null && s.equals(TRUE);
		//logger.debug(includeAirpediaClass);

		s = request.getParameter("class");
		includeClass = s != null && s.equals(TRUE);
		//logger.debug(includeClass);

		s = request.getParameter("min_weight");
		if (s != null) {
			includesMinimumWeight = true;
			try {
				minimumWeight = Double.parseDouble(s);
			} catch (NumberFormatException e) {
				logger.error(e);
				//minimumWeight = DEFAULT_MIN_WEIGHT;
			}
		}
		else {
			includesMinimumWeight = false;
		}

		lang = request.getParameter("lang");
		targetLang = request.getParameter("target_lang");

	}

	int getCategoryDepth() {
		return categoryDepth;
	}


	public boolean includesOverlappingFilter() {
		return includeOverlappingFilter;
	}

	public boolean includesPersonNameFilter() {
		return includePersonNameFilter;
	}

	public boolean includesTypeFilter() {
		return includeTypeFilter;
	}

	public boolean includesNBestFilter() {
		return includeNBestFilter;
	}

	public boolean includesMinimumWeight() {
		return includesMinimumWeight;
	}

	public double getMinimumWeight() {
		return minimumWeight;
	}

	public boolean isTargetLangSpecified() {
		if (targetLang == null) {
			return false;
		}
		return true;
	}

	public boolean isLangSpecified() {
		if (lang == null) {
			return false;
		}
		return true;
	}

	String getTargetLang() {
		return targetLang;
	}

	public String getLang() {
		return lang;
	}

	public String getText() {
		return text;
	}

	public boolean includesCategory() {
		return includeCategory;
	}

	public boolean includesAbstract() {
		return includeAbstract;
	}

	public boolean includesImage() {
		return includeImage;
	}

	public boolean includesAirpediaClass() {
		return includeAirpediaClass;
	}

	public boolean includesDisambiguation() {
		return includeDisambiguation;
	}

	public boolean includesTopic() {
		return includeTopic;
	}

	public boolean includesText() {
		return includeText;
	}

	public boolean includesClass() {
		return includeClass;
	}

	public boolean includesId() {
		return includeId;
	}

	public String getId() {
		return id;
	}

	public boolean includesLink() {
		return includeLink;
	}

	public boolean includesForm() {
		return includeForm;
	}

	public boolean includesCross() {
		return includeCross;
	}

	public boolean includesType() {
		return includeType;
	}

	@Override
	public String toString() {
		return "AnnotateParameter{" +
						"text='" + text + '\'' +
						", includeText=" + includeText +
						", id='" + id + '\'' +
						", includeId=" + includeId +
						", includeCategory=" + includeCategory +
						", includeDisambiguation=" + includeDisambiguation +
						", includeForm=" + includeForm +
						", includeAirpediaClass=" + includeAirpediaClass +
						", includeLink=" + includeLink +
						", includeCross=" + includeCross +
						", includeType=" + includeType +
						", includeAbstract=" + includeAbstract +
						", includeImage=" + includeImage +
						", lang='" + lang + '\'' +
						", minimumWeight=" + minimumWeight +
						", includesMinimumWeight=" + includesMinimumWeight +
						"} " + super.toString();
	}
}
