/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */
package com.machinelinking.jservice.server;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.*;

import com.machinelinking.jservice.annotation.key.*;
import com.machinelinking.jservice.annotation.key.enhance.*;
import com.machinelinking.jservice.annotation.key.filter.*;
import com.machinelinking.jservice.annotation.topic.*;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.machinelinking.jservice.annotation.lang.LangIdentifier;
import eu.fbk.twm.utils.ClassResource;
import eu.fbk.twm.utils.analysis.Token;
import eu.fbk.twm.utils.CharacterTable;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 12/22/12 ServiceTime: 10:07
 * AM To change this template use File | Settings | File Templates.
 * <p/>
 * <code>http://twm.apnetwork.it:9000/annotate?app_id=c3e5ca13&app_key=cc193754ccbfd7c7f85648585b4e00f3&text=Ulica+Francuska+%E2%80%93+warszawska+ulica+na+Saskiej+K%C4%99pie%2C+rozpoczynaj%C4%85ca+si%C4%99+przy+placu+Przymierza%2C+a+ko%C5%84cz%C4%85ca+przy+rondzie+Waszyngtona.+Znajduj%C4%85+si%C4%99+przy+niej+domy+mieszkalne+%28w+tym+zabytkowe+obiekty+z+okresu+dwudziestolecia+mi%C4%99dzywojennego%29+oraz+lokale+us%C5%82ugowe%2C+w+tym+gastronomiczne.+Wzd%C5%82u%C5%BC+ca%C5%82ej+d%C5%82ugo%C5%9Bci+ul.</code>
 */
public class AnnotateHttpHandler extends AbstractHttpHandler {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>AnnotateHttpHandler</code>.
	 */
	static Logger logger = Logger.getLogger(AnnotateHttpHandler.class.getName());

	protected static DecimalFormat tf = new DecimalFormat("0.000");

	LangIdentifier langIdentifier;

	KeywordExtractor keywordExtractor;

	//BaselineKeywordLinker baselineKeywordLinker;

	OneExamplePerSenseKeywordLinker oneExamplePerSenseKeywordLinker;

	//PageTextDisambiguation pageTextDisambiguation;

	WeightFilter weightFilter;

	//ExpWeightFilter expWeightFilter;

	//AdjacentFilter adjacentFilter;

	WeightSortFilter weightSortFilter;

	LinkEnhancer linkEnhancer;

	OverlappingFilter overlappingFilter;

	PersonNameFilter personNameFilter;

	//VectorFilter vectorFilter;

	FormEnhancer formEnhancer;

	NormFilter normFilter;

	StopFilter stopFilter;

	TermLengthFilter termLengthFilter;

	TypeFilter typeFilter;

	TrafficFilter trafficFilter;

	//MergeFilter mergeFilter;

	CrossLangEnhancer crossLangEnhancer;

	TypeEnhancer typeEnhancer;

	TopicClassifier topicClassifier;
	//RankFilter rankFilter;

	//LogManagement logManagement;

	//IncomingOutgoingFilter incomingOutgoingFilter;

	//todo: review
	NBestCorrelationFilter nBestCorrelationFilter;

	MutualLinkFilter mutualLinkFilter;

	CorrelationAdjacentFilter correlationAdjacentFilter;

	ImageEnhancer imageEnhancer;

	AirpediaEnhancer airpediaEnhancer;

	//DBpediaEnhancer dBpediaEnhancer;

	PageAbstractEnhancer pageAbstractEnhancer;

	CategoryHierarchyEnhancer categoryEnhancer;

	TopicDetection topicDetection;
	TopicDetector topicDetector;
	TopicEnhancer topicEnhancer;
	TopicFilter topicFilter;
	LongestNotWorkFilter longestNotWorkFilter;
	//PageLinkDisambiguation pageLinkDisambiguation;

	//AutomaticConfiguration automaticConfiguration;

	protected int textMaxLength;

	private boolean authentication;

	public AnnotateHttpHandler() {
		this(true);
	}

	public AnnotateHttpHandler(boolean authentication) {
		super();
		this.authentication = authentication;
		AutomaticConfiguration automaticConfiguration = AutomaticConfiguration.getInstance();
		textMaxLength = automaticConfiguration.getMaxTextLength();

		langIdentifier = LangIdentifier.getInstance();
		keywordExtractor = KeywordExtractor.getInstance();
		//baselineKeywordLinker = BaselineKeywordLinker.getInstance();
		logger.debug("creating a classifier");

		oneExamplePerSenseKeywordLinker = OneExamplePerSenseKeywordLinker.getInstance();
		//pageTextDisambiguation = PageTextDisambiguation.getInstance();

		weightFilter = WeightFilter.getInstance();
		//expWeightFilter = ExpWeightFilter.getInstance();
		weightSortFilter = WeightSortFilter.getInstance();
		normFilter = NormFilter.getInstance();
		overlappingFilter = OverlappingFilter.getInstance();
		//incomingOutgoingFilter = IncomingOutgoingFilter.getInstance();
		//todo: review
		nBestCorrelationFilter = NBestCorrelationFilter.getInstance();

		//mutualLinkFilter = MutualLinkFilter.getInstance();
		//correlationAdjacentFilter = CorrelationAdjacentFilter.getInstance();
		stopFilter = StopFilter.getInstance();
		termLengthFilter = TermLengthFilter.getInstance();
		//adjacentFilter = AdjacentFilter.getInstance();
		//mergeFilter = MergeFilter.getInstance();
		//vectorFilter = VectorFilter.getInstance();
		personNameFilter = PersonNameFilter.getInstance();
		typeFilter = TypeFilter.getInstance();
		trafficFilter = TrafficFilter.getInstance();
		//rankFilter = RankFilter.getInstance();

		linkEnhancer = LinkEnhancer.getInstance();
		formEnhancer = FormEnhancer.getInstance();
		crossLangEnhancer = CrossLangEnhancer.getInstance();
		typeEnhancer = TypeEnhancer.getInstance();
		imageEnhancer = ImageEnhancer.getInstance();
		airpediaEnhancer = AirpediaEnhancer.getInstance();
		//dBpediaEnhancer = DBpediaEnhancer.getInstance();
		pageAbstractEnhancer = PageAbstractEnhancer.getInstance();
		categoryEnhancer = CategoryHierarchyEnhancer.getInstance();
		//pageLinkDisambiguation = PageLinkDisambiguation.getInstance();
		logManagement = LogManagement.getInstance();

		topicEnhancer = TopicEnhancer.getInstance();
		//topicDetection = TopicDetection.getInstance();
		topicDetector = TopicDetector.getInstance();
		topicClassifier = TopicClassifier.getInstance();
		topicFilter = TopicFilter.getInstance();

		longestNotWorkFilter = LongestNotWorkFilter.getInstance();
	}

	//@Override
	public void serviceTC(MLRequest request, MLResponse response) throws MissingRequiredParameterException, LanguageNotSupportedException, AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException, AuthTextLengthExceededException {
		logger.info(Thread.currentThread().getName() + " handling annotation (Topic Detection)...");
		logger.debug(request.toString());

		ServiceTime serviceTime = new ServiceTime();
		// todo: allow text downloading from url
		AnnotateParameter annotateParameter = new AnnotateParameter(request);
		List<Topic> textTopicList = null;

		if (authentication) {
			accessManagement.authorize(serviceTime, annotateParameter, ANNOTATE_CMD, annotateParameter.getText().length());
		}
		else {
			logger.debug("No authentication");
		}

		serviceTime.executionBegin();

		// the tokenization is always done
		String text = annotateParameter.getText();
		if (text.length() > textMaxLength) {
			throw new AuthTextLengthExceededException(text.length(), textMaxLength);
		}
		Token[] tokenArray = tokenizer.tokenArray(text);

		Locale locale = annotateParameter.isLangSpecified() ? new Locale(annotateParameter.getLang()) : langIdentifier.detect(tokenArray);

		Keyword[] keywordArray = keywordExtractor.extract(tokenArray, text, locale);
		logger.trace(print(keywordArray, "pure"));

		termLengthFilter.filter(keywordArray);
		logger.trace(print(keywordArray, "term length filter"));

		stopFilter.filter(keywordArray, locale);
		logger.trace(print(keywordArray, "stop word filter"));

		// apply normalization filter
		normFilter.filter(keywordArray);
		logger.trace(print(keywordArray, "norm"));

		//todo: REVIEW THIS PART!!!!!!
		//todo: At least take the first N terms and apply the weight filter on the rest.
		// apply weight filter on keywords

		weightFilter.filter(keywordArray, locale, annotateParameter.getMinimumWeight());

//		if (annotateParameter.includesMinimumWeight()) {
//			weightFilter.filter(keywordArray, locale, annotateParameter.getMinimumWeight());
//		}
//		else {
//			logger.warn("FIRST FILTER " + annotateParameter.includesMinimumWeight());
//			weightFilter.filter(keywordArray, locale, 0.3);
//		}
		logger.trace(print(keywordArray, "first weight filter"));

		//pageLinkDisambiguation.enhance(keywordArray, locale);

		// type disambiguation is optional
		if (annotateParameter.includesDisambiguation()) {
			//baselineKeywordLinker.link(keywordArray, text, locale);
			oneExamplePerSenseKeywordLinker.link(keywordArray, tokenArray, locale);
			//pageTextDisambiguation.link(keywordArray, tokenArray, locale);
			logger.trace(print(keywordArray, "linking"));

			logger.trace("Enhancing type");
			typeEnhancer.enhance(keywordArray, locale);

			if (annotateParameter.includesClass()) {
				logger.trace("Enhancing class using airpedia");
				airpediaEnhancer.enhance(keywordArray, locale);
			}

			//longestNotWorkFilter.filter(keywordArray, locale);
			//logger.trace(print(keywordArray, "longest not work filter"));

			//todo: problems with http, https,...
			if (annotateParameter.includesTypeFilter()) {
				typeFilter.filter(keywordArray, locale);
				logger.trace(print(keywordArray, "type filter"));
			}

			//incomingOutgoingFilter.filter(keywordArray, locale);
			//print(keywordArray, "in/out");

			//weightSortFilter.filter(keywordArray);
			//print(keywordArray, "sort");

			// +P ~R: en, it
			//logger.debug(annotateParameter.includesNBestFilter());
			if (annotateParameter.includesNBestFilter()) {
				nBestCorrelationFilter.filter(keywordArray, locale);
				logger.trace(print(keywordArray, "best filter"));
			}

			//mutualLinkFilter.filter(keywordArray, locale);
			//print(keywordArray, "in/out");


			//correlationAdjacentFilter.filter(keywordArray, locale);
			//print(keywordArray, "correlation");

			//vectorFilter.filter(keywordArray, locale);
			//print(keywordArray, "vec");

			//normFilter.filter(keywordArray);
			//print(keywordArray, "norm 2");

			// apply weight filter on linked keywords
			/*if (annotateParameter.includesMinimumWeight()) {
				weightFilter.filter(keywordArray, locale, annotateParameter.getMinimumWeight());
			}
			else {
				weightFilter.filter(keywordArray, locale);
			}*/
			//print(keywordArray, "weight");

			//adjacentFilter.filter(keywordArray, locale);
			//print(keywordArray, "adjacent");

			//+P +R: en, it
			// it has problem with different entities with the same surname
			if (annotateParameter.includesPersonNameFilter()) {
				personNameFilter.filter(keywordArray, locale, tokenArray);
				logger.trace(print(keywordArray, "person name filter"));
			}

			//todo: bug DC comics
			//mergeFilter.filter(keywordArray);
			//print(keywordArray, "merge");

			//+P +F1: it
			//todo: this is not only for improving the disambiguation performance but to better sorting the keywords
			//trafficFilter.filter(keywordArray, locale);
			//print(keywordArray, "traffic");
		}

		weightSortFilter.filter(keywordArray);
		logger.trace(print(keywordArray, "sorting"));

		//rankFilter.filter(keywordArray, locale, annotateParameter.getMinimumWeight());
		//print(keywordArray, "rank");

		if (annotateParameter.includesOverlappingFilter()) {
			overlappingFilter.filter(keywordArray);
			logger.trace(print(keywordArray, "overlapping"));
		}

		normFilter.filter(keywordArray);
		//logger.debug(print(keywordArray, "norm 2"));

		// apply weight filter on linked keywords
		if (annotateParameter.includesMinimumWeight()) {
			weightFilter.filter(keywordArray, locale, annotateParameter.getMinimumWeight());
		}
		else {
			weightFilter.filter(keywordArray, locale);
		}
		logger.trace(print(keywordArray, "weight filter 2"));

		// enrich
		if (annotateParameter.includesDisambiguation()) {
			if (annotateParameter.includesLink()) {
				linkEnhancer.enhance(keywordArray, locale);
			}
			if (annotateParameter.includesForm()) {
				formEnhancer.enhance(keywordArray, locale);
			}
			if (annotateParameter.includesCategory()) {
				categoryEnhancer.enhance(keywordArray, locale, annotateParameter.getCategoryDepth());
			}

			if (annotateParameter.includesCross()) {
				crossLangEnhancer.enhance(keywordArray, locale);
			}
			//if (annotateParameter.includesType()) {
			//	typeEnhancer.enhance(keywordArray, locale);
			//}
			if (annotateParameter.includesImage()) {
				imageEnhancer.enhance(keywordArray, locale);
			}
			if (annotateParameter.includesAbstract()) {
				pageAbstractEnhancer.enhance(keywordArray, locale);
			}
			//todo: complete topic detection
			if (annotateParameter.includesTopic()) {
				topicEnhancer.enhance(keywordArray, locale);
				logger.debug(locale + " detecting text topic");
				TopicSet textTopicSet = topicDetector.detect(keywordArray);
				textTopicList = textTopicSet.topicList();
				logger.debug("topic set: " + textTopicSet);
				logger.debug("topic list: " + textTopicList);
				//topicClassifier.classify(keywordArray, topicSet);
				//topicFilter.filter(keywordArray, locale, textTopicList);
			}

		}

		serviceTime.executionEnd();
		//logger.info(keywordArray.length + " annotations done in " + nf.format(serviceTime.executionNanoTime()) + " ns (" + new Date() + ")");
		logger.info("annotate\t" + keywordArray.length + "\t" + nf.format(serviceTime.executionNanoTime()) + "\t" + new Date() + "\t" + request.getHeaders());

		String result = null;
		if (annotateParameter.getOutputFormat().equalsIgnoreCase("jsonld")) {
			result = toJSonLD(keywordArray, textTopicList, locale, annotateParameter, serviceTime, 0);
		}
		else if (annotateParameter.getOutputFormat().equalsIgnoreCase("csv")) {
			result = toCSV(keywordArray, textTopicList, locale, annotateParameter, serviceTime, 0);
		}
		else {
			result = toJSon(keywordArray, textTopicList, locale, annotateParameter, serviceTime, 0);
		}

		response.setResult(result);

		//todo: check if to move in AbstractHttpHandler
		logManagement.writeLog(annotateParameter, ANNOTATE_CMD, serviceTime, text.length(), locale, request.getHeaders());
	}

	@Override
	// this is the stable method that runs on piotta
	public void service(MLRequest request, MLResponse response) throws MissingRequiredParameterException, LanguageNotSupportedException, AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException, AuthTextLengthExceededException {
		// this is the stable method that runs on piotta
		serviceTC(request, response);

		//old version
		//serviceStable(request, response);
	}

	//@Override
	//old version
	public void serviceOld(MLRequest request, MLResponse response) throws MissingRequiredParameterException, LanguageNotSupportedException, AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException, AuthTextLengthExceededException {
		logger.debug(Thread.currentThread().getName() + " handling annotation (Stable version)...");

		ServiceTime serviceTime = new ServiceTime();
		// todo: allow text downloading from url
		AnnotateParameter annotateParameter = new AnnotateParameter(request);
		List<Topic> topicList = null;

		if (authentication) {
			accessManagement.authorize(serviceTime, annotateParameter, ANNOTATE_CMD, annotateParameter.getText().length());
		}
		serviceTime.executionBegin();

		// the tokenization is always done
		String text = annotateParameter.getText();
		if (text.length() > textMaxLength) {
			throw new AuthTextLengthExceededException(text.length(), textMaxLength);
		}
		Token[] tokenArray = tokenizer.tokenArray(text);

		Locale locale = annotateParameter.isLangSpecified() ? new Locale(annotateParameter.getLang()) : langIdentifier.detect(tokenArray);

		Keyword[] keywordArray = keywordExtractor.extract(tokenArray, text, locale);
		//logger.debug(print(keywordArray, "pure"));

		termLengthFilter.filter(keywordArray);
		//logger.debug(print(keywordArray, "term length filter"));

		stopFilter.filter(keywordArray, locale);
		//logger.debug(print(keywordArray, "stop word filter"));

		// apply normalization filter
		normFilter.filter(keywordArray);
		//logger.debug(print(keywordArray, "norm"));

		//todo: REVIEW THIS PART!!!!!!
		//todo: At least take the first N terms and apply the weight filter on the rest.
		// apply weight filter on keywords
		if (annotateParameter.includesMinimumWeight()) {
			weightFilter.filter(keywordArray, locale, annotateParameter.getMinimumWeight());
		}
		else {
			//logger.warn("FIRST FILTER " + annotateParameter.includesMinimumWeight());
			weightFilter.filter(keywordArray, locale, 0.3);
		}
		logger.trace(print(keywordArray, "weight filter"));

		//pageLinkDisambiguation.enhance(keywordArray, locale);

		// type disambiguation is optional
		if (annotateParameter.includesDisambiguation()) {
			//baselineKeywordLinker.link(keywordArray, text, locale);
			oneExamplePerSenseKeywordLinker.link(keywordArray, tokenArray, locale);
			//pageTextDisambiguation.link(keywordArray, tokenArray, locale);
			logger.trace(print(keywordArray, "linking"));

			//+P : en, it
			typeEnhancer.enhance(keywordArray, locale);
			//todo: problems with http, https,...
			if (annotateParameter.includesTypeFilter()) {
				typeFilter.filter(keywordArray, locale);
				//logger.debug(print(keywordArray, "type filter"));
			}

			//incomingOutgoingFilter.filter(keywordArray, locale);
			//print(keywordArray, "in/out");

			//weightSortFilter.filter(keywordArray);
			//print(keywordArray, "sort");

			// +P ~R: en, it
			//logger.debug(annotateParameter.includesNBestFilter());
			if (annotateParameter.includesNBestFilter()) {
				nBestCorrelationFilter.filter(keywordArray, locale);
				logger.trace(print(keywordArray, "best filter"));
			}

			//mutualLinkFilter.filter(keywordArray, locale);
			//print(keywordArray, "in/out");


			//correlationAdjacentFilter.filter(keywordArray, locale);
			//print(keywordArray, "correlation");

			//vectorFilter.filter(keywordArray, locale);
			//print(keywordArray, "vec");

			//normFilter.filter(keywordArray);
			//print(keywordArray, "norm 2");

			// apply weight filter on linked keywords
			/*if (annotateParameter.includesMinimumWeight()) {
				weightFilter.filter(keywordArray, locale, annotateParameter.getMinimumWeight());
			}
			else {
				weightFilter.filter(keywordArray, locale);
			}*/
			//print(keywordArray, "weight");

			//adjacentFilter.filter(keywordArray, locale);
			//print(keywordArray, "adjacent");

			//+P +R: en, it
			// it has problem with different entities with the same surname
			if (annotateParameter.includesPersonNameFilter()) {
				personNameFilter.filter(keywordArray, locale, tokenArray);
				logger.trace(print(keywordArray, "person name filter"));
			}

			//todo: bug DC comics
			//mergeFilter.filter(keywordArray);
			//print(keywordArray, "merge");

			//+P +F1: it
			//trafficFilter.filter(keywordArray, locale);
			//print(keywordArray, "traffic");
		}

		weightSortFilter.filter(keywordArray);
		logger.trace(print(keywordArray, "sorting"));

		//rankFilter.filter(keywordArray, locale, annotateParameter.getMinimumWeight());
		//print(keywordArray, "rank");

		if (annotateParameter.includesOverlappingFilter()) {
			overlappingFilter.filter(keywordArray);
			logger.trace(print(keywordArray, "overlapping"));
		}

		normFilter.filter(keywordArray);
		logger.trace(print(keywordArray, "norm 2"));

		// apply weight filter on linked keywords
		if (annotateParameter.includesMinimumWeight()) {
			weightFilter.filter(keywordArray, locale, annotateParameter.getMinimumWeight());
		}
		else {
			weightFilter.filter(keywordArray, locale);
		}
		/*if (annotateParameter.includesMinimumWeight()) {
			expWeightFilter.filter(keywordArray, locale, annotateParameter.getMinimumWeight());
		}
		else {
			expWeightFilter.filter(keywordArray, locale);
		} */
		logger.trace(print(keywordArray, "weight filter 2"));

		// enrich
		if (annotateParameter.includesDisambiguation()) {
			if (annotateParameter.includesLink()) {
				linkEnhancer.enhance(keywordArray, locale);
			}
			if (annotateParameter.includesForm()) {
				formEnhancer.enhance(keywordArray, locale);
			}
			if (annotateParameter.includesCategory()) {
				categoryEnhancer.enhance(keywordArray, locale, annotateParameter.getCategoryDepth());
			}

			if (annotateParameter.includesCross()) {
				crossLangEnhancer.enhance(keywordArray, locale);
			}
			//if (annotateParameter.includesType()) {
			//	typeEnhancer.enhance(keywordArray, locale);
			//}
			if (annotateParameter.includesImage()) {
				imageEnhancer.enhance(keywordArray, locale);
			}
			if (annotateParameter.includesClass()) {
				airpediaEnhancer.enhance(keywordArray, locale);
			}
			if (annotateParameter.includesAbstract()) {
				pageAbstractEnhancer.enhance(keywordArray, locale);
			}

			//todo: complete topic detection: replace with an Enhancer
			/*if (annotateParameter.includesTopic() && (locale.getLanguage().equals("en") || locale.getLanguage().equals("it"))) {
				logger.debug(locale + " detect topic");
				topicList = topicDetection.detect(keywordArray, locale);
				logger.debug("topic:\n" + topicList);
			} */
		}

		serviceTime.executionEnd();
		logger.info(keywordArray.length + " annotations done in " + nf.format(serviceTime.executionNanoTime()) + " ns");
		String result = null;
		if (annotateParameter.getOutputFormat().equalsIgnoreCase("jsonld")) {
			result = toJSonLD(keywordArray, topicList, locale, annotateParameter, serviceTime, 0);
		}
		else if (annotateParameter.getOutputFormat().equalsIgnoreCase("csv")) {
			result = toCSV(keywordArray, topicList, locale, annotateParameter, serviceTime, 0);
		}
		else {
			result = toJSon(keywordArray, topicList, locale, annotateParameter, serviceTime, 0);
		}

		response.setResult(result);

		//todo: check if to move in AbstractHttpHandler
		logManagement.writeLog(annotateParameter, ANNOTATE_CMD, serviceTime, text.length(), locale, request.getHeaders());
	}

	private void print(Token[] tokenArray, String label) {

		logger.debug(label);
		for (int i = 0; i < tokenArray.length; i++) {
			logger.debug(i + "\t" + tokenArray[i]);
		}
	}

	private String print(Keyword[] keywordArray, String label) {

		//logger.debug("i\tidf\tlf\tlenght\ttf\tweight\tfiltered\tform\tsense\tnorm");
		StringBuilder sb = new StringBuilder();

		//todo: comment
		Arrays.sort(keywordArray, new KeywordWeightComparator());

		sb.append(label + "\n");
		sb.append("i\tidf\tlf\tlength\ttf\tcorr\ttraffic\tweight\tform\tsense\tnorm\n");
		int c = 0;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered()) {
				//logger.debug(i + "\t" + keywordArray[i]);
				sb.append((c++) + "/" + i + "\t" + keywordArray[i] + "\n");
			}
		}
		return sb.toString();
	}

	String toJSon(Keyword[] keywordArray, List<Topic> topicList, Locale locale, AnnotateParameter annotateParameter, ServiceTime serviceTime, double cost) {
		try {
			StringWriter w = new StringWriter();

			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			if (annotateParameter.includesText()) {
				g.writeStringField("text", annotateParameter.getText());
			}

			g.writeObjectFieldStart("annotation");
			// g.writeStringField("status", "OK");
			if (annotateParameter.includesId()) {
				g.writeStringField("id", annotateParameter.getId());
			}

			// Call common code!
			toJSonCommon(g, keywordArray, topicList, locale, serviceTime, cost, annotateParameter);

			g.writeEndObject();
			g.close();

			return w.toString();
		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}
	}

	static void toJSonCommon(JsonGenerator g, Keyword[] keywordArray, List<Topic> topicList, Locale locale, ServiceTime serviceTime, double cost, AnnotateParameter annotateParameter) throws JsonGenerationException, IOException {
		g.writeStringField("lang", locale.getLanguage());
		g.writeObjectFieldStart("time");
		g.writeNumberField("auth", serviceTime.authenticationTime());
		g.writeNumberField("exec", serviceTime.executionTime());
		g.writeEndObject();
		// g.writeNumberField("time", (double) time / NANO);
		g.writeNumberField("cost", cost);

		if (topicList != null && topicList.size() > 0) {
			Topic topic;
			g.writeFieldName("topic");
			g.writeStartArray();
			for (int i = 0; i < topicList.size(); i++) {
				topic = topicList.get(i);
				//logger.debug(i + "\t" + topic);
				g.writeStartObject();
				g.writeStringField("label", topic.getLabel());
				g.writeStringField("url", topic.getURL().toString());
				g.writeNumberField("prob", topic.getConfidence());
				g.writeEndObject();
			}
			g.writeEndArray();
		}

		Sense sense;

		g.writeFieldName("keyword");
		g.writeStartArray();
		for (int j = 0; j < keywordArray.length; j++) {
			// logger.debug(j + "\t" + keywordArray[j]);
			if (!keywordArray[j].isFiltered()) {
				sense = keywordArray[j].getSense();

				g.writeStartObject();
				g.writeStringField("form", keywordArray[j].form());
				g.writeNumberField("rel", keywordArray[j].weight());
				// g.writeBooleanField("filtered",
				// keywordArray[j].isFiltered());
				if (sense != null) {
					g.writeObjectFieldStart("sense");
					g.writeStringField("page", sense.getPage());
					// it's not normalized
//					g.writeNumberField("prob", sense.getCombo());
					g.writeNumberField("prob", 1);
					g.writeEndObject();
				}

				//todo: start new code for topic
				//start work in progress
				//List<Topic> keywordTopicList = keywordArray[j].getTopicList();
				List<Topic> keywordTopicList = keywordArray[j].getTopic().topicList();
				if (keywordTopicList != null && keywordTopicList.size() > 0) {
					g.writeFieldName("topic");
					g.writeStartArray();
					for (int i = 0; i < keywordTopicList.size(); i++) {
						Topic topic = keywordTopicList.get(i);
						if (topic != null) {
							//logger.debug(j + "/" + i + "\t" + topic.getLabel() + "\t" + tf.format(topic.getConfidence()));
							g.writeStartObject();
							g.writeStringField("label", topic.getLabel());
							g.writeStringField("url", topic.getURL().toString());
							g.writeNumberField("prob", topic.getConfidence());
							g.writeEndObject();
						}
					}
					g.writeEndArray();
				}
				//end work in progress

				//todo: end new code
				List<Category> categoryList = keywordArray[j].getCategoryList();
				if (categoryList.size() > 0) {
					Category category;
					g.writeFieldName("category");
					g.writeStartArray();
					for (int i = 0; i < categoryList.size(); i++) {
						category = categoryList.get(i);
						g.writeStartObject();
						g.writeStringField("label", category.getLabel());
						g.writeStringField("url", category.getURL().toString());
						g.writeEndObject();
					}
					g.writeEndArray();
				}

				List<Link> linkList = keywordArray[j].getLinkList();
				if (linkList.size() > 0) {
					Link link;
					g.writeFieldName("external");
					g.writeStartArray();
					for (int i = 0; i < linkList.size(); i++) {
						link = linkList.get(i);
						g.writeStartObject();
						g.writeStringField("label", link.getLabel());
						g.writeStringField("url", link.getURL().toString());
						g.writeStringField("resource", link.getResource());
						g.writeEndObject();
					}
					g.writeEndArray();

				}

				List<Form> formList = keywordArray[j].getFormList();
				if (formList.size() > 0) {
					Form form;
					g.writeFieldName("alt");
					g.writeStartArray();
					for (int i = 0; i < formList.size(); i++) {
						form = formList.get(i);
						g.writeStartObject();
						g.writeStringField("form", form.getForm());
						g.writeNumberField("freq", form.getProbability());
						g.writeEndObject();
					}
					g.writeEndArray();

				}

				List<Image> imageList = keywordArray[j].getImageList();
				if (imageList.size() > 0) {
					Image image;
					g.writeFieldName("image");
					g.writeStartArray();
					for (int i = 0; i < imageList.size(); i++) {
						image = imageList.get(i);
						g.writeStartObject();
						g.writeStringField("image", image.getImage());
						g.writeStringField("thumb", image.getThumb());

						g.writeEndObject();
					}
					g.writeEndArray();
				}

				//todo: remove
				/*if (annotateParameter.includesType()) {
					g.writeStringField("type", keywordArray[j].getType());
				}*/

				if (annotateParameter.includesAbstract()) {
					g.writeStringField("abstract", keywordArray[j].getPageAbstract());
				}

				//todo: remove
				/*List<String> airpediaClassList = keywordArray[j].getAirpediaClassList();
				//logger.debug("airpedia " + airpediaClassList.size());
				if (airpediaClassList.size() > 0) {
					//String airpediaClass;

					g.writeFieldName("dbpedia");
					g.writeStartArray();
					for (int i = 0; i < airpediaClassList.size(); i++) {
						//airpediaClass = airpediaClassList.get(i);
						//logger.debug(i + "\t" + airpediaClassList.get(i));
						g.writeString(airpediaClassList.get(i));
					}
					g.writeEndArray();
				}*/

				//todo: check if remove
				/*List<Type> typeList = keywordArray[j].getTypeList();
				if (typeList.size() > 0) {
					g.writeFieldName("class");
					g.writeStartArray();
					Type type;
					for (int i = 0; i < typeList.size(); i++) {
						type = typeList.get(i);
						g.writeStartObject();
						g.writeStringField("label", type.getLabel());
						g.writeStringField("url", type.getURL().toString());
						g.writeStringField("resource", type.getResource());
						g.writeNumberField("prob", type.getProb());
						g.writeEndObject();
					}
					g.writeEndArray();
				}*/

				List<ClassResource> airpediaClassList = keywordArray[j].getAirpediaClassList();
				if (airpediaClassList.size() > 0) {
					g.writeFieldName("class");
					g.writeStartArray();

					ClassResource classResource;
					for (int i = 0; i < airpediaClassList.size(); i++) {
						classResource = airpediaClassList.get(i);
						g.writeStartObject();
						g.writeStringField("label", LabelBuilder.build(classResource.getLabel()));
						g.writeStringField("url", "http://www.airpedia.org/ontology/class/" + classResource.getLabel());
						g.writeStringField("resource", "airpedia");
						g.writeNumberField("prob", classResource.getConfidence());
						g.writeEndObject();
					}
					g.writeEndArray();
				}

				Map<String, String> crossLangMap = keywordArray[j].getCrossLangMap();
				if (crossLangMap.size() > 0) {
					g.writeFieldName("cross");
					g.writeStartArray();
					String foreignLang;
					Iterator<String> itr = crossLangMap.keySet().iterator();
					for (int i = 0; itr.hasNext(); i++) {
						foreignLang = itr.next();

						g.writeStartObject();
						// g.writeStringField(foreignLang,
						// crossLangMap.get(foreignLang));
						g.writeStringField("lang", foreignLang);
						g.writeStringField("page", crossLangMap.get(foreignLang));
						g.writeEndObject();
					}
					g.writeEndArray();

				}

				List<NGram> nGramList = keywordArray[j].getNGramList();
				NGram nGram;

				g.writeFieldName("ngram");
				g.writeStartArray();
				for (int i = 0; i < nGramList.size(); i++) {
					nGram = nGramList.get(i);
					//todo: remove the filtered ngrams
					g.writeStartObject();
					g.writeStringField("form", nGram.getForm());
					g.writeNumberField("n", nGram.getNGramLength());
					g.writeObjectFieldStart("span");
					g.writeNumberField("start", nGram.getStart());
					g.writeNumberField("end", nGram.getEnd());
					g.writeEndObject();
					g.writeEndObject();
				}
				g.writeEndArray();
				g.writeEndObject();
			}

		}
		g.writeEndArray();

	}

	String toJSonLD(Keyword[] keywordArray, List<Topic> topicList, Locale locale, AnnotateParameter annotateParameter, ServiceTime serviceTime, double cost) {
		try {
			StringWriter w = new StringWriter();

			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();

			g.writeObjectFieldStart("@context");
			g.writeStringField("@language", locale.getLanguage());
			g.writeStringField("ml", "http://machinelinking.com/terms#");
			g.writeEndObject();


			if (annotateParameter.includesText()) {
				g.writeStringField("ml:text", annotateParameter.getText());
			}

			g.writeObjectFieldStart("ml:annotation");
			// g.writeStringField("status", "OK");
			if (annotateParameter.includesId()) {
				g.writeStringField("ml:id", annotateParameter.getId());
			}

			// Call common code!
			toJSonLDCommon(g, keywordArray, topicList, locale, serviceTime, cost, annotateParameter.includesType());

			g.writeEndObject();
			g.close();

			return w.toString();
		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}

	}

	static void toJSonLDCommon(JsonGenerator g, Keyword[] keywordArray, List<Topic> topicList, Locale locale, ServiceTime serviceTime, double cost, boolean includeTypes) throws JsonGenerationException, IOException {
		g.writeStringField("ml:lang", locale.getLanguage());
		g.writeObjectFieldStart("ml:time");
		g.writeNumberField("ml:auth", serviceTime.authenticationTime());
		g.writeNumberField("ml:exec", serviceTime.executionTime());
		g.writeEndObject();
		// g.writeNumberField("time", (double) time / NANO);
		g.writeNumberField("ml:cost", cost);

		Sense sense;

		g.writeFieldName("ml:keyword");
		g.writeStartArray();
		for (int j = 0; j < keywordArray.length; j++) {
			// logger.debug(j + "\t" + keywordArray[j]);
			if (!keywordArray[j].isFiltered()) {
				sense = keywordArray[j].getSense();

				g.writeStartObject();
				g.writeStringField("ml:form", keywordArray[j].form());
				g.writeNumberField("ml:rel", keywordArray[j].weight());
				// g.writeBooleanField("filtered",
				// keywordArray[j].isFiltered());
				if (sense != null) {
					g.writeObjectFieldStart("ml:sense");
					g.writeStringField("ml:page", sense.getPage());
//					g.writeNumberField("ml:prob", sense.getCombo());
					g.writeNumberField("ml:prob", 1);
					g.writeEndObject();
				}

				if (includeTypes) {
					g.writeStringField("ml:type", keywordArray[j].getType());
				}

				List<Link> linkList = keywordArray[j].getLinkList();

				if (linkList.size() > 0) {
					Link link;
					g.writeFieldName("ml:external");
					g.writeStartArray();
					for (int i = 0; i < linkList.size(); i++) {
						link = linkList.get(i);
						g.writeStartObject();
						g.writeStringField("ml:label", link.getLabel());
						g.writeStringField("ml:url", link.getURL().toString());
						g.writeStringField("ml:resource", link.getResource());

						g.writeEndObject();
					}
					g.writeEndArray();

				}

				List<Form> formList = keywordArray[j].getFormList();
				if (formList.size() > 0) {
					Form form;
					g.writeFieldName("ml:alt");
					g.writeStartArray();
					for (int i = 0; i < formList.size(); i++) {
						form = formList.get(i);
						g.writeStartObject();
						g.writeStringField("ml:form", form.getForm());
						g.writeNumberField("ml:freq", form.getProbability());

						g.writeEndObject();
					}
					g.writeEndArray();
				}


				List<Image> imageList = keywordArray[j].getImageList();
				if (imageList.size() > 0) {
					Image image;
					g.writeFieldName("ml:image");
					g.writeStartArray();
					for (int i = 0; i < imageList.size(); i++) {
						image = imageList.get(i);
						g.writeStartObject();
						g.writeStringField("ml:image", image.getImage());
						g.writeStringField("ml:thumb", image.getThumb());
						g.writeEndObject();
					}
					g.writeEndArray();
				}

				Map<String, String> crossLangMap = keywordArray[j].getCrossLangMap();
				if (crossLangMap.size() > 0) {
					g.writeFieldName("ml:cross");
					g.writeStartArray();
					String foreignLang;
					Iterator<String> itr = crossLangMap.keySet().iterator();
					for (int i = 0; itr.hasNext(); i++) {
						foreignLang = itr.next();

						g.writeStartObject();
						// g.writeStringField(foreignLang,
						// crossLangMap.get(foreignLang));
						g.writeStringField("ml:lang", foreignLang);
						g.writeStringField("ml:page", crossLangMap.get(foreignLang));
						g.writeEndObject();
					}
					g.writeEndArray();

				}

				List<NGram> nGramList = keywordArray[j].getNGramList();
				NGram nGram;
				g.writeFieldName("ml:ngram");
				g.writeStartArray();
				for (int i = 0; i < nGramList.size(); i++) {
					nGram = nGramList.get(i);
					g.writeStartObject();
					g.writeStringField("ml:form", nGram.getForm());
					g.writeNumberField("ml:n", nGram.getNGramLength());
					g.writeObjectFieldStart("ml:span");
					g.writeNumberField("ml:start", nGram.getStart());
					g.writeNumberField("ml:end", nGram.getEnd());
					g.writeEndObject();
					g.writeEndObject();
				}
				g.writeEndArray();
				g.writeEndObject();
			}
		}
		g.writeEndArray();
	}

	String toCSV(Keyword[] keywordArray, List<Topic> topicList, Locale locale, AnnotateParameter annotateParameter, ServiceTime serviceTime, double cost) {

		StringBuilder sb = new StringBuilder();
		StringBuilder textTopic = new StringBuilder();
		if (topicList != null && topicList.size() > 0) {
			Topic topic0 = topicList.get(0);
			//logger.debug("topic0 " + topic0);
			textTopic.append(topic0.getLabel());
			//add topics with same score
			for (int i = 1; i < topicList.size(); i++) {
				Topic topicI = topicList.get(i);
				//logger.debug("topic" +i+" " + topicI);
				if (topic0.getConfidence() == topicI.getConfidence()) {
					textTopic.append(",");
					textTopic.append(topicI.getLabel());
				}
				else {
					break;
				}
			}
		}
		//logger.debug("textTopic " + textTopic);
		for (int j = 0; j < keywordArray.length; j++) {

			if (!keywordArray[j].isFiltered()) {
				//logger.debug(j + "\t" + keywordArray[j].getSense().getPage());
				Sense sense = keywordArray[j].getSense();
				sb.append(j);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);

				sb.append(keywordArray[j].getTokenizedForm());
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				if (sense != null) {
					sb.append(sense.getPage());
				}
				else {
					sb.append("Null_result");
				}
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(keywordArray[j].weight());
				//text topic
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(textTopic);
				//term topics
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				//List<Topic> keywordTopicList = keywordArray[j].getTopicList();
				//Topic[] topicArray = keywordArray[j].getTopicArray();
				//if (keywordTopicList != null && keywordTopicList.size() > 0) {
				//if (keywordArray != null && keywordArray.length > 0) {
				//Topic topic0 = keywordTopicList.get(0);
				//todo:
				TopicSet keywordTopicSet = keywordArray[j].getTopic();
				if (keywordTopicSet.size() > 0) {
					List<Topic> keywordTopicList = keywordTopicSet.topicList();
					if (keywordTopicList.size() > 0) {
						Topic topic0 = keywordTopicList.get(0);
						sb.append(topic0.getLabel());
						//add topics with same score
						for (int i = 1; i < keywordTopicList.size(); i++) {
							Topic topicI = keywordTopicList.get(i);
							if (topic0.getConfidence() == topicI.getConfidence()) {
								sb.append(",");
								sb.append(topicI.getLabel());
							}
							else {
								break;
							}
						}
					}
				}
				//Topic topic0 = keywordArray[j].getTopic().topicList().get(0);
				//if (topic0 != null) {
				//	sb.append(topic0.getLabel());
				//}

				 /*
					//add topics with same score
					//for (int i = 1; i < keywordTopicList.size(); i++) {
					for (int i = 1; i < keywordArray.length; i++) {
						//Topic topicI = keywordTopicList.get(i);
						Topic topicI = keywordArray[i].getTopic();
						if (topic0.getConfidence() == topicI.getConfidence()) {
							sb.append(",");
							sb.append(topicI.getLabel());
						}
						else {
							break;
						}
					} */
				//}
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				Map<String, String> crossLangMap = keywordArray[j].getCrossLangMap();
				if (crossLangMap.size() > 0) {
					/*String foreignLang;
					Iterator<String> itr = crossLangMap.keySet().iterator();
					for (int i = 0; itr.hasNext(); i++) {
						foreignLang = itr.next();
						sb.append(CharacterTable.HORIZONTAL_TABULATION);
						sb.append(foreignLang);
						sb.append(CharacterTable.COLON);
						sb.append(crossLangMap.get(foreignLang));
						logger.debug(foreignLang + ":" + crossLangMap.get(foreignLang));

					} */

					if (annotateParameter.isTargetLangSpecified()) {
						String targetPage = crossLangMap.get(annotateParameter.getTargetLang());
						if (targetPage != null) {
							sb.append(targetPage);
						}

					}

				}
				sb.append(CharacterTable.CARRIADGE_RETURN);
			}


		}
		//logger.debug(sb.length());
		//logger.debug(sb.toString());
		return sb.toString();
	}
}