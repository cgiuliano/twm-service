/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 1/6/13 Time: 9:36 AM To
 * change this template use File | Settings | File Templates.
 */
class SummaryParameter extends AuthenticationParameter {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>LangParameter</code>.
	 */
	static Logger logger = Logger.getLogger(LangParameter.class.getName());

	public static final String AVERAGE = "AVERAGE";

	public static final String ARG_MAX = "ARG_MAX";

	public static final String SUM = "SUM";

	public static final double DEFAULT_COMPRESSION_RATIO = 0.3;

	String text;

	boolean includeText;

	String id;

	boolean includeId;

	String func;

	String lang;

	double minimumWeight;

	boolean includesMinimumWeight;

	double compressionRatio;

	boolean includesCompressionRatio;


	SummaryParameter(MLRequest request) throws MissingRequiredParameterException {
		super(request);
		text = getRequiredParameter(request, "text");

		//logger.debug("text = " + text + "(" + text.length() + ")");

		String s = request.getParameter("include_text");
		includeText = s != null && s.equals(TRUE);

		id = request.getParameter("id");

		lang = request.getParameter("lang");

		s = request.getParameter("min_weight");
		if (s != null) {
			includesMinimumWeight = true;
			minimumWeight = Double.parseDouble(s);
		}
		else {
			includesMinimumWeight = false;
		}
		s = request.getParameter("compression_ratio");
		if (s != null) {
			includesCompressionRatio = true;
			compressionRatio = Double.parseDouble(s);
		}
		else {
			compressionRatio = DEFAULT_COMPRESSION_RATIO;
			includesCompressionRatio = false;
		}

		func = request.getParameter("func");
		if (func == null) {
			func = SUM;
		}
		//logger.debug("func = " + func);
	}

	public boolean includesCompressionRatio() {
		return includesCompressionRatio;
	}

	public double getCompressionRatio() {
		return compressionRatio;
	}

	public boolean includesMinimumWeight() {
		return includesMinimumWeight;
	}

	public double getMinimumWeight() {
		return minimumWeight;
	}

	public boolean isLangSpecified() {
		if (lang == null) {
			return false;
		}
		return true;
	}

	public String getLang() {
		return lang;
	}

	public boolean isAverage() {
		if (func.equalsIgnoreCase(AVERAGE)) {
			return true;
		}
		return false;
	}

	public boolean isArgMax() {
		if (func.equalsIgnoreCase(ARG_MAX)) {
			return true;
		}
		return false;
	}

	public boolean isSum() {
		if (func.equalsIgnoreCase(SUM)) {
			return true;
		}
		return false;
	}

	public String getFunc() {
		return func;
	}

	public String getText() {
		return text;
	}

	public boolean includesText() {
		return includeText;
	}

	public boolean includesId() {
		return includeId;
	}

	public String getId() {
		return id;
	}


	@Override
	public String toString() {
		return "SummaryParameter{" +
				"text='" + text + '\'' +
				", includeText=" + includeText +
				", id='" + id + '\'' +
				", includeId=" + includeId +
				", func='" + func + '\'' +
				'}';
	}
}
