/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.machinelinking.jservice.help.HelpService;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 12/22/12 ServiceTime: 10:07
 * AM To change this template use File | Settings | File Templates.
 */
public class HelpHttpHandler extends AbstractHttpHandler {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>HelpHttpHandler</code>.
	 */
	static Logger logger = Logger.getLogger(HelpHttpHandler.class.getName());

	HelpService helpService;
	private boolean authentication;

	public HelpHttpHandler(boolean authentication) {
		this.authentication = authentication;
		helpService = HelpService.getInstance();
	}

	@Override
	public void service(MLRequest request, MLResponse response) throws MissingRequiredParameterException, AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException {
		logger.info(Thread.currentThread().getName() + " handling help...");
		ServiceTime serviceTime = new ServiceTime();

		HelpParameter helpParameter = new HelpParameter(request);
		logger.debug(helpParameter);

		if (authentication) {
			accessManagement.authorize(serviceTime, helpParameter, HELP_CMD, 0);
		}

		String result = null;
		String[] languages = null;
		if (helpParameter.includesLanguages()) {
			languages = helpService.supportedLanguages();
			logger.debug(Arrays.toString(languages));
			result = toJSon(languages);
		}

		response.setResult(result);

	}

	String toJSon(String[] languages) {

		try {
			StringWriter w = new StringWriter();

			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			g.writeFieldName("supportedLanguages");
			g.writeStartArray();

			for (int i = 0; i < languages.length; i++) {

				g.writeStartObject();
				g.writeStringField("label", new Locale(languages[i]).getDisplayLanguage(Locale.US));
				g.writeStringField("code", languages[i]);
				g.writeEndObject();
			}
			g.writeEndArray();
			g.writeEndObject();
			g.close();

			return w.toString();
		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}
	}

}