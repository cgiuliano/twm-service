/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Locale;

import com.machinelinking.jservice.annotation.lang.LangIdentifier;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.machinelinking.jservice.comp.TextSimilarity;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 12/22/12 ServiceTime: 10:07
 * AM To change this template use File | Settings | File Templates.
 * <p/>
 * <code>http://twm.apnetwork.it:5000/compare?app_id=c3e5ca13&app_key=cc193754ccbfd7c7f85648585b4e00f3&text1=Valentino lascia la Ducati&text2=Rossi returns to Yamaha&id1=1&id2=2</code>
 */
public class CompareHttpHandler extends AbstractHttpHandler {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>CompareHttpHandler</code>.
	 */
	static Logger logger = Logger.getLogger(CompareHttpHandler.class.getName());

	TextSimilarity textSimilarity;

	LangIdentifier langIdentifier;

	LogManagement logManagement;
	private boolean authentication;

	public CompareHttpHandler() {
		this(true);
	}

	public CompareHttpHandler(boolean authentication) {
		super();
		this.authentication= authentication;
		textSimilarity = TextSimilarity.getInstance();
		langIdentifier = LangIdentifier.getInstance();
		logManagement = LogManagement.getInstance();

	}

	@Override
	public void service(MLRequest request, MLResponse response) throws MissingRequiredParameterException, AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException {
		logger.debug(Thread.currentThread().getName() + " handling compering texts...");
		ServiceTime serviceTime = new ServiceTime();

		CompareParameter compareParameter = new CompareParameter(request);
		//logger.debug(compareParameter);

		//logger.debug(compareParameter);
		//logger.debug(compareParameter.getText1().length());
		//logger.debug(compareParameter.getText2().length());

		if (authentication){
			accessManagement.authorize(serviceTime, compareParameter, COMPARE_CMD, compareParameter.getText1().length() + compareParameter.getText2().length());
		}
		serviceTime.executionBegin();
		// the tokenization is always done
		String[] str1 = tokenizer.stringArray(compareParameter.getText1().toLowerCase());
		String[] str2 = tokenizer.stringArray(compareParameter.getText2().toLowerCase());

		Locale locale1 = langIdentifier.detect(str1);
		Locale locale2 = langIdentifier.detect(str2);
		//logger.debug(locale1 + "\t" + locale2);

		double d = 0;

		if (compareParameter.isLS()) {
			d = textSimilarity.compareLS(str1, str2);
		}
		else if (compareParameter.isCOMBO()) {
			d = textSimilarity.compareCombo(str1, str2);
		}
		else {
			d = textSimilarity.compareBOW(str1, str2);
		}
		serviceTime.executionEnd();
		//logger.debug("Similarity " + d + " computed in " + sf.format(serviceTime.executionNanoTime()) + " ns");

		String result = null;
		if (compareParameter.getOutputFormat().equalsIgnoreCase("jsonld")) {
			result = toJSonLD(d, compareParameter, serviceTime, 0);
		}
		else {
			result = toJSon(d, compareParameter, serviceTime, 0);
		}

		response.setResult(result);

		logManagement.writeLog(compareParameter, COMPARE_CMD, serviceTime, compareParameter.getText1().length() + compareParameter.getText2().length(), locale1, request.getHeaders());

	}

	String toJSon(double d, CompareParameter compareParameter, ServiceTime serviceTime, double cost) {
		try {
			StringWriter w = new StringWriter();

			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			if (compareParameter.includesText()) {
				g.writeStringField("text1", compareParameter.getText1());
				g.writeStringField("text2", compareParameter.getText2());
			}

			g.writeObjectFieldStart("annotation");
			if (compareParameter.includesId()) {
				g.writeStringField("id1", compareParameter.getId1());
				g.writeStringField("id2", compareParameter.getId2());
			}
			g.writeStringField("func", compareParameter.getFunc());
			g.writeNumberField("value", d);
			g.writeObjectFieldStart("time");
			g.writeNumberField("auth", serviceTime.authenticationTime());
			g.writeNumberField("exec", serviceTime.executionTime());
			g.writeEndObject();
			g.writeNumberField("cost", cost);
			g.writeEndObject();
			g.close();
			return w.toString();
		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}
	}

	String toJSonLD(double d, CompareParameter compareParameter, ServiceTime serviceTime, double cost) {
		try {
			StringWriter w = new StringWriter();

			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();

			g.writeObjectFieldStart("@context");
			//g.writeStringField("@language", locale.getLanguage());
			g.writeStringField("ml", "http://machinelinking.com/terms#");
			g.writeEndObject();

			if (compareParameter.includesText()) {
				g.writeStringField("ml:text1", compareParameter.getText1());
				g.writeStringField("ml:text2", compareParameter.getText2());
			}

			g.writeObjectFieldStart("ml:annotation");
			if (compareParameter.includesId()) {
				g.writeStringField("ml:id1", compareParameter.getId1());
				g.writeStringField("ml:id2", compareParameter.getId2());
			}
			g.writeStringField("ml:func", compareParameter.getFunc());
			g.writeNumberField("ml:value", d);
			g.writeObjectFieldStart("ml:time");
			g.writeNumberField("ml:auth", serviceTime.authenticationTime());
			g.writeNumberField("ml:exec", serviceTime.executionTime());
			g.writeEndObject();
			g.writeNumberField("ml:cost", cost);
			g.writeEndObject();
			g.close();
			return w.toString();
		} catch (IOException e) // Because we are writing to a String a
		// IOException is not possible! If occour we
		// fire a RuntimeException(server error)
		{
			throw new RuntimeException(e);
		}

	}
}