/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 1/6/13 Time: 9:36 AM To
 * change this template use File | Settings | File Templates.
 */
class LangParameter extends AuthenticationParameter {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>LangParameter</code>.
	 */
	static Logger logger = Logger.getLogger(LangParameter.class.getName());

	String text;

	boolean includeText;

	String id;

	boolean includeId;

	LangParameter(MLRequest request) throws MissingRequiredParameterException {

		super(request);

		text = getRequiredParameter(request, "text");
		//logger.debug("text = " + text + "(" + text.length() + ")");

		String s = request.getParameter("include_text");
		includeText = s != null && s.equals(TRUE);

		// todo: decode
		id = request.getParameter("id");
		includeId = id != null;

	}

	public String getText() {
		return text;
	}

	public boolean includesText() {
		return includeText;
	}

	public boolean includesId() {
		return includeId;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return "LangParameter{" + "text='" + text + '\'' + ", includeText=" + includeText + ", id='" + id + '\'' + ", includeId=" + includeId + '}';
	}
}
