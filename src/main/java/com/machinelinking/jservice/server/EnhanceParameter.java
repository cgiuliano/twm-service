/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;


public class EnhanceParameter extends AuthenticationParameter {
	String page;
	String lang;

	EnhanceParameter(MLRequest request) throws MissingRequiredParameterException {
		super(request);
		page = getRequiredParameter(request, "page");
		lang = getRequiredParameter(request, "lang");
	}

	public String getPage() {
		return page;
	}

	public String getLang() {
		return lang;
	}

}
