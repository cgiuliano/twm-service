/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


/**
 * Created with IntelliJ IDEA. User: giuliano Date: 1/12/13 Time: 9:11 PM To
 * change this template use File | Settings | File Templates.
 *
 * Demo on wikifiki:
 *
 * http://wikifiki.apnetwork.ch/frontend-alessio/demo-lang.html
 * <p/>
 * wikifiki:
 * java -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient --disambiguation -o wikifiki.apnetwork.ch/service-claudio -m annotate -t "Vado a Roma a vedere il papa"
 * java -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient --disambiguation -o http://wikifiki.apnetwork.ch/ -p 9000 -m annotate -t "Vado a Roma a vedere il papa"
 * <p/>
 * piotta: annotate
 * java -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient --host api.machinelinking.com -t "ciao sono italiano e sono andato alle primarie del PD a votare" -m annotate --app_id 050caa55 --app_key 6bd5a0c598c9d4135aa07e3052208390 --category 5  --class --cross --dbpedia --disambiguation --form --image --link --type
 * java -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient --host api.machinelinking.com -t "ciao sono italiano e sono andato alle primarie del PD a votare" -m summary --app_id=83e9bc8c --app_key=b696a737d483d694dd4cdaaf5ef1492b
 * java com.machinelinking.server.SimpleHttpClient --disambiguation -o wikifiki.apnetwork.ch/service-claudio --evaluation-file examples/en-200102.tsv --output-format csv --output-file prova.tsv -m annotate
 *
 * polibio
 * ssh -L 9000:4polibio:9000 polibio
 * java -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient --host localhost -p 9000 -t "ciao sono italiano e sono andato alle primarie del PD a votare" -m annotate --app_id 050caa55 --app_key 6bd5a0c598c9d4135aa07e3052208390 --category 5  --class --cross --dbpedia --disambiguation --form --image --link --type
 *
 * Browser
 * http://hlt-services6.fbk.eu:5000/annotate?text=%22Andare%20a%20Busca%20in%20Piemonte%20a%20incotrare%20il%20sentare%20Fassino%22&app_id=1&app_key=1&disambiguation=1&link=1&type=1&form=1&class=1
 *
 */
public class SimpleHttpClient {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>SimpleHttpClient</code>.
	 */
	static Logger logger = Logger.getLogger(SimpleHttpClient.class.getName());

	DecimalFormat sf = new DecimalFormat(".000");

	DecimalFormat vf = new DecimalFormat("###0");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private Pattern tabPattern = Pattern.compile("\t");

	//Account Claudio TN
	//public static final String DEFAULT_APP_ID = "c3e5ca13";
	//public static final String DEFAULT_APP_KEY = "cc193754ccbfd7c7f85648585b4e00f3";

	//Account Paolo TN
	public static final String DEFAULT_APP_ID = "947f4644";
	public static final String DEFAULT_APP_KEY = "08b27366b25f4a7cae2e80e423364950";

	public static final double DEFAULT_MINIMUM_WEIGHT = 0.25;

	public static final double DEFAULT_COMPRESSION_RATIO = 0.3;

	public static final String DEFAULT_SUMMARY_FUNC = "SUM";

	public SimpleHttpClient(CommandLine line) throws IOException {
		try {
			String serverCall = buildServerCall(line);
			URL serverAddress = buildServerAddress(line);
			if (line.hasOption("text")) {

				logger.info(serverAddress + "?" + serverCall);
				String answer = call(serverAddress, serverCall);
				logger.info("\n" + answer);
				if (line.hasOption("outputFile")) {
					File outputFile = new File(line.getOptionValue("output-file"));
					logger.debug("writing in " + outputFile + "...");
					write(outputFile, answer);
				}
			}
			else if (line.hasOption("input-file")) {

				File inputFile = new File(line.getOptionValue("input-file"));

				if (inputFile.isFile()) {
					String answer = null;
					File outputFile = null;
					if (line.hasOption("output-file")) {
						outputFile = new File(line.getOptionValue("output-file"));
					}
					boolean html = false;
					if (line.hasOption("html")) {
						html = true;
					}
					boolean split = false;
					logger.debug("\n\n" + line.hasOption("split") + "\n\n");
					if (line.hasOption("split")) {
						answer = processFile(serverAddress, serverCall, inputFile, outputFile, html, split);
					} else {
						answer = processFile(serverAddress, serverCall, inputFile, outputFile, html);
					}

					logger.info("\n" + answer);
				}
				else if (inputFile.isDirectory()) {
					logger.debug("reading from directory " + inputFile + "...");
					//todo: allow parametric extension
					/*final String extension  = ".txt";
					if (line.hasOption("file-extension")) {
						extension = line.getOptionValue("file-extension");
					}*/
					File[] files = inputFile.listFiles(new FilenameFilter() {
						public boolean accept(File directory, String fileName) {
							return fileName.endsWith(".txt");
						}
					});
					File outputDir = null;
					if (line.hasOption("output-file")) {
						outputDir = new File(line.getOptionValue("output-file"));
						if (!outputDir.exists()) {
							boolean b = outputDir.mkdir();
							logger.info(outputDir + (b ? " successfully " : " not ") + "created");
						}
					}

					for (int i = 0; i < files.length; i++) {
						try {
							boolean html = false;
							if (line.hasOption("html")) {
								html = true;
							}
							File outputFile = null;
							if (outputDir != null) {
								outputFile = new File(outputDir.getAbsolutePath() + File.separator + files[i].getName() + ".ml");
							}

							//String answer = processFile(serverAddress, serverCall, files[i], outputFile, html);
							boolean split = false;
							String answer = null;

							if (line.hasOption("split")) {
								answer = processFile(serverAddress, serverCall, files[i], outputFile, html, split);
							} else {
								answer = processFile(serverAddress, serverCall, files[i], outputFile, html);
							}
							logger.info("\n" + answer);
						} catch (Exception e) {
							logger.error("Error at " + files[i]);
							logger.error(e);
						}


					}
				}
			}
			else if (line.hasOption("evaluation-file")) {
				evaluation(serverAddress, line);
			}

		} catch (MalformedURLException e) {
			logger.error(e);
		} catch (ProtocolException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	String processFile(URL serverAddress, String serverCall, File inputFile, File outputFile, boolean html, boolean split) throws IOException {
		StringBuilder answerBuilder = new StringBuilder();
		logger.debug("reading from " + inputFile + " (" + inputFile.length() + ")...");

		String text = read(inputFile);
		if (text != null) {
			String[] lines = text.split("\n");
			for (int i = 0; i < lines.length; i++) {
				StringBuilder sb = new StringBuilder();
				if (html) {
					text = StringEscapeUtils.unescapeHtml(text);
					//logger.warn(text);
				}
				sb.append("&text");
				sb.append("=");
				sb.append(URLEncoder.encode(lines[i], CharEncoding.UTF_8));
				logger.info(serverAddress + "?" + serverCall + sb.toString());
				String answer = call(serverAddress, serverCall + sb.toString());
				answerBuilder.append(answer);
				if (outputFile != null) {
					logger.debug("writing in " + outputFile + "...");
					write(outputFile, answer, true);
				}
			}
		}
		return answerBuilder.toString();
	}

	String processFile(URL serverAddress, String serverCall, File inputFile, File outputFile, boolean html) throws IOException {
		String answer = null;


		logger.debug("reading from " + inputFile + " (" + inputFile.length() + ")...");

		String text = read(inputFile);

		StringBuilder sb = new StringBuilder();
		if (text != null) {
			if (html) {
				text = StringEscapeUtils.unescapeHtml(text);
				logger.warn(text);
			}
			sb.append("&text");
			sb.append("=");
			sb.append(URLEncoder.encode(text, CharEncoding.UTF_8));
		}
		logger.info(serverAddress + "?" + serverCall + sb.toString());
		answer = call(serverAddress, serverCall + sb.toString());
		if (outputFile != null) {

			logger.debug("writing in " + outputFile + "...");
			write(outputFile, answer);
		}


		return answer;
	}

	void write(File f, String s, boolean append) throws IOException {
		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, append), "UTF-8")));
		pw.println(s);
		pw.close();
	}

	void write(File f, String s) throws IOException {
		write(f, s, false);
	}

	String read(File f) throws IOException {
		StringBuilder sb = new StringBuilder();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line = null;
		// run the rest of the file
		while ((line = lnr.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		} // end while
		lnr.close();
		return sb.toString();
	}

	private void evaluation(URL serverAddress, CommandLine line) throws IOException {
		String fileName = line.getOptionValue("evaluation-file");
		String outputFileName = line.getOptionValue("output-file");
		StringBuilder outputBuilder = new StringBuilder();
		double threshold = DEFAULT_MINIMUM_WEIGHT;
		if (line.hasOption("min-weight")) {
			threshold = Double.parseDouble(line.getOptionValue("min-weight"));
		}
		//logger.debug(threshold);
		logger.info("evaluating " + fileName + " (" + new Date() + ")...");
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = null;
		lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
		String fileLine;

		double tp = 0, fp = 0, fn = 0, tn = 0, tot = 0;
		double p = 0, r = 0, f1 = 0, acc = 0, correct = 0;
		System.out.println("#t=" + threshold);
		System.out.println("#t=" + threshold);
		System.out.println("#date=" + new Date());

		System.out.println("#id\ttp\tfp\tfn\tp\tr\tf1\tacc\tans\tgold\tterm\ttime");
		while ((fileLine = lnr.readLine()) != null) {
			String[] s = tabPattern.split(fileLine);
			if (s.length == 5) {
				String correctPage = s[0];
				StringBuilder sb = new StringBuilder();
				sb.append("app_id=");
				sb.append(DEFAULT_APP_ID);
				sb.append("&app_key=");
				sb.append(DEFAULT_APP_KEY);
				sb.append("&text=");
				sb.append(URLEncoder.encode(s[2] + " " + s[3] + " " + s[4], CharEncoding.UTF_8));
				sb.append("&disambiguation=1&output_format=csv&min_weight=" + threshold);
				if (line.hasOption("nbest-filter")) {
					sb.append("&nbest_filter=");
					sb.append(line.getOptionValue("nbest-filter"));
				}
				if (line.hasOption("overlapping-filter")) {
					sb.append("&overlapping_filter=");
					sb.append(line.getOptionValue("overlapping-filter"));
				}
				if (line.hasOption("type-filter")) {
					sb.append("&type_filter=");
					sb.append(line.getOptionValue("type-filter"));
				}
				if (line.hasOption("person-filter")) {
					sb.append("&person_filter=");
					sb.append(line.getOptionValue("person-filter"));
				}
				if (line.hasOption("topic")) {
					sb.append("&topic=1");
				}
				long b = System.currentTimeMillis();
				String answer = call(serverAddress, sb.toString());
				long e = System.currentTimeMillis();
				Map<String, String[]> map = parserAnswer(answer);
				String[] answerLine = map.get(s[3]);
				//logger.debug(answerLine);
				//logger.debug(Arrays.toString(answerLine));
				String page = null;
				String pageTopic = null, textTopic = null;
				boolean right = false;
				if (answerLine == null) {
					if (s[0].equals("Null_result")) {
						tp++;
						correct++;
						right = true;
					}
					else {
						fn++;
					}
				}
				else {
					page = answerLine[2];
					if (answerLine.length > 5) {
						pageTopic = answerLine[5];
					}
					if (answerLine.length > 4) {
						textTopic = answerLine[4];
					}
					if (page.equals(s[0])) {
						tp++;
						correct++;
						right=true;
					}
					else {
						fn++;
						fp++;
					}
				}
				tot++;
				p = tp / (tp + fp);
				r = tp / (tp + fn);
				f1 = (2 * p * r) / (p + r);
				//acc = correct / tot;


				//System.out.println(s[1] + "\t" + vf.format(correct) + "\t" + vf.format(tot) + "\t" + vf.format(tp) + "\t" + vf.format(fp) + "\t" + vf.format(fn) + "\t" + sf.format(p) + "\t" + sf.format(r) + "\t" + sf.format(f1) + "\t" + sf.format(acc) + "\t" + page + "\t" + s[0] + "\t" + s[3] + "\t" + textTopic + "\t" + pageTopic + "\t" + df.format(e - b));
				//no accuracy
				System.out.println(s[1] + "\t" + vf.format(tp) + "\t" + vf.format(fp) + "\t" + vf.format(fn) + "\t" + sf.format(p) + "\t" + sf.format(r) + "\t" + sf.format(f1) + "\t" + page + "\t" + s[0] + "\t" + s[3] + "\t" + textTopic + "\t" + pageTopic + "\t" + df.format(e - b));
				outputBuilder.append(s[1] + "\t" + vf.format(tp) + "\t" + vf.format(fp) + "\t" + vf.format(fn) + "\t" + sf.format(p) + "\t" + sf.format(r) + "\t" + sf.format(f1) + "\t" + page + "\t" + s[0] + "\t" + s[3] + "\t" + textTopic + "\t" + pageTopic + "\t" + right + "\n");
			}
		}

		lnr.close();
		end = System.currentTimeMillis();
		//System.out.println("\t" + vf.format(correct) + "\t" + vf.format(tot) + "\t" + vf.format(tp) + "\t" + vf.format(fp) + "\t" + vf.format(fn) + "\t" + sf.format(p) + "\t" + sf.format(r) + "\t" + sf.format(f1) + "\t" + sf.format(acc));
		//no accuracy
		System.out.println("\t" + vf.format(tp) + "\t" + vf.format(fp) + "\t" + vf.format(fn) + "\t" + sf.format(p) + "\t" + sf.format(r) + "\t" + sf.format(f1));
		outputBuilder.append("\t" + vf.format(tp) + "\t" + vf.format(fp) + "\t" + vf.format(fn) + "\t" + sf.format(p) + "\t" + sf.format(r) + "\t" + sf.format(f1) + "\n");
		if (outputFileName != null) {
			write(new File(outputFileName), outputBuilder.toString());
		}

		logger.info(df.format(tot) + "\t" + df.format(end - begin) + "\t" + new Date());


	}

	private Map<String, String[]> parserAnswer(String answer) throws IOException {

		LineNumberReader lnr = null;
		lnr = new LineNumberReader(new StringReader(answer));
		String line;

		Map<String, String[]> map = new HashMap<String, String[]>();
		while ((line = lnr.readLine()) != null) {
			//logger.debug(line);
			String[] s = tabPattern.split(line);
			if (s.length > 2) {
				//logger.debug(line);
				//id \t form \t page
				map.put(s[1], s);
			}
		}
		lnr.close();
		return map;
	}


	private String call(URL serverAddress, String serverCall) throws ProtocolException, IOException {
		String fromServer = null;
		// Set up the initial connection
		HttpURLConnection connection = (HttpURLConnection) serverAddress.openConnection();
		connection.setRequestMethod("POST");
		//connection.setRequestMethod("GET");
		//logger.debug(connection.getRequestMethod());
		connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");

		// used to debug Mashape
		//connection.setRequestProperty("X-Mashape-user", "poffis");
		//connection.setRequestProperty("X-Mashape-Subscription", "PRO");

		connection.setDoOutput(true);
		//connection.setReadTimeout(10000);
		//connection.setConnectTimeout(10000);
		connection.getOutputStream().write(serverCall.getBytes(CharEncoding.UTF_8));
		connection.connect();
		//logger.debug(serverCall);

		// read the result from the server
		BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuilder sb = new StringBuilder();
		while ((fromServer = rd.readLine()) != null) {
			sb.append(fromServer + '\n');
		}
		return sb.toString();
	}

	private URL buildServerAddress(CommandLine line) throws MalformedURLException {
		StringBuilder sbUrl = new StringBuilder();
		String host = line.getOptionValue("host");
		if (host == null) {
			host = MainHttpServer.DEFAULT_HOST;
		}
		String port = line.getOptionValue("port");
		sbUrl.append("http://");
		sbUrl.append(host);
		if (port != null) {
			sbUrl.append(":");
			sbUrl.append(port);
		}
		sbUrl.append("/");
		String method = line.getOptionValue("method");

		if (method != null) {
			sbUrl.append(method);
		}
		else {
			sbUrl.append("ping");
		}
		sbUrl.append("/");
		return new URL(sbUrl.toString());
	}

	private String buildServerCall(CommandLine line) throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();

		String appId = line.getOptionValue("app_id");
		if (appId != null) {
			sb.append("app_id=");
			sb.append(appId);
		}
		else {
			sb.append("app_id=");//c3e5ca13");
			sb.append(DEFAULT_APP_ID);
		}
		String appKey = line.getOptionValue("app_key");
		if (appId != null) {
			sb.append("&app_key=");
			sb.append(appKey);
		}
		else {
			sb.append("&app_key=");
			sb.append(DEFAULT_APP_KEY);
			//cc193754ccbfd7c7f85648585b4e00f3");
		}
		String text = line.getOptionValue("text");
		if (text != null) {

			sb.append("&text");
			sb.append("=");
			//logger.debug("\n\n" + line.hasOption("html") + "\n\n");
			if (line.hasOption("html")) {
				text = StringEscapeUtils.unescapeHtml(text);
				logger.debug(text);
			}

			sb.append(URLEncoder.encode(text, CharEncoding.UTF_8));
		}
		String lang = line.getOptionValue("lang");
		if (lang != null) {
			sb.append("&lang");
			sb.append("=");
			sb.append(lang);
		}
		String targetLang = line.getOptionValue("target-lang");
		if (targetLang != null) {
			sb.append("&target_lang");
			sb.append("=");
			sb.append(targetLang);
		}
		if (line.hasOption("id")) {
			sb.append("&id=" + line.getOptionValue("id"));
		}
		if (line.hasOption("include-text")) {
			sb.append("&include_text=1");
		}

		if (line.hasOption("disambiguation")) {
			sb.append("&disambiguation=1");
		}

		if (line.hasOption("form")) {
			sb.append("&form=1");
		}
		if (line.hasOption("image")) {
			sb.append("&image=1");
		}
		if (line.hasOption("topic")) {
			sb.append("&topic=1");
		}
		if (line.hasOption("category")) {
			//sb.append("&category=1");
			sb.append("&category=" + line.getOptionValue("category"));
		}
		if (line.hasOption("type")) {
			sb.append("&type=1");
		}
		if (line.hasOption("cross")) {
			sb.append("&cross=1");
		}
		if (line.hasOption("link")) {
			sb.append("&link=1");
		}
		if (line.hasOption("dbpedia")) {
			sb.append("&dbpedia=1");
		}
		if (line.hasOption("abstract")) {
			sb.append("&abstract=1");
		}
		if (line.hasOption("nbest-filter")) {
			sb.append("&nbest_filter=");
			sb.append(line.getOptionValue("nbest-filter"));
		}
		if (line.hasOption("overlapping-filter")) {
			sb.append("&overlapping_filter=");
			sb.append(line.getOptionValue("overlapping-filter"));
		}
		if (line.hasOption("type-filter")) {
			sb.append("&type_filter=");
			sb.append(line.getOptionValue("type-filter"));
		}
		if (line.hasOption("person-filter")) {
			sb.append("&person_filter=");
			sb.append(line.getOptionValue("person-filter"));
		}
		if (line.hasOption("class")) {
			sb.append("&class=1");
		}
		if (line.hasOption("output-format")) {
			sb.append("&output_format=");
			sb.append(line.getOptionValue("output-format"));
		}
		if (line.hasOption("min-weight")) {
			sb.append("&min_weight=");
			sb.append(Double.parseDouble(line.getOptionValue("min-weight")));
		}
		if (line.hasOption("compression-ratio")) {
			sb.append("&compression_ratio=");
			sb.append(Double.parseDouble(line.getOptionValue("compression-ratio")));
		}
		if (line.hasOption("summary-func")) {
			sb.append("&func=");
			sb.append(line.getOptionValue("summary-func"));
		}
		if (line.hasOption("output-format")) {
			sb.append("&output_format=");
			sb.append(line.getOptionValue("output-format"));
		}
		return sb.toString();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// java -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option hostOpt = OptionBuilder.withArgName("string").hasArg().withDescription("host name (default is " + MainHttpServer.DEFAULT_HOST + ")").withLongOpt("host").create("o");
			Option portOpt = OptionBuilder.withArgName("int").hasArg().withDescription("port number (default is " + MainHttpServer.DEFAULT_PORT + ")").withLongOpt("port").create("p");
			Option methodOpt = OptionBuilder.withArgName("string").hasArg().withDescription("method request").withLongOpt("method").create("m");
			Option textOpt = OptionBuilder.withArgName("string").hasArg().withDescription("text to annotate").withLongOpt("text").create("t");

			Option inputFileOpt = OptionBuilder.withArgName("[file|dir]").hasArg().withDescription("file or directory from which to read the text(s) to annotate").withLongOpt("input-file").create();

			Option evaluationOpt = OptionBuilder.withArgName("file").hasArg().withDescription("evaluation file to annotate").withLongOpt("evaluation-file").create();

			Option langOpt = OptionBuilder.withArgName("string").hasArg().withDescription("text language").withLongOpt("lang").create("l");
			Option includeTextOpt = OptionBuilder.withDescription("include text").withLongOpt("include-text").create();
			//Option includeSummaryOpt = OptionBuilder.withDescription("include summary").withLongOpt("include-summary").create();
			Option idOpt = OptionBuilder.withArgName("string").hasArg().withDescription("text id").withLongOpt("id").create();
			Option outputFormatOpt = OptionBuilder.withArgName("[json|csv]").hasArg().withDescription("output format").withLongOpt("output-format").create();
			Option outputFileOpt = OptionBuilder.withArgName("[file|dir]").hasArg().withDescription("file or directory in which to write the annotated text(s)").withLongOpt("output-file").create();
			Option fileExtensionOpt = OptionBuilder.withArgName("string").hasArg().withDescription("extension of the file(s) from which to read the text(s) to annotate").withLongOpt("file-extension").create();
			Option targetLanguageOpt = OptionBuilder.withArgName("string").hasArg().withDescription("target language").withLongOpt("target-lang").create();
			Option includeDisambiguationOpt = OptionBuilder.withDescription("include disambiguation").withLongOpt("disambiguation").create();
			Option includeFormOpt = OptionBuilder.withDescription("include forms").withLongOpt("form").create();
			Option htmlOpt = OptionBuilder.withDescription("un/escapes html").withLongOpt("html").create();
			Option nBestFilterOpt = OptionBuilder.withArgName("[0|1]").hasArg().withDescription("nbest filter").withLongOpt("nbest-filter").create();
			Option personFilterOpt = OptionBuilder.withArgName("[0|1]").hasArg().withDescription("person filter").withLongOpt("person-filter").create();
			Option typeFilterOpt = OptionBuilder.withArgName("[0|1]").hasArg().withDescription("type filter").withLongOpt("type-filter").create();
			Option overlappingFilterOpt = OptionBuilder.withArgName("[0|1]").hasArg().withDescription("overlapping filter").withLongOpt("overlapping-filter").create();
			Option includeImageOpt = OptionBuilder.withDescription("include images").withLongOpt("image").create();
			Option includeCrossOpt = OptionBuilder.withDescription("include cross language links").withLongOpt("cross").create();
			Option includeTypeOpt = OptionBuilder.withDescription("include types").withLongOpt("type").create();
			Option includeLinkOpt = OptionBuilder.withDescription("include links").withLongOpt("link").create();
			Option includeClassOpt = OptionBuilder.withDescription("include class").withLongOpt("class").create();

			Option includeCategoryOpt = OptionBuilder.withArgName("int").hasArg().withDescription("include category with specified depth").withLongOpt("category").create();
			Option includeTopicOpt = OptionBuilder.withDescription("include topic").withLongOpt("topic").create();
			Option includeAirpediaOpt = OptionBuilder.withDescription("include Airpedia class").withLongOpt("dbpedia").create();
			Option splitOpt = OptionBuilder.withDescription("split the input file").withLongOpt("split").create();
			Option includeWikipediaAbstrctOpt = OptionBuilder.withDescription("include Wikipedia abstract").withLongOpt("abstract").create();
			Option appIdOpt = OptionBuilder.withArgName("string").hasArg().withDescription("application id (default is " + DEFAULT_APP_ID + ")").create("app_id");
			Option appKeyOpt = OptionBuilder.withArgName("string").hasArg().withDescription("application key (default is " + DEFAULT_APP_KEY + ")").create("app_key");
			Option minWeightOpt = OptionBuilder.withArgName("double").hasArg().withDescription("minimum keyword weight (default is " + DEFAULT_MINIMUM_WEIGHT + ")").withLongOpt("min-weight").create();
			Option compressionRatioOpt = OptionBuilder.withArgName("double").hasArg().withDescription("summary compression ratio (default is " + DEFAULT_COMPRESSION_RATIO + ")").withLongOpt("compression-ratio").create();
			Option summaryFuncOpt = OptionBuilder.withArgName("string").hasArg().withDescription("summary compression function (default is " + DEFAULT_SUMMARY_FUNC + ")").withLongOpt("summary-func").create();

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(evaluationOpt);
			options.addOption(inputFileOpt);
			options.addOption(hostOpt);
			options.addOption(portOpt);
			options.addOption(methodOpt);
			options.addOption(textOpt);
			options.addOption(langOpt);
			options.addOption(appIdOpt);
			options.addOption(appKeyOpt);
			options.addOption(includeTextOpt);
			options.addOption(includeCategoryOpt);
			options.addOption(includeDisambiguationOpt);
			options.addOption(includeFormOpt);
			options.addOption(includeImageOpt);
			options.addOption(includeCrossOpt);
			options.addOption(includeTypeOpt);
			options.addOption(includeLinkOpt);
			options.addOption(idOpt);
			options.addOption(includeAirpediaOpt);
			options.addOption(minWeightOpt);
			options.addOption(includeWikipediaAbstrctOpt);
			options.addOption(includeClassOpt);
			options.addOption(compressionRatioOpt);
			options.addOption(summaryFuncOpt);
			options.addOption(outputFormatOpt);
			options.addOption(includeTopicOpt);
			options.addOption(nBestFilterOpt);
			options.addOption(personFilterOpt);
			options.addOption(typeFilterOpt);
			options.addOption(overlappingFilterOpt);
			options.addOption(outputFileOpt);
			options.addOption(fileExtensionOpt);
			options.addOption(targetLanguageOpt);
			options.addOption(htmlOpt);
			options.addOption(splitOpt);


			CommandLineParser parser = new PosixParser();


			// parse the command line arguments
			CommandLine line = parser.parse(options, args);

			if (line.getOptions().length == 0 || line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			new SimpleHttpClient(line);

		} catch (ParseException e) {
			// oops, something went wrong
			System.err.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient", "\n", options, "\n", true);
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
