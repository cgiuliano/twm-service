package com.machinelinking.jservice.server;


import org.glassfish.grizzly.http.server.Response;

/**
 * Reponse wrapper. This wrapper automatically handles the encoding and writing
 * response.
 *
 * @author d@vide.bz
 */
public class MLResponse {

	private boolean alreadyWritten;

	private String result;

	//private Response response;

	MLResponse() {
		//this.response = response;
		//super();
		alreadyWritten = false;
		result = null;
	}

	/*void writeResult(String result) {
		checkOnlyOnce();
		this.result = result;
	} */

	private void checkOnlyOnce() {
		if (alreadyWritten) {
			throw new IllegalStateException("Response writeResult can be called only once!");
		}
		alreadyWritten = true;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
