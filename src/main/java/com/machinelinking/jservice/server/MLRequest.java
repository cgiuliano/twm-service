package com.machinelinking.jservice.server;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang.CharEncoding;
import org.apache.log4j.Logger;
import org.glassfish.grizzly.http.server.Request;

/**
 * Reponse wrapper. This wrapper automatically handles the parameters reading
 * and the enconding problems.
 *
 * @author d@vide.bz
 */
public class MLRequest {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>LangParameter</code>.
	 */
	static Logger logger = Logger.getLogger(LangParameter.class.getName());

	//todo: check if we can extend Request
	private Request request;

	MLRequest(Request request) {
		this.request = request;
		//logger.debug("header:\n" + toString());
	}

	public String getHeaders(){
		// this information is provided by Mashape to specify user and subscription
		String subscription = request.getHeader("x-mashape-subscription");
		String user = request.getHeader("x-mashape-user");
		String host = request.getHeader("x-forwarded-host");
		String secret = request.getHeader("x-mashape-proxy-secret");
		return user+"\t"+subscription+"\t"+host+"\t"+secret;
	}

	/*public String getParameters(){
		String subscription = request.getParameter("x-mashape-subscription");
		String user = request.getParameter("x-mashape-user");
		String host = request.getParameter("x-forwarded-host");

		return user+"\t"+subscription+"\t"+host;
	}*/

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("method="+request.getMethod()+"\n");
		sb.append("headers="+request.getHeaderNames()+"\n");
		sb.append("authorization="+request.getAuthorization()+"\n");
		sb.append("encoding="+request.getCharacterEncoding()+"\n");
		sb.append("length="+request.getContentLength()+"\n");
		sb.append("type="+request.getContentType()+"\n");
		sb.append("address="+request.getRemoteAddr()+"\n");
		sb.append("host="+request.getRemoteHost()+"\n");
		sb.append("user="+request.getRemoteUser()+"\n");
		sb.append("headers:\n");
		Iterator<String> it = request.getHeaderNames().iterator();
		for (int i = 0; it.hasNext() ; i++) {
			String name = it.next();
			String value = request.getHeader(name);
			sb.append(name);
			sb.append("=");
			sb.append(value);
			sb.append("\n");
		}

		sb.append("parameters:\n");
		Iterator<String> parameters = request.getParameterNames().iterator();
		for (int i = 0; parameters.hasNext() ; i++) {
			String name = parameters.next();
			String value = request.getParameter(name);
			sb.append(name);
			sb.append("=");
			sb.append(value);
			sb.append("\n");
		}
		sb.append("date="+new Date()+"\n");
		return sb.toString();
	}

	public String getIP() {
		//todo: we have the proxy we cannot see the IP
		return request.getRemoteHost() + ":" + request.getRemoteAddr() + ":" + request.getRemotePort();
	}


	public String getMethod() {
		return request.getMethod().toString();
	}

	/**
	 * Because Grizzly behaves strange (different from tomcat for example) with
	 * parameter given in the URL with utf-8 encoding, we create a unique and
	 * common method that handle correctly this problems. It may be a bug. In
	 * the future only one place is necessary to change when changing container
	 * of after grizzly bug fix.
	 *
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	String getParameter(String name) {
		if (request.getMethod().getMethodString().equalsIgnoreCase("get")) {
			return readRawUtf8String(request.getParameter(name));
		}
		if (request.getMethod().getMethodString().equalsIgnoreCase("post")) {
			String queryString = request.getQueryString();
			if (queryString != null) {
				throw new IllegalStateException("If you use post don't use parameters on the url. Give all the parameters in the input stream!");
			}
			String postParameter = request.getParameter(name);
			// Post parameter does not need of utf8 encoding! If needed is because the utf-8 was not specified
			// in the post Content type header
			// connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			// postParameter = readRawUtf8String(postParameter);
			return postParameter;
		}
		throw new IllegalStateException("Only get or post supported!");
	}

	private static String readRawUtf8String(String rawUtf8String) {
		if (rawUtf8String == null) {
			return null;
		}
		byte[] rawUtf8Bytes = new byte[rawUtf8String.length()];
		for (int i = 0; i < rawUtf8Bytes.length; i++) {
			int c = rawUtf8String.charAt(i);
			if (c > 255 || c < 0) {
				// I use a runtime exception, because reading parameter must be
				// present, I am handling a bug!
				throw new RuntimeException("Unexpected utf-8 value!");
			}
			rawUtf8Bytes[i] = (byte) c;
		}
		try {
			return new String(rawUtf8Bytes, CharEncoding.UTF_8);
		} catch (UnsupportedEncodingException encxxx) {
			// I use a runtime exception, because reading parameter must be
			// present, I am handling a bug!
			throw new RuntimeException("Unexpected utf-8 exception!", encxxx);
		}
	}

}
