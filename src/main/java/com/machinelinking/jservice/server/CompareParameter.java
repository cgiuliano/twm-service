/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 1/6/13 Time: 9:36 AM To
 * change this template use File | Settings | File Templates.
 */
class CompareParameter extends AuthenticationParameter {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>LangParameter</code>.
	 */
	static Logger logger = Logger.getLogger(LangParameter.class.getName());

	public static final String BOW_SIMILARITY = "BOW";

	public static final String LS_SIMILARITY = "LS";

	public static final String COMBO_SIMILARITY = "COMBO";

	// public static final String DEFAULT_SIMILARITY_FUNCTION =
	// COMBO_SIMILARITY;

	String text1, text2;

	boolean includeText;

	String id1, id2;

	boolean includeId;

	String func;

	CompareParameter(MLRequest request) throws MissingRequiredParameterException {
		super(request);
		text1 = getRequiredParameter(request, "text1");
		text2 = getRequiredParameter(request, "text2");

		int tot = text1.length() + text2.length();

		//logger.debug("text1 = " + text1 + "(" + text1.length() + "/" + tot + ")");
		//logger.debug("text2 = " + text2 + "(" + text2.length() + "/" + tot + ")");

		String s = request.getParameter("include_text");
		includeText = s != null && s.equals(TRUE);

		// todo: decode
		id1 = request.getParameter("id1");
		id2 = request.getParameter("id2");
		includeId = id1 != null && id2 != null;

		func = request.getParameter("func");
		if (func == null) {
			func = COMBO_SIMILARITY;
		}
	}

	public boolean isCOMBO() {
		if (func.equalsIgnoreCase(COMBO_SIMILARITY)) {
			return true;
		}
		return false;
	}

	public boolean isBOW() {
		if (func.equalsIgnoreCase(BOW_SIMILARITY)) {
			return true;
		}
		return false;
	}

	public boolean isLS() {
		if (func.equalsIgnoreCase(LS_SIMILARITY)) {
			return true;
		}
		return false;
	}

	public String getFunc() {
		return func;
	}

	public String getText1() {
		return text1;
	}

	public String getText2() {
		return text2;
	}

	public boolean includesText() {
		return includeText;
	}

	public boolean includesId() {
		return includeId;
	}

	public String getId1() {
		return id1;
	}

	public String getId2() {
		return id2;
	}

	@Override
	public String toString() {
		return "CompareParameter{" + "text1='" + text1 + '\'' + ", text2='" + text2 + '\'' + ", includeText=" + includeText + ", id1='" + id1 + '\'' + ", id2='" + id2 + '\'' + ", includeId=" + includeId + ", func='" + func + '\'' + '}';
	}
}
