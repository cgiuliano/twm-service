/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import org.apache.commons.lang.CharEncoding;
import org.glassfish.grizzly.http.server.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.commons.cli.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/22/12
 * ServiceTime: 9:32 AM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * java -Dfile.encoding=UTF-8 -mx60G -cp dist/jservice.jar com.machinelinking.server.MainHttpServer -p 9000 -a -l -e
 * java -Dfile.encoding=UTF-8 -mx60G com.machinelinking.server.MainHttpServer -p 9000 -a -l -e
 * <p/>
 * polibio:
 * java -Dfile.encoding=UTF-8 -mx4G -cp dist/jservice.jar com.machinelinking.server.MainHttpServer --no-authentication -p 9000 -a -l -e
 */
public class MainHttpServer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>MainHttpServer</code>.
	 */
	static Logger logger = Logger.getLogger(MainHttpServer.class.getName());

	private HttpServer httpServer;

	public static final String DEFAULT_HOST = "localhost";

	DecimalFormat df = new DecimalFormat("###,###,###");

	public static final String DEFAULT_PORT = "8080";

	public MainHttpServer(CommandLine line) {
		this(DEFAULT_HOST, DEFAULT_PORT, line);
	}

	public MainHttpServer(String host, String port, CommandLine line) {

		logger.info("starting " + host + "\t" + port + " (" + new Date() + ")...");
		long begin = System.currentTimeMillis();
		httpServer = HttpServer.createSimpleServer(host, new Integer(port).intValue());

		boolean authentication = true;
		if (line.hasOption("no-authentication")) {
			authentication = false;
		}

		if (line.hasOption("annotate")) {
			httpServer.getServerConfiguration().addHttpHandler(new AnnotateHttpHandler(authentication), "/annotate");
		}
		if (line.hasOption("compare")) {
			httpServer.getServerConfiguration().addHttpHandler(new CompareHttpHandler(authentication), "/compare");
		}
		if (line.hasOption("lang")) {
			httpServer.getServerConfiguration().addHttpHandler(new LangHttpHandler(authentication), "/lang");
		}
		if (line.hasOption("enrich")) {
			httpServer.getServerConfiguration().addHttpHandler(new EnhanceHttpHandler(authentication), "/enrich");
		}

		if (line.hasOption("summary")) {
			httpServer.getServerConfiguration().addHttpHandler(new SummaryHttpHandler(authentication), "/summary");
		}

		//if (line.hasOption("topic")) {
		//httpServer.getServerConfiguration().addHttpHandler(new TopicHttpHandler(), "/topic");
		//}

		httpServer.getServerConfiguration().addHttpHandler(new HelpHttpHandler(authentication), "/help");
		httpServer.getServerConfiguration().addHttpHandler(new PingHttpHandler(), "/ping");
		httpServer.getServerConfiguration().addHttpHandler(new LogHttpHandler(), "/log");

		//todo: Davide code will start here
		//httpServer.getServerConfiguration().addHttpHandler(new DisambiguationHttpHandler(), "/disambiguation");
		long end = System.currentTimeMillis();
		logger.debug("starting " + host + "\t" + port + " in " + df.format((double) (end - begin) / 1000) + " s...");
		try {
			httpServer.start();
			Thread.currentThread().join();
			//logger.info("Press any key to stop the server...");
			//System.in.read();
			//System.exit(0);
		} catch (Exception e) {
			logger.error("error running " + host + ":" + port);
			logger.error(e);
		}

	}

	public static void main(String[] args) throws Exception {
		// java com.ml.test.net.HttpServerDemo
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		//PropertyConfigurator.configure(logConfig);

		Option hostOpt = OptionBuilder.withArgName("host").hasArg().withDescription("host name").withLongOpt("host").create("o");
		Option portOpt = OptionBuilder.withArgName("port").hasArg().withDescription("host port").withLongOpt("port").create("p");

		Options options = new Options();
		options.addOption("h", "help", false, "print this message");
		options.addOption("v", "version", false, "output version information and exit");

		options.addOption("a", "annotate", false, "start the text annotation server");
		options.addOption("s", "summary", false, "start the text summarization server");
		options.addOption("e", "enrich", false, "start the text enrichment server");
		options.addOption("l", "lang", false, "start the language recognition server");
		options.addOption("c", "compare", false, "start the text comparison server");
		//options.addOption("t", "topic", false, "start the text categorization server");
		options.addOption(OptionBuilder.withDescription("trace mode").withLongOpt("trace").create());
		options.addOption(OptionBuilder.withDescription("debug mode").withLongOpt("debug").create());
		options.addOption(OptionBuilder.withDescription("exclude the authentication").withLongOpt("no-authentication").create());

		options.addOption(hostOpt);
		options.addOption(portOpt);
		CommandLineParser parser = new PosixParser();
		CommandLine line = null;

		try {
			// parse the command line arguments
			line = parser.parse(options, args);

			Properties defaultProps = new Properties();
			try {
				defaultProps.load(new InputStreamReader(new FileInputStream(logConfig), "UTF-8"));
			} catch (Exception e) {
				defaultProps.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
				defaultProps.setProperty("log4j.appender.stdout.layout.ConversionPattern", "[%t] %-5p (%F:%L) - %m %n");
				defaultProps.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
				defaultProps.setProperty("log4j.appender.stdout.Encoding", "UTF-8");
			}

			if (line.hasOption("trace")) {
				defaultProps.setProperty("log4j.rootLogger", "trace,stdout");
			}
			else if (line.hasOption("debug")) {
				defaultProps.setProperty("log4j.rootLogger", "debug,stdout");
			}
			else {
				if (defaultProps.getProperty("log4j.rootLogger") == null) {
					defaultProps.setProperty("log4j.rootLogger", "info,stdout");
				}
			}
			PropertyConfigurator.configure(defaultProps);
			logger.trace(line);
			//String[] args = line.getArgs();
			// validate that block-size has been set
			//if (argv.length == 0 || line.hasOption("help"))
			if (line.hasOption("help")) {
				// print the help
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(200, "java com.machinelinking.server.MainHttpServer", "\n", options, "\n", true);
			}
			else if (line.hasOption("version")) {
				logger.info("version 1.0");
			}
			else {
				String host = line.getOptionValue("host");
				if (host == null) {
					host = DEFAULT_HOST;
				}
				String port = line.getOptionValue("port");
				if (port == null) {
					port = DEFAULT_PORT;
				}
				logger.info("****************************************************");
				logger.info("starting the service... " + new Date());
				logger.info("****************************************************");

				if (isRunning(host, port)) {
					logger.warn("server is already running... " + new Date());
				}
				else {
					logger.info("running server... " + new Date());
					new MainHttpServer(host, port, line);
				}

				//new MainHttpServer(host, port, line);
			}
		} catch (ParseException exp) {
			// oops, something went wrong
			System.err.println("Parsing failed: " + exp.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/jservice.jar com.machinelinking.server.MainHttpServer", "\n", options, "\n", true);
			System.exit(1);
		}
	}

	private static boolean isRunning(String host, String port) throws IOException {
		URL serverAddress = new URL(host + ":" + port + "/ping");
		logger.info("connecting to " + serverAddress + "...");

		// Set up the initial connection
		HttpURLConnection connection = (HttpURLConnection) serverAddress.openConnection();
		connection.setRequestMethod("POST");
		//connection.setRequestMethod("GET");
		//logger.debug(connection.getRequestMethod());
		connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
		connection.setDoOutput(true);
		//connection.setReadTimeout(10000);
		//connection.setConnectTimeout(10000);


		try {
			connection.getOutputStream().write("".getBytes(CharEncoding.UTF_8));
			connection.connect();
			logger.debug(connection);
			// read the result from the server
			BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String fromServer = null;
			if ((fromServer = rd.readLine()) != null) {
				logger.debug("server answer: " + fromServer);
				if (fromServer.equals("OK")) {
					return true;
				}
			}
		} catch (ConnectException e) {
			logger.error(e);
		}

		return false;
	}
}