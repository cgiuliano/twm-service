/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.Date;

import org.apache.commons.lang.CharEncoding;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.machinelinking.jservice.annotation.key.LanguageNotSupportedException;
import eu.fbk.twm.utils.analysis.Tokenizer;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 1/2/13 ServiceTime: 4:04 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractHttpHandler extends HttpHandler {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>AbstractHttpHandler</code>.
	 */
	static Logger logger = Logger.getLogger(AbstractHttpHandler.class.getName());

	// protected static final double NANO = Math.pow(10, 9);

	protected LogManagement logManagement;

	protected static DecimalFormat nf = new DecimalFormat("000,000,000.#");

	protected static DecimalFormat sf = new DecimalFormat("###,###,##0.#########");

	protected static final String LANG_CMD = "lang";

	protected static final String ANNOTATE_CMD = "annotate";

	protected static final String SUMMARY_CMD = "summary";

	protected static final String COMPARE_CMD = "compare";

	protected static final String PAGE_ENHANCH_CMD = "enrich";

	protected static final String DISAMBIGUATION_CMD = "disambiguation";

	protected static final String ENHANCEMENT_CMD = "enhancement";

	protected static final String HELP_CMD = "help";

	/*
	public static final int TEXT_NOT_SPECIFIED = 1;

	public static final int TEXT1_NOT_SPECIFIED = 2;

	public static final int TEXT2_NOT_SPECIFIED = 3;

	public static final int TEXT1_AND_TEXT2_NOT_SPECIFIED = 4;

	public static final int LANGUAGE_NOT_SUPPORTED = 5;

	public static final int PAGE_NOT_SPECIFIED = 6;

	public static final int LANG_NOT_SPECIFIED = 7;

	public static final String[] messages = {	"Access authorized",
												"text not specified",
												"text1 not specified",
												"text2 not specified",
												"text1 and text2 not specified",
												"Language not supported",
												"Page not specified",
												"Language not specified" };
	*/ AccessManagement accessManagement;

	Tokenizer tokenizer;

	protected AbstractHttpHandler() {
		logger.debug("creating a http handler...");
		accessManagement = AccessManagement.getInstance();
		tokenizer = HardTokenizer.getInstance();
	}

	@Override
	public final void service(Request request, Response response) throws UnsupportedEncodingException, IOException {
		MLResponse mlResponse = new MLResponse();
		response.setContentType("text/plain");
		response.setCharacterEncoding(CharEncoding.UTF_8);
		String result = null;
		MLRequest mlRequest = new MLRequest(request);
		logger.trace(mlRequest);
		//logger.debug("request (" + new Date() + "):\n" + mlRequest);

		//mlRequest.get
		String callback = mlRequest.getParameter("callback");
		logger.debug(callback);
		try {
			service(mlRequest, mlResponse);
			result = mlResponse.getResult();
			logger.debug(result);
			if (result == null) {
				logger.error("The result is null");
				throw new RuntimeException("Unexpected null response!");
			}
		} catch (MissingRequiredParameterException missingParam) {
			result = errorToJSon(MissingRequiredParameterException.ERROR_CODE, "Missing required parameter: " + missingParam.parameterName);
			missingParam.printStackTrace();
			logger.error(missingParam);
			logger.error(mlRequest);
			response.setStatus(400);
		} catch (LanguageNotSupportedException unsupxxx) {
			result = errorToJSon(LanguageNotSupportedException.ERROR_CODE, "LanguageNotSupported");
			logger.error(unsupxxx);
			logger.error(mlRequest);
			response.setStatus(400);
		} catch (AuthAppIdAppKeyWrongException authxxx) {
			result = errorToJSon(AuthAppIdAppKeyWrongException.ERROR_CODE, "Unauthorized: invalid app_id and app_key");
			logger.error(authxxx);
			logger.error(mlRequest);
			response.setStatus(401); // http Unauthorized
		} catch (AuthHitVolumeExceededException authxxx) {
			result = errorToJSon(AuthHitVolumeExceededException.ERROR_CODE, "Unauthorized: hit/volume exceeded!");
			logger.error(authxxx);
			logger.error(mlRequest);
			response.setStatus(401); // http Unauthorized
		} catch (AuthServiceException authxxx) {
			result = errorToJSon(AuthServiceException.ERROR_CODE, "Unauthorized: error contacting authentication service: " + authxxx.getApiException().getErrorCode());
			logger.error(authxxx);
			logger.error(mlRequest);
			response.setStatus(401); // http Unauthorized
		} catch (AuthTextLengthExceededException authxxx) {
			result = errorToJSon(AuthTextLengthExceededException.ERROR_CODE, "Unauthorized: input text too long " + authxxx.getMessage());
			logger.error(authxxx);
			logger.error(mlRequest);
			response.setStatus(401); // http Unauthorized

		} catch (RuntimeException exxx) {
			// This is the case for a unexpected code exception, really a server
			// error!
			// We return a 500 http code!
			response.setStatus(500);
			logger.error("Runtime server error", exxx);
			logger.error("Date "+ new Date());
			logger.error("Request "+ mlRequest.toString());
			result = errorToJSon(50000, exxx.getClass().getName() + ": " + exxx.getMessage());
		}
		// If there is an exception here, we simply throw it, we can do anything
		// else!
		OutputStream os = null;
		if (callback == null) {
			os = response.getOutputStream();
			os.write(result.getBytes(CharEncoding.UTF_8));
		}
		else {
			response.setStatus(200);
			os = response.getOutputStream();

			os.write((callback + "(" + result + ")").getBytes(CharEncoding.UTF_8));
		}
		os.flush();

	}

	@Override
	public void destroy() {
		// TODO: accessManagement.flushAccessManagement();\
		logManagement.close();
	}

	protected abstract void service(MLRequest request, MLResponse response) throws MissingRequiredParameterException, LanguageNotSupportedException, AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException, AuthTextLengthExceededException;

	private static String errorToJSon(int code, String msg) throws IOException {
		StringWriter w = new StringWriter();

		JsonFactory f = new JsonFactory();
		JsonGenerator g = f.createJsonGenerator(w);
		g.writeStartObject();
		g.writeObjectFieldStart("error");
		g.writeNumberField("code", code);
		g.writeStringField("msg", msg);
		g.writeEndObject();
		g.writeEndObject();
		g.close();

		return w.toString();
	}

}
