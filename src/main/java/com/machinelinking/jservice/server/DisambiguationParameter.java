package com.machinelinking.jservice.server;


/**
 * This class parses the request object and extract the parameters for the
 * Disambiguation function.
 *
 * @author d@vide.bz
 */
public class DisambiguationParameter extends AnnotateParameter {
	private String left;

	private String term;

	private String right;

	public DisambiguationParameter(MLRequest request) throws MissingRequiredParameterException {
		super(request);
		left = request.getParameter("left");
		term = request.getParameter("term");
		right = request.getParameter("right");
	}

	public String getLeft() {
		return left;
	}

	public String getTerm() {
		return term;
	}

	public String getRight() {
		return right;
	}
}
