package com.machinelinking.jservice.server;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.CharacterTable;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 6/28/13
 * Time: 3:44 PM
 * To change this template use File | Settings | File Templates.
 */
class LabelBuilder {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>LabelBuilder</code>.
	 */
	static Logger logger = Logger.getLogger(LabelBuilder.class.getName());

	public final static String START_SUFFIX_PATTERN = "_(";

	public final static char END_SUFFIX_PATTERN = ')';


	public static String build(String label) {
		if (label.contains("_")) {
			//return label.replace(CharacterTable.LOW_LINE, CharacterTable.SPACE);
			String form;//, suffix;
			int b = label.lastIndexOf(START_SUFFIX_PATTERN);
			if (b == -1) {
				// no suffix
				form = label.replace(CharacterTable.LOW_LINE, CharacterTable.SPACE);
			}
			else {
				int e = label.length() - 1;
				if (label.charAt(e) == END_SUFFIX_PATTERN) {

					//suffix = label.substring(b + 2, e);
					form = label.substring(0, b).replace(CharacterTable.LOW_LINE, CharacterTable.SPACE);
				}
				else {
					form = label.replace(CharacterTable.LOW_LINE, CharacterTable.SPACE);

				}
			}
			return form;

		}

		String[] s = StringUtils.splitByCharacterTypeCamelCase(label);
		StringBuilder sb = new StringBuilder();
		if (s.length > 0) {
			sb.append(s[0]);
		}
		for (int i = 1; i < s.length; i++) {
			sb.append(CharacterTable.SPACE);
			sb.append(s[i]);
		}
		return sb.toString();
	}
}
