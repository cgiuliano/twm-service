/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/20/13
 * Time: 8:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class AuthTextLengthExceededException extends Exception {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AuthTextLengthExceededException</code>.
	 */
	static Logger logger = Logger.getLogger(AuthTextLengthExceededException.class.getName());

	public static final int ERROR_CODE = 40103;

	private int length;
	private int maxLength;

	public AuthTextLengthExceededException(int length, int maxLength) {
		this.length = length;
		this.maxLength = maxLength;
	}

	public int getLength() {
		return length;
	}

	@Override
	public String getMessage() {
		return "The text exceeds the maximum length (" + length + ">" + maxLength + ")";
	}
}
