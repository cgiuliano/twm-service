/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.server;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 1/6/13 Time: 9:36 AM To
 * change this template use File | Settings | File Templates.
 */
class HelpParameter extends AuthenticationParameter {

	boolean includeLanguages;

	HelpParameter(MLRequest request) throws MissingRequiredParameterException {
		super(request);

		String s = request.getParameter("supported_languages");
		logger.debug(s);
		includeLanguages = s != null && s.equals(TRUE);
//		includeLanguages = true;
	}

	public boolean includesLanguages() {
		return includeLanguages;
	}

}
