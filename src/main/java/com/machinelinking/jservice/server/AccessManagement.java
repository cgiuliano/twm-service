/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */
package com.machinelinking.jservice.server;

import java.text.DecimalFormat;
import java.util.HashMap;

import com.machinelinking.jservice.util.ConfigurationReaderException;
import net.threescale.api.ApiFactory;
import net.threescale.api.v2.*;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.machinelinking.jservice.util.ConfigurationReader;

/**
 * Created with IntelliJ IDEA. User: giuliano Date: 12/28/12 ServiceTime: 9:23
 * PM To change this template use File | Settings | File Templates.
 */
public class AccessManagement {
	/**
	 * Define a static logger variable so that it references the Logger instance
	 * named <code>AccessManagement</code>.
	 */
	static Logger logger = Logger.getLogger(AccessManagement.class.getName());

	String providerKey;

	Configuration config;

	private static AccessManagement ourInstance = null;

	private final Api2Impl server;

	private static DecimalFormat df = new DecimalFormat("000,000,000.#");

	private void setThreescaleLogger() {
		java.util.logging.Level level = null;
		Level log4jLevel = logger.getLevel();
		//if (log4jLevel == Level.INFO) {
			level = java.util.logging.Level.SEVERE;
		//}
		//todo: check if we can make it at package level
		java.util.logging.Logger loggerApi2Impl = java.util.logging.Logger.getLogger(Api2Impl.class.getName());
		loggerApi2Impl.setLevel(level);
		java.util.logging.Logger loggerHttpSenderImpl = java.util.logging.Logger.getLogger(HttpSenderImpl.class.getName());
		loggerHttpSenderImpl.setLevel(level);
		java.util.logging.Logger loggerAuthorizeResponse = java.util.logging.Logger.getLogger(AuthorizeResponse.class.getName());
		loggerAuthorizeResponse.setLevel(level);

	}
	private AccessManagement() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		setThreescaleLogger();

		try {
			config = configurationReader.getConfiguration();
			providerKey = config.getString("threescale-providerKey");
			logger.info("accessing threescale using " + providerKey + "...");
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
		//without cache!
		//server = (Api2Impl) ApiFactory.createV2Api(providerKey);
		//with the default cache!
		// https://support.3scale.net/forum/topics/createv2apiwithlocalcache-and-server-update
		server = (Api2Impl) MLThreescaleApiFactory.createV2ApiWithLocalCache(ApiFactory.DEFAULT_3SCALE_PROVIDER_API_URL,/*"http://localhost:81"*/
						providerKey);

		logger.debug("createV2ApiWithLocalCache");
	}

	public static synchronized AccessManagement getInstance() {
		if (ourInstance == null) {
			ourInstance = new AccessManagement();
		}
		return ourInstance;
	}

	public void authorize(ServiceTime serviceTime, AuthenticationParameter authenticationParameter, String command, int volume) throws AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException {
		//logger.info("authentication...");
		serviceTime.authenticationBegin();

		authorize(authenticationParameter.getAppId(), authenticationParameter.getAppKey(), command, volume);

		serviceTime.authenticationEnd();
		logger.debug("authentication done in " + df.format(serviceTime.authenticationNanoTime()) + " ns");

	}

	private void authorize(String appId, String appKey, String command, int volume) throws AuthAppIdAppKeyWrongException, AuthServiceException, AuthHitVolumeExceededException {
		final AuthorizeResponse response;
		try {
			response = server.authorize(appId, appKey, null);
		} catch (ApiException apiException) // Fired in case app_id is wrong but not if app_key is wrong
		{
			if (apiException.getErrorCode().equals("application_not_found")) {
				throw new AuthAppIdAppKeyWrongException();
			}
			throw new AuthServiceException(apiException);
		}
		boolean auth = response.getAuthorized();
		// if app_key is wrong
		if (!auth) {
			String reason = response.getReason();
			if (reason.equals("usage limits are exceeded")) {
				throw new AuthHitVolumeExceededException();
			}
			throw new AuthAppIdAppKeyWrongException();
		}
		HashMap<String, String> hits = new HashMap<String, String>();
		//todo: check if hits counts 2 times
		hits.put("hits", "1");
		hits.put(command, "1");
		hits.put("volume", String.valueOf(volume).toString());
		//logger.debug(hits);
		try {
			//todo: can be done in a new thread?
			//logger.debug("befor report ------");
			ApiTransaction[] transactions = {new ApiTransactionForAppId(appId, hits)};
			server.report(transactions);
			//logger.debug("after report ------");
		} catch (ApiException apiException) {
			throw new AuthServiceException(apiException);
		}
	}
}
