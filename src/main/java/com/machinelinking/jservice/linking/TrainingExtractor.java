/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.linking;

import org.apache.commons.cli.*;
import org.apache.log4j.Logger;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Pattern;

import org.apache.log4j.PropertyConfigurator;
import eu.fbk.utils.lsa.LSM;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.math.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/14/13
 * Time: 9:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class TrainingExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TrainingExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(TrainingExtractor.class.getName());

	static Pattern tabPattern = Pattern.compile("\t");

	static Pattern spacePattern = Pattern.compile(" ");

	static final int PAGE = 1;

	static final int ID = 2;

	static final int LEFT = 3;

	static final int FORM = 4;

	static final int RIGHT = 5;

	static final int COLUMN_NUMBER = 6;

	static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private BlockingQueue<Runnable> queue;

	private ExecutorService executor;

	public static final int DEFAULT_LSM_DIM = 100;

	public static final int DEFAULT_NUMBER_OF_THREAD = 1;

	public static final int DEFAULT_QUEUE_SIZE = 1000;

	public static final String TAB = "\t";

	PrintWriter pw;

	int numThreads;

	int queueSize;

	LSM lsm;

	public TrainingExtractor(LSM lsm, String outputFileName, int numThreads, int queueSize) throws IOException {
		this(lsm, new File(outputFileName), numThreads, queueSize);
	}

	public TrainingExtractor(LSM lsm, File outputFile, int numThreads, int queueSize) throws IOException {
		logger.info(lsm.getDimension() + " dim");
		logger.info(numThreads + " threads");
		logger.info("queue size " + queueSize);

		this.lsm = lsm;
		this.numThreads = numThreads;
		this.queueSize = queueSize;

		pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8")));
		queue = new ArrayBlockingQueue<Runnable>(queueSize, true);
		executor = Executors.newFixedThreadPool(numThreads);

		Thread t = new Thread(new Runnable() {
			public void run() {
				for (; ; ) {
					try {
						logger.debug("waiting to execute (" + queue.size() + ")");
						executor.execute(queue.take());
					} catch (InterruptedException e) {
						logger.error(e);
					}
				}
			}
		});
		t.setDaemon(true);
		t.start();
	}

	void createLSM(String indexName, int dim, boolean rescaleIdf) throws IOException {
		// cross language model files
		File fileUt = new File(indexName + "X-Ut");
		File fileSk = new File(indexName + "X-S");
		File fileR = new File(indexName + "X-row");
		File fileC = new File(indexName + "X-col");
		File fileDf = new File(indexName + "X-df");
		lsm = new LSM(fileUt, fileSk, fileR, fileC, fileDf, dim, rescaleIdf);
	}

	public int getNumThreads() {
		return numThreads;
	}

	public int getQueueSize() {
		return queueSize;
	}

	public void train(String name) throws IOException {
		train(new File(name));
	}

	public void train(File in) throws IOException {
		logger.info("reading " + in + "...");

		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		String line;
		int count = 0, part = 0, tot = 0;
		String previousForm = "";
		//Map<String, BOW> map = new HashMap<String, BOW>();
		String[] t = null;
		List<String[]> list = new ArrayList<String[]>();
		logger.info("tot\tcount\ttime\tdate");
		// read the first line
		if ((line = lnr.readLine()) != null) {
			try {
				t = tabPattern.split(line);
				if (t.length == COLUMN_NUMBER) {
					list.add(t);
					previousForm = t[FORM];
					//logger.info(part + "\t\"" + t[3] + "\"");
					part++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + count);
				logger.error(e);
			} finally {
				tot++;
			}
		} // end if

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			try {
				t = tabPattern.split(line);
				if (t.length == COLUMN_NUMBER) {
					if (!t[FORM].equals(previousForm)) {
						logger.debug("putting " + previousForm + " (" + list.size() + ")...");
						queue.put(new Training(list, previousForm));
						list = new ArrayList<String[]>();
						count++;
						part = 0;
					}
					list.add(t);
					previousForm = t[FORM];
					part++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + tot);
				logger.error(e);
			} finally {
				tot++;
			}

			//if (count > 500) break;

			if ((tot % 1000) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		} // end while

		// add the last line
		list.add(t);
		try {
			logger.debug("putting " + previousForm + " (" + list.size() + ")...");
			queue.put(new Training(list, previousForm));
		} catch (InterruptedException e) {
			logger.error(e);
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());


		executor.shutdown();
		try {
			logger.info("waiting for execution...");
			executor.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			logger.error(e);
		}
		logger.info("closing the streams...");
		lnr.close();
		pw.flush();
		pw.close();
		logger.info("done it");

	} // end read


	synchronized void write(Map<String, BOW> map, String form) {
		logger.debug(Thread.currentThread().getName() + " is writing " + form + "...");
		Iterator<String> it = map.keySet().iterator();
		String label;
		BOW bow;
		for (int i = 0; it.hasNext(); i++) {
			label = it.next();
			bow = map.get(label);
			pw.print(form);
			pw.print(TAB);
			pw.print(label);
			pw.print(TAB);
			pw.println(bow.toSingleLine());

		}
		pw.flush();
	}

	synchronized void write(Set<String> set) {
		//logger.debug(Thread.currentThread().getName() + " is writing " +  form + "...");
		Iterator<String> it = set.iterator();
		String line;
		for (int i = 0; it.hasNext(); i++) {
			line = it.next();
			pw.println(line);
		}
		pw.flush();
	}

	class Training extends Thread implements Runnable {
		List<String[]> list;

		String form;

		Training(List<String[]> list, String form) {
			this.form = form;
			this.list = list;
		}

		public void run() {
			logger.debug(Thread.currentThread().getName() + " is training " + form + "...");
			Map<String, Example> map = createBowMap();
			Set<String> set = createVectorSet(map);
			write(set);
		}

		class Example {
			BOW bow;

			int freq;

			Example() {
				bow = new BOW();
				freq = 0;
			}

			void inc() {
				freq++;
			}

			void add(String[] tokens) {
				bow.addAll(tokens);
			}

			public BOW getBow() {
				return bow;
			}

			public int getFreq() {
				return freq;
			}
		}

		Map<String, Example> createBowMap() {
			Map<String, Example> map = new HashMap<String, Example>();
			String[] line;
			for (int i = 0; i < list.size(); i++) {
				line = list.get(i);
				//logger.debug(Arrays.toString(line));
				//logger.debug(line[PAGE]);
				//logger.debug(line[ID]);
				//logger.debug(line[LEFT]);
				//logger.debug(line[FORM]);
				//logger.debug(line[RIGHT]);
				//BOW bow = map.get(line[PAGE]);
				Example example = map.get(line[PAGE]);
				if (example == null) {
					//bow = new BOW();
					example = new Example();
					//map.put(line[PAGE], bow);
					map.put(line[PAGE], example);
				}
				example.inc();
				example.add(spacePattern.split(line[LEFT].toLowerCase()));
				example.add(spacePattern.split(line[RIGHT].toLowerCase()));
				//bow.addAll(spacePattern.split(line[LEFT].toLowerCase()));
				//bow.addAll(spacePattern.split(line[RIGHT].toLowerCase()));
			}
			return map;
		}

		Set<String> createVectorSet(Map<String, Example> map) {
			Set<String> set = new HashSet<String>();
			Iterator<String> it = map.keySet().iterator();
			StringBuilder sb;
			String label;
			BOW bow;
			Example example;
			for (int i = 0; it.hasNext(); i++) {
				label = it.next();
				//bow = map.get(label);
				example = map.get(label);
				//Vector vec = lsm.mapDocument(bow);
				bow = example.getBow();
				Vector vec = lsm.mapDocument(bow);
				vec.normalize();

				Vector pseudoVec = lsm.mapPseudoDocument(vec);
				pseudoVec.normalize();
				sb = new StringBuilder();
				sb.append(form);
				sb.append(TAB);
				sb.append(example.getFreq());
				sb.append(TAB);
				sb.append(label);
				sb.append(TAB);
				sb.append(bow.toSortedLine());
				sb.append(TAB);
				sb.append(pseudoVec.toString());
				sb.append(TAB);
				sb.append(vec.toString(lsm.getDimension()));
				set.add(sb.toString());
			}
			return set;
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// java com.ml.test.net.HttpServerDemo
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option inputFileNameOpt = OptionBuilder.withArgName("input").hasArg().withDescription("input file").isRequired().withLongOpt("input").create("i");
			Option outputFileNameOpt = OptionBuilder.withArgName("output").hasArg().withDescription("output file").isRequired().withLongOpt("output").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("nt").hasArg().withDescription("number of threads").withLongOpt("nt").create("nGramLength");
			Option queueSizeOpt = OptionBuilder.withArgName("qs").hasArg().withDescription("queue size").withLongOpt("qs").create("q");
			Option lsmDirOpt = OptionBuilder.withArgName("lsm").hasArg().withDescription("lsm dir").isRequired().withLongOpt("lsm").create("l");
			Option lsmDimOpt = OptionBuilder.withArgName("dim").hasArg().withDescription("lsm dim").withLongOpt("dim").create("d");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(inputFileNameOpt);
			options.addOption(outputFileNameOpt);
			options.addOption(numThreadOpt);
			options.addOption(queueSizeOpt);
			options.addOption(lsmDirOpt);
			options.addOption(lsmDimOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			logger.debug(options);

			int numThread = DEFAULT_NUMBER_OF_THREAD;
			if (line.hasOption("nt")) {
				numThread = Integer.parseInt(line.getOptionValue("nt"));
			}

			int queueSize = DEFAULT_QUEUE_SIZE;
			if (line.hasOption("qs")) {
				queueSize = Integer.parseInt(line.getOptionValue("qs"));
			}
			logger.debug(line.getOptionValue("nt") + "\t" + line.getOptionValue("qs"));
			logger.debug(line.getOptionValue("output") + "\t" + line.getOptionValue("input") + "\t" + line.getOptionValue("lsm"));

			String lsmDirName = line.getOptionValue("lsm");
			if (!lsmDirName.endsWith(File.separator)) {
				lsmDirName += File.separator;
			}

			File fileUt = new File(lsmDirName + "X-Ut");
			File fileSk = new File(lsmDirName + "X-S");
			File fileR = new File(lsmDirName + "X-row");
			File fileC = new File(lsmDirName + "X-col");
			File fileDf = new File(lsmDirName + "X-df");
			int dim = DEFAULT_LSM_DIM;
			if (line.hasOption("dim")) {
				dim = Integer.parseInt(line.getOptionValue("dim"));
			}
			logger.debug(line.getOptionValue("lsm") + "\t" + line.getOptionValue("dim"));

			LSM lsm = new LSM(fileUt, fileSk, fileR, fileC, fileDf, dim, true);

			TrainingExtractor trainingExtractor = new TrainingExtractor(lsm, line.getOptionValue("output"), numThread, queueSize);
			trainingExtractor.train(line.getOptionValue("input"));


		} catch (ParseException e) {
			// oops, something went wrong
			System.err.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/jservice.jar com.machinelinking.linking.TrainingExtractor", "\n", options, "\n", true);
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
