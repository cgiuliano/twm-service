/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.linking;

import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/15/13
 * Time: 3:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class TrainingIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TrainingIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(TrainingIndexer.class.getName());

	private IndexWriter indexWriter;

	private static Pattern tabPattern = Pattern.compile("\t");

	private static Pattern spacePattern = Pattern.compile(" ");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	static final int PAGE = 2;

	static final int FREQ = 1;

	static final int FORM = 0;

	static final int BOW_TEXT = 3;

	static final int LS_VECTOR = 4;

	static final int BOW_VECTOR = 5;

	/**
	 * Default constructor.
	 */
	public TrainingIndexer(String dir, String in) throws IOException {
		indexWriter = new IndexWriter(FSDirectory.open(new File(dir)), new WhitespaceAnalyzer(), IndexWriter.MaxFieldLength.LIMITED);
		read(new File(in));

		try {
			logger.info("optimizing and closing...");
			indexWriter.optimize();
			indexWriter.close();
		} catch (CorruptIndexException e) {
			logger.error(e);
		}

	}

	private void read(File in) throws IOException {
		logger.info("reading " + in + "...");

		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		logger.info("tot\tcount\ttime\tdate");
		String line;
		int count = 0, tot = 0;
		String[] t;
		// read the file
		while ((line = lnr.readLine()) != null) {
			try {
				t = tabPattern.split(line);
				if (t.length == 3) {
					addDocuments(t);
					count++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + tot);
				logger.error(e);
			} finally {
				tot++;
			}

			//if (tot > 1000)
			//	break;

			if ((tot % 10000) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());


		lnr.close();
	}

	private void addDocuments(String[] t) throws IOException {
		//logger.info(t[0]);
		Document doc = new Document();
		doc.add(new Field("form", t[FORM], Field.Store.YES, Field.Index.NOT_ANALYZED));
		doc.add(new Field("freq", intToByteArray(Integer.parseInt(t[FREQ])), Field.Store.YES));
		doc.add(new Field("page", t[PAGE], Field.Store.YES, Field.Index.NOT_ANALYZED));

		doc.add(new Field("bow", toByte(t[BOW_VECTOR]), Field.Store.YES));
		doc.add(new Field("ls", toByte(t[LS_VECTOR]), Field.Store.YES));
		indexWriter.addDocument(doc);

	}

	//
	public static final byte[] intToByteArray(int value) {
		return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value};
	}


	private byte[] toByte(String str) throws IOException {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(1024);

		//DataOutputStream dataStream = new DataOutputStream(new OutputStreamWriter(byteStream));
		DataOutputStream dataStream = new DataOutputStream(byteStream);
		StringTokenizer st = new StringTokenizer(str, " :");
		int m = st.countTokens() / 2;
		dataStream.writeInt(m);
		for (int j = 0; j < m; j++) {
			dataStream.writeInt(Integer.parseInt(st.nextToken()));
			dataStream.writeDouble(Double.valueOf(st.nextToken()));
		}

		return byteStream.toByteArray();
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 2) {
			System.err.println("Wrong number of parameters " + args.length);
			System.err.println("Usage: java com.machinelinking.linking.TrainingIndexer in-page-vec-csv out-lucene-index-dir");
			System.exit(-1);
		}

		Options options = new Options();
		try {
			Option inputFileNameOpt = OptionBuilder.withArgName("input").hasArg().withDescription("training file").isRequired().withLongOpt("input").create("i");
			Option outputFileNameOpt = OptionBuilder.withArgName("output").hasArg().withDescription("lucene dir").isRequired().withLongOpt("output").create("o");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(inputFileNameOpt);
			options.addOption(outputFileNameOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			new TrainingIndexer(line.getOptionValue("input"), line.getOptionValue("output"));
		} catch (ParseException e) {
			// oops, something went wrong
			System.err.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/jservice.jar com.machinelinking.linking.TrainingExtractor", "\n", options, "\n", true);
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
