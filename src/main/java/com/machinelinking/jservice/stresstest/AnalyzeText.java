package com.machinelinking.jservice.stresstest;

import org.apache.commons.lang.CharEncoding;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/21/13
 * Time: 7:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class AnalyzeText implements Runnable {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AnalyzeText</code>.
	 */
	static Logger logger = Logger.getLogger(AnalyzeText.class.getName());

	private String text;
	private String title;
	private RequestBuilder requestBuilder;
	private PrintWriter outputWriter;
	private String id;

	public AnalyzeText(String text, String title, String id, RequestBuilder requestBuilder, PrintWriter outputWriter) {
		this.id = id;
		this.text = text;
		this.title = title;
		this.requestBuilder = requestBuilder;
		this.outputWriter = outputWriter;
	}

	public void run() {
		//logger.debug("running thread " + Thread.currentThread().getName() + "...");

		try {
			long begin = System.currentTimeMillis();
			String wholeText = title + "\n" + text;
			URL serverAddress = requestBuilder.getServerAddress();
			String serverCall = requestBuilder.getServerCall(wholeText);
			//logger.debug(serverAddress + serverCall);
			String answer = call(serverAddress, serverCall);
			long end = System.currentTimeMillis();
			long time = end - begin;
			logger.debug(id + "\t" + title + "\t" + wholeText.length() + "\t" + time);
			//logger.debug("\n" + answer);
			StringBuilder sb = new StringBuilder();
			sb.append("<file id=\"");
			sb.append(id);
			sb.append("\">\n<head>\n<title>");
			sb.append(title);
			sb.append("<title>\n</head>\n<content length=");
			sb.append(text.length());
			sb.append("\">\n");
			sb.append(text);
			sb.append("\n</content>\n<annotation time=\"");
			sb.append(time);
			sb.append("\">\n");
			sb.append(answer);
			sb.append("</annotation>\n</file>");
			synchronized (this) {
				outputWriter.println(sb.toString());
			}

		} catch (MalformedURLException e) {
			logger.error(e);
		} catch (ProtocolException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	private String call(URL serverAddress, String serverCall) throws ProtocolException, IOException {
		String fromServer = null;

		// Set up the initial connection
		HttpURLConnection connection = (HttpURLConnection) serverAddress.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
		connection.setDoOutput(true);
		//connection.setReadTimeout(10000);
		//connection.setConnectTimeout(10000);
		connection.getOutputStream().write(serverCall.getBytes(CharEncoding.UTF_8));
		connection.connect();
		//logger.debug(serverCall);

		// read the result from the server
		BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuilder sb = new StringBuilder();
		while ((fromServer = rd.readLine()) != null) {
			sb.append(fromServer + '\n');
		}
		return sb.toString();
	}
}
