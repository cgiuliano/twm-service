package com.machinelinking.jservice.stresstest;

import com.machinelinking.jservice.server.MainHttpServer;
import eu.fbk.twm.utils.Defaults;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xml.sax.*;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/21/13
 * Time: 7:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class EuronewsReader extends DefaultHandler implements LexicalHandler {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>EuronewsReader</code>.
	 */
	static Logger logger = Logger.getLogger(EuronewsReader.class.getName());


	protected static final String LEXICAL_HANDLER_PROPERTY_ID = "http://xml.org/sax/properties/lexical-handler";
	protected static final String DEFAULT_PARSER_NAME = "org.apache.xerces.parsers.SAXParser";

	// Variables
	protected int fElementDepth;
	protected boolean fXML11;
	protected boolean fInCDATA;
	protected long begin, end;
	public AtomicLong genBegin = new AtomicLong(), genEnd = new AtomicLong();
	protected DecimalFormat decimalFormat;
	protected String xpath;

	public AtomicInteger generalCount = new AtomicInteger();

	// Wikipedia variables
	private String currentTitle = "";
	private String currentID = "";
	private String currentText = "";
	protected StringBuilder content = new StringBuilder();


	public final static int DEFAULT_THREADS_NUMBER = 1;

	// Multithread stuff
	private int numThreads;
	private int numFiles;
	private int notificationPoint;
	private int fileCount;

	private ExecutorService myExecutor;

	public final static int DEFAULT_QUEUE_SIZE = 10000;

	private RequestBuilder requestBuilder;
	
	private PrintWriter outputWriter;
	
	public EuronewsReader(int numThreads, int numFiles, int notificationPoint, RequestBuilder requestBuilder) {
		this.numThreads = numThreads;
		this.numFiles = numFiles;
		this.notificationPoint = notificationPoint;
		this.requestBuilder = requestBuilder;
		fileCount = 0;
		logger.info("creating the thread executor (" + numThreads + ")");
		int blockQueueSize = DEFAULT_QUEUE_SIZE;
		BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(blockQueueSize);
		RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();
		myExecutor = new ThreadPoolExecutor(numThreads, numThreads, 1, TimeUnit.MINUTES, blockingQueue, rejectedExecutionHandler);
	}

	public int getNumThreads() {
		return numThreads;
	}

	public int getNotificationPoint() {
		return notificationPoint;
	}

	public int getnumFiles() {
		return numFiles;
	}

	/**
	 * Receive notification of the begin of a process.
	 */
	public void startProcess(String xin, String xout) {
		//this.wikipediaFileName = wikipediaFileName;
		XMLReader parser;

		try {
			outputWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(xout), "UTF-8")));
			outputWriter.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			outputWriter.println("<xml>");
		} catch (IOException e) {
			logger.error(e);
		}

		xpath = "";
		decimalFormat = new DecimalFormat("###,###,###,###");

		try {
			parser = XMLReaderFactory.createXMLReader(DEFAULT_PARSER_NAME);

			// set parser
			parser.setContentHandler(this);
			parser.setErrorHandler(this);
			parser.setProperty(LEXICAL_HANDLER_PROPERTY_ID, this);
			//todo: handle different compressed input streams
			parser.parse(xin);
		} catch (SAXParseException e) {
			logger.error("SAXParseException at " + currentTitle + " " + e);
		} catch (Exception e) {
			logger.error("Error at " + currentTitle + " " + e.getMessage());
			logger.error(e);
		}
	}

	public void printSituation() {
		String s = String.format("File: %10s - %7s ms - %s", decimalFormat.format(generalCount.intValue()), decimalFormat.format(end - begin), new Date());
		logger.info(s);
	}

	public void printLog() {

	}

	public void endProcess() {
		outputWriter.println("</xml>");
		outputWriter.close();
		System.exit(0);
	}

	public void startDocument() throws SAXException {
		logger.info("Process started at " + new Date());
		begin = System.currentTimeMillis();
		genBegin.set(System.currentTimeMillis());
		fElementDepth = 0;
		fXML11 = false;
		fInCDATA = false;
	}

	public void endDocument() throws SAXException {
		end = System.currentTimeMillis();

		//printSituation();
		logger.info("Finished to read the document " + new Date());

		boolean b = shutdown();

		logger.info("ending process " + b + " " + new Date() + "...");
		endProcess();
	}

	boolean shutdown() {
		boolean b = true;
		try {
			myExecutor.shutdown();
			logger.debug("waiting to end " + new Date() + "...");
			b = myExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			logger.error(e);
		}
		return b;
	}
	/**
	 * Characters.
	 */
	public void characters(char ch[], int start, int length) throws SAXException {
		try {
			content.append(ch, start, length);
		} catch (Exception e) {
			logger.error("Error at characters " + currentTitle);
		}
	}

	public void ignorableWhitespace(char ch[], int start, int length) throws SAXException {
		try {
			characters(ch, start, length);
		} catch (Exception e) {
			logger.error("Error at ignorableWhitespace " + currentTitle);
		}
	}

	//todo: replace with constants the wiki paths
	public void startElement(String uri, String local, String raw, Attributes attrs) throws SAXException {
		xpath += "/" + raw;
		fElementDepth++;

		content.setLength(0);
		//logger.debug(xpath);
		if (xpath.equals("/xml/file")) {
			currentID = attrs.getValue("id");

			if (fileCount >= numFiles) {
				endDocument();
			}
			fileCount++;
			//logger.info("id: " + currentID);
		}
	}

	public void endElement(String uri, String local, String raw) throws SAXException {
		fElementDepth--;
		if (xpath.equals("/xml/file/title")) {
			currentTitle = content.toString();
		}
		else if (xpath.equals("/xml/file/content")) {
			currentText = content.toString();
		}
		else if (xpath.equals("/xml/file")) {
			try {
				myExecutor.execute(new AnalyzeText(currentText, currentTitle, currentID, requestBuilder, outputWriter));
			} catch (Exception e) {
				logger.error(e);
			}
		}
		xpath = xpath.substring(0, xpath.length() - raw.length() - 1);

	}

	public void startDTD(String name, String publicId, String systemId) throws SAXException {
	}

	public void endDTD() throws SAXException {
	}

	public void startEntity(String name) throws SAXException {
	}

	public void endEntity(String name) throws SAXException {
	}

	public void startCDATA() throws SAXException {
	}

	public void endCDATA() throws SAXException {
	}

	public void comment(char ch[], int start, int length) throws SAXException {
	}

	public static void main(String[] args) throws Exception {
		// java com.ml.test.net.HttpServerDemo
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Option euronewsDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("Euronews xml file").isRequired().withLongOpt("euronews-dump").create("d");
		Option outputFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("file in which to store output").isRequired().withLongOpt("output-file").create();
		Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
		Option numFileOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of files to process (default all)").withLongOpt("num-files").create("f");
		Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");

		Option hostOpt = OptionBuilder.withArgName("string").hasArg().withDescription("host name (default is " + MainHttpServer.DEFAULT_HOST + ")").withLongOpt("host").create("o");
		Option portOpt = OptionBuilder.withArgName("int").hasArg().withDescription("port number (default is " + MainHttpServer.DEFAULT_PORT + ")").withLongOpt("port").create("p");
		Option methodOpt = OptionBuilder.withArgName("string").hasArg().withDescription("method request").withLongOpt("method").create("m");

		Option langOpt = OptionBuilder.withArgName("string").hasArg().withDescription("text language").withLongOpt("lang").create("l");
		Option includeTextOpt = OptionBuilder.withDescription("include text").withLongOpt("include-text").create();
		//Option includeSummaryOpt = OptionBuilder.withDescription("include summary").withLongOpt("include-summary").create();
		Option idOpt = OptionBuilder.withArgName("string").hasArg().withDescription("text id").withLongOpt("id").create();
		Option outputFormatOpt = OptionBuilder.withArgName("string").hasArg().withDescription("output format").withLongOpt("output-format").create();
		Option includeDisambiguationOpt = OptionBuilder.withDescription("include disambiguation").withLongOpt("disambiguation").create();
		Option includeFormOpt = OptionBuilder.withDescription("include forms").withLongOpt("form").create();
		Option nBestFilterOpt = OptionBuilder.withArgName("[0|1]").hasArg().withDescription("nbest filter").withLongOpt("nbest").create();
		Option includeImageOpt = OptionBuilder.withDescription("include images").withLongOpt("image").create();
		Option includeCrossOpt = OptionBuilder.withDescription("include cross language links").withLongOpt("cross").create();
		Option includeTypeOpt = OptionBuilder.withDescription("include types").withLongOpt("type").create();
		Option includeLinkOpt = OptionBuilder.withDescription("include links").withLongOpt("link").create();
		Option includeClassOpt = OptionBuilder.withDescription("include class").withLongOpt("class").create();
		Option includeCategoryOpt = OptionBuilder.withArgName("int").hasArg().withDescription("include category with specified depth").withLongOpt("category").create();
		Option includeTopicOpt = OptionBuilder.withDescription("include topic").withLongOpt("topic").create();
		Option includeAirpediaOpt = OptionBuilder.withDescription("include Airpedia class").withLongOpt("dbpedia").create();
		Option includeWikipediaAbstrctOpt = OptionBuilder.withDescription("include Wikipedia abstract").withLongOpt("abstract").create();
		Option appIdOpt = OptionBuilder.withArgName("string").hasArg().withDescription("application id (default is " + RequestBuilder.DEFAULT_APP_ID + ")").create("app_id");
		Option appKeyOpt = OptionBuilder.withArgName("string").hasArg().withDescription("application key (default is " + RequestBuilder.DEFAULT_APP_KEY + ")").create("app_key");
		Option minWeightOpt = OptionBuilder.withArgName("double").hasArg().withDescription("minimum keyword weight (default is " + RequestBuilder.DEFAULT_MINIMUM_WEIGHT + ")").withLongOpt("min-weight").create();
		Option compressionRatioOpt = OptionBuilder.withArgName("double").hasArg().withDescription("summary compression ratio (default is " + RequestBuilder.DEFAULT_COMPRESSION_RATIO + ")").withLongOpt("compression-ratio").create();
		Option summaryFuncOpt = OptionBuilder.withArgName("string").hasArg().withDescription("summary compression function (default is " + RequestBuilder.DEFAULT_SUMMARY_FUNC + ")").withLongOpt("summary-func").create();

		Options options = new Options();
		options.addOption("h", "help", false, "print this message");
		options.addOption("v", "version", false, "output version information and exit");

		options.addOption(euronewsDumpOpt);
		options.addOption(outputFileOpt);
		options.addOption(numThreadOpt);
		options.addOption(numFileOpt);
		options.addOption(notificationPointOpt);

		options.addOption(hostOpt);
		options.addOption(portOpt);
		options.addOption(methodOpt);

		options.addOption(langOpt);
		options.addOption(appIdOpt);
		options.addOption(appKeyOpt);
		options.addOption(includeTextOpt);
		options.addOption(includeCategoryOpt);
		options.addOption(includeDisambiguationOpt);
		options.addOption(includeFormOpt);
		options.addOption(includeImageOpt);
		options.addOption(includeCrossOpt);
		options.addOption(includeTypeOpt);
		options.addOption(includeLinkOpt);
		options.addOption(idOpt);
		options.addOption(includeAirpediaOpt);
		options.addOption(minWeightOpt);
		options.addOption(includeWikipediaAbstrctOpt);
		options.addOption(includeClassOpt);
		options.addOption(compressionRatioOpt);
		options.addOption(summaryFuncOpt);
		options.addOption(outputFormatOpt);
		options.addOption(includeTopicOpt);
		options.addOption(nBestFilterOpt);


		CommandLineParser parser = new PosixParser();
		CommandLine line = null;

		try {
			// parse the command line arguments
			line = parser.parse(options, args);
			if (line.hasOption("help")) {
				// print the help
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(200, "java com.machinelinking.stresstest.EuronewsReader", "\n", options, "\n", true);
			}
			else if (line.hasOption("version")) {
				logger.info("version 1.0");
			}
			else {
				int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
				if (line.hasOption("num-threads")) {
					numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
				}

				int numFiles = Defaults.DEFAULT_NUM_PAGES;
				if (line.hasOption("num-files")) {
					numFiles = Integer.parseInt(line.getOptionValue("num-files"));
				}

				int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
				if (line.hasOption("notification-point")) {
					notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
				}

				RequestBuilder requestBuilder = new RequestBuilder(line);

				EuronewsReader euronewsReader = new EuronewsReader(numThreads, numFiles, notificationPoint, requestBuilder);
				euronewsReader.startProcess(line.getOptionValue("euronews-dump"), line.getOptionValue("output-file"));

			}
		} catch (ParseException exp) {
			// oops, something went wrong
			System.err.println("Parsing failed: " + exp.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/jservice.jar com.machinelinking.stresstest.EuronewsReader", "\n", options, "\n", true);
			System.exit(1);
		}
	}
}