package com.machinelinking.jservice.stresstest;

import com.machinelinking.jservice.server.MainHttpServer;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.lang.CharEncoding;
import org.apache.log4j.Logger;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/21/13
 * Time: 12:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class RequestBuilder {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>RequestBuilder</code>.
	 */
	static Logger logger = Logger.getLogger(RequestBuilder.class.getName());


	public static final double DEFAULT_MINIMUM_WEIGHT = 0.25;

	public static final double DEFAULT_COMPRESSION_RATIO = 0.3;

	public static final String DEFAULT_SUMMARY_FUNC = "SUM";

	// Account: Michele Mostarda
	public static final String DEFAULT_APP_ID = "0ccb23d8";
	public static final String DEFAULT_APP_KEY = "7ad370d3f107133120712c40b30f6dec";

	URL serverAddress;
	String serverCall;

	public RequestBuilder(CommandLine line) {
		try {

			serverAddress = buildServerAddress(line);
			serverCall = buildServerCall(line);
			logger.info(serverAddress + serverCall);
		} catch (MalformedURLException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public URL getServerAddress() {
		return serverAddress;
	}

	public String getServerCall(String text) {
		StringBuilder sb = new StringBuilder();

			sb.append("&text");
			sb.append("=");
		try {
			sb.append(URLEncoder.encode(text, CharEncoding.UTF_8));
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
		}
		return serverCall + sb.toString();
	}

	private URL buildServerAddress(org.apache.commons.cli.CommandLine line) throws MalformedURLException {
		StringBuilder sbUrl = new StringBuilder();
		String host = line.getOptionValue("host");
		if (host == null) {
			host = MainHttpServer.DEFAULT_HOST;
		}
		String port = line.getOptionValue("port");
		sbUrl.append("http://");
		sbUrl.append(host);
		if (port != null) {
			sbUrl.append(":");
			sbUrl.append(port);
		}
		sbUrl.append("/");
		String method = line.getOptionValue("method");

		if (method != null) {
			sbUrl.append(method);
		}
		else {
			sbUrl.append("ping");
		}
		sbUrl.append("/");
		return new URL(sbUrl.toString());
	}

	private String buildServerCall(org.apache.commons.cli.CommandLine line) throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();

		String appId = line.getOptionValue("app_id");
		if (appId != null) {
			sb.append("app_id=");
			sb.append(appId);
		}
		else {
			sb.append("app_id=");//c3e5ca13");
			sb.append(DEFAULT_APP_ID);
		}
		String appKey = line.getOptionValue("app_key");
		if (appId != null) {
			sb.append("&app_key=");
			sb.append(appKey);
		}
		else {
			sb.append("&app_key=");
			sb.append(DEFAULT_APP_KEY);
			//cc193754ccbfd7c7f85648585b4e00f3");
		}
		/*String text = line.getOptionValue("text");
		if (text != null) {
			sb.append("&text");
			sb.append("=");
			sb.append(URLEncoder.encode(text, CharEncoding.UTF_8));
		} */
		String lang = line.getOptionValue("lang");
		if (lang != null) {
			sb.append("&lang");
			sb.append("=");
			sb.append(lang);
		}

		if (line.hasOption("id")) {
			sb.append("&id=" + line.getOptionValue("id"));
		}
		if (line.hasOption("include-text")) {
			sb.append("&include_text=1");
		}

		if (line.hasOption("disambiguation")) {
			sb.append("&disambiguation=1");
		}

		if (line.hasOption("form")) {
			sb.append("&form=1");
		}
		if (line.hasOption("image")) {
			sb.append("&image=1");
		}
		if (line.hasOption("topic")) {
			sb.append("&topic=1");
		}
		if (line.hasOption("category")) {
			//sb.append("&category=1");
			sb.append("&category=" + line.getOptionValue("category"));
		}
		if (line.hasOption("type")) {
			sb.append("&type=1");
		}
		if (line.hasOption("cross")) {
			sb.append("&cross=1");
		}
		if (line.hasOption("link")) {
			sb.append("&link=1");
		}
		if (line.hasOption("dbpedia")) {
			sb.append("&dbpedia=1");
		}
		if (line.hasOption("abstract")) {
			sb.append("&abstract=1");
		}
		if (line.hasOption("nbest")) {
			sb.append("&nbest-filter=");
			sb.append(line.getOptionValue("nbest"));
		}
		if (line.hasOption("class")) {
			sb.append("&class=1");
		}
		if (line.hasOption("min-weight")) {
			sb.append("&min_weight=");
			sb.append(Double.parseDouble(line.getOptionValue("min-weight")));
		}
		if (line.hasOption("compression-ratio")) {
			sb.append("&compression_ratio=");
			sb.append(Double.parseDouble(line.getOptionValue("compression-ratio")));
		}
		if (line.hasOption("summary-func")) {
			sb.append("&func=");
			sb.append(line.getOptionValue("summary-func"));
		}
		if (line.hasOption("output-format")) {
			sb.append("&output_format=");
			sb.append(line.getOptionValue("output-format"));
		}
		return sb.toString();
	}
}
