package com.machinelinking.jservice.comp;

import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.utils.lsa.LSM;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.math.Vector;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/25/12
 * ServiceTime: 3:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class TextSimilarity {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TextSimilarity</code>.
	 */
	static Logger logger = Logger.getLogger(TextSimilarity.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	//todo: use LSI
	LSM lsm;

	private static TextSimilarity ourInstance = null;

	public static synchronized TextSimilarity getInstance() {
		if (ourInstance == null) {
			try {
				ourInstance = new TextSimilarity();
			} catch (IOException e) {
				logger.error(e);
			}
		}
		return ourInstance;
	}

	private TextSimilarity() throws IOException {
		logger.debug("creating text similarity...");
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
			/*Iterator<String> it = config.getKeys();
			while (it.hasNext())
			{
				logger.debug(it.next());
			} */
			String indexName = config.getString("lsm-dir");
			if (!indexName.endsWith(File.separator)) {
				indexName += File.separator;
			}
			// cross language model files
			File fileUt = new File(indexName + "X-Ut");
			File fileSk = new File(indexName + "X-S");
			File fileR = new File(indexName + "X-row");
			File fileC = new File(indexName + "X-col");
			File fileDf = new File(indexName + "X-df");

			int dim = config.getInt("lsm-dim");
			boolean rescaleIdf = config.getBoolean("lsm-rescale-idf");

			lsm = new LSM(fileUt, fileSk, fileR, fileC, fileDf, dim, rescaleIdf);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}

	}

	public double compareBOW(String[] t1, String[] t2) {
		long begin = System.nanoTime();
		//logger.debug(Arrays.toString(t1));
		//logger.debug(Arrays.toString(t2));
		BOW bow1 = new BOW(t1);
		BOW bow2 = new BOW(t2);

		//logger.debug(bow1);
		//logger.debug(bow2);

		Vector vec1 = lsm.mapDocument(bow1);
		vec1.normalize();

		Vector vec2 = lsm.mapDocument(bow2);
		vec2.normalize();

		//logger.debug(vec1);
		//logger.debug(vec2);

		double result = vec1.dotProduct(vec2);
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(result + " computed in " + nf.format(time) + " ns");

		return result;

	}

	public double compareLS(String[] t1, String[] t2) {
		long begin = System.nanoTime();
		//logger.debug(Arrays.toString(t1));
		//logger.debug(Arrays.toString(t2));

		BOW bow1 = new BOW(t1);
		BOW bow2 = new BOW(t2);

		//logger.debug(bow1);
		//logger.debug(bow2);

		Vector vec1 = lsm.mapDocument(bow1);
		vec1.normalize();

		Vector vec2 = lsm.mapDocument(bow2);
		vec2.normalize();

		//logger.debug(vec1);
		//logger.debug(vec2);

		Vector pvec1 = lsm.mapPseudoDocument(vec1);
		pvec1.normalize();

		Vector pvec2 = lsm.mapPseudoDocument(vec2);
		pvec2.normalize();

		//logger.debug(pvec1);
		//logger.debug(pvec2);

		double result = pvec1.dotProduct(pvec2);
		long end = System.nanoTime();
		long time = end - begin;
		//logger.debug(result + " computed in " + nf.format(time) + " ns");

		return result;
	}

	public double compareCombo(String[] t1, String[] t2) {
		long begin = System.nanoTime();
		//logger.debug(Arrays.toString(t1));
		//logger.debug(Arrays.toString(t2));

		BOW bow1 = new BOW(t1);
		BOW bow2 = new BOW(t2);

		//logger.debug(bow1);
		//logger.debug(bow2);

		Vector vec1 = lsm.mapDocument(bow1);
		vec1.normalize();

		Vector vec2 = lsm.mapDocument(bow2);
		vec2.normalize();

		//logger.debug(vec1);
		//logger.debug(vec2);

		Vector pvec1 = lsm.mapPseudoDocument(vec1);
		pvec1.normalize();

		Vector pvec2 = lsm.mapPseudoDocument(vec2);
		pvec2.normalize();

		//logger.debug(pvec1);
		//logger.debug(pvec2);

		double result = (vec1.dotProduct(vec2) + pvec1.dotProduct(pvec2)) / 2;
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(result + " computed in " + nf.format(time) + " ns");

		return result;
	}

}
