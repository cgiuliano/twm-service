/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.util;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/23/12
 * ServiceTime: 7:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class FeatureIndex {

	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>FeatureIndex</code>.
	 */
	static Logger logger = Logger.getLogger(FeatureIndex.class.getName());

	/**
	 * to do.
	 */
	private Map<String, Entry> map;

	/**
	 * to do.
	 */
	private int count;

	/**
	 * to do.
	 */
	private boolean readOnly;

	//
	private static Pattern tabPattern = Pattern.compile("\t");

	/**
	 * Constructs a <code>FeatureIndex</code> object.
	 */
	public FeatureIndex(boolean readOnly) {
		this(readOnly, 0);
	} // end constructor

	/**
	 * Constructs a <code>FeatureIndex</code> object.
	 */
	public FeatureIndex(boolean readOnly, int count) {
		//logger.info("FeatureIndex " + count);

		//map = new TreeMap<String, Entry>();
		map = new HashMap<String, Entry>();

		this.count = count;
		this.readOnly = readOnly;
	} // end constructor

	//
	public void readOnly(boolean b) {
		this.readOnly = readOnly;
	} // end readOnly

	/**
	 * Returns the <i>index</i> if this index contains the specified
	 * feature.
	 *
	 * @param feature the feature.
	 * @return <i>index</i> if this index contains the specified
	 *         feature; -1 othewise.
	 */
	public int add(String feature) {
		//logger.debug("FeatureIndex.put : " + feature + "(" + count + ")");
		Entry entry = map.get(feature);

		if (entry == null) {
			if (readOnly) {
				return -1;
			}

			entry = new Entry(count++, 1);
			map.put(feature, entry);
			return entry.getIndex();
		}

		entry.inc();
		return entry.getIndex();
	} // end get

	//
	public int size() {
		return map.size();
	} // end size

	/**
	 * Returns the feature <i>index</i> if this index contains the specified
	 * feature.
	 *
	 * @param feature the feature.
	 * @return <i>index</i> if this index contains the specified
	 *         feature; -1 othewise.
	 */
	public int getIndex(String feature) {
		Entry entry = map.get(feature);

		if (entry == null) {
			return -1;
		}

		return entry.getIndex();
	} // end getIndex

	/**
	 * Returns the feature <i>weight</i> if this index contains the specified
	 * feature.
	 *
	 * @param feature the feature.
	 * @return <i>weight</i> if this index contains the specified
	 *         feature; 0 othewise.
	 */
	public int getWeight(String feature) {
		Entry entry = map.get(feature);

		if (entry == null) {
			return 0;
		}

		return entry.getFreq();
	} // end getWeight


	//
	public Set<String> featureSet() {
		return map.keySet();
	} // end featureSet

	//
	public void clear() {
		count = 0;
		map.clear();
	} // end clear

	/**
	 * Returns a <code>String</code> object representing this
	 * <code>FeatureIndex</code>.
	 *
	 * @return a string representation of this object.
	 */
	public String toString1() {
		StringBuffer sb = new StringBuffer();

		String feat = null;
		Entry entry = null;
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			feat = it.next();
			entry = map.get(feat);
			//logger.debug(feat + "\t" + entry);
			sb.append(feat);
			sb.append("\t");
			sb.append(entry);
			sb.append("\n");
		}
		return sb.toString();
	} // end toString1

	/**
	 * Returns a <code>String</code> object representing this
	 * <code>FeatureIndex</code>.
	 *
	 * @return a string representation of this object.
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		SortedMap<Integer, String> smap = new TreeMap<Integer, String>();
		String feat = null;
		Entry entry = null;
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			feat = it.next();
			entry = map.get(feat);

			smap.put(entry.getFreq(), feat + "\t" + entry);
		}

		Iterator<Integer> it1 = smap.keySet().iterator();
		int freq = 0;
		while (it1.hasNext()) {
			freq = it1.next();

			sb.append(smap.get(freq));
			sb.append("\n");
		}
		return sb.toString();

	} // end toString

	/**
	 * Writes the feature index into the specified
	 * output stream in a format suitable for loading
	 * into a <code>Map</code> using the
	 * {@link #read(Reader) write} method.
	 *
	 * @param out a <code>Writer</code> object to
	 *            provide the underlying stream.
	 * @throws IOException if writing this feature index
	 *                     to the specified  output stream
	 *                     throws an <code>IOException</code>.
	 */
	public void write(Writer out) throws IOException {
		logger.debug("writing feature index (only features with frequency > 1 are stored)");

		String feat = null;
		Entry entry = null;
		PrintWriter pw = new PrintWriter(out);
		pw.println("#feat\tindex\tweight");
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			feat = it.next();
			entry = map.get(feat);
			//if (entry.getFreq() > 1)
			pw.println(feat + "\t" + entry);
		} // end while
		pw.flush();
		pw.close();

	} // end read

	/**
	 * Reads the feature index from the specified input stream.
	 * <p/>
	 * This method processes input in features of lines. A natural
	 * line of input is terminated either by a set of line
	 * terminator  characters (\nGramLength or \r or  \r\nGramLength) or by the end
	 * of the file. A natural line  may be either a blank line,
	 * a comment line, or hold some part  of a id-feature pair.
	 * Lines are read from the input stream until  end of file
	 * is reached.
	 * <p/>
	 * A natural line that contains only white space characters
	 * is  considered blank and is ignored. A comment line has
	 * an ASCII  '#' as its first non-white  space character;
	 * comment lines are also ignored and do not encode id-feature
	 * information.
	 * <p/>
	 * The id contains all of the characters in the line starting
	 * with the first non-white space character and up to, but
	 * not  including, the first '\t'. All remaining characters
	 * on the line become part of  the associated feature string;
	 * if there are no remaining  characters, the feature is the
	 * empty string "".
	 *
	 * @param in a <code>Reader</code> object to
	 *           provide the underlying stream.
	 * @throws IOException if reading this feature index
	 *                     from the specified  input stream
	 *                     throws an <code>IOException</code>.
	 */
	public void read(Reader in) throws IOException {
		////logger.debug("reading feature index...");

		map.clear();
		String line;
		String[] s;
		LineNumberReader lnr = new LineNumberReader(in);
		while ((line = lnr.readLine()) != null) {
			//line = line.trim();
			//logger.debug("\"" +line + "\"");
			if (!line.startsWith("#")) {
				//s = line.split("\t");
				s = tabPattern.split(line);
				if (s.length == 3) {
					map.put(s[0], new Entry(Integer.parseInt(s[1]), Integer.parseInt(s[2])));
					count++;
				}
			} // end if
		} // end while
		lnr.close();
	} // end read

	//
	class Entry {
		//
		int index, freq;

		//
		Entry(int index, int freq) {
			this.index = index;
			this.freq = freq;
		} // end constructor

		//
		public int getIndex() {
			return index;
		} // end getIndex

		//
		public int getFreq() {
			return freq;
		} // end getFreq

		//
		public String toString() {
			return index + "\t" + freq;
		} // end getFreq

		//
		public void inc() {
			freq++;
		} // end inc

	} // end class Entry
} // end class FeatureIndex