/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.util;

import org.apache.commons.configuration.*;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/23/12
 * ServiceTime: 9:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class ConfigurationReader {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>ConfigurationReader</code>.
	 */
	static Logger logger = Logger.getLogger(ConfigurationReader.class.getName());

	Configuration config = null;

	//
	public final static String DEFAULT_ENV = "development";

	public ConfigurationReader() {
	}

	public void load() throws ConfigurationReaderException {
		try {
			String name = getEnvConfigXMLPath();
			logger.info("configuration file " + name);
			DefaultConfigurationBuilder defaultConfigurationBuilder = new DefaultConfigurationBuilder(name);
			defaultConfigurationBuilder.setBasePath(".");
			config = defaultConfigurationBuilder.getConfiguration();
		} catch (Exception exc) {
			throw new ConfigurationReaderException(exc.getMessage());
		}
	}

	public synchronized Configuration getConfiguration() throws ConfigurationReaderException {
		if (config == null) {
			load();
		}
		return config;
	}

	private String getEnvConfigXMLPath() {
		logger.debug("config.xml");
		/*StringBuilder sb = new StringBuilder("configuration");
		sb.append(File.separator);
		// System env variable is mapped to configuration/[env] folder
		String env = System.getProperty("env");
		if (env == null)
		{
			env = DEFAULT_ENV;
		}
		sb.append(env);
		sb.append(File.separator);
		sb.append("config.xml");
		return sb.toString();*/
		return "config.xml";
	}
}
