package com.machinelinking.jservice.util;

import eu.fbk.twm.utils.RedirectPageMap;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/28/13
 * Time: 11:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class EvaluationRedirFixer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>EvaluationRedirFixer</code>.
	 */
	static Logger logger = Logger.getLogger(EvaluationRedirFixer.class.getName());
	private static Pattern tabPattern = Pattern.compile("\t");

	RedirectPageMap redirectPageMap;

	public EvaluationRedirFixer(File evalFile, File redirFile, File newEvalFile) throws IOException {
		redirectPageMap = new RedirectPageMap(redirFile);
		process(evalFile, newEvalFile);
	}

	public void process(File evalFile, File newEvalFile) throws IOException {
		////logger.debug("reading feature index...");
		int count=0;
		String line;
		String[] s;
		String redirectPage;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(evalFile), "UTF8"));
		StringBuilder sb = new StringBuilder();
		while ((line = lnr.readLine()) != null) {
			//line = line.trim();
			//logger.debug("\"" +line + "\"");
			if (!line.startsWith("#")) {
				//s = line.split("\t");
				s = tabPattern.split(line);
				if (s.length > 0) {
					redirectPage = redirectPageMap.get(s[0]);
					if (redirectPage != null) {

						count++;
						logger.info(s[0] +"\t"+redirectPage);
						sb.append(redirectPage);
						for (int i = 1; i < s.length; i++) {
							sb.append("\t" + s[i]);
						}
					} else {
						sb.append(line);
					}
					sb.append("\n");
				}
			} // end if
		} // end while
		lnr.close();
		//File newName = new File(evalFile.getAbsolutePath() + "-" + Calendar.getInstance().getTime());
		//boolean b = evalFile.renameTo(newName);

		if (count > 0) {
			logger.info(evalFile + " updated to " + newEvalFile + " " + count + " lines modified");
			PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newEvalFile), "UTF-8")));
			pw.print(sb.toString());
			pw.close();

		}
	}
	public static void main(String[] args) {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option evalOpt = OptionBuilder.withArgName("file").hasArg().withDescription("old evaluation file").isRequired().withLongOpt("old-eval").create("o");
			Option newEvalOpt = OptionBuilder.withArgName("file").hasArg().withDescription("new evaluation file").isRequired().withLongOpt("new-eval").create("n");
			Option redirOpt = OptionBuilder.withArgName("file").hasArg().withDescription("redirection file").isRequired().withLongOpt("redir").create("r");


			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");


			options.addOption(evalOpt);
			options.addOption(redirOpt);
			options.addOption(newEvalOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			File evalFile  = new File(line.getOptionValue("old-eval"));
			File newEvalFile  = new File(line.getOptionValue("new-eval"));
			File redirFile  = new File(line.getOptionValue("redir"));

			try {
				new EvaluationRedirFixer(evalFile, redirFile, newEvalFile);
			} catch (IOException e) {
				logger.error(e);
			}

			logger.info("extraction ended " + new Date());

		} catch (ParseException e) {
			// oops, something went wrong
			System.out.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/jservice.jar com.machinelinking.util.EvaluationRedirFixer", "\n", options, "\n", true);
		}
	}
}
