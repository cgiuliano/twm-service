/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.util;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/23/12
 * ServiceTime: 9:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class ConfigurationReaderException extends Exception {
	/**
	 * Default constructor for the exception, that takes no parameters.
	 * <p/>
	 * Whenever possible, please give a parameter to the
	 * constructor in order to provide feedback to the user.
	 */
	public ConfigurationReaderException() {
		super();
	}

	/**
	 * Constructor that displays a message including a string representative
	 * of the exception.
	 *
	 * @param msg the message
	 */
	public ConfigurationReaderException(String msg) {
		super(msg);
	}
}
