package com.machinelinking.jservice.util;

//import com.machinelinking.util.FeatureIndex;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.GenericFileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.String;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/13/13
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class AutomaticConfiguration {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AutomaticConfiguration</code>.
	 */
	static Logger logger = Logger.getLogger(AutomaticConfiguration.class.getName());

	private static AutomaticConfiguration ourInstance = null;

	private Map<String, Map<String, String>> map;

	private String[] languages;

	Configuration config;

	String modelsDir;

	String wikipediaModelsDir;

	String languageModelDir;

	String lsModelDir;

	int maxTextLength;

	private AutomaticConfiguration() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
			Iterator<String> it = config.getKeys();
			/*while (it.hasNext()) {
				logger.debug(it.next());
			}*/
			modelsDir = config.getString("models-dir");
			if (!modelsDir.endsWith(File.separator)) {
				modelsDir += File.separator;
			}

			maxTextLength = config.getInt("text-max-length");

			wikipediaModelsDir = modelsDir + "wikipedia" + File.separator;
			languageModelDir = modelsDir + "lang" + File.separator;
			lsModelDir = modelsDir + "lsa" + File.separator;

			logger.info("models dir " + modelsDir);

			logger.info("wikipedia models " + wikipediaModelsDir);
			logger.info("lang models " + languageModelDir);
			languages = new File(wikipediaModelsDir).list();
			logger.info("supported languages");
			logger.info(Arrays.toString(languages));
			map = new TreeMap<String, Map<String, String>>();
			for (int i = 0; i < languages.length; i++) {


				Map<String, String> resourceMap = null;
				try {
					//todo: create the page-id-index and page-outgoing-index for all languages
					//resourceMap = GenericFileUtils.searchForFilesInTheSameFolder(wikipediaModelsDir + languages[i], "type-index", "page-form-index", "incoming-outgoing-index", "form-page-index", "ngram-index", "page-freq.csv", "form-freq", "cross-lang-index", "ngram.csv", "unigram", "one-example-per-sense-index", "page-file-source-index", "first-name-index", "person-info-index", "airpedia-class-index", "abstract-index", "page-category-index", "category-super-category-index", "page-id-index", "page-outgoing-index", "page-freq-index", "page-vector-index", "incoming-outgoing-weighted-index", "page-traffic-index");
					//"page-vector-index", "incoming-outgoing-weighted-index"
					resourceMap = GenericFileUtils.searchForFilesInTheSameFolder(wikipediaModelsDir + languages[i], "page-namnom-index", "page-form-index", "form-page-index", "ngram-index", "page-freq.csv", "form-freq", "cross-lang-index", "ngram.csv", "unigram", "one-example-per-sense-index", "page-file-source-index", "first-name-index", "person-info-index", "page-airpedia2class-index", "abstract-index", "page-category-index", "category-super-category-index", "incoming-outgoing-weighted-index", "page-topics-index", "page-all-category-index");
					if (resourceMap == null) {
						GenericFileUtils.checkPatterns(wikipediaModelsDir + languages[i], "page-namnom-index", "page-form-index", "form-page-index", "ngram-index", "page-freq.csv", "form-freq", "cross-lang-index", "ngram.csv", "unigram", "one-example-per-sense-index", "page-file-source-index", "first-name-index", "person-info-index", "page-airpedia2class-index", "abstract-index", "page-category-index", "category-super-category-index", "incoming-outgoing-weighted-index", "page-topics-index", "page-all-category-index");
						System.exit(0);
					}

					logger.debug("adding " + languages[i] + "\t" + resourceMap);
					map.put(languages[i], resourceMap);
				} catch (IOException e) {
					logger.error(e);
				}

			}
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		} catch (Exception e) {
			logger.error(e);
			System.exit(0);
		}
	}

	public int getMaxTextLength() {
		return maxTextLength;
	}

	public String getLsModelDir() {
		return lsModelDir;
	}

	public String getModelsDir() {
		return modelsDir;
	}

	public String getWikipediaModelsDir() {
		return wikipediaModelsDir;
	}

	public String getLanguageModelDir() {
		return languageModelDir;
	}

	public String[] getLanguages() {
		return languages;
	}

	public Map<String, String> getResources(String language) {
		return map.get(language);
	}

	public static synchronized AutomaticConfiguration getInstance() {
		if (ourInstance == null) {
			ourInstance = new AutomaticConfiguration();
		}
		return ourInstance;
	}

	//todo:add a main to check the presence of all files and dir into the models

	public static void main(String[] args) {

		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		//java com.machinelinking.util.AutomaticConfiguration
		String folder = args[0];

		try {
			Map<String, String> resourceMap = GenericFileUtils.searchForFilesInTheSameFolder(folder, "page-namnom-index", "page-form-index", "form-page-index", "ngram-index", "page-freq.csv", "form-freq", "cross-lang-index", "ngram.csv", "unigram", "one-example-per-sense-index", "page-file-source-index", "first-name-index", "person-info-index", "page-airpedia2class-index", "abstract-index", "page-category-index", "category-super-category-index", "incoming-outgoing-weighted-index", "page-topics-index", "page-all-category-index");
			if (resourceMap == null) {
				GenericFileUtils.checkPatterns(folder, "page-namnom-index", "page-form-index", "form-page-index", "ngram-index", "page-freq.csv", "form-freq", "cross-lang-index", "ngram.csv", "unigram", "one-example-per-sense-index", "page-file-source-index", "first-name-index", "person-info-index", "page-airpedia2class-index", "abstract-index", "page-category-index", "category-super-category-index", "incoming-outgoing-weighted-index", "page-topics-index", "page-all-category-index");
				System.exit(0);
			}

			logger.info(resourceMap);

			logger.info(resourceMap);
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}
}
