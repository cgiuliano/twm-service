/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.math;

import java.io.Serializable;
import java.util.StringTokenizer;
//import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/23/12
 * ServiceTime: 7:02 AM
 * To change this template use File | Settings | File Templates.
 */
//
public class Node implements Serializable, Comparable<Node> {
	//
	private static final long serialVersionUID = 5024396602591514749L;

	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Node</code>.
	 */
	//static Logger logger = Logger.getLogger(Node.class.getName());

	public int    index;
	public double value;

	//
	public int compareTo(Node o) {
		if (index < o.index) {
			return -1;
		}
		else if (index > o.index) {
			return 1;
		}

		return 0;
	} // end compareTo

	//
	public static double dot(Node[] x, Node[] y) {
		double sum = 0;
		int xlen = x.length;
		int ylen = y.length;
		int i = 0;
		int j = 0;
		while (i < xlen && j < ylen) {
			if (x[i].index == y[j].index) {
				sum += x[i++].value * y[j++].value;
			}
			else {
				if (x[i].index > y[j].index) {
					++j;
				}
				else {
					++i;
				}
			}
		}
		return sum;
	}

	//
	static public Node[] parse(String line) {
		StringTokenizer st = new StringTokenizer(line, " \t\n\r\f:");
		int m = st.countTokens() / 2;
		Node[] x = new Node[m];
		for (int j = 0; j < m; j++) {
			x[j] = new Node();
			x[j].index = Integer.parseInt(st.nextToken());
			x[j].value = Double.valueOf(st.nextToken());
		}

		return x;
	} // end parse

	//
	static public String toString(Node[] node, int k) {
		StringBuilder sb = new StringBuilder();
		if (node.length > 0) {
			sb.append((node[0].index + k) + ":" + node[0].value);
		}
		for (int i = 1; i < node.length; i++) {
			sb.append(" " + (node[i].index + k) + ":" + node[i].value);
		}
		//sb.append("\nGramLength");
		return sb.toString();
	} // end toString

	//
	static public String toString(Node[] node) {
		StringBuilder sb = new StringBuilder();
		if (node.length > 0) {
			sb.append(node[0].index + ":" + node[0].value);
		}
		for (int i = 1; i < node.length; i++) {
			sb.append(" " + node[i].index + ":" + node[i].value);
		}
		//sb.append("\nGramLength");
		return sb.toString();
	} // end toString
}
