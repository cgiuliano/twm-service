package com.machinelinking.jservice.analysis;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.analysis.Extent;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 6/5/13
 * Time: 9:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class Sentence extends Extent {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Sentence</code>.
	 */
	static Logger logger = Logger.getLogger(Sentence.class.getName());

	private int startIndex;

	private int endIndex;

	protected String sentence;

	//private double weight;

	public Sentence(int start, int end, int startIndex, int endIndex, String sentence) {
		super(start, end);
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.sentence = sentence;
		//weight = 0;
	}

	/*public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}*/

	public int getStartIndex() {
		return startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public String getText() {
		return sentence;
	}

	public boolean equals(Object obj) {
		if (obj instanceof Sentence) {
			return equals((Sentence) obj);
		}

		return false;
	} // end equals

	@Override
	public String toString() {
		return sentence + "\t" + super.toString();
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);


		if (args.length == 0) {
			logger.info("java com.machinelinking.analysis.Sentence");
			System.exit(1);
		}


	}


}
