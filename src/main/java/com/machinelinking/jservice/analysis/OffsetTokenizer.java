package com.machinelinking.jservice.analysis;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Token;
import eu.fbk.twm.utils.analysis.Tokenizer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/23/13
 * Time: 7:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class OffsetTokenizer extends HardTokenizer implements Tokenizer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>OffsetTokenizer</code>.
	 */
	static Logger logger = Logger.getLogger(OffsetTokenizer.class.getName());

	private static OffsetTokenizer ourInstance = null;

	public static synchronized OffsetTokenizer getInstance() {
		if (ourInstance == null) {
			ourInstance = new OffsetTokenizer();
		}
		return ourInstance;
	}

	public Token[] tokenArray(String text, int offset) //throws Exception
	{
		if (text.length() == 0) {
			return new Token[0];
		}
		List<Token> list = new ArrayList<Token>();
		char currentChar = text.charAt(0);
		char previousChar = currentChar;
		int start = 0;
		boolean isCurrentCharLetterOrDigit;
		boolean isPreviousCharLetterOrDigit;
		Token token;

		if (!Character.isLetterOrDigit(currentChar)) {
			if (!isSeparatorChar(currentChar)) {
				list.add(new Token(offset, 1 + offset, new String(new char[]{currentChar})));
			}
		}

		//logger.debug("0\t" + (int) previousChar + "\t<" + previousChar + ">");
		for (int i = 1; i < text.length(); i++) {
			currentChar = text.charAt(i);
			isCurrentCharLetterOrDigit = Character.isLetterOrDigit(currentChar);
			isPreviousCharLetterOrDigit = Character.isLetterOrDigit(previousChar);
			//logger.debug(i + (int) currentChar + "\t<" + currentChar + ">");
			if (isCurrentCharLetterOrDigit) {
				if (!isPreviousCharLetterOrDigit) {
					start = i;
				}
			}
			else {
				if (isPreviousCharLetterOrDigit) {
					// word o number

					list.add(new Token(start + offset, i + offset, text.substring(start, i)));
					if (!isSeparatorChar(currentChar)) {
						// otherPageCounter
						list.add(new Token(i + offset, i + offset + 1, new String(new char[]{currentChar})));
					}
				}
				else {
					//otherPageCounter
					if (!isSeparatorChar(currentChar)) {
						list.add(new Token(i + offset, i + offset + 1, new String(new char[]{currentChar})));
					}
				}
			}
			previousChar = currentChar;
		}
		if (Character.isLetterOrDigit(previousChar)) {
			list.add(new Token(start + offset, text.length() + offset, text.substring(start, text.length())));
		}

		return list.toArray(new Token[list.size()]);
	}

	public static void main(String argv[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		// java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.analysis.OffsetTokenizer

		File f = new File(argv[0]);
		String s = null;
		if (f.exists()) {
			s = read(new File(argv[0]));
		}
		else {
			s = argv[0];
		}
		OffsetTokenizer hardTokenizer = new OffsetTokenizer();
		String t = hardTokenizer.tokenizedString(s);
		logger.info(t);
	}

}
