package com.machinelinking.jservice.analysis;

import eu.fbk.twm.utils.analysis.Token;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 6/5/13
 * Time: 9:55 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SentenceBoundariesDetector {

	public Sentence[] split(Token[] tokenArray, String text);

	public Sentence[] split(String text);
}
