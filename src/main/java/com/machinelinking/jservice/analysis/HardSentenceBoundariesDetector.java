package com.machinelinking.jservice.analysis;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Token;
import eu.fbk.twm.utils.analysis.Tokenizer;
import eu.fbk.twm.utils.StringTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 6/5/13
 * Time: 9:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class HardSentenceBoundariesDetector implements SentenceBoundariesDetector {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>HardSentenceBoundariesDetector</code>.
	 */
	static Logger logger = Logger.getLogger(HardSentenceBoundariesDetector.class.getName());

	private static OffsetTokenizer ourInstance = null;

	public static synchronized OffsetTokenizer getInstance() {
		if (ourInstance == null) {
			ourInstance = new OffsetTokenizer();
		}
		return ourInstance;
	}

	@Override
	public Sentence[] split(String text) {
		Tokenizer tokenizer = HardTokenizer.getInstance();
		Token[] tokenArray = tokenizer.tokenArray(text);
		return split(tokenArray, text);
	}

	@Override
	public Sentence[] split(Token[] tokenArray, String text) {
		//logger.debug(text);
		List<Sentence> sentenceList = new ArrayList<Sentence>();

		int start = 0;
		for (int i = 1; i < tokenArray.length - 1; i++) {
			/*logger.debug(i + "\t" + start + "\t" + tokenArray[i - 1].getForm() + "\t[" + tokenArray[i].getForm() + "]\t" + tokenArray[i + 1].getForm());
			logger.debug("\t" + tokenArray[i - 1].getForm().length() + "\t" + (tokenArray[i - 1].getForm().length() > 1));
			logger.debug("\t" + tokenArray[i - 1].getForm().charAt(0) + "\t" + (Character.isLowerCase(tokenArray[i - 1].getForm().charAt(0))));
			logger.debug("\t" + tokenArray[i].getForm() + "\t" + (tokenArray[i].getForm().equals(StringTable.FULL_STOP)));
			logger.debug("\t" + tokenArray[i + 1].getForm().charAt(0) + "\t" + (Character.isUpperCase(tokenArray[i + 1].getForm().charAt(0))));*/
			if (
					//tokenArray[i - 1].getForm().length() > 1 &&
					//Character.isLowerCase(tokenArray[i - 1].getForm().charAt(0)) &&
					tokenArray[i].getForm().equals(StringTable.FULL_STOP) &&
					Character.isUpperCase(tokenArray[i + 1].getForm().charAt(0))) {

				//logger.debug(tokenArray[start].getStart() + "\t" + tokenArray[i].getEnd() + "\t" + start + "\t" + i);
				sentenceList.add(new Sentence(tokenArray[start].getStart(), tokenArray[i].getEnd(), start, i, text.substring(tokenArray[start].getStart(), tokenArray[i].getEnd())));


				start = i + 1;
			}
		}

		sentenceList.add(new Sentence(tokenArray[start].getStart(), tokenArray[tokenArray.length - 1].getEnd(), start, tokenArray.length - 1, text.substring(tokenArray[start].getStart(), tokenArray[tokenArray.length - 1].getEnd())));
		//logger.debug(sentenceList.size());
		return sentenceList.toArray(new Sentence[sentenceList.size()]);
	}

	public static void main(String[] args) {
		// java com.machinelinking.analysis.HardSentenceBoundariesDetector
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		SentenceBoundariesDetector sentenceBoundariesDetector = new HardSentenceBoundariesDetector();

		Sentence[] sentences = sentenceBoundariesDetector.split(args[0]);
		for (int i = 0; i < sentences.length; i++) {
			logger.debug(i + "\t" + sentences[i]);
		}
	}
}
