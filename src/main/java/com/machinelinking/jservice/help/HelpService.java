/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.help;

import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/7/13
 * Time: 12:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class HelpService {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>HelpService</code>.
	 */
	static Logger logger = Logger.getLogger(HelpService.class.getName());

	Configuration config;

	private static HelpService ourInstance;

	String[] languages;

	AutomaticConfiguration automaticConfiguration;

	public static HelpService getInstance() {
		if (ourInstance == null) {
			ourInstance = new HelpService();
		}
		return ourInstance;
	}

	private HelpService() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
			Iterator<String> it = config.getKeys();
			/*while (it.hasNext()) {
				logger.debug(it.next());
			}*/

			automaticConfiguration = AutomaticConfiguration.getInstance();
			languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));

		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	public String[] supportedLanguages() {
		logger.debug(languages.length);
		return languages;
	}

}
