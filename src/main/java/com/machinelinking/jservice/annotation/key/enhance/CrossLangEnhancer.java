/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.index.CrossLanguageSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class CrossLangEnhancer extends AbstractEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CrossLangEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(CrossLangEnhancer.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static CrossLangEnhancer ourInstance;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, CrossLanguageSearcher> searcherMap;


	public static CrossLangEnhancer getInstance() {
		if (ourInstance == null) {
			ourInstance = new CrossLangEnhancer();
		}
		return ourInstance;
	}

	private CrossLangEnhancer() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		searcherMap = new HashMap<Locale, CrossLanguageSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("cross-lang-index") + "...");

			String indexName = resourceMap.get("cross-lang-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (indexName != null) {
					CrossLanguageSearcher searcher = new CrossLanguageSearcher(indexName, true);
					searcher.loadCache(cacheName, minFreq);
					searcherMap.put(new Locale(languages[i]), searcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		CrossLanguageSearcher searcher = searcherMap.get(locale);

		String page;
		Sense sense;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				sense = keywordArray[i].getSense();

				page = sense.getPage();
				Map<String, String> map = searcher.search(page);
				keywordArray[i].setCrossLangMap(map);

			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");
	}

	public Map<String, String> enhance(String page, Locale locale) {
		long begin = System.nanoTime();

		CrossLanguageSearcher searcher = searcherMap.get(locale);
		Map<String, String> map = searcher.search(page);

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(page + " enhanced in " + nf.format(time) + " ns");
		return map;
	}

}
