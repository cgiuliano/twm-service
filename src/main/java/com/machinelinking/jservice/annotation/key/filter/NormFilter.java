/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import org.apache.log4j.Logger;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class NormFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>NormFilter</code>.
	 */
	static Logger logger = Logger.getLogger(NormFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static NormFilter ourInstance;

	public static NormFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new NormFilter();
		}
		return ourInstance;
	}

	public void filter(Keyword[] keywordArray) {
		filter(keywordArray, null);
	}

	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		double max = 0, weight;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered()) {
				weight = keywordArray[i].weight();
				if (weight > max) {
					max = weight;
				}
			}
		}
		double norm = 1 / max;
		//logger.debug("max, norm\t" + max + ", " + norm);
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered()) {
				keywordArray[i].setNorm(norm);
			}
			else
			{
				keywordArray[i].setNorm(0);
			}

			//keywordArray[i].setNorm(keywordArray[i].getNorm() / norm);
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords normalized in " + nf.format(time) + " ns");
	}
}
