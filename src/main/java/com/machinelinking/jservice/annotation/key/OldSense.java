/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key;

import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/29/12
 * ServiceTime: 9:35 PM
 * To change this template use File | Settings | File Templates.
 * @deprecated
 */
@Deprecated public class OldSense implements Serializable, Comparable<OldSense> {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>OldSense</code>.
	 */
	static Logger logger = Logger.getLogger(OldSense.class.getName());

	String id;

	double probability;

	public OldSense(String id, double probability) {
		this.id = id;
		this.probability = probability;
	}

	public double getProbability() {
		return probability;
	}

	public void setProbability(double probability) {
		this.probability = probability;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public int compareTo(OldSense o) {
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof OldSense)) {
			return false;
		}

		OldSense sense = (OldSense) o;

		//if (Double.compare(sense.probability, probability) != 0) {
		//	return false;
		//}
		if (id != null ? !id.equals(sense.id) : sense.id != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
		/*int result;
		long temp;
		result = id != null ? id.hashCode() : 0;
		temp = Double.doubleToLongBits(probability);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;*/
	}

	/*
	public String toString()
	{
		StringWriter w = new StringWriter();
		try {
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			g.writeStringField("id", id);
			g.writeNumberField("prob", probability);
			g.writeEndObject();
			g.close();

		}
		catch (IOException e) {
			logger.error(e);
		}
		return w.toString();
	} // end toString
   */

	@Override
	public String toString() {
		return "OldSense{" +
						"id='" + id + '\'' +
						", probability=" + probability +
						'}';
	}
}