/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;

import java.util.Collection;
import java.util.Comparator;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:50 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractFilter {
	public abstract void filter(Keyword[] keywordArray, Locale locale);
}
