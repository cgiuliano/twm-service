/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.index.PageFormSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class FormEnhancer extends AbstractEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>FormEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(FormEnhancer.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static FormEnhancer ourInstance;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageFormSearcher> pageFormSearcherMap;

	public static FormEnhancer getInstance() {
		if (ourInstance == null) {
			ourInstance = new FormEnhancer();
		}
		return ourInstance;
	}

	private FormEnhancer() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		pageFormSearcherMap = new HashMap<Locale, PageFormSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-form-index") + "...");

			String indexName = resourceMap.get("page-form-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (indexName != null) {
					PageFormSearcher pageFormSearcher = new PageFormSearcher(indexName);
					pageFormSearcher.loadCache(cacheName, minFreq);
					pageFormSearcherMap.put(new Locale(languages[i]), pageFormSearcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		PageFormSearcher pageFormSearcher = pageFormSearcherMap.get(locale);
		String page;
		Sense sense;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered()&&keywordArray[i].hasSense()) {
				sense = keywordArray[i].getSense();
				page = sense.getPage();
				PageFormSearcher.Entry[] entry = pageFormSearcher.search(page);
				Form form;
				for (int j = 0; j < entry.length; j++) {
					form = new Form(entry[j].getValue(), entry[j].getFreq());
					keywordArray[i].addForm(form);
				}
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");

	}

	public List<Form> enhance(String page, Locale locale) {
		long begin = System.nanoTime();
		List<Form> formList = new ArrayList<Form>();
		PageFormSearcher pageFormSearcher = pageFormSearcherMap.get(locale);

		PageFormSearcher.Entry[] entry = pageFormSearcher.search(page);
		Form form;
		for (int j = 0; j < entry.length; j++) {
			form = new Form(entry[j].getValue(), entry[j].getFreq());
			formList.add(form);
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(page + " enhanced in " + nf.format(time) + " ns");
		return formList;
	}

}
