/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.annotation.key.KeywordWeightComparator;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.index.PageAllCategorySearcher;
import eu.fbk.twm.index.PageIncomingOutgoingWeightedSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/20/13
 * Time: 2:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class NBestCorrelationFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>NBestCorrelationFilter</code>.
	 */
	static Logger logger = Logger.getLogger(NBestCorrelationFilter.class.getName());

	public static final int MAX_FIRST_N = 5;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	DecimalFormat sf = new DecimalFormat(".0000");

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageIncomingOutgoingWeightedSearcher> pageIncomingOutgoingWeightedSearcherMap;

	Map<Locale, PageAllCategorySearcher> pageAllCategorySearcherMap;

	private static NBestCorrelationFilter ourInstance;

	Configuration config;

	public static NBestCorrelationFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new NBestCorrelationFilter();
		}
		return ourInstance;
	}

	private NBestCorrelationFilter() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		//pageIncomingOutgoingSearcherMap = new HashMap<Locale, PageIncomingOutgoingSearcher>();
		pageIncomingOutgoingWeightedSearcherMap = new HashMap<Locale, PageIncomingOutgoingWeightedSearcher>();
		pageAllCategorySearcherMap = new HashMap<Locale, PageAllCategorySearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("incoming-outgoing-weighted-index") + "...");
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-all-category-index") + "...");

			//String indexName = resourceMap.get("incoming-outgoing-index");
			String weightedIndexName = resourceMap.get("incoming-outgoing-weighted-index");
			String categoryIndexName = resourceMap.get("page-all-category-index");
			String cacheName = resourceMap.get("page-freq.csv");

			//logger.debug(indexName + "\t" + cacheName);
			//int minFreq = config.getInt("page-min-freq");
			int inOutMinFreq = config.getInt("incoming-outgoing-weighted-index-page-min-freq");
			int allCategoryMinFreq = config.getInt("all-category-index-page-min-freq");

			try {
				if (weightedIndexName != null) {
					Locale locale = new Locale(languages[i]);
					PageIncomingOutgoingWeightedSearcher pageIncomingOutgoingWeightedSearcher = new PageIncomingOutgoingWeightedSearcher(weightedIndexName);
					pageIncomingOutgoingWeightedSearcher.loadCache(cacheName, inOutMinFreq);
					//pageIncomingOutgoingSearcher.loadCache(cacheName, 1000);
					pageIncomingOutgoingWeightedSearcherMap.put(locale, pageIncomingOutgoingWeightedSearcher);

					PageAllCategorySearcher pageAllCategorySearcher = new PageAllCategorySearcher(categoryIndexName);
					pageAllCategorySearcher.loadCache(cacheName, allCategoryMinFreq);
					pageAllCategorySearcherMap.put(locale, pageAllCategorySearcher);

				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void filter(Keyword[] keywordArray) {
		filter(keywordArray, null);
	}


	int findFirstN(Keyword[] keywordArray) {
		int firstN = 0;
		Set<String> pageSet = new HashSet<String>();
		for (int i = 0; i < keywordArray.length && pageSet.size() < MAX_FIRST_N; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				pageSet.add(keywordArray[i].getSense().getPage());
				firstN++;
			}
		}
		return firstN;
	}

	//+P ~R +F1
	// avg
	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		PageIncomingOutgoingWeightedSearcher pageIncomingOutgoingWeightedSearcher = pageIncomingOutgoingWeightedSearcherMap.get(locale);
		PageAllCategorySearcher pageAllCategorySearcher = pageAllCategorySearcherMap.get(locale);

		Arrays.sort(keywordArray, new KeywordWeightComparator());
		/*int firstN = 0;
		Set<String> pageSet = new HashSet<String>();
		for (int i = 0; i < keywordArray.length && pageSet.size() < 5; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				pageSet.add(keywordArray[i].getSense().getPage());
				firstN++;
			}
		}*/
		int firstN = findFirstN(keywordArray);
		if (firstN < 2) {
			logger.warn("too few keywords (" + keywordArray.length + ") to apply the filter");
			return;
		}

		//int firstN = 5;
		if (keywordArray.length < firstN) {
			firstN = keywordArray.length;
		}
		//logger.debug("\n\nfirstN=" + firstN + "\n\n");
		String pi, pj = null;
		double[][] simMatrix = new double[firstN][keywordArray.length];
		//logger.debug("firstN " + firstN);
		for (int i = 0; i < firstN; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				pi = keywordArray[i].getSense().getPage();
				for (int j = 0; j < keywordArray.length; j++) {
					if (i != j) {
						if (!keywordArray[j].isFiltered() && keywordArray[j].hasSense()) {
							pj = keywordArray[j].getSense().getPage();
							if (!pi.equals(pj)) {
								long b = System.nanoTime();
								double inOutSim = pageIncomingOutgoingWeightedSearcher.compare(pi, pj);
								double allCategorySim = pageAllCategorySearcher.compare(pi, pj);
								simMatrix[i][j] = inOutSim + allCategorySim;
								long e = System.nanoTime();
								logger.trace(i + "\t" + j + "\t" + pi + "\t" + pj + "\t" + sf.format(inOutSim) + "+" + sf.format(allCategorySim) + "\t" + "\tm[" + i + ", " + j + "]=" + sf.format(simMatrix[i][j]));
							}
						}
					}
				}
			}
		}

		for (int i = 0; i < firstN; i++) {
			double iSum = 0;
			int iCount = 0;
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				pi = keywordArray[i].getSense().getPage();
				for (int j = 0; j < keywordArray.length; j++) {
					if (i != j) {
						if (!keywordArray[j].isFiltered() && keywordArray[j].hasSense()) {
							pj = keywordArray[j].getSense().getPage();
							if (!pi.equals(pj)) {
								iSum += simMatrix[i][j];
								iCount++;
								logger.trace(i + "\t" + j + "\t" + pi + "\t" + pj + "\t" + sf.format(simMatrix[i][j]) + "\t(" + iSum + ")\t" + iCount);
							}
						}
					}
				}

				double m = iSum / iCount;
				//logger.debug(i + "\t" + iSum + "/" + iCount + " = " + m);
				keywordArray[i].setCorrelation(m);
				logger.trace(i + "\t-\t" + keywordArray[i].getSense().getPage() + "\t" + sf.format(m) + "\t" + sf.format(keywordArray[i].getCorrelation()));
			}
		}

		for (int i = firstN; i < keywordArray.length; i++) {
			double iSum = 0;
			int iCount = 0;
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				pi = keywordArray[i].getSense().getPage();
				for (int j = 0; j < firstN; j++) {
					if (i != j) {
						if (!keywordArray[j].isFiltered() && keywordArray[j].hasSense()) {
							pj = keywordArray[j].getSense().getPage();
							if (!pi.equals(pj)) {
								iSum += simMatrix[j][i];
								iCount++;
								logger.trace(j + "\t" + i + "\t" + pj + "\t" + pi + "\t" + sf.format(simMatrix[j][i]) + "\t(" + iSum + ")\t" + iCount);
							}
						}
					}
				}

				double m = iSum / iCount;
				//logger.debug(i + "\t" + iSum + "/" + iCount + " + " + m);
				keywordArray[i].setCorrelation(m);
				logger.trace(i + "\t-\t" + keywordArray[i].getSense().getPage() + "\t" + sf.format(m) + "\t" + sf.format(keywordArray[i].getCorrelation()));
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords boosted in " + nf.format(time) + " ns");
	}

	//-P +R +F1
	// argmax
	/*public void filter2(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		PageIncomingOutgoingWeightedSearcher PageIncomingOutgoingWeightedSearcher = pageIncomingOutgoingWeightedSearcherMap.get(locale);

		Arrays.sort(keywordArray, new KeywordWeightComparator());
		int firstN = findFirstN(keywordArray);
		if (firstN < 2) {
			logger.warn("too few keywords (" + keywordArray.length + ") to apply the filter");
			return;
		}


		String pi, pj = null;
		double max = 0;
		double sum;
		int count;
		//double[] matrix = new double[keywordArray.length];
		double[] maxMatrix = new double[keywordArray.length];
		logger.debug("firstN " + firstN);
		for (int i = 0; i < firstN; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				pi = keywordArray[i].getSense().getPage();
				for (int j = 0; j < keywordArray.length; j++) {
					if (i != j) {
						if (!keywordArray[j].isFiltered() && keywordArray[j].hasSense()) {
							pj = keywordArray[j].getSense().getPage();
							;
							if (!pi.equals(pj)) {
								long b = System.nanoTime();
								double sim = PageIncomingOutgoingWeightedSearcher.compare(pi, pj);
								//matrix[j] = sim;
								if (sim > maxMatrix[j]) {
									logger.warn(i + "\t" + j + "\t" + sim + " > " + maxMatrix[j]);
									maxMatrix[j] = sim;
								}

								long e = System.nanoTime();
								logger.debug(i + "\t" + j + "\t" + pi + "\t" + pj + "\t" + sf.format(sim) + "\t" + "\tm[" + j + "]=" + sf.format(maxMatrix[j]) + "\t" + nf.format(e - b));
							}
						}
					}
				}

			}
		}

		for (int j = 0; j < firstN; j++) {
			if (!keywordArray[j].isFiltered() && keywordArray[j].hasSense()) {
				double m = max(maxMatrix);
				keywordArray[j].setCorrelation(m);
				logger.debug(j + "\t-\t" + keywordArray[j].getSense().getPage() + "\t-\t" + sf.format(keywordArray[j].getCorrelation()));
				logger.debug(j + "\t-\t" + keywordArray[j].getSense().getPage() + "\t-\t" + sf.format(maxMatrix[j]));
			}
		}

		for (int j = firstN; j < keywordArray.length; j++) {
			if (!keywordArray[j].isFiltered() && keywordArray[j].hasSense()) {
				//double m = max(maxMatrix);
				keywordArray[j].setCorrelation(maxMatrix[j]);
				logger.debug(j + "\t-\t" + keywordArray[j].getSense().getPage() + "\t-\t" + sf.format(keywordArray[j].getCorrelation()));
				logger.debug(j + "\t-\t" + keywordArray[j].getSense().getPage() + "\t-\t" + sf.format(maxMatrix[j]));
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords boosted in " + nf.format(time) + " ns");
	} */

	/*double max(double[] d) {
		double max = 0;
		for (int i = 0; i < d.length; i++) {
			if (d[i] > max) {
				max = d[i];
			}
		}
		return max;
	}*/


	/*private int findBestKeyword(Keyword[] keywordArray) {
		double maxWeight = 0;
		int maxIndex = -1;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].weight() > maxWeight) {
				maxWeight = keywordArray[i].weight();
				maxIndex = i;
			}
		}
		logger.debug("best\t" + maxIndex + "\t" + maxWeight + "\t" + (maxIndex > -1 ? keywordArray[maxIndex] : "-"));
		//logger.debug("best\t" + keywordArray[maxIndex]);
		return maxIndex;
	} */

	//P R F1
	// argmax
	/*public void filterARG(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		PageIncomingOutgoingWeightedSearcher pageIncomingOutgoingWeightedSearcher = pageIncomingOutgoingWeightedSearcherMap.get(locale);

		Arrays.sort(keywordArray, new KeywordWeightComparator());
		int firstN = findFirstN(keywordArray);
		if (firstN < 2) {
			logger.warn("too few keywords (" + keywordArray.length + ") to apply the filter");
			return;
		}

		String pi, pj = null;
		double[][] simMatrix = new double[firstN][keywordArray.length];
		//todo: check if the first n are not filtered!
		//logger.debug("firstN " + firstN);
		for (int i = 0; i < firstN; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				pi = keywordArray[i].getSense().getPage();
				for (int j = 0; j < keywordArray.length; j++) {
					if (i != j) {
						if (!keywordArray[j].isFiltered() && keywordArray[j].hasSense()) {
							pj = keywordArray[j].getSense().getPage();
							;
							if (!pi.equals(pj)) {
								//long b = System.nanoTime();
								double sim = pageIncomingOutgoingWeightedSearcher.compare(pi, pj);
								simMatrix[i][j] = sim;
								//long e = System.nanoTime();
								//logger.debug(i + "\t" + j + "\t" + pi + "\t" + pj + "\t" + sf.format(sim) + "\t" + "\tm[" + i + ", " + j + "]=" + sf.format(simMatrix[i][j]) + "\t" + nf.format(e - b));
							}
						}
					}
				}
			}
		}

		for (int i = 0; i < firstN; i++) {
			double max = 0;
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				pi = keywordArray[i].getSense().getPage();
				for (int j = 0; j < keywordArray.length; j++) {
					if (i != j) {
						if (!keywordArray[j].isFiltered() && keywordArray[j].hasSense()) {
							pj = keywordArray[j].getSense().getPage();
							;
							if (!pi.equals(pj)) {
								if (simMatrix[i][j] > max) {
									max = simMatrix[i][j];
								}
								//logger.debug(i + "\t" + j + "\t" + pi + "\t" + pj + "\t" + sf.format(simMatrix[i][j]) + "\t(" +max + ")");
							}
						}
					}
				}


				//logger.debug(i + "\t" + max);
				keywordArray[i].setCorrelation(max);
				//logger.debug(i + "\t-\t" + keywordArray[i].getSense().getPage(); + "\t" + sf.format(max) + "\t" + sf.format(keywordArray[i].getCorrelation()));
			}
		}

		for (int i = firstN; i < keywordArray.length; i++) {
			double max = 0;
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				pi = keywordArray[i].getSense().getPage();
				for (int j = 0; j < firstN; j++) {
					if (i != j) {
						if (!keywordArray[j].isFiltered() && keywordArray[j].hasSense()) {
							pj = keywordArray[j].getSense().getPage();
							;
							if (!pi.equals(pj)) {
								if (simMatrix[j][i] > max) {
									max = simMatrix[j][i];
								}
								//logger.debug(j + "\t" + i + "\t" + pj + "\t" + pi + "\t" + sf.format(simMatrix[j][i]) + "\t(" +max + ")");
							}
						}
					}
				}


				keywordArray[i].setCorrelation(max);
				//logger.debug(i + "\t-\t" + keywordArray[i].getSense().getPage(); + "\t" + sf.format(max) + "\t" + sf.format(keywordArray[i].getCorrelation()));
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords boosted in " + nf.format(time) + " ns");
	} */
}