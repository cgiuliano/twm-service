package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.annotation.key.NGram;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.index.FirstNameSearcher;
import eu.fbk.twm.index.PersonInfoSearcher;
import eu.fbk.twm.utils.analysis.Token;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/25/13
 * Time: 12:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersonNameFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PersonNameFilter</code>.
	 */
	static Logger logger = Logger.getLogger(PersonNameFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	Configuration config;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, FirstNameSearcher> firstNameSearcherMap;

	Map<Locale, PersonInfoSearcher> personInfoSearcherMap;

	Tokenizer tokenizer;

	private static PersonNameFilter ourInstance;

	public static PersonNameFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new PersonNameFilter();
		}
		return ourInstance;
	}

	private PersonNameFilter() {
		tokenizer = HardTokenizer.getInstance();
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			initFirstName(languages);
			initPersonInfo(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void initFirstName(String[] languages) {
		firstNameSearcherMap = new HashMap<Locale, FirstNameSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("first-name-index") + "...");

			String indexName = resourceMap.get("first-name-index");
			int minFreq = config.getInt("first-name-min-freq");
			logger.debug(i + "\t" + languages[i] + "\t" + indexName + "\t" + minFreq);
			try {
				if (indexName != null) {
					FirstNameSearcher searcher = new FirstNameSearcher(indexName, true);
					searcher.loadCache(minFreq);
					firstNameSearcherMap.put(new Locale(languages[i]), searcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	void initPersonInfo(String[] languages) {
		personInfoSearcherMap = new HashMap<Locale, PersonInfoSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("person-info-index") + "...");

			String indexName = resourceMap.get("person-info-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			logger.debug(i + "\t" + languages[i] + "\t" + indexName + "\t" + minFreq);
			try {
				if (indexName != null) {
					PersonInfoSearcher personInfoSearcher = new PersonInfoSearcher(indexName, true);
					personInfoSearcher.loadCache(cacheName, minFreq);
					personInfoSearcherMap.put(new Locale(languages[i]), personInfoSearcher);

				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}


	public void filter(Keyword[] keywordArray, Locale locale) {
	}

	public void filter(Keyword[] keywordArray, Locale locale, Token[] tokenArray) {
		long begin = System.nanoTime();
		FirstNameSearcher firstNameSearcher = firstNameSearcherMap.get(locale);
		PersonInfoSearcher personInfoSearcher = personInfoSearcherMap.get(locale);
		int count = 0, total = 0;
		for (int i = 0; i < keywordArray.length; i++) {
			//logger.debug(i + "\t" + keywordArray[i].getForm() + "\t" + keywordArray[i].weight() + "\t" + keywordArray[i].isFiltered());
			if (!keywordArray[i].isFiltered()) {
				total++;
				List<NGram> nGramList = keywordArray[i].getNGramList();
				Sense sense = keywordArray[i].getSense();
				if (sense != null) {
					//todo: ask airpedia
					boolean isPerson = personInfoSearcher.contains(sense.getPage());
					if (isPerson) {
						//logger.debug(i + "\t" + sense);
						for (int j = 0; j < nGramList.size(); j++) {
							NGram ngram = nGramList.get(j);
							int previousPosition = ngram.getStartIndex() - 1;
							if (previousPosition >= 0) {
								Token previousToken = tokenArray[previousPosition];
								int freq = firstNameSearcher.search(previousToken.getForm());
								//logger.debug("\t" + j + "\t" + freq + "\t[" + token + "]\t" + ngram);
								if (freq > 0) {
									if (!sense.getPage().contains(previousToken.getForm())) {
										//logger.debug("\t\tfirstName(" + token.getForm() + ")\tfullName" + sense.getPage() + "\t" + ngram + "\tNO");
										keywordArray[i].setFiltered(true);
										count++;
										break;
									}
								}
							}
							int nextPosition = ngram.getEndIndex() + 1;
							//logger.debug(nextPosition);
							if (nextPosition < tokenArray.length) {
								Token nextToken = tokenArray[nextPosition];
								//Token previousToken = tokenArray[previousPosition];
								//todo: solve the bug: "Miley Cyrus We Can't Stop"
								//logger.debug(token);
								if (nextToken.getForm().length() >  0) {
									if (Character.isUpperCase(nextToken.getForm().charAt(0))) {
										keywordArray[i].setFiltered(true);
										count++;
										break;
									}
								}
							}
						}
					}
				}
			}
		}
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + total + " keywords filtered out in " + nf.format(time) + " ns");
	}
}
