/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import eu.fbk.twm.utils.Defaults;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.index.PageIdSearcher;
import eu.fbk.twm.index.PageIncomingOutgoingSearcher;
import eu.fbk.twm.utils.StringTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 8/8/13
 * Time: 4:28 PM
 * To change this template use File | Settings | File Templates.
 */
@Deprecated public class MutualLinkSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>MutualLinkSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(MutualLinkSearcher.class.getName());

	protected static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	private PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher;

	private PageIdSearcher pageIdSearcher;

	private static final int DEFAULT_MIN_FREQ = 1000;

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	public MutualLinkSearcher(String pageIncomingOutgoingIndex, String pageIdIndex) throws IOException {
		pageIncomingOutgoingSearcher = new PageIncomingOutgoingSearcher(pageIncomingOutgoingIndex);
		pageIdSearcher = new PageIdSearcher(pageIdIndex);
	}

	public MutualLinkSearcher(String pageIncomingOutgoingIndex, String pageIdIndex, String cacheName, int minFreq) throws IOException {
		this(pageIncomingOutgoingIndex, pageIdIndex);
		pageIncomingOutgoingSearcher.loadCache(cacheName, minFreq);
		pageIdSearcher.loadCache(cacheName, minFreq);
	}

	public int search1(String p1, String p2) {
		int[] l1 = pageIncomingOutgoingSearcher.search(p1);
		//logger.debug(p1 + "\t" + Arrays.toString(l1));
		int[] l2 = pageIncomingOutgoingSearcher.search(p2);
		//logger.debug(p2 + "\t" + Arrays.toString(l2));
		int i1 = pageIdSearcher.search(p1);
		//logger.debug(p1 + "\t" + i1);

		int i2 = pageIdSearcher.search(p2);
		//logger.debug(p2 + "\t" + i2);
		int count = 0;
		int f1 = Arrays.binarySearch(l1, i2);
		if (f1 >= 0) {
			//logger.warn(f1);
			count++;
		}
		int f2 = Arrays.binarySearch(l2, i1);
		if (f2 >= 0) {
			//logger.warn(f2);
			count++;
		}
		return count;
	}


	//todo: return boolean
	public int search(String p1, String p2) {
		int[] l1 = pageIncomingOutgoingSearcher.search(p1);
		//logger.debug(p1 + "\t" + l1.length + "\t" + Arrays.toString(l1));
		int[] l2 = pageIncomingOutgoingSearcher.search(p2);
		//logger.debug(p2 + "\t" + l2.length +  "\t" + Arrays.toString(l2));

		if (l1.length >= l2.length) {
			int i1 = pageIdSearcher.search(p1);
			//logger.debug(p1 + "\t" + i1);
			int f2 = Arrays.binarySearch(l2, i1);
			if (f2 >= 0) {
				//logger.warn(f2);
				return 1;
			}
		} else {
			int i2 = pageIdSearcher.search(p2);
			//logger.debug(p2 + "\t" + i2);
			//logger.debug(p2 + "\t" + i2);
			int count = 0;
			int f1 = Arrays.binarySearch(l1, i2);
			if (f1 >= 0) {
				//logger.warn(f1);
				return 1;
			}
		}


		return 0;
	}


	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 2) {
				long begin = System.nanoTime();
				int i = search(s[0], s[1]);
				long end = System.nanoTime();
				logger.info(i + "\t" + tf.format(end - begin));


			}
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option inOutIndexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("inout").create();
			Option idIndexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("id").create();

			Option interactiveModeOpt = OptionBuilder.withArgName("interactive-mode").withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option searchOpt = OptionBuilder.withArgName("search").hasArgs(2).withDescription("search for the specified key").withLongOpt("search").create("s");
			Option freqFileOpt = OptionBuilder.withArgName("key-freq").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			Option minimumKeyFreqOpt = OptionBuilder.withArgName("minimum-freq").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
			Option notificationPointOpt = OptionBuilder.withArgName("notification-point").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(inOutIndexNameOpt);
			options.addOption(idIndexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(searchOpt);
			options.addOption(freqFileOpt);
			//options.addOption(keyFieldNameOpt);
			//options.addOption(valueFieldNameOpt);
			options.addOption(minimumKeyFreqOpt);
			options.addOption(notificationPointOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			MutualLinkSearcher mutualLinkSearcher = null;

			if (line.hasOption("key-freq")) {

				mutualLinkSearcher = new MutualLinkSearcher(line.getOptionValue("inout"), line.getOptionValue("id"), line.getOptionValue("key-freq"), minFreq);
			}
			else {
				mutualLinkSearcher = new MutualLinkSearcher(line.getOptionValue("inout"), line.getOptionValue("id"));
			}

			if (line.hasOption("search")) {
				logger.debug("searching " + line.getOptionValue("search") + "...");
				int result = mutualLinkSearcher.search(line.getOptionValues("search")[0], line.getOptionValues("search")[1]);
				logger.info(result);
			}
			if (line.hasOption("interactive-mode")) {
				mutualLinkSearcher.interactive();
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java cp dist/jservice.jar com.machinelinking.annotation.key.filter.MutualLinkSearcher", "\n", options, "\n", true);
		}
	}
}
