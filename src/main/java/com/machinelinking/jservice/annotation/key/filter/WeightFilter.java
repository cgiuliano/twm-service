/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class WeightFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WeightFilter</code>.
	 */
	static Logger logger = Logger.getLogger(WeightFilter.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	private static WeightFilter ourInstance;

	Map<Locale, Double> thresholdMap;

	public static WeightFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new WeightFilter();
		}
		return ourInstance;
	}

	private WeightFilter() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		thresholdMap = new HashMap<Locale, Double>();
		Double threshold = config.getDouble("weight-filter-threshold");
		logger.debug("weight-filter-threshold: " + threshold);

		for (int i = 0; i < languages.length; i++) {

			thresholdMap.put(new Locale(languages[i]), threshold);
		}
	}

	public void filter(Keyword[] keywordArray, Locale locale) {
		double threshold = thresholdMap.get(locale);
		filter(keywordArray, locale, threshold);
	}

	public void filter(Keyword[] keywordArray, Locale locale, double threshold) {
		long begin = System.nanoTime();

		double weight;
		int count = 0;
		for (int i = 0; i < keywordArray.length; i++) {
			weight = keywordArray[i].weight();
			if (weight == 0 || weight <= threshold) {
				//logger.debug("filtered " + keywordArray[i]);
				keywordArray[i].setFiltered(true);
				count++;
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + keywordArray.length + " keywords filtered out (t=" + threshold + ")  in " + nf.format(time) + " ns");
	}

}
