package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import org.apache.log4j.Logger;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/25/13
 * Time: 12:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdjacentFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AdjacentFilter</code>.
	 */
	static Logger logger = Logger.getLogger(AdjacentFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static AdjacentFilter ourInstance;

	public static AdjacentFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new AdjacentFilter();
		}
		return ourInstance;
	}

	private AdjacentFilter() {
	}


	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();
		int count = 0;
		/*for (int i=0;i<keywordArray.length;i++)
		{
			logger.debug(i + "\t" + keywordArray[i].getForm() + "\t" + keywordArray[i].weight() + "\t" + keywordArray[i].isFiltered());
		}*/
		for (int i = 0; i < keywordArray.length; i++) {
			//logger.debug(i + "\t" + keywordArray[i].getForm() + "\t" + keywordArray[i].weight() + "\t" + keywordArray[i].isFiltered());
			//if (!keywordArray[i].isFiltered()) {

				for (int j = i + 1; j < keywordArray.length; j++) {
					//logger.debug("\t" + j + "\t" + keywordArray[j].getForm() + "\t" + keywordArray[j].weight() + "\t" + keywordArray[j].isFiltered());
					//if (!keywordArray[j].isFiltered()) {
						//logger.debug("B\t " + i + "/" + j + "\t" + keywordArray[i].isFiltered() + "\t" + keywordArray[j].isFiltered());
						if (keywordArray[i].isAdjacentTo(keywordArray[j]) && keywordArray[i].isNam() && keywordArray[j].isNam()) {
							logger.debug(keywordArray[i].getForm() + " \t " + keywordArray[j].getForm());
							keywordArray[j].setFiltered(true);
							keywordArray[i].setFiltered(true);
							count += 2;
						}
					//}
					//logger.debug("A\t " + i + "/" + j + "\t" + keywordArray[i].isFiltered() + "\t" + keywordArray[j].isFiltered());
				}
			//}
		}
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + keywordArray.length + " keyword filtered in " + nf.format(time) + " ns");
	}

}
