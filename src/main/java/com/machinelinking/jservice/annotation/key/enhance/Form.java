/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.StringWriter;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 4:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class Form {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Form</code>.
	 */
	static Logger logger = Logger.getLogger(Form.class.getName());

	String form;

	double probability;

	public Form(String form, double probability) {
		this.form = form;
		this.probability = probability;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public double getProbability() {
		return probability;
	}

	public void setProbability(double probability) {
		this.probability = probability;
	}

	@Override
	public String toString() {
		StringWriter w = new StringWriter();
		try {
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			g.writeStringField("form", form);
			g.writeNumberField("freq", probability);
			g.writeEndObject();
			g.close();

		} catch (IOException e) {
			logger.error(e);
		}
		return w.toString();

	}
}
