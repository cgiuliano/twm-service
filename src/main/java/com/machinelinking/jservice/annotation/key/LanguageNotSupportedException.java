/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/3/13
 * ServiceTime: 9:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class LanguageNotSupportedException extends Exception {

	public static final int ERROR_CODE = 40101;

	public LanguageNotSupportedException() {
	}

	public LanguageNotSupportedException(String s) {
		super(s);
	}

	public LanguageNotSupportedException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public LanguageNotSupportedException(Throwable throwable) {
		super(throwable);
	}
}
