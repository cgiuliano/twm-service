/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/20/13
 * Time: 2:41 PM
 * To change this template use File | Settings | File Templates.
 */
@Deprecated public class MutualLinkFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>MutualLinkFilter</code>.
	 */
	static Logger logger = Logger.getLogger(MutualLinkFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, MutualLinkSearcher> mutualLinkSearcherMap;

	private static MutualLinkFilter ourInstance;

	Configuration config;

	public static MutualLinkFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new MutualLinkFilter();
		}
		return ourInstance;
	}

	private MutualLinkFilter() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		mutualLinkSearcherMap = new HashMap<Locale, MutualLinkSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("incoming-outgoing-index") + "...");

			String inOutIndexName = resourceMap.get("incoming-outgoing-index");
			String idIndexName = resourceMap.get("page-id-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(inOutIndexName + "\t" + idIndexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (inOutIndexName != null && idIndexName != null) {
					MutualLinkSearcher pageIncomingOutgoingSearcher = new MutualLinkSearcher(inOutIndexName, idIndexName, cacheName, minFreq);
					mutualLinkSearcherMap.put(new Locale(languages[i]), pageIncomingOutgoingSearcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void filter(Keyword[] keywordArray) {
		filter(keywordArray, null);
	}

	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		MutualLinkSearcher mutualLinkSearcher = mutualLinkSearcherMap.get(locale);
		String pi, pj = null;
		double max = 0;
		boolean prod;
		int count;
		//todo: create the matrix using boolean
		int[][] matrix = new int[keywordArray.length][keywordArray.length];
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].getSense() != null) {
				pi = keywordArray[i].getSense().getPage();
				prod = false;
				count = 0;
				for (int j = 0; j < i; j++) {
					//prod *= Math.pow(2, matrix[i][j]);
					prod |= matrix[i][j] > 0;
					count++;
					//logger.debug(i + "\t" + j + "\t" + pi + "\t?\t" + matrix[i][j] + "\t" + prod + "\t" + count);
				}
				for (int j = i + 1; j < keywordArray.length; j++) {
					if (!keywordArray[j].isFiltered() && keywordArray[j].getSense() != null) {
						pj = keywordArray[j].getSense().getPage();;
						if (!pi.equals(pj)) {
							long b = System.nanoTime();

							matrix[i][j] = matrix[j][i] = mutualLinkSearcher.search(pi, pj);

							//prod *= Math.pow(2, matrix[i][j]);
							prod |= matrix[i][j] > 0;
							count++;
							long e = System.nanoTime();
							logger.debug(i + "\t" + j + "\t" + pi + "\t" + pj + "\t" + matrix[i][j] + "\t" + prod + "\t" + count + "\t" + nf.format(e - b));
						}
						else {
							//logger.debug(i + "\t" + j + "\t" + pi + "\t" + pj + "\t0\t" + prod + "\t" + count);
						}
					}
				}
				logger.debug("prod " + prod + ", count " + count);
				if (count != 0) {
					//keywordArray[i].setBoost(sum / (keywordArray.length - 1));

					if (prod) {
						keywordArray[i].setCorrelation(1);
					} else {
						keywordArray[i].setCorrelation((double) 1/keywordArray.length);
					}

					/*if (prod > max) {
						max = prod;
					} */
				}

				logger.debug(i + "\t-\t" + pi + "\t-\t" + keywordArray[i].getCorrelation());

			}
		}
		// normalize
		/*for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].getSense() != null) {
				keywordArray[i].setBoost(keywordArray[i].getBoost() / max);
				logger.debug(i + "\t" + keywordArray[i].getSense().getPage(); + "\t" + keywordArray[i].getBoost());
			}
		} */
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords boosted in " + nf.format(time) + " ns");
	}
}
