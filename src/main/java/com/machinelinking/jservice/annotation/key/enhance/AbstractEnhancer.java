package com.machinelinking.jservice.annotation.key.enhance;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/8/13
 * Time: 8:15 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AbstractEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(AbstractEnhancer.class.getName());
}
