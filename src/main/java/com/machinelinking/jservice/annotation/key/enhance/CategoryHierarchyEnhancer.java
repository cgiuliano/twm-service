/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;
//import eu.fbk.twm.index.CategoryHierarchySearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class CategoryHierarchyEnhancer extends AbstractEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CategoryHierarchyEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(CategoryHierarchyEnhancer.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static CategoryHierarchyEnhancer ourInstance;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, CategoryHierarchySearcher> pageCategorySearcherMap;


	public static CategoryHierarchyEnhancer getInstance() {
		if (ourInstance == null) {
			ourInstance = new CategoryHierarchyEnhancer();
		}
		return ourInstance;
	}

	private CategoryHierarchyEnhancer() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		pageCategorySearcherMap = new HashMap<Locale, CategoryHierarchySearcher>();


		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-category-index") + "...");

			String pageCategoryIndexName = resourceMap.get("page-category-index");
			String categorySuperCategoryIndexName = resourceMap.get("category-super-category-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(pageCategoryIndexName + "\t" + categorySuperCategoryIndexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (pageCategoryIndexName != null && categorySuperCategoryIndexName != null) {
					CategoryHierarchySearcher categoryHierarchySearcher = new CategoryHierarchySearcher(pageCategoryIndexName, categorySuperCategoryIndexName, cacheName, minFreq);
					pageCategorySearcherMap.put(new Locale(languages[i]), categoryHierarchySearcher);
				}
				else {
					logger.warn(pageCategoryIndexName + " or " + categorySuperCategoryIndexName + " not found");
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		enhance(keywordArray, locale, 0);
	}

	public void enhance(Keyword[] keywordArray, Locale locale, int depth) {
		long begin = System.nanoTime();

		CategoryHierarchySearcher pageCategorySearcher = pageCategorySearcherMap.get(locale);
		String page;
		Sense sense;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				sense = keywordArray[i].getSense();

				page = sense.getPage();
				Set<String> entry = pageCategorySearcher.search(page, depth);
				Category category;
				Iterator<String> it = entry.iterator();
				for (int j = 0; it.hasNext(); j++) {
					category = new Category(it.next(), locale);
					keywordArray[i].addCategory(category);
				}

			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");

	}

	public List<Category> enhance(String page, Locale locale, int depth) {
		long begin = System.nanoTime();
		List<Category> categoryList = new ArrayList<Category>();
		CategoryHierarchySearcher pageCategorySearcher = pageCategorySearcherMap.get(locale);

		Set<String> entry = pageCategorySearcher.search(page, depth);
		Category category;
		Iterator<String> it = entry.iterator();
		for (int j = 0; it.hasNext(); j++) {
			category = new Category(it.next(), locale);
			categoryList.add(category);
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(page + " enhanced in " + nf.format(time) + " ns");
		return categoryList;
	}

	@Override
	public String toString() {
		return "CategoryHierarchyEnhancer{" +
				"config=" + config +
				", nf=" + nf +
				", automaticConfiguration=" + automaticConfiguration +
				", pageCategorySearcherMap=" + pageCategorySearcherMap +
				'}';
	}
}