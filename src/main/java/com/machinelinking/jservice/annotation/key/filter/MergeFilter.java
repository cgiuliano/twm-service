/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.annotation.key.NGram;
import org.apache.log4j.Logger;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/21/13
 * Time: 8:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class MergeFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>MergeFilter</code>.
	 */
	static Logger logger = Logger.getLogger(MergeFilter.class.getName());

	//todo: merge the the ngrams otherwise (Silvio Berlusconi ... Berusconi -> win the second)

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	//KeywordComparator keywordComparator;

	private static MergeFilter ourInstance;

	public static MergeFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new MergeFilter();
		}
		return ourInstance;
	}

	private MergeFilter() {
		//keywordComparator = new KeywordComparator();
	}

	/*class KeywordComparator implements Comparator<Keyword> {
		public int compare(Keyword k1, Keyword k2) {
			return (int) (k1.weight() - k2.weight());
		}
	}*/

	public void filter(Keyword[] keywordArray) {
		filter(keywordArray, null);
	}

	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();
		int count = 0;
		//Map<OldSense, List<Keyword>> map = new HashMap<OldSense, List<Keyword>>();

		for (int i = 0; i < keywordArray.length; i++) {
			//logger.debug(i + "\t" + keywordArray[i].getForm() + "\t" + keywordArray[i].weight() + "\t" + keywordArray[i].isFiltered());
			if (!keywordArray[i].isFiltered()) {

				for (int j = i + 1; j < keywordArray.length; j++) {
					//logger.debug("\t" + j + "\t" + keywordArray[j].getForm() + "\t" + keywordArray[j].weight() + "\t" + keywordArray[j].isFiltered());
					if (!keywordArray[j].isFiltered()) {
						//logger.debug("B\t " + i + "/" + j + "\t" + keywordArray[i].isFiltered() + "\t" + keywordArray[j].isFiltered());
						if (keywordArray[i].getSense() != null && keywordArray[i].getSense().equals(keywordArray[j].getSense())) {
							logger.warn(keywordArray[i].getForm() + ":" + keywordArray[i].getSense().getPage() + ":" + keywordArray[i].getNGramList().size() + " == " + keywordArray[j].getForm() + ":" + keywordArray[j].getSense().getPage() + ":" + keywordArray[j].getNGramList().size());
							/*List<Keyword> list = map.get(keywordArray[i].getSense());
							if (list == null) {
								list = new ArrayList<Keyword>();
								map.put(list);
							}
							list.add(keywordArray[i]);*/
							//if (keywordArray[i].getNGramList().size() >= keywordArray[j].getNGramList().size()) {
							if (keywordArray[i].getMaxNGramLength() >= keywordArray[j].getMaxNGramLength()) {
								keywordArray[j].setFiltered(true);
								//logger.warn(keywordArray[i].getNGramList().size() + " >= " + keywordArray[j].getNGramList().size());
								logger.warn(keywordArray[i].getMaxNGramLength() + " >= " + keywordArray[j].getMaxNGramLength());
								addNGrams(keywordArray[i], keywordArray[j].getNGramList());
								count++;
								logger.debug(keywordArray[i].getNGramList());
							}
							else {
								keywordArray[i].setFiltered(true);
								//logger.warn(keywordArray[i].getNGramList().size() + " < " + keywordArray[j].getNGramList().size());
								logger.warn(keywordArray[i].getMaxNGramLength() + " < " + keywordArray[j].getMaxNGramLength());
								addNGrams(keywordArray[j], keywordArray[i].getNGramList());
								count++;
								logger.debug(keywordArray[j].getNGramList());

							}
							logger.debug(keywordArray[i].isFiltered() + "\t" + keywordArray[j].isFiltered());

						}
						//else {
							//logger.warn(keywordArray[i].getForm() + ":" + keywordArray[i].getSense() + " != " + keywordArray[j].getForm() + ":" + keywordArray[j].getSense());
						//}
					}
				}
			}
		}

		logger.debug("========");
		logger.debug("i\tidf\tlf\tnorm\tweight\tfiltered\tform\tsense\tngrams");
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered()) {
				logger.debug(i + "\t" + keywordArray[i]);
			}

		}logger.debug("-----------");
	}

	public void addNGrams(Keyword keyword, List<NGram> nGramList) {


		for (int j = 0; j < nGramList.size(); j++) {
			boolean b = true;
			for (int i = 0; i < keyword.getNGramList().size(); i++) {

				if (keyword.getNGramList().get(i).intersects(nGramList.get(j))) {
					b = false;
				}
			}
			if (b) {
				keyword.addNGram(nGramList.get(j));
			}

		}
	}
}
