/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.annotation.topic.Topic;
import com.machinelinking.jservice.annotation.topic.TopicSet;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import eu.fbk.twm.index.PageTopicSearcher;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.utils.WeightedSet;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopicEnhancer extends AbstractEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TopicEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(TopicEnhancer.class.getName());

	public static final int DEFAULT_MAX_NUMBER_OF_TOPICS_TO_RETURN = 3;

	public static final int DEFAULT_MAX_NUMBER_OF_TOPICS_TO_PROCESS = 10;

	private int maxNumberOfTopicsToProcess;

	private int maxNumberOfTopicsToReturn;

	//public static final int DEFAULT_MAX_NUMBER_OF_KEYWORDS = 5;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static TopicEnhancer ourInstance;

	Configuration config;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageTopicSearcher> pageTopicSearcherMap;

	public static TopicEnhancer getInstance() {
		if (ourInstance == null) {
			ourInstance = new TopicEnhancer();
		}
		return ourInstance;
	}

	private TopicEnhancer() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();


			if (config.getProperty("max-number-of-topics-to-process") != null) {
				maxNumberOfTopicsToProcess = config.getInt("max-number-of-topics-to-process");
			}
			else {
				maxNumberOfTopicsToProcess = DEFAULT_MAX_NUMBER_OF_TOPICS_TO_PROCESS;
			}

			if (config.getProperty("max-number-of-topics-to-return") != null) {
				maxNumberOfTopicsToReturn = config.getInt("max-number-of-topics-to-return");
			}
			else {
				maxNumberOfTopicsToReturn = DEFAULT_MAX_NUMBER_OF_TOPICS_TO_RETURN;
			}

			//logger.debug(maxNumberOfKeywords + "\t" + maxNumberOfTopicsToReturn);


			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		pageTopicSearcherMap = new HashMap<Locale, PageTopicSearcher>();

		for (String language : languages) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(language);

			logger.debug("Topic loading " + language + "\t" + resourceMap.get("page-topics-index") + "...");
			String indexName = resourceMap.get("page-topics-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				PageTopicSearcher pageTopicSearcher = new PageTopicSearcher(indexName);
				pageTopicSearcher.loadCache(cacheName, minFreq);
				pageTopicSearcherMap.put(new Locale(language), pageTopicSearcher);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		enhance(keywordArray, locale, 0);
	}

	public void enhance(Keyword[] keywordArray, Locale locale, int weight) {
		long begin = System.nanoTime();

		// Load fake topic searcher
		PageTopicSearcher pageTopicSearcher = pageTopicSearcherMap.get(locale);

		String page;
		Sense[] senseArray;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				senseArray = keywordArray[i].getSenseArray();

				TopicSet[] topicSetArray = new TopicSet[senseArray.length];

				//The j < 1 part takes only the first sense
				for (int j = 0; j < senseArray.length && j < 1; j++) {
					page = senseArray[j].getPage();

					// from categories
					//todo: weight is not used any more
					WeightedSet weightedSet = pageTopicSearcher.search(page);

					weightedSet.normalize();
					topicSetArray[j] = new TopicSet(weightedSet);
					//logger.debug("\t" + j + "\t" + keywordArray[i].getForm() + "\t" + page + "\t" + topicSetArray[j]);

				}
				keywordArray[i].setTopicSetArray(topicSetArray);


			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");

	}

	public List<Topic> enhance(String page, Locale locale) {
		long begin = System.nanoTime();

		PageTopicSearcher pageTopicSearcher = pageTopicSearcherMap.get(locale);
		//todo: weight is not used any more
		WeightedSet pageWeightedSet = pageTopicSearcher.search(page);
		List<Topic> pageTopicList = Topic.createTopicList(pageWeightedSet, maxNumberOfTopicsToReturn);

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(page + " enhanced in " + nf.format(time) + " ns");
		return pageTopicList;
	}

	@Override
	public String toString() {
		return "TopicEnhancer{" +
				"config=" + config +
				", nf=" + nf +
				", automaticConfiguration=" + automaticConfiguration +
				", pageTopicSearcherMap=" + pageTopicSearcherMap +
				'}';
	}
}