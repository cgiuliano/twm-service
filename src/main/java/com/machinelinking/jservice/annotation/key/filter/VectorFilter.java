package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.index.PageVectorSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/23/13
 * Time: 8:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class VectorFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>VectorFilter</code>.
	 */
	static Logger logger = Logger.getLogger(VectorFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageVectorSearcher> pageVectorSearcherMap;

	private static VectorFilter ourInstance;

	Configuration config;

	public static VectorFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new VectorFilter();
		}
		return ourInstance;
	}

	private VectorFilter() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		pageVectorSearcherMap = new HashMap<Locale, PageVectorSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-vector-index") + "...");

			String indexName = resourceMap.get("page-vector-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (indexName != null) {
					PageVectorSearcher pageVectorSearcher = new PageVectorSearcher(indexName);
					pageVectorSearcher.loadCache(cacheName, minFreq);

					//pageVectorSearcher.loadCache(cacheName, 10000);
					pageVectorSearcherMap.put(new Locale(languages[i]), pageVectorSearcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void filter(Keyword[] keywordArray) {
		filter(keywordArray, null);
	}

	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		PageVectorSearcher pageVectorSearcher = pageVectorSearcherMap.get(locale);
		String pi, pj = null;

		double sum;
		int count;
		double[][] matrix = new double[keywordArray.length][keywordArray.length];
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].getSense() != null) {
				pi = keywordArray[i].getSense().getPage();
				sum = 0;
				count = 0;
				for (int j = 0; j < i; j++) {
					sum += matrix[i][j];
					count++;
					//logger.debug(i + "\t" + j + "\t" + pi + "\t?\t" + matrix[i][j] + "\t" + sum + "\t" + count);
				}
				for (int j = i + 1; j < keywordArray.length; j++) {
					if (!keywordArray[j].isFiltered() && keywordArray[j].getSense() != null) {
						pj = keywordArray[j].getSense().getPage();;
						if (!pi.equals(pj)) {
							long b = System.nanoTime();
							//todo: normalize?
							matrix[i][j] = matrix[j][i] = pageVectorSearcher.compareBOW(pi, pj);
							sum += matrix[i][j];
							count++;
							long e = System.nanoTime();
							//logger.debug(i + "\t" + j + "\t" + pi + "\t" + pj + "\t" + matrix[i][j] + "\t" + sum + "\t" + count + "\t" + nf.format(e - b));
						}
						else {
							//logger.debug(i + "\t" + j + "\t" + pi + "\t" + pj + "\t0\t" + sum + "\t" + count);
						}
					}
				}
				if (count != 0) {
					keywordArray[i].setCorrelation(sum / (keywordArray.length - 1));
				}

				logger.debug(i + "\t-\t" + pi + "\t-\t" + keywordArray[i].getCorrelation());

			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords boosted in " + nf.format(time) + " ns");
	}

}
