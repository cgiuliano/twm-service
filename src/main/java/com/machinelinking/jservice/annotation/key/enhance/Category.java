/*
 * Copyright (2012) Machine Categorying srl
 *
 * Machine Categorying reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Categorying.  A license under Machine Categorying's
 * rights in the Program may be available directly from Machine Categorying.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.CharacterTable;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/29/12
 * ServiceTime: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class Category implements Serializable, Comparable<Category> {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Category</code>.
	 */
	static Logger logger = Logger.getLogger(Category.class.getName());

	//public static final String WIKIPEDIA = "Wikipedia";

	private static final String WIKIPEDIA_ADDRESS = ".wikipedia.org/wiki/";

	//Wikipedia automatically replaces Category with the language specific labale
	private static final String CATEGORY_PREFIX = "Category:";

	private static final String HTTP_PREFIX = "http://";

	URL url;

	//String resource;

	String label;

	public Category(String label, Locale locale) {
		this.label = label.replace(CharacterTable.LOW_LINE, CharacterTable.SPACE);
		try {
			this.url = new URL(HTTP_PREFIX + locale.getLanguage() + WIKIPEDIA_ADDRESS + CATEGORY_PREFIX + label);
		} catch (MalformedURLException e) {
			logger.equals(e);
		}
		//this.resource = resource;
	}

	public String getLabel() {
		return label;
	}

	public URL getURL() {
		return url;
	}

	public int compareTo(Category o) {
		return 0;
	}

	//
	public String toString() {
		StringWriter w = new StringWriter();
		try {
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			g.writeStringField("id", label);
			g.writeStringField("url", url.toString());
			g.writeEndObject();
			g.close();

		} catch (IOException e) {
			logger.error(e);
		}
		return w.toString();
	} // end toString

}