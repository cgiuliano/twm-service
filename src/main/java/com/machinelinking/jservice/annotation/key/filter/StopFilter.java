package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/23/13
 * Time: 9:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class StopFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>StopFilter</code>.
	 */
	static Logger logger = Logger.getLogger(StopFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static StopFilter ourInstance;

	Configuration config;

	int stopWordSize;

	private static Pattern tabPattern = Pattern.compile("\t");

	private final HashMap<Locale, Set<String>> stopWordMap;

	public static synchronized StopFilter getInstance() {
		if (ourInstance == null) {
			try {
				ourInstance = new StopFilter();
			} catch (IOException e) {
				logger.error(e);
			}
		}
		return ourInstance;
	}

	private StopFilter() throws IOException {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}

		//stopWordSize = config.getInt("stop-words-size");
		//logger.info("language model size " + stopWordSize);
		AutomaticConfiguration automaticConfiguration = AutomaticConfiguration.getInstance();

		String stopSetDir = config.getString("stop-words-dir");
		if (!stopSetDir.endsWith(File.separator)) {
			stopSetDir += File.separator;
		}

		String[] languages = automaticConfiguration.getLanguages();
		stopWordMap = new HashMap<Locale, Set<String>>();
		//logger.info("creating a language detector for " + languages.length + " languages...");
		for (int i = 0; i < languages.length; i++) {
			//Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			//logger.debug(languages[i] + "\t" + resourceMap);
			//logger.debug(resourceMap.get("ngram.csv"));
			//String unigramFileName = resourceMap.get("ngram.csv");
			stopWordMap.put(new Locale(languages[i]), loadStopSet(stopSetDir + languages[i] + ".stop"));
		}
	}

	private Set<String> loadStopSet(String name) {
		logger.info("reading stop words from" + name + "...");
		Set<String> set = new HashSet<String>();
		File file = new File(name);
		if (!file.exists()) {
			return set;
		}
		try {
			LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			String line;
			while ((line = lnr.readLine()) != null) {
				line = line.trim();
				if (line.length() > 0) {
					//logger.debug(line);
					// lowercase
					set.add(line);
					// capitalized
					set.add(Character.toUpperCase(line.charAt(0)) + line.substring(1, line.length()));
				}
			}
			lnr.close();

		} catch (IOException e) {
			logger.error(e);
		}
		logger.debug(set);
		logger.info(set.size() + " stop words read from " + name);
		return set;
	}

	@Override
	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		Set<String> set = stopWordMap.get(locale);
		String tokenizedForm;
		int count = 0;
		for (int i = 0; i < keywordArray.length; i++) {
			tokenizedForm = keywordArray[i].getTokenizedForm();
			if (set.contains(tokenizedForm)) {
				//logger.debug("stop filtered " + keywordArray[i]);
				keywordArray[i].setFiltered(true);
				count++;
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + keywordArray.length + " keywords filtered out in " + nf.format(time) + " ns");
	}
}
