/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key;

import com.machinelinking.jservice.annotation.key.enhance.*;
import com.machinelinking.jservice.annotation.topic.TopicSet;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.ClassResource;
import eu.fbk.twm.classifier.ContextualSense;
import eu.fbk.twm.classifier.Sense;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/22/12
 * ServiceTime: 5:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class Keyword implements Serializable, Comparable<Keyword> {
	//
	private static final long serialVersionUID = 1021396602591514749L;

	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Keyword</code>.
	 */
	static Logger logger = Logger.getLogger(Keyword.class.getName());

	//protected int nGramLength;

	String form;

	List<NGram> nGramList;

	//double weight;

	DecimalFormat nf = new DecimalFormat("###0.000");

	DecimalFormat pf = new DecimalFormat("###0.000");

	DecimalFormat sf = new DecimalFormat("###,###,###");

	double idf;

	double lf;

	double norm;

	List<Link> linkList;

	List<Type> typeList;

	List<Form> formList;

	List<Image> imageList;

	List<ClassResource> airpediaClassList;

	List<Category> categoryList;

	Map<String, String> crossLangMap;

	//Sense sense;

	Sense[] senseArray;

	TopicSet[] topicSetArray;

	String pageAbstract;

	String type;

	//List<Topic> topicList;

	String tokenizedForm;

	double correlation;

	boolean filtered;

	double traffic;

	int maxNGramLength;

	int maxSenseIndex;

	double multiplier = 1;

	public double getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(double multiplier) {
		this.multiplier = multiplier;
	}

	public void clearAirpediaList() {
		airpediaClassList.clear();
	}

	public Keyword(String form, String tokenizedForm) {
		this.form = form;
		this.tokenizedForm = tokenizedForm;
		nGramList = new ArrayList<NGram>();
		linkList = new ArrayList<Link>();
		typeList = new ArrayList<Type>();
		formList = new ArrayList<Form>();
		imageList = new ArrayList<Image>();
		categoryList = new ArrayList<Category>();
		//topicList = new ArrayList<Topic>();
		crossLangMap = new HashMap<String, String>();
		airpediaClassList = new ArrayList<ClassResource>();
		norm = 1;
		correlation = 1;
		traffic = 1;
		filtered = false;
		type = TypeEnhancer.UNKNOWN_TYPE;
		maxSenseIndex = 0;
	} // constructor

	public int getMaxSenseIndex() {
		return maxSenseIndex;
	}

	public void setMaxSenseIndex(int maxSenseIndex) {
		this.maxSenseIndex = maxSenseIndex;
	}

	public TopicSet[] getTopicSetArray() {
		return topicSetArray;
	}

	public void setTopicSetArray(TopicSet[] topicArray) {
		this.topicSetArray = topicArray;
	}

	public Sense[] getSenseArray() {
		return senseArray;
	}

	public void setSenseArray(Sense[] senseArray) {
		this.senseArray = senseArray;
	}

	/*public List<Topic> getTopicList() {
		return topicList;
	}

	public void setTopicList(List<Topic> topicList) {
		this.topicList = topicList;
	}*/

	public double getTraffic() {
		return traffic;
	}

	public void setTraffic(double traffic) {
		this.traffic = traffic;
	}

	public String getPageAbstract() {
		return pageAbstract;
	}

	public void setPageAbstract(String pageAbstract) {
		this.pageAbstract = pageAbstract;
	}

	public String getTokenizedForm() {
		return tokenizedForm;
	}

	public double getCorrelation() {
		return correlation;
	}

	public void setCorrelation(double correlation) {
		this.correlation = correlation;
	}

	/*public int nGramLength() {
		return nGramList.get(0).getNGramLength();
	} */

	public boolean hasSense() {
		return senseArray != null;
	}

	public boolean hasTopic() {
		return topicSetArray != null;
	}

	public boolean hasAirpediaClass() {
		return airpediaClassList != null || airpediaClassList.size() == 0;
	}

	public boolean hasAirpediaClass(String classToTest) {
		for (ClassResource c : airpediaClassList) {
			if (c.getLabel().equals(classToTest)) {
				return true;
			}
		}
		return false;
	}

	public int length() {
		return form.length();
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public boolean isFiltered() {
		return filtered;
	}

	public void setFiltered(boolean filtered) {
		this.filtered = filtered;
	}

	@Deprecated
	public String getType() {
		return type;
	}

	@Deprecated
	public void setType(String type) {
		this.type = type;
	}

	public Sense getSense() {
		//logger.warn(maxSenseIndex+"\t"+senseArray[maxSenseIndex]);
		if (senseArray == null) {
			return new ContextualSense();
		}
		return senseArray[maxSenseIndex];
	}

	public TopicSet getTopic() {
		//logger.warn(maxSenseIndex+"\t"+topicArray[maxSenseIndex]);

		if (topicSetArray == null) {
			return new TopicSet();
		}
		return topicSetArray[maxSenseIndex];
	}

	/*public void setSense(Sense sense) {
		this.sense = sense;
	}*/

	public double getNorm() {
		return norm;
	}

	public void setNorm(double norm) {
		this.norm *= norm;
	}

	public void setCrossLangMap(Map<String, String> crossLangMap) {
		this.crossLangMap = crossLangMap;
	}

	public Map<String, String> getCrossLangMap() {
		return crossLangMap;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public List<Type> getTypeList() {
		return typeList;
	}

	public List<Form> getFormList() {
		return formList;
	}

	public void addCategory(Category category) {
		categoryList.add(category);
	}

	public void addType(Type type) {
		typeList.add(type);
	}


	public void addAirpediaClass(ClassResource airpediaClass) {
		airpediaClassList.add(airpediaClass);
	}


	public List<ClassResource> getAirpediaClassList() {
		return airpediaClassList;
	}

	public void addForm(Form form) {
		formList.add(form);
	}

	public void addLink(Link link) {
		linkList.add(link);
	}

	public void addImage(Image image) {
		imageList.add(image);
	}

	public List<Link> getLinkList() {
		return linkList;
	}

	public List<Image> getImageList() {
		return imageList;
	}

	public String form() {
		return form;
	}

	public void addNGram(List<NGram> nGramList) {
		for (int i = 0; i < nGramList.size(); i++) {
			addNGram(nGramList.get(i));
		}
	}

	public void addNGram(NGram ngram) {
		if (ngram.length() > maxNGramLength) {
			maxNGramLength = ngram.getNGramLength();
		}
		nGramList.add(ngram);
	}

	public List<NGram> getNGramList() {
		return nGramList;
	}

	public double getIdf() {
		return idf;
	}

	public void setIdf(double idf) {
		this.idf = idf;
	}

	public double getLf() {
		return lf;
	}

	public void setLf(double lf) {
		this.lf = lf;
	}

	public int getMaxNGramLength() {
		return maxNGramLength;
	}

	public double weight() {
		//logger.debug(idf + " * " + lf + " * " +  Math.log(1 + nGramList.size()) + " * " + norm + " = " + idf * lf * Math.log(1 + nGramList.size()) * norm);
		//return maxNGramLength * idf * lf * Math.log(1 + nGramList.size()) * norm * correlation;
		//return Math.log(1 + maxNGramLength * idf * lf * nGramList.size() * correlation) * norm;
		if (isFiltered()) {
			return 0;
		}

		//todo: rescale using the frequency of the sense (page) or the page access
		if (hasSense()) {
			//return Math.log10(1 + (maxNGramLength * lf * nGramList.size() * correlation) / idf) * norm * sense.getProbability();
			///return idf * lf * norm * (sense.getProbability() == 0  ? 0 : 1);

			//this score is OK for page text disambiguation
			//return sense.getProbability() * Math.log10(1 + nGramList.size()) * Math.log10(1 + maxNGramLength) * correlation * norm;

			////return idf * lf * Math.log10(1 + nGramList.size()) * Math.log10(1 + maxNGramLength) * norm * correlation;
			//logger.trace(tokenizedForm + "\t" + idf + "\t" +lf + "\t" + Math.log10(1 + nGramList.size()) + "\t" + Math.log10(1 + maxNGramLength) + "\t"+norm+"\t"+multiplier * idf * lf * Math.log10(1 + nGramList.size()) * Math.log10(1 + maxNGramLength) * traffic * norm * correlation);
			return multiplier * idf * lf * Math.log10(1 + nGramList.size()) * Math.log10(1 + maxNGramLength) * traffic * norm * correlation; // PRODUCTION
//			return idf * Math.log10(1 + nGramList.size()) * Math.log10(1 + maxNGramLength) * traffic * norm * correlation; // TEST
			//return idf *  log2(1 + nGramList.size()) * log2(1 + maxNGramLength) * traffic * norm * correlation;
			//
			///return senseArray[maxSenseIndex].getCombo() *idf *  log2(1 + nGramList.size()) * log2(1 + maxNGramLength) * traffic * norm * correlation;
		}
		////return Math.log10(1 + (maxNGramLength * lf * nGramList.size() * correlation) / idf) * norm;
		//return idf * lf * Math.log10(1 + nGramList.size())* Math.log10(1 + maxNGramLength) * norm;
		//logger.trace(tokenizedForm + "\t" + idf + "\t" +lf + "\t" + Math.log10(1 + nGramList.size()) + "\t" + Math.log10(1 + maxNGramLength) + "\t"+norm + "\t"+(idf * Math.log10(1 + nGramList.size()) * Math.log10(1 + maxNGramLength) * norm));
		return idf * Math.log10(1 + nGramList.size()) * Math.log10(1 + maxNGramLength) * norm;
		//return idf * log2(1 + nGramList.size())* log2(1 + maxNGramLength) * norm;
		//return maxNGramLength * idf * lf * nGramList.size() * correlation * norm;


		//return nGramList.size() * idf * lf * norm;
		//return Math.log(1 + idf * lf * nGramList.size()) * correlation * norm;
		////return Math.log(1 + maxNGramLength * idf * lf * nGramList.size()) * correlation * norm;
		//return Math.log(1 + maxNGramLength * idf * lf * tf() * correlation) * norm;
	}

	public static final double LOG2 = Math.log(2);

	public double log2(double d) {
		return Math.log(d) / LOG2;
	}

	public double confidence() {
		return Math.pow(2, weight());
	}

	public int tf() {
		int tf = 0;
		for (int i = 0; i < nGramList.size(); i++) {
			if (!nGramList.get(i).isFiltered()) {
				tf++;
			}
		}
		return tf;
	}

	public int compareTo(Keyword o) {
		double diff = weight() - o.weight();
		if (diff == 0) {
			return 0;
		}
		else if (diff > 0) {
			return -1;
		}
		return 1;
	}

	//
	public boolean equals(Object obj) {
		if (obj instanceof Keyword) {
			return equals((Keyword) obj);
		}

		return false;
	} // end equals


	public boolean contains(Keyword anotherKeyword) {
		for (int i = 0; i < nGramList.size(); i++) {
			for (int j = 0; j < anotherKeyword.nGramList.size(); j++) {
				if (nGramList.get(i).contains(anotherKeyword.nGramList.get(j))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns <code>true</code> if this keyword has at least one ngram that follows
	 * a ngram in the specified keyword; <code>false</code> otherwise.
	 *
	 * @param anotherKeyword keyword whose ngrams must be tested.
	 * @return <code>true</code> if at least one ngrams follows a ngram in the specified keyword.
	 */
	public boolean follows(Keyword anotherKeyword) {
		for (int i = 0; i < nGramList.size(); i++) {
			for (int j = 0; j < anotherKeyword.nGramList.size(); j++) {
				if (nGramList.get(i).follows(anotherKeyword.nGramList.get(j))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns <code>true</code> if this keyword has at least one ngram that precedes
	 * a ngram in the specified keyword; <code>false</code> otherwise.
	 *
	 * @param anotherKeyword keyword whose ngrams must be tested.
	 * @return <code>true</code> if at least one ngrams preceded a ngram in the specified keyword; <code>false</code> otherwise.
	 */
	public boolean precedes(Keyword anotherKeyword) {
		for (int i = 0; i < nGramList.size(); i++) {
			for (int j = 0; j < anotherKeyword.nGramList.size(); j++) {
				if (nGramList.get(i).precedes(anotherKeyword.nGramList.get(j))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns <code>true</code> if at least a pair of ngrams are adjacent; <code>false</code> otherwise.
	 *
	 * @param anotherKeyword keyword whose ngrams must be tested.
	 * @return <code>true</code> if at least a pair of ngrams are adjacent; <code>false</code> otherwise.
	 */
	public boolean isAdjacentTo(Keyword anotherKeyword) {
		for (int i = 0; i < nGramList.size(); i++) {
			for (int j = 0; j < anotherKeyword.nGramList.size(); j++) {
				if (nGramList.get(i).isAdjacentTo(anotherKeyword.nGramList.get(j))) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isNom() {
		if (type == null) {
			return false;
		}

		return type.equals(TypeEnhancer.NOM_TYPE);
	}

	public boolean isNam() {
		if (type == null) {
			return false;
		}

		return type.equals(TypeEnhancer.NAM_TYPE);
	}

	/**
	 * Returns <code>true</code> if at least a pair of ngrams intersect; <code>false</code> otherwise.
	 *
	 * @param anotherKeyword keyword whose ngrams must be tested.
	 * @return <code>true</code> if at least a pair of ngrams intersect; <code>false</code> otherwise.
	 */
	public boolean intersects(Keyword anotherKeyword) {
		for (int i = 0; i < nGramList.size(); i++) {
			for (int j = 0; j < anotherKeyword.nGramList.size(); j++) {
				if (nGramList.get(i).intersects(anotherKeyword.nGramList.get(j))) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		/*return "Keyword{" +
						"form='" + form + '\'' +
						", nGramList=" + nGramList +
						", idf=" + idf +
						", lf=" + lf +
						", norm=" + norm +
						", weight=" + weight() +
						", linkList=" + linkList +
						", formList=" + formList +
						", crossLangMap=" + crossLangMap +
						", sense=" + sense +
						", type='" + type + '\'' +
						", filtered=" + filtered +
						'}'; */
		//return form + "\t" + nGramList.size() + "\t" + nf.format(idf) + "\t" + nf.format(lf) + "\t" + nf.format(norm) + "\t" + nf.format(weight()) + "\t" + (sense != null ? sense.getId() : "-") + "\t" + filtered;

		//return nf.format(idf) + "\t" + nf.format(lf) + "\t" + pf.format(norm) + "\t" + pf.format(weight()) + "\t" + (filtered ? "T" : "F") + "\t" + tokenizedForm + "\t" + (sense != null ? sense.getId() : "-") + "\t" + nGramList.size() + "\t" + nGramList;
		//return pf.format(idf) + "\t" + pf.format(lf) + "\t" + maxNGramLength + "\t" + nGramList.size() + "\t" + pf.format(correlation) + "\t" + pf.format(traffic) + "\t" + pf.format(weight()) + "\t" + (filtered ? "T" : "F") + "\t" + tokenizedForm + "\t" + (sense != null ? sense.getId() : "-") + "\t" + pf.format(norm);
		return pf.format(idf) + "\t" + pf.format(lf) + "\t" + maxNGramLength + "\t" + nGramList.size() + "\t" + pf.format(correlation) + "\t" + pf.format(traffic) + "\t" + pf.format(weight()) + "\t" + tokenizedForm + "\t" + (hasSense() ? getSense().getPage() : "-") + "\t" + pf.format(norm);
	}
} // end class Keyword


