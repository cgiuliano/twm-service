/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.annotation.key.KeywordWeightComparator;
import org.apache.log4j.Logger;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
@Deprecated public class RankFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>RankFilter</code>.
	 */
	static Logger logger = Logger.getLogger(RankFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	KeywordWeightComparator keywordWeightComparator;

	private static RankFilter ourInstance;

	public static final double DEFAULT_RANK = 0.2;

	public static RankFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new RankFilter();
		}
		return ourInstance;
	}

	private RankFilter() {
		keywordWeightComparator = new KeywordWeightComparator();
	}

	@Override
	public void filter(Keyword[] keywordArray, Locale locale) {
		filter(keywordArray, locale, DEFAULT_RANK);
	}

	public void filter(Keyword[] keywordArray) {
		filter(keywordArray, null);
	}

	public void filter(Keyword[] keywordArray, Locale locale, double threshold) {
		long begin = System.nanoTime();
		//logger.debug("B\t" + Arrays.toString(keywordArray));
		Arrays.sort(keywordArray, keywordWeightComparator);
		int count = 0;
		int size = (int) Math.round((double) (1 - threshold) * keywordArray.length);
		logger.debug("size = " + (1 - threshold + " * " + keywordArray.length + " = " +size));
		for (int i = 0; i < size; i++) {
			//logger.debug("filtered " + keywordArray[i]);
			if (keywordArray[i].weight() == 0) {
				keywordArray[i].setFiltered(true);
			}

			count++;
		}
		for (int i = size; i < keywordArray.length; i++) {
				//logger.debug("filtered " + keywordArray[i]);
				keywordArray[i].setFiltered(true);
				count++;
		}

		//logger.debug("A\t" + Arrays.toString(keywordArray));
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + keywordArray.length + " (" + (keywordArray.length - count) + ") filtered (t=" + threshold + ")  in " + nf.format(time) + " ns");
	}

}
