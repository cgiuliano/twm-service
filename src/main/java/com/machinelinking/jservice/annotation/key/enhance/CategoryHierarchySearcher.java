/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.index.CategorySuperCategorySearcher;
import eu.fbk.twm.index.PageCategorySearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 7/25/13
 * Time: 3:09 PM
 * To change this template use File | Settings | File Templates.
 * <p/>
 *
 * @see com.machinelinking.jservice.annotation.topic.PageTopicSearcher
 */
public class CategoryHierarchySearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CategoryHierarchySearcher</code>.
	 */
	static Logger logger = Logger.getLogger(CategoryHierarchySearcher.class.getName());

	private static final DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private PageCategorySearcher pageCategorySearcher;

	private CategorySuperCategorySearcher categorySuperCategorySearcher;

	public CategoryHierarchySearcher(String pageIndex, String categoryIndex) throws IOException {
		pageCategorySearcher = new PageCategorySearcher(pageIndex);
		categorySuperCategorySearcher = new CategorySuperCategorySearcher(categoryIndex);
	}

	public CategoryHierarchySearcher(String pageIndex, String categoryIndex, String cacheName, int minFreq) throws IOException {
		this(pageIndex, categoryIndex);
		pageCategorySearcher.loadCache(cacheName, minFreq);
		categorySuperCategorySearcher.loadCache(cacheName, minFreq);
	}

	public String[] search(String[] categories, Set<String> set, int depth, int maxDepth) {
		if (depth > maxDepth) {
			return new String[0];
		}
		//logger.debug("searching: " + Arrays.toString(categories) + "\t" + depth + "...");
		for (int i = 0; i < categories.length; i++) {
			////logger.debug(i + "\t" + categories[i]);

			if (!set.contains(categories[i])) {
				set.add(categories[i]);
				//logger.debug(depth + "\t" + set);
				try {
					String[] superCategories = categorySuperCategorySearcher.search(categories[i]);
					search(superCategories, set, depth + 1, maxDepth);
				} catch (Exception e) {
					logger.error(e);
				}
			}

		}
		return new String[0];
	}

	public Set<String> search(String page, int maxDepth) {
		String[] categories = pageCategorySearcher.search(page);
		//logger.debug(Arrays.toString(categories));
		Set<String> set = new HashSet<String>();
		search(categories, set, 1, maxDepth);
		return set;
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}
		PropertyConfigurator.configure(logConfig);
		//java -cp dist/jservice.jar com.machinelinking.annotation.key.enhance.CategoryHierarchySearcher /data/models/wikipedia/en/20130604/enwiki-20130604-page-category-index/ /data/models/wikipedia/en/20130604/enwiki-20130604-category-super-category-index/ List_of_airlines_of_India 1
		//java -cp dist/jservice.jar com.machinelinking.annotation.key.enhance.CategoryHierarchySearcher
		CategoryHierarchySearcher categoryHierarchySearcher = new CategoryHierarchySearcher(args[0], args[1]);
		Set<String> set = categoryHierarchySearcher.search(args[2], Integer.parseInt(args[3]));
		logger.info(set);
	}
}
