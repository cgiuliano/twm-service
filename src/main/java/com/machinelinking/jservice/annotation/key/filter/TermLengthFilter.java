package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/23/13
 * Time: 9:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class TermLengthFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TermLengthFilter</code>.
	 */
	static Logger logger = Logger.getLogger(TermLengthFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static TermLengthFilter ourInstance;

	Configuration config;

	int stopTermLength;

	public static synchronized TermLengthFilter getInstance() {
		if (ourInstance == null) {
			try {
				ourInstance = new TermLengthFilter();
			} catch (IOException e) {
				logger.error(e);
			}
		}
		return ourInstance;
	}

	private TermLengthFilter() throws IOException {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
		stopTermLength = config.getInt("stop-term-length");
	}

	public void filter(Keyword[] keywordArray) {
		 filter(keywordArray, null);
	}

	@Override
	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		String form;
		int count = 0;
		for (int i = 0; i < keywordArray.length; i++) {
			form = keywordArray[i].getTokenizedForm();
			if (form.length() <= stopTermLength) {
				//logger.debug("stop filtered " + keywordArray[i]);
				keywordArray[i].setFiltered(true);
				count++;
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + keywordArray.length + " keywords filtered out in " + nf.format(time) + " ns");
	}
}
