package com.machinelinking.jservice.annotation.key;

import com.machinelinking.jservice.annotation.topic.PageTopicSearcher;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import eu.fbk.twm.utils.Defaults;
import org.apache.commons.cli.*;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.utils.lsa.LSI;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.math.Node;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Token;
import eu.fbk.twm.utils.analysis.Tokenizer;
import eu.fbk.twm.classifier.ContextualSense;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.index.OneExamplePerSenseSearcher;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.xerial.snappy.SnappyInputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/6/13
 * Time: 11:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class OneExamplePerSenseClassifier3 {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>OneExamplePerSenseClassifier3</code>.
	 */
	static Logger logger = Logger.getLogger(OneExamplePerSenseClassifier3.class.getName());

	protected LSI lsi;

	protected OneExamplePerSenseSearcher oneExamplePerSenseSearcher;

	protected static DecimalFormat rf = new DecimalFormat("###,###,##0.000000");

	protected static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	protected static DecimalFormat mf = new DecimalFormat("#.000");

	protected boolean normalized;

	Map<Integer, String> termMap;
	Configuration config;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageTopicSearcher> pageTopicSearcherMap;

	public static final int DEFAULT_MAX_NUMBER_OF_TOPICS_TO_PROCESS = 10;

	private int maxNumberOfTopicsToProcess;

	public OneExamplePerSenseClassifier3(LSI lsi, OneExamplePerSenseSearcher oneExamplePerSenseSearcher, Map<Integer, String> termMap) {
		this.lsi = lsi;
		this.oneExamplePerSenseSearcher = oneExamplePerSenseSearcher;
		normalized = true;
		this.termMap = termMap;
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();


			if (config.getProperty("max-number-of-topics-to-process") != null) {
				maxNumberOfTopicsToProcess = config.getInt("max-number-of-topics-to-process");
			}
			else {
				maxNumberOfTopicsToProcess = DEFAULT_MAX_NUMBER_OF_TOPICS_TO_PROCESS;
			}

			//logger.debug(maxNumberOfKeywords + "\t" + maxNumberOfTopicsToReturn);


			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			//init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}


	}

	void init(String[] languages) {
		pageTopicSearcherMap = new HashMap<Locale, PageTopicSearcher>();


		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-category-index") + "...");

			String pageCategoryIndexName = resourceMap.get("page-category-index");
			String categorySuperCategoryIndexName = resourceMap.get("category-super-category-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(pageCategoryIndexName + "\t" + categorySuperCategoryIndexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			String dir = config.getString("category-mapping-dir");
			if (!dir.endsWith(File.separator)) {
				dir += File.separator;
			}

			try {
				if (pageCategoryIndexName != null && categorySuperCategoryIndexName != null) {

					File file = new File(dir + languages[i] + ".properties");
					logger.debug("reading topic mappings from " + file + "...");
					Properties properties = new Properties();
					properties.load(new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")));
					logger.debug(properties.size() + " category mappings read");
					PageTopicSearcher pageTopicSearcher = new PageTopicSearcher(pageCategoryIndexName, categorySuperCategoryIndexName, cacheName, minFreq, properties);
					pageTopicSearcherMap.put(new Locale(languages[i]), pageTopicSearcher);
				}
				else {
					logger.warn(pageCategoryIndexName + " or " + categorySuperCategoryIndexName + " not found");
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void classify(File f, boolean compress) throws IOException {
		logger.info("classifying " + f);
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = null;
		if (compress) {
			lnr = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(f)), "UTF-8"));
		}
		else {
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		}

		//Tokenizer tokenizer = HardTokenizer.getInstance();
		String line;
		String[] s;
		int tot = 0;
		//Node[][] nodes;
		logger.info("totalFreq\tsize\ttime (ms)\tdate");
		int tp = 0, fp = 0, fn = 0;
		while ((line = lnr.readLine()) != null) {
			s = tabPattern.split(line);
			//nodes = mapInstance(s);
			Sense[] senses = classify(s);
			String page = "";
			if (senses.length > 0) {
				page = senses[0].getPage();
				/*logger.info("i\tprior\tbow\tls\tcombo\tpage");
				for (int i = 0; i < senses.length; i++) {
					logger.info(i + "\t" + rf.format(senses[i].getPrior()) + "\t" + rf.format(senses[i].getBow()) + "\t" + rf.format(senses[i].getLs()) + "\t" + rf.format(senses[i].getCombo()) + "\t" + senses[i].getPage());
				} */
			}

			if (s[0].equals(page)) {
				tp++;
			}
			else {
				fp++;
				fn++;
			}
			logger.debug(tot + "\t" + tp + "\t" + fp + "\t" + fn + "\t" + s[0] + "\t" + s[1] + "\t" + page);

			tot++;
		}
		lnr.close();
		end = System.currentTimeMillis();

		double precision = (double) tp / (tp + fp);
		double recall = (double) tp / (tp + fn);
		double f1 = (2 * precision * recall) / (precision + recall);
		logger.debug(tot + "\t" + tp + "\t" + fp + "\t" + fn + "\t" + rf.format(precision) + "\t" + rf.format(recall) + "\t" + rf.format(f1));
		logger.info(df.format(tot) + "\t" + df.format(end - begin) + "\t" + new Date());

		logger.info("ending the process " + new Date() + "...");
	}

	public Sense[] classify(Token[] s, String form) {
		return classify(createBow(s), form);
	}

	public Sense[] classify(String[] s) {
		return classify(createBow(s), s[2]);
	}

	protected double dot(Node[] n1, Node[] n2) {
		double dot12 = Node.dot(n1, n2);
		double dot11 = Node.dot(n1, n1);
		double dot22 = Node.dot(n2, n2);
		double dot = dot12 / Math.sqrt(dot11 * dot22);
		return dot;
	}

	public static double dot(Node[] x, Node[] y, Map<Integer, String> termMap, String form, String page) {
		double sum = 0;
		int xlen = x.length;
		int ylen = y.length;
		int i = 0;
		int j = 0;
		while (i < xlen && j < ylen) {
			if (x[i].index == y[j].index) {

				//logger.debug(x[i].index + "\t" + x[i].value + "\t" + y[j].value + "\t" + x[i].value * y[j].value);

				logger.debug(form + "/" + page + "\t" + x[i].index + "\t" + termMap.get(x[i].index) + "\t" + mf.format(x[i].value) + "*" + mf.format(y[j].value) + "=" + mf.format(x[i].value * y[j].value));
				sum += x[i++].value * y[j++].value;
			}
			else {
				if (x[i].index > y[j].index) {
					++j;
				}
				else {
					++i;
				}
			}
		}
		logger.debug(form + "/" + page + "\t\t\t" + mf.format(sum));
		return sum;
	}

	public static Map<Integer, String> read(Reader in) throws IOException {
		long begin = System.currentTimeMillis();
		logger.info("\n\nreading index - term...");
		Map<Integer, String> termMap = new HashMap<Integer, String>();
		LineNumberReader lnr = new LineNumberReader(in);

		String line;
		String[] s;
		Integer id;
		while ((line = lnr.readLine()) != null) {
			line = line.trim();
			if (!line.startsWith("#")) {
				//s = line.split("\t");
				s = tabPattern.split(line);
				if (s.length == 2) {
					termMap.put(new Integer(s[0]), s[1]);
				}
			}
		}
		lnr.close();


		long end = System.currentTimeMillis();
		logger.debug(termMap.size() + " terms read in " + tf.format(end - begin));
		return termMap;
	}


	//variant strategy
	public Sense[] classifyVariant(BOW bow, String form) {


		//logger.debug(bow);
		long begin = System.nanoTime();
		//logger.debug("searching " + form);
		OneExamplePerSenseSearcher.Entry[] entries = oneExamplePerSenseSearcher.search(form);
		//logger.debug(Arrays.toString(entries));
		long end = System.nanoTime();

		Node[] bowVector = lsi.mapDocument(bow);

		Node[] lsVector = lsi.mapPseudoDocument(bowVector);

		Sense[] senses = new ContextualSense[entries.length];
		double[][] matrix = new double[senses.length][2];
		int maxBow = 0;
		int maxLs = 0;

		for (int i = 0; i < entries.length; i++) {

			matrix[i][0] = Node.dot(bowVector, entries[i].getBowVector());
			matrix[i][1] = Node.dot(lsVector, entries[i].getLsVector());
			logger.debug(i + "\t" + form + "\t" + entries[i].getPage() + "\t" + matrix[i][0] + "\t" + matrix[i][1]);
			if (matrix[i][0] > matrix[maxBow][0]) {
				maxBow = i;
			}
			if (matrix[i][1] > matrix[maxLs][1]) {
				maxLs = i;
			}

			//senses[i] = new ContextualSense(entries[i].getPage(), entries[i].getFreq(), bowKernel, lsKernel);
		}
		logger.debug("\t" + form + "\t\t" + matrix[maxBow][0] + "\t" + matrix[maxLs][1]);
		for (int i = 0; i < entries.length; i++) {

			double bowKernel = matrix[i][0] / matrix[maxBow][0];
			double lsKernel = 0;//matrix[i][1]/matrix[maxLs][1];
			senses[i] = new ContextualSense(entries[i].getPage(), entries[i].getFreq(), bowKernel, lsKernel);
		}

		Arrays.sort(senses, new Comparator<Sense>() {
			@Override
			public int compare(Sense sense, Sense sense2) {
				double diff = sense.getCombo() - sense2.getCombo();
				if (diff > 0) {
					return -1;
				}
				else if (diff < 0) {
					return 1;
				}
				return 0;
			}
		}
		);
		//logger.info("i\tprior\tbow\tls\tcombo\tpage");
		//for (int i = 0; i < senses.length; i++) {
		//	logger.info(i + "\t" + rf.format(senses[i].getPrior()) + "\t" + rf.format(senses[i].getBow()) + "\t" + rf.format(senses[i].getLs()) + "\t" + rf.format(senses[i].getCombo()) + "\t" + rf.format(senses[i].getCombo() * senses[i].getPrior())+  "\t" + senses[i].getPage());
		//}

		return senses;
	}

	//best strategy
	public Sense[] classify(BOW bow, String form) {

		//logger.debug(pageTopicSearcherMap.size());
		//PageTopicSearcher pageTopicSearcher = pageTopicSearcherMap.get(new Locale("en"));
		//logger.debug(pageTopicSearcher.toString());
		//logger.debug(bow);
		long begin = System.nanoTime();
		//logger.debug("searching " + form);
		OneExamplePerSenseSearcher.Entry[] entries = oneExamplePerSenseSearcher.search(form);
		//logger.debug(Arrays.toString(entries));
		long end = System.nanoTime();

		Node[] bowVector = lsi.mapDocument(bow);
		//logger.debug("bow\t" + Node.toString(bowVector));

		//if (normalized) {
		//	Node.normalize(bowVector);
		//}
		Node[] lsVector = lsi.mapPseudoDocument(bowVector);
		//bowVector.normalize();
		//if (normalized) {
		//	Node.normalize(lsVector);
		//}
		//logger.debug("bow\t"+bow);
		//logger.debug("bow\t" + Node.toString(bowVector));
		//logger.debug("lsi\t" + Node.toString(lsVector));
		double normBow, maxBow = 0;
		double normLs, maxLs = 0;
		Sense[] senses = new ContextualSense[entries.length];
		for (int i = 0; i < entries.length; i++) {
			//logger.debug(i + "\t" + entries[i].getPage() + "\t" + entries[i].getFreq());
			normBow = Node.norm(entries[i].getBowVector());
			normLs = Node.norm(entries[i].getLsVector());
			//logger.debug(i + "\t" + Node.toString(entries[i].getLsVector()));
			if (normBow > maxBow) {
				maxBow = normBow;
			}
			if (normLs > maxLs) {
				maxLs = normLs;
			}
			//logger.debug(i + "\t" + normBow + "\t" + normLs);
		}
		//logger.debug("-\t" + maxBow + "\t" + maxLs);
		//for (int i = 0; i < entries.length; i++) {
		//	Node.normalize(entries[i].getBowVector(), maxBow);
		//	Node.normalize(entries[i].getLsVector(), maxLs);
		////logger.debug(i + "\t" + Node.toString(entries[i].getLsVector()));
		//}

		//Node.normalize(bowVector, maxBow);
		//logger.debug("*\t" + Node.toString(lsVector));
		Node.normalize(bowVector);
		Node.normalize(lsVector, maxLs);
		//logger.debug("+\t" + Node.toString(lsVector));
		for (int i = 0; i < entries.length; i++) {

			//logger.debug(i + "\tB:" + Node.toString(entries[i].getBowVector()));
			//logger.debug(i + "\tB:" + Node.toString(entries[i].getLsVector()));

			Node.normalize(entries[i].getBowVector(), maxBow);
			Node.normalize(entries[i].getLsVector(), maxLs);

			//logger.debug(i + "\tA:" + Node.toString(entries[i].getBowVector()));
			//logger.debug(i + "\tA:" + Node.toString(entries[i].getLsVector()));
			//if (normalized) {
			//	Node.normalize(entries[i].getBowVector());
			//	Node.normalize(entries[i].getLsVector());
			//}
			double bowKernel = Node.dot(bowVector, entries[i].getBowVector());
			//double bowKernel = dot(bowVector, entries[i].getBowVector(),termMap, form, entries[i].getPage());
			double lsKernel =0;// Node.dot(lsVector, entries[i].getLsVector());
			//user_interface  application computer_interface abstract_data_type computer_networking software graphical_user_interfaces command
			//if (entries[i].getPage().endsWith("(computing)") || entries[i].getPage().endsWith("(computer_science)")||entries[i].getPage().endsWith("(user_interface)")||entries[i].getPage().endsWith("(application)")||entries[i].getPage().endsWith("(computer_interface)")||entries[i].getPage().endsWith("(abstract_data_type)")||entries[i].getPage().endsWith("(computer_networking)")||entries[i].getPage().endsWith("(software)")||entries[i].getPage().endsWith("(graphical_user_interfaces)")||entries[i].getPage().endsWith("(command)")) {
			/*if (entries[i].getPage().endsWith("(computing)") || entries[i].getPage().endsWith("(computer_science)")){
			//if (entries[i].getPage().toLowerCase().contains("computing") || entries[i].getPage().toLowerCase().contains("computer") || entries[i].getPage().toLowerCase().contains("software")){
				lsKernel += 0.0625;
				logger.debug("<" + i +">\t" + mf.format(entries[i].getFreq()) + "\t"+  mf.format(bowKernel) + "\t" + mf.format(lsKernel) + "\t" + entries[i].getPage() + "\t" + form);
			}*/

			/*WeightedSet weightedSet = pageTopicSearcher.search(entries[i].getPage(), 0);
			TopicSet topicSet = new TopicSet(weightedSet);
			List<Topic> topicList = topicSet.topicList();

			if (topicList.size() > 0) {
				Topic topic = topicList.get(0);
				//if (topic.getLabel().equalsIgnoreCase("spo")||topic.getLabel().equalsIgnoreCase("pol")||topic.getLabel().equalsIgnoreCase("mil")||topic.getLabel().equalsIgnoreCase("int")||topic.getLabel().equalsIgnoreCase("mus")||topic.getLabel().equalsIgnoreCase("eco")||topic.getLabel().equalsIgnoreCase("art")||topic.getLabel().equalsIgnoreCase("sci")) {
				//if (topic.getLabel().equalsIgnoreCase("spo")||topic.getLabel().equalsIgnoreCase("pol")||topic.getLabel().equalsIgnoreCase("mil")||topic.getLabel().equalsIgnoreCase("int")||topic.getLabel().equalsIgnoreCase("mus")||topic.getLabel().equalsIgnoreCase("arc")||topic.getLabel().equalsIgnoreCase("sto")||topic.getLabel().equalsIgnoreCase("tur")||topic.getLabel().equalsIgnoreCase("let")||topic.getLabel().equalsIgnoreCase("rel")||topic.getLabel().equalsIgnoreCase("art")||topic.getLabel().equalsIgnoreCase("com")||topic.getLabel().equalsIgnoreCase("dir")||topic.getLabel().equalsIgnoreCase("cul")||topic.getLabel().equalsIgnoreCase("med")||topic.getLabel().equalsIgnoreCase("app")||topic.getLabel().equalsIgnoreCase("amb")||topic.getLabel().equalsIgnoreCase("inf")) {
				if (topic.getLabel().equalsIgnoreCase("cpt")) {

					lsKernel = 0.05;
					//bowKernel=0;
					logger.debug("<" + i + ">\t" + mf.format(entries[i].getFreq()) + "\t" + mf.format(bowKernel) + "\t" + mf.format(lsKernel) + "\t" + entries[i].getPage() + "\t" + form + "\t" + topic);
				}
				else {
					logger.debug("[" + i + "]\t" + mf.format(entries[i].getFreq()) + "\t" + mf.format(bowKernel) + "\t" + mf.format(lsKernel) + "\t" + entries[i].getPage() + "\t" + form + "\t" + topic);
				}

			}
			else {
				logger.debug("[" + i + "]\t" + mf.format(entries[i].getFreq()) + "\t" + mf.format(bowKernel) + "\t" + mf.format(lsKernel) + "\t" + entries[i].getPage() + "\t" + form);
			}
        */
			//logger.debug(i + "\t" + entries[i].getPage() + "\t" + rf.format(bowKernel) + "\t" + rf.format(lsKernel) + "\t" + rf.format(entries[i].getFreq()));
			senses[i] = new ContextualSense(entries[i].getPage(), entries[i].getFreq(), bowKernel, lsKernel);

		}

		Arrays.sort(senses, new Comparator<Sense>() {
			@Override
			public int compare(Sense sense, Sense sense2) {
				double diff = sense.getCombo() - sense2.getCombo();
				if (diff > 0) {
					return -1;
				}
				else if (diff < 0) {
					return 1;
				}
				return 0;
			}
		}
		);
		//logger.info("i\tprior\tbow\tls\tcombo\tpage");
		//for (int i = 0; i < senses.length; i++) {
		//	logger.info(i + "\t" + rf.format(senses[i].getPrior()) + "\t" + rf.format(senses[i].getBow()) + "\t" + rf.format(senses[i].getLs()) + "\t" + rf.format(senses[i].getCombo()) + "\t" + rf.format(senses[i].getCombo() * senses[i].getPrior())+  "\t" + senses[i].getPage());
		//}

		return senses;
	}

	public Sense[] classify1(BOW bow, String form) {


		//logger.debug(bow);
		long begin = System.nanoTime();
		//logger.debug("searching " + form);
		OneExamplePerSenseSearcher.Entry[] entries = oneExamplePerSenseSearcher.search(form);
		//logger.debug(Arrays.toString(entries));
		long end = System.nanoTime();

		Node[] bowVector = lsi.mapDocument(bow);
		//logger.debug("bow\t" + Node.toString(bowVector));

		if (normalized) {
			Node.normalize(bowVector);
		}
		Node[] lsVector = lsi.mapPseudoDocument(bowVector);
		//bowVector.normalize();
		if (normalized) {
			Node.normalize(lsVector);
		}
		Sense[] senses = new ContextualSense[entries.length];
		for (int i = 0; i < entries.length; i++) {
			if (normalized) {
				Node.normalize(entries[i].getBowVector());
				Node.normalize(entries[i].getLsVector());
			}
			double bowKernel = Node.dot(bowVector, entries[i].getBowVector());
			double lsKernel = Node.dot(lsVector, entries[i].getLsVector());
			//logger.debug(i + "\t" + entries[i].getPage() + "\t" + rf.format(bowKernel) + "\t" + rf.format(lsKernel) + "\t" + rf.format(entries[i].getFreq()));
			senses[i] = new ContextualSense(entries[i].getPage(), entries[i].getFreq(), bowKernel, lsKernel);
		}

		Arrays.sort(senses, new Comparator<Sense>()

		{
			@Override
			public int compare(Sense sense, Sense sense2) {
				double diff = sense.getCombo() - sense2.getCombo();
				if (diff > 0) {
					return -1;
				}
				else if (diff < 0) {
					return 1;
				}
				return 0;
			}
		}

		);
		//logger.info("i\tprior\tbow\tls\tcombo\tpage");
		//for (int i = 0; i < senses.length; i++) {
		//	logger.info(i + "\t" + rf.format(senses[i].getPrior()) + "\t" + rf.format(senses[i].getBow()) + "\t" + rf.format(senses[i].getLs()) + "\t" + rf.format(senses[i].getCombo()) + "\t" + rf.format(senses[i].getCombo() * senses[i].getPrior())+  "\t" + senses[i].getPage());
		//}

		return senses;
	}

	private BOW createBow(Token[] tokenArray) {
		BOW bow = new BOW();
		for (int i = 0; i < tokenArray.length; i++) {
			bow.add(tokenArray[i].getForm().toLowerCase());
		}
		return bow;
	}

	private BOW createBow(String[] s) {
		Tokenizer tokenizer = HardTokenizer.getInstance();
		BOW bow = new BOW();
		String[] left = tokenizer.stringArray(s[2].toLowerCase());
		bow.addAll(left);
		if (s.length == 5) {
			String[] right = tokenizer.stringArray(s[4].toLowerCase());
			bow.addAll(right);
		}
		return bow;
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = query.split("\t");

			String context = s[0];
			if (s.length > 2) {
				context += " " + s[2];
			}

			String form = s[1];
			HardTokenizer hardTokenizer = new HardTokenizer();
			Token[] tokens = hardTokenizer.tokenArray(context);
			Sense[] sense = classify(tokens, form);
			//logger.info(Arrays.toString(sense));
			logger.info("i\tpage\tprior\tbow\tls\tcombo");
			for (int i = 0; i < sense.length; i++) {
				logger.info(i + "\t" + sense[i]);

			}
		}
	}


	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option interactiveModeOpt = OptionBuilder.withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option instanceFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the instances to classify from the specified file").withLongOpt("instance-file").create("f");
			Option lsmDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("lsi dir").isRequired().withLongOpt("lsi").create("l");
			Option lsmDimOpt = OptionBuilder.withArgName("int").hasArg().withDescription("lsi dim").withLongOpt("dim").create("d");
			Option normalizedOpt = OptionBuilder.withDescription("normalize vectors (default is " + WikipediaExtractor.DEFAULT_NORMALIZE + ")").withLongOpt("normalized").create();

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(instanceFileOpt);
			options.addOption(lsmDirOpt);
			options.addOption(lsmDimOpt);
			options.addOption(normalizedOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = OneExamplePerSenseSearcher.DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			String lsmDirName = line.getOptionValue("lsi");
			if (!lsmDirName.endsWith(File.separator)) {
				lsmDirName += File.separator;
			}

			boolean normalized = WikipediaExtractor.DEFAULT_NORMALIZE;
			if (line.hasOption("normalized")) {
				normalized = true;
			}

			File fileUt = new File(lsmDirName + "X-Ut");
			File fileSk = new File(lsmDirName + "X-S");
			File fileR = new File(lsmDirName + "X-row");
			File fileC = new File(lsmDirName + "X-col");
			File fileDf = new File(lsmDirName + "X-df");
			int dim = 100;
			if (line.hasOption("dim")) {
				dim = Integer.parseInt(line.getOptionValue("dim"));
			}
			logger.debug(line.getOptionValue("lsi") + "\t" + line.getOptionValue("dim"));
			Map<Integer, String> termMap = null;
			try {
				termMap = read(new InputStreamReader(new FileInputStream(fileR), "UTF-8"));
			} catch (IOException e) {
				logger.error(e);
			}

			LSI lsi = new LSI(fileUt, fileSk, fileR, fileC, fileDf, dim, true, normalized);
			OneExamplePerSenseSearcher oneExamplePerSenseSearcher = new OneExamplePerSenseSearcher(line.getOptionValue("index"));
			oneExamplePerSenseSearcher.setNotificationPoint(notificationPoint);


			if (line.hasOption("instance-file")) {
				OneExamplePerSenseClassifier3 oneExamplePerSenseClassifier = new OneExamplePerSenseClassifier3(lsi, oneExamplePerSenseSearcher, termMap);
				oneExamplePerSenseClassifier.classify(new File(line.getOptionValue("instance-file")), false);
			}

			if (line.hasOption("interactive-mode")) {
				OneExamplePerSenseClassifier3 oneExamplePerSenseClassifier = new OneExamplePerSenseClassifier3(lsi, oneExamplePerSenseSearcher, termMap);
				oneExamplePerSenseClassifier.interactive();
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar eu.fbk.twm.classifier.OneExamplePerSenseClassifier3", "\n", options, "\n", true);
		}
	}
}
