/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/29/12
 * ServiceTime: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class Link implements Serializable, Comparable<Link> {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Link</code>.
	 */
	static Logger logger = Logger.getLogger(Link.class.getName());

	public static final String WIKIPEDIA = "Wikipedia";

	public static final String DBPEDIA = "DBPedia";

	public static final String HOME_PAGE = "Home";

	private static final String WIKIPEDIA_ADDRESS = ".wikipedia.org/wiki/";

	private static final String DBPEDIA_ADDRESS = ".dbpedia.org/page/";

	private static final String EN_DBPEDIA_ADDRESS = "http://dbpedia.org/page/";

	private static final String EN_CODE = "en";

	private static final String HTTP_PREFIX = "http://";

	URL url;

	String resource;

	String label;

	private Link(String label, URL url, String resource) {
		this.label = label;
		this.url = url;
		this.resource = resource;
	}

	public static Link getWikipediaLink(String page, Locale locale) {
		URL url = null;
		try {
			url = new URL(HTTP_PREFIX + locale.getLanguage() + WIKIPEDIA_ADDRESS + page);
		} catch (MalformedURLException e) {
			logger.error(e);
		}

		return new Link(page.replace('_', ' '), url, WIKIPEDIA);
	}


	public static Link getDBPediaLink(String page, Locale locale) {
		URL url = null;
		try {
			if (locale.getLanguage().equals(EN_CODE)) {
				url = new URL(EN_DBPEDIA_ADDRESS + page);
			}
			else {
				url = new URL(HTTP_PREFIX + locale.getLanguage() + DBPEDIA_ADDRESS + page);
			}

		} catch (MalformedURLException e) {
			logger.error(e);
		}

		return new Link(page.replace('_', ' '), url, DBPEDIA);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}


	public URL getURL() {
		return url;
	}

	public void setURL(URL url) {
		this.url = url;
	}


	public int compareTo(Link o) {
		return 0;
	}

	//
	public String toString() {
		StringWriter w = new StringWriter();
		try {
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			g.writeStringField("id", label);
			g.writeStringField("url", url.toString());
			g.writeStringField("resource", resource);
			g.writeEndObject();
			g.close();

		} catch (IOException e) {
			logger.error(e);
		}
		return w.toString();
	} // end toString

}