/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key;

import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.utils.lsa.LSI;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.twm.utils.analysis.Token;
import eu.fbk.twm.classifier.OneExamplePerSenseBowClassifier;
import eu.fbk.twm.index.OneExamplePerSenseSearcher;
//import eu.fbk.twm.classifier.OneExamplePerSenseClassifier3;
//todo: investigate the bug...
//import eu.fbk.twm.classifier.OneExamplePerSenseClassifier2;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.utils.StringTable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/29/12
 * ServiceTime: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class OneExamplePerSenseKeywordLinker {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>OneExamplePerSenseKeywordLinker</code>.
	 */
	static Logger logger = Logger.getLogger(OneExamplePerSenseKeywordLinker.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	DecimalFormat sf = new DecimalFormat(".000");

	private static OneExamplePerSenseKeywordLinker ourInstance;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, OneExamplePerSenseBowClassifier> classifierMap;

	private static Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	public static OneExamplePerSenseKeywordLinker getInstance() {
		if (ourInstance == null) {
			ourInstance = new OneExamplePerSenseKeywordLinker();
		}
		return ourInstance;
	}

	private OneExamplePerSenseKeywordLinker() {
		logger.debug("creating classifiers...");
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
			/*Iterator<String> it = config.getKeys();
			while (it.hasNext()) {
				logger.debug(it.next());
			}*/

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		logger.debug("initializing classifiers...");
		classifierMap = new HashMap<Locale, OneExamplePerSenseBowClassifier>();

		Locale locale;
		for (int i = 0; i < languages.length; i++) {
			locale = new Locale(languages[i]);
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.info("loading " + languages[i] + "\t" + resourceMap.get("one-example-per-sense-index") + "...");

			String indexName = resourceMap.get("one-example-per-sense-index");
			String cacheName = resourceMap.get("form-freq");
			//int minFreq = config.getInt("form-min-freq");
			int minFreq = config.getInt("one-example-per-sense-index-form-min-freq");

			logger.debug(i + "\t" + languages[i] + "\t" + indexName + "\t" + minFreq);
			try {
				if (indexName != null) {
					OneExamplePerSenseSearcher searcher = new OneExamplePerSenseSearcher(indexName);
					if (cacheName != null) {
						searcher.loadCache(cacheName, minFreq);
					}
					boolean normalized = true;

					String lsmDirName = automaticConfiguration.getLsModelDir() + languages[i] + File.separator + "current" + File.separator;
					File fileUt = new File(lsmDirName + "X-Ut");
					File fileSk = new File(lsmDirName + "X-S");
					File fileR = new File(lsmDirName + "X-row");
					File fileC = new File(lsmDirName + "X-col");
					File fileDf = new File(lsmDirName + "X-df");
					int dim = 100;

					LSI lsi = new LSI(fileUt, fileSk, fileR, fileC, fileDf, dim, true, normalized);
					//
					Map<Integer, String> termMap = null;
					try {
						termMap = OneExamplePerSenseBowClassifier.read(new InputStreamReader(new FileInputStream(fileR), "UTF-8"));
						logger.debug(termMap.size() +" read now...");
					} catch (IOException e) {
						logger.error(e);
					}
					//
//					OneExamplePerSenseBowClassifier classifier = new OneExamplePerSenseBowClassifier(lsi, searcher, termMap);
					OneExamplePerSenseBowClassifier classifier = new OneExamplePerSenseBowClassifier(lsi, searcher);
					classifierMap.put(locale, classifier);
				}
			} catch (IOException e) {
				logger.error(e);
			}


		}
	}

	public void link(Keyword[] keywordArray, Token[] tokenArray, Locale locale) throws LanguageNotSupportedException {
		long begin = System.nanoTime();
		logger.debug("linking " + locale + ", " + keywordArray.length + ", " + tokenArray.length + "...");
		//logger.trace(Arrays.toString(tokenArray));
		int count = 0;
		OneExamplePerSenseBowClassifier classifier = classifierMap.get(locale);
		if (classifier == null) {
			throw new LanguageNotSupportedException(locale.getDisplayLanguage(Locale.US) + " not supported yet");
		}

		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered()) {

				//Sense[] senseArray = classifier.classify(createBow(tokenArray, keywordArray[i]), keywordArray[i].getTokenizedForm());
				//todo: decide if to use the keywords, if yes they must be not filtered with a high threshold
				//+P +R: en, it
				Sense[] senseArray = classifier.classify(createKeywordBow(keywordArray, i), keywordArray[i].getTokenizedForm());
				if (senseArray.length > 0) {
					keywordArray[i].setSenseArray(senseArray);
					//un/comment: to show/hide the senses
					logger.debug(count + "/" + i + "\t" + senseArray[0] + "\t" + keywordArray[i].getTokenizedForm());
					//for (int j = 1; j < senseArray.length && j < 5; j++) {
					//	logger.warn(j + "\t" + senseArray[j] + "\t" + keywordArray[i].getTokenizedForm());
					//}
					count++;
				}
				else {
					//logger.warn("NOT ABLE TO DISAMBIGUATE " + keywordArray[i]);
					keywordArray[i].setFiltered(true);
				}

			}
		}
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + keywordArray.length + " keywords linked (" + (keywordArray.length - count) + ") in " + nf.format(time) + " ns");
	}

	private BOW createBow(Token[] tokenArray, Keyword keyword) {
		BOW bow = new BOW();
		List<NGram> nGramList = keyword.getNGramList();
		int tokenStart = 0;
		int tokenEnd = 0;
		NGram nGram;
		for (int i = 0; i < nGramList.size(); i++) {
			nGram = nGramList.get(i);
			tokenEnd = nGram.getStartIndex();
			for (int j = tokenStart; j < tokenEnd; j++) {
				//logger.debug("L\t" + i + "\t" + j + "\t" + tokenStart + "\t" + tokenEnd + "\t" + tokenArray[j] + "\t" + nGram);
				bow.add(tokenArray[j].getForm().toLowerCase());
			}
			tokenStart = nGram.getEndIndex() + 1;
		}

		for (int j = tokenStart; j < tokenArray.length; j++) {
			//logger.debug("R\t" + "-\t" + j + "\t" + tokenStart + "\t" + tokenEnd + "\t" + tokenArray[j] + "\t" + nGram);
			bow.add(tokenArray[j].getForm().toLowerCase());
		}

		return bow;
	}

	public BOW createKeywordBow(Keyword[] keywordArray, int j) {

		//BOW bow = new BOW(BOW.AUGMENTED_TERM_FREQUENCY);
		BOW bow = new BOW();
		String tokenizedForm;
		String[] tokenArray;
		for (int i = 0; i < keywordArray.length; i++) {
			//todo: if weight > threashold
			if (!keywordArray[i].isFiltered() && i != j) {
				tokenizedForm = keywordArray[i].getTokenizedForm().toLowerCase();
				//logger.debug("C\t" + i + "/" + j + "\t" + tokenizedForm);

				tokenArray = spacePattern.split(tokenizedForm);
				//todo: weight the tf from the keywords tf

				for (int k = 0; k < keywordArray[i].tf(); k++) {
					bow.addAll(tokenArray);
				}

			}
		}
		return bow;
	}

	public Map<String, Double> createKeywordMap(Keyword[] keywordArray, int j) {

		Map<String, Double> map = new HashMap<String, Double>();
		String tokenizedForm;
		String[] tokenArray;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && i != j) {
				tokenizedForm = keywordArray[i].getTokenizedForm().toLowerCase();
				//logger.debug("C\t" + i + "/" + j + "\t" + tokenizedForm);
				tokenArray = spacePattern.split(tokenizedForm);
				for (int k = 0; k < tokenArray.length; k++) {
					map.put(tokenArray[k], keywordArray[i].weight());
				}

			}
		}
		return map;
	}
}

