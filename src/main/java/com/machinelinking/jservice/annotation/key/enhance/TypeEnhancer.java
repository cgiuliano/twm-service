package com.machinelinking.jservice.annotation.key.enhance;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.ClassResource;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.index.TypeSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/8/13
 * Time: 8:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class TypeEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TypeEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(TypeEnhancer.class.getName());

	public static final String NOM_TYPE = "TopicalConcept";

	public static final String NAM_TYPE = "NameEntity";

	public static final String UNKNOWN_TYPE = "Unknown";

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, TypeSearcher> typeSearcherMap;

	private static TypeEnhancer ourInstance;

	public static TypeEnhancer getInstance() {
		if (ourInstance == null) {
			ourInstance = new TypeEnhancer();
		}
		return ourInstance;
	}

	private TypeEnhancer() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		logger.debug(languages.length);
		typeSearcherMap = new HashMap<Locale, TypeSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-namnom-index") + "...");

			String indexName = resourceMap.get("page-namnom-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			//int minFreq = config.getInt("page-min-freq");
			int minFreq = config.getInt("type-index-page-min-freq");

			try {
				if (indexName != null) {
					TypeSearcher typeSearcher = new TypeSearcher(indexName);
					typeSearcher.loadCache(cacheName, minFreq);
					typeSearcherMap.put(new Locale(languages[i]), typeSearcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		//todo: ?
		long begin = System.nanoTime();

		TypeSearcher typeSearcher = typeSearcherMap.get(locale);
		String page;
		Sense sense;
		TypeSearcher.Entry entry;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				sense = keywordArray[i].getSense();
				page = sense.getPage();

				entry = typeSearcher.search(page);
				//logger.debug(page + "\t" + entry.getType());
				//todo: remove this
				keywordArray[i].setType(entry.getType());
				//keywordArray[i].addType(Type.getMachineLinkingType(entry.getType(), entry.getFreq()));
				//keywordArray[i].addAirpediaClass(new ClassResource(entry.getType(), PageAirpediaClassSearcher.NAMESPACES[3], entry.getFreq(), PageAirpediaClassSearcher.RESOURCES[3]));
				keywordArray[i].addAirpediaClass(new ClassResource(entry.getType(), 3, entry.getFreq(), 3));
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");

	}

	public Type enhance(String page, Locale locale) {
		long begin = System.nanoTime();
		TypeSearcher typeSearcher = typeSearcherMap.get(locale);
		//logger.debug(typeSearcher);
		TypeSearcher.Entry entry = typeSearcher.search(page);

		long end = System.nanoTime();
		long time = end - begin;

		logger.debug(page + " enhanced in " + nf.format(time) + " ns");
		return Type.getMachineLinkingType(entry.getType(), entry.getFreq());
	}
}
