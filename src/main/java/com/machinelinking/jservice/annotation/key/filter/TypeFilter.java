package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.StringTable;

import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/26/13
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class TypeFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TypeFilter</code>.
	 */
	static Logger logger = Logger.getLogger(TypeFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	Set<String> stopwords;

	private static TypeFilter ourInstance;

	private static Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	Configuration config;

	public static TypeFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new TypeFilter();
		}
		return ourInstance;
	}

	private TypeFilter() {

		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}

		/*String fileName = config.getString("stop-words-file");
		try {
			stopwords = readStopwords(fileName);
		} catch (IOException e) {
			logger.error(e);
		}*/

	}

	/*Set<String> readStopwords(String f) throws IOException {
		Set<String> set = new HashSet<String>();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line;
		while ((line = lnr.readLine()) != null) {
			set.add(line);
		}
		return set;
	}*/

	@Override
	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		String tokenziedForm;
		int count = 0, total = 0;
		String type;
		for (int i = 0; i < keywordArray.length; i++) {
			tokenziedForm = keywordArray[i].getTokenizedForm();
			type = keywordArray[i].getType();
			if (!keywordArray[i].isFiltered()) {
				total++;
			//if (type.equals(TypeEnhancer.NAM_TYPE)) {
				//logger.trace(i + "\t" + keywordArray[i].getForm() + "\t" + keywordArray[i].getAirpediaClassList());
				if (keywordArray[i].hasAirpediaClass("Agent") || keywordArray[i].hasAirpediaClass("PopulatedPlace") || keywordArray[i].hasAirpediaClass("Work")  || keywordArray[i].hasAirpediaClass("NameEntity")) {
				//logger.debug(keywordArray[i] + " is " + type);
				if (!isCapitalized(tokenziedForm)) {
					keywordArray[i].setFiltered(true);
					count++;
				}
			}
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + total + " keywords filtered out in " + nf.format(time) + " ns");
	}

	boolean isCapitalized(String tokenizedForm) {
		//logger.debug(tokenizedForm);
		if (tokenizedForm.length() == 0) {
			return false;
		}
		char ch = tokenizedForm.charAt(0);
		if (Character.isLetter(ch)) {
			if (!Character.isUpperCase(ch)) {
				//logger.debug(tokenizedForm + " is not a uppercase");
				return false;
			}
		}
		/*
		String[] s = spacePattern.split(tokenizedForm);
		char ch;
		for (int i = 0; i < s.length; i++) {
			if (!stopwords.contains(s[i])) {
				logger.debug(s[i] + " is not a stopword");
				ch = s[i].charAt(0);
				if (Character.isLetter(ch)) {
					if (!Character.isUpperCase(ch)) {
						logger.debug(s[i] + " is not a uppercase");
						return false;
					}

				}
			}
		}*/

		return true;
	}
}
