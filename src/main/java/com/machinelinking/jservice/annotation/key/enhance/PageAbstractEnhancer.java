package com.machinelinking.jservice.annotation.key.enhance;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.index.PageAbstractSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/8/13
 * Time: 8:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class PageAbstractEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageAbstractEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(PageAbstractEnhancer.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageAbstractSearcher> pageAbstractSearcherMap;

	private static PageAbstractEnhancer ourInstance;

	public static PageAbstractEnhancer getInstance() {
		if (ourInstance == null) {
			ourInstance = new PageAbstractEnhancer();
		}
		return ourInstance;
	}

	private PageAbstractEnhancer() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		logger.debug(languages.length);
		pageAbstractSearcherMap = new HashMap<Locale, PageAbstractSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("abstract-index") + "...");

			String indexName = resourceMap.get("abstract-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (indexName != null) {
					PageAbstractSearcher pageAbstractSearcher = new PageAbstractSearcher(indexName);
					pageAbstractSearcher.loadCache(cacheName, minFreq);
					pageAbstractSearcherMap.put(new Locale(languages[i]), pageAbstractSearcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		//todo: ?
		long begin = System.nanoTime();

		PageAbstractSearcher pageAbstractSearcher = pageAbstractSearcherMap.get(locale);
		String page;
		Sense sense;
		String pageAbstract;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				sense = keywordArray[i].getSense();
				page = sense.getPage();

				pageAbstract = pageAbstractSearcher.search(page);
				//logger.debug(page + "\t" + pageAbstract);
				keywordArray[i].setPageAbstract(pageAbstract);
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");

	}

	public String enhance(String page, Locale locale) {
		long begin = System.nanoTime();
		PageAbstractSearcher pageAbstractSearcher = pageAbstractSearcherMap.get(locale);
		//logger.debug(pageAbstractSearcher);
		String pageAbstract = pageAbstractSearcher.search(page);

		long end = System.nanoTime();
		long time = end - begin;

		logger.debug(page + " enhanced in " + nf.format(time) + " ns");
		return pageAbstract;
	}
}
