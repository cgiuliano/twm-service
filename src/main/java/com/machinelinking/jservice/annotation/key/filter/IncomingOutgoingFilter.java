/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.index.PageIncomingOutgoingSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/20/13
 * Time: 2:41 PM
 * To change this template use File | Settings | File Templates.
 */
@Deprecated public class IncomingOutgoingFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>IncomingOutgoingFilter</code>.
	 */
	static Logger logger = Logger.getLogger(IncomingOutgoingFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageIncomingOutgoingSearcher> pageIncomingOutgoingSearcherMap;

	private static IncomingOutgoingFilter ourInstance;

	Configuration config;

	public static IncomingOutgoingFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new IncomingOutgoingFilter();
		}
		return ourInstance;
	}

	private IncomingOutgoingFilter() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		pageIncomingOutgoingSearcherMap = new HashMap<Locale, PageIncomingOutgoingSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("incoming-outgoing-index") + "...");

			String indexName = resourceMap.get("incoming-outgoing-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (indexName != null) {
					PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher = new PageIncomingOutgoingSearcher(indexName);

					pageIncomingOutgoingSearcher.loadCache(cacheName, minFreq);
					//todo: change 1000
					//pageIncomingOutgoingSearcher.loadCache(cacheName, 1000);
					pageIncomingOutgoingSearcherMap.put(new Locale(languages[i]), pageIncomingOutgoingSearcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void filter(Keyword[] keywordArray) {
		filter(keywordArray, null);
	}

	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher = pageIncomingOutgoingSearcherMap.get(locale);
		String pi, pj = null;
		double max = 0;
		double sum;
		int count;
		double[][] matrix = new double[keywordArray.length][keywordArray.length];
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].getSense() != null) {
				pi = keywordArray[i].getSense().getPage();
				sum = 0;
				count = 0;
				for (int j = 0; j < i; j++) {
					sum += matrix[i][j];
					count++;
					//logger.debug(i + "\t" + j + "\t" + pi + "\t?\t" + matrix[i][j] + "\t" + sum + "\t" + count);
				}
				for (int j = i + 1; j < keywordArray.length; j++) {
					if (!keywordArray[j].isFiltered() && keywordArray[j].getSense() != null) {
						pj = keywordArray[j].getSense().getPage();;
						if (!pi.equals(pj)) {
							long b = System.nanoTime();
							//matrix[i][j] = matrix[j][i] = pageIncomingOutgoingSearcher.normalizedIntersection(pi, pj);
							matrix[i][j] = matrix[j][i] = pageIncomingOutgoingSearcher.compare(pi, pj);

							sum += matrix[i][j];
							count++;
							long e = System.nanoTime();
							logger.debug(i + "\t" + j + "\t" + pi + "\t" + pj + "\t" + matrix[i][j] + "\t" + sum + "\t" + count + "\t" + nf.format(e - b));
						}
						else {
							logger.debug(i + "\t" + j + "\t" + pi + "\t" + pj + "\t0\t" + sum + "\t" + count);
						}
					}
				}
				if (count != 0) {
					//keywordArray[i].setBoost(sum / (keywordArray.length - 1));
					keywordArray[i].setCorrelation(sum);
					if (sum > max) {
						max = sum;
					}
				}

				logger.debug(i + "\t-\t" + pi + "\t-\t" + keywordArray[i].getCorrelation());

			}
		}
		// normalize
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].getSense() != null) {
				keywordArray[i].setCorrelation(keywordArray[i].getCorrelation() / max);
				logger.debug(i + "\t" + keywordArray[i].getSense().getPage() + "\t" + keywordArray[i].getCorrelation());
			}
		}
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords boosted in " + nf.format(time) + " ns");
	}
}
