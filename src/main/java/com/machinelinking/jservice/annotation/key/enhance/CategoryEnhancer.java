/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.index.PageCategorySearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:53 PM
 * To change this template use File | Settings | File Templates.
 *
 * @deprecated
 */
@Deprecated
public class CategoryEnhancer extends AbstractEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CategoryEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(CategoryEnhancer.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static CategoryEnhancer ourInstance;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageCategorySearcher> pageCategorySearcherMap;


	public static CategoryEnhancer getInstance() {
		if (ourInstance == null) {
			ourInstance = new CategoryEnhancer();
		}
		return ourInstance;
	}

	private CategoryEnhancer() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		pageCategorySearcherMap = new HashMap<Locale, PageCategorySearcher>();


		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-category-index") + "...");

			String indexName = resourceMap.get("page-category-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (indexName != null) {
					PageCategorySearcher pageCategorySearcher = new PageCategorySearcher(indexName);
					pageCategorySearcher.loadCache(cacheName, minFreq);
					pageCategorySearcherMap.put(new Locale(languages[i]), pageCategorySearcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		PageCategorySearcher pageCategorySearcher = pageCategorySearcherMap.get(locale);
		String page;
		Sense sense;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				sense = keywordArray[i].getSense();

				page = sense.getPage();
				String[] entry = pageCategorySearcher.search(page);
				Category category;
				for (int j = 0; j < entry.length; j++) {
					category = new Category(entry[j], locale);
					keywordArray[i].addCategory(category);
				}
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.info(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");

	}

	public List<Category> enhance(String page, Locale locale) {
		long begin = System.nanoTime();
		List<Category> categoryList = new ArrayList<Category>();
		PageCategorySearcher pageCategorySearcher = pageCategorySearcherMap.get(locale);

		String[] entry = pageCategorySearcher.search(page);
		Category category;
		for (int j = 0; j < entry.length; j++) {
			category = new Category(entry[j], locale);
			categoryList.add(category);
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.info(page + " enhanced in " + nf.format(time) + " ns");
		return categoryList;
	}

	@Override
	public String toString() {
		return "CategoryEnhancer{" +
				"config=" + config +
				", nf=" + nf +
				", automaticConfiguration=" + automaticConfiguration +
				", pageCategorySearcherMap=" + pageCategorySearcherMap +
				'}';
	}
}