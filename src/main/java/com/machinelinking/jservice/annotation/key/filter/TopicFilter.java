package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.annotation.topic.Topic;
import com.machinelinking.jservice.annotation.topic.TopicSet;
import org.apache.log4j.Logger;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/29/13
 * Time: 8:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class TopicFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TopicFilter</code>.
	 */
	static Logger logger = Logger.getLogger(TopicFilter.class.getName());

	private static TopicFilter ourInstance;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	public static TopicFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new TopicFilter();
		}
		return ourInstance;
	}

	private TopicFilter() {
	}

	public void filter(Keyword[] keywordArray, Locale locale, List<Topic> textTopic) {
		long begin = System.nanoTime();
		int count = 0;
		for (int i = 0; i < keywordArray.length; i++) {
			TopicSet pageTopicSet = keywordArray[i].getTopic();
			if (pageTopicSet != null && textTopic != null) {
				List<Topic> pageTopicList = pageTopicSet.topicList();
				if (pageTopicList.size() > 0 && textTopic.size() > 0) {
					logger.debug(pageTopicList.get(0).getLabel() + "\t" + textTopic.get(0).getLabel());
					if (!pageTopicList.get(0).getLabel().equals(textTopic.get(0).getLabel())) {
						keywordArray[i].setFiltered(true);
						logger.debug("F\t" + keywordArray[i] + "\t" + pageTopicList.get(0).getLabel() + "\t" + textTopic.get(0).getLabel());
						count++;
					}
					else {
						logger.debug("K\t" + keywordArray[i] + "\t" + pageTopicList.get(0).getLabel() + "\t" + textTopic.get(0).getLabel());
					}
				}
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + keywordArray.length + " keywords filtered in " + nf.format(time) + " ns");
	}
}
