/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key;

import com.machinelinking.jservice.annotation.key.enhance.Category;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.index.*;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 8/19/13
 * Time: 6:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageLinkDisambiguation {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageLinkDisambiguation</code>.
	 */
	static Logger logger = Logger.getLogger(PageLinkDisambiguation.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	DecimalFormat df = new DecimalFormat(".0000");

	DecimalFormat sf = new DecimalFormat("###,###,###");

	protected static Pattern numberPattern = Pattern.compile("\\d+");

	private static PageLinkDisambiguation ourInstance;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageOutgoingLinkSearcher> pageOutgoingLinkSearcherMap;

	Map<Locale, FormPageSearcher> formPageSearcherMap;

	Map<Locale, PageFormSearcher> pageFormSearcherMap;

	Map<Locale, CrossLanguageSearcher> crossLanguageSearcherMap;

	Map<Locale, PageFreqSearcher> pageFreqSearcherMap;

	Map<Locale, TypeSearcher> typeSearcherMap;

	private String[] languages;

	public static PageLinkDisambiguation getInstance() {
		if (ourInstance == null) {
			ourInstance = new PageLinkDisambiguation();
		}
		return ourInstance;
	}

	private PageLinkDisambiguation() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		this.languages = languages;
		pageOutgoingLinkSearcherMap = new HashMap<Locale, PageOutgoingLinkSearcher>();
		formPageSearcherMap = new HashMap<Locale, FormPageSearcher>();
		pageFormSearcherMap = new HashMap<Locale, PageFormSearcher>();
		crossLanguageSearcherMap = new HashMap<Locale, CrossLanguageSearcher>();
		pageFreqSearcherMap = new HashMap<Locale, PageFreqSearcher>();
		typeSearcherMap = new HashMap<Locale, TypeSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-outgoing-index") + "...");
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("form-page-index") + "...");
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-form-index") + "...");
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("cross-lang-index") + "...");
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-freq-index") + "...");
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("type-index") + "...");

			String pageOutgoingIndexName = resourceMap.get("page-outgoing-index");
			String formPageIndexName = resourceMap.get("form-page-index");
			String pageFormIndexName = resourceMap.get("page-form-index");
			String crossLangIndexName = resourceMap.get("cross-lang-index");
			String pageFreqIndexName = resourceMap.get("page-freq-index");
			String typeIndexName = resourceMap.get("type-index");

			String pageFreqCacheName = resourceMap.get("page-freq.csv");
			String formPageCacheName = resourceMap.get("form-freq");

			int pageMinFreq = config.getInt("page-min-freq");
			//int pageMinFreq = 1000;
			int formMinFreq = config.getInt("form-min-freq");
			//int formMinFreq = 1000;

			try {
				if (pageOutgoingIndexName != null) {
					PageOutgoingLinkSearcher pageOutgoingLinkSearcher = new PageOutgoingLinkSearcher(pageOutgoingIndexName);
					pageOutgoingLinkSearcher.loadCache(pageFreqCacheName, pageMinFreq);
					pageOutgoingLinkSearcherMap.put(new Locale(languages[i]), pageOutgoingLinkSearcher);
				}
				else {
					logger.warn(pageOutgoingIndexName + " not found");
				}

				if (formPageIndexName != null) {
					FormPageSearcher formPageSearcher = new FormPageSearcher(formPageIndexName);
					formPageSearcher.loadCache(formPageCacheName, formMinFreq);
					formPageSearcherMap.put(new Locale(languages[i]), formPageSearcher);
				}
				else {
					logger.warn(pageOutgoingIndexName + " not found");
				}

				if (crossLangIndexName != null) {
					CrossLanguageSearcher crossLanguageSearcher = new CrossLanguageSearcher(crossLangIndexName);
					crossLanguageSearcher.loadCache(pageFreqCacheName, pageMinFreq);
					crossLanguageSearcherMap.put(new Locale(languages[i]), crossLanguageSearcher);
				}
				else {
					logger.warn(pageOutgoingIndexName + " not found");
				}

				if (pageFormIndexName != null) {
					PageFormSearcher pageFormSearcher = new PageFormSearcher(pageFormIndexName);
					pageFormSearcher.loadCache(pageFreqCacheName, pageMinFreq);
					pageFormSearcherMap.put(new Locale(languages[i]), pageFormSearcher);
				}
				else {
					logger.warn(pageOutgoingIndexName + " not found");
				}

				if (pageFreqIndexName != null) {
					PageFreqSearcher pageFreqSearcher = new PageFreqSearcher(pageFreqIndexName);
					pageFreqSearcher.loadCache(pageMinFreq);
					pageFreqSearcherMap.put(new Locale(languages[i]), pageFreqSearcher);
				}
				else {
					logger.warn(pageOutgoingIndexName + " not found");
				}

				if (typeIndexName != null) {
					TypeSearcher typeSearcher = new TypeSearcher(typeIndexName);
					typeSearcher.loadCache(pageFreqCacheName, pageMinFreq);
					typeSearcherMap.put(new Locale(languages[i]), typeSearcher);
				}
				else {
					logger.warn(pageOutgoingIndexName + " not found");
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		enhance(keywordArray, locale, 0);
	}

	public void enhance(Keyword[] keywordArray, Locale locale, int depth) {
		long begin = System.nanoTime();

		PageOutgoingLinkSearcher pageOutgoingLinkSearcher = pageOutgoingLinkSearcherMap.get(locale);
		FormPageSearcher formPageSearcher = formPageSearcherMap.get(locale);
		PageFormSearcher pageFormSearcher = pageFormSearcherMap.get(locale);
		CrossLanguageSearcher crossLanguageSearcher = crossLanguageSearcherMap.get(locale);
		PageFreqSearcher pageFreqSearcher = pageFreqSearcherMap.get(locale);
		TypeSearcher typeSearcher = typeSearcherMap.get(locale);

		logger.debug(formPageSearcher);

		Set<String> keywordSet = new HashSet<String>();
		for (int k = 0; k < keywordArray.length; k++) {
			if (!keywordArray[k].isFiltered() && keywordArray[k].getForm().length() > 2) {
				keywordSet.add(keywordArray[k].getTokenizedForm());
			}
		}
		logger.debug(keywordSet);

		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].getForm().length() > 2) {
				FormPageSearcher.Entry[] pages = formPageSearcher.search(keywordArray[i].getTokenizedForm());
				logger.debug(i + "\t" + keywordArray[i].getTokenizedForm() + "\t" + keywordArray[i].getForm() + "\t" + pages.length);
				double[] score = new double[pages.length];
				double min = 0;
				if (pages.length > 1 && pages[0].getFreq() != pages[pages.length - 1].getFreq()) {
					min = pages[pages.length - 1].getFreq();
				}
				// iterates over the form's senses
				for (int j = 0; j < pages.length; j++) {
					//remove the less frequent senses and type inconsistencies
					TypeSearcher.Entry type = typeSearcher.search(pages[j].getValue());
					boolean b = true;
					if (type.getType().equals(TypeSearcher.NAM_LABEL) && Character.isLowerCase(keywordArray[i].getForm().charAt(0))) {
						b = false;
					}
					else if (type.getType().equals(TypeSearcher.NOM_LABEL) && Character.isUpperCase(keywordArray[i].getForm().charAt(0))) {
						b = false;
					}

					if (pages[j].getFreq() > min && b) {
						//logger.debug(j + "\t" + pages[j]);
						for (int k = 0; k < languages.length; k++) {
							if (languages[k].equals(locale.getLanguage())) {
								//logger.debug("same language " + locale);
								String[] subpages = pageOutgoingLinkSearcher.search(pages[j].getValue());
								score[j] += score(keywordArray[i].getTokenizedForm(), keywordSet, pageFormSearcher, subpages, languages[k], pageFreqSearcher);
							}
							else {
								//logger.debug("diff language " + locale);
								String[] crossSubPages = xxx(crossLanguageSearcher, pages[j].getValue(), locale.getLanguage(), languages[k]);
								if (crossSubPages != null) {
									score[j] += score(keywordArray[i].getTokenizedForm(), keywordSet, pageFormSearcher, crossSubPages, languages[k], pageFreqSearcher);
									//logger.debug(pages[j] + "\t" + score[j] + "\ten");
								}
								else {
									//logger.warn(pages[j] + " has cross null");
								}
							}
						}
						logger.debug("\t" + j + "\t" + pages[j].getValue() + "\t" + keywordArray[i].getTokenizedForm() + "\t" + df.format(pages[j].getFreq()) + "\t" + df.format(score[j]));

					}
					else {
						logger.debug("Discarded: " + pages[j].getValue() + "\t" + keywordArray[i].getTokenizedForm() + "\t" + df.format(pages[j].getFreq()) + "\t" + type.getType());
					}


				}
				logger.debug(i + "\t" + keywordArray[i].getTokenizedForm() + "\t" + keywordArray[i].getForm() + "\t" + pages.length + "\t" + Arrays.toString(score));
				for (int j = 0; j < score.length; j++) {
					score[j] = score[j] * Math.exp(pages[j].getFreq());
				}
				logger.debug(i + "\t" + keywordArray[i].getTokenizedForm() + "\t" + keywordArray[i].getForm() + "\t" + pages.length + "\t" + Arrays.toString(score));
				int maxIndex = -1;
				double maxScore = 0;
				for (int j = 0; j < score.length; j++) {
					if (score[j] > maxScore) {
						maxIndex = j;
						maxScore = score[j];
					}
				}
				if (maxIndex > -1) {
					logger.info("<<<" + keywordArray[i].getForm() + "\t" + pages[maxIndex] + "\t" + df.format(maxScore) + ">>>");
				}
				else {
					logger.info("<<<" + keywordArray[i].getForm() + "\tUNKNOWN>>>");
				}

			}
		}
		long end = System.nanoTime();
		long time = end - begin;
		logger.info(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");

	}

	private String[] xxx(CrossLanguageSearcher crossLanguageSearcher, String page, String sourceLang, String targetLang) {
		Map<String, String> crossLangMap = crossLanguageSearcher.search(page);
		//Iterator<String> it = crossLangMap.keySet().iterator();
		String[] result = null;
		//String[] result = new String[1];
		int i = 0;
		//for (int i = 0; it.hasNext(); i++) {
		//String lang = it.next();
		//String targetLang = "en";
		String otherLangPage = crossLangMap.get(targetLang);
		//logger.debug("*\t" + page + "\t" + targetLang + "\t" + otherLangPage);
		//logger.debug("*\t" + page + "\t" + targetLang + "\t" + pageOutgoingLinkSearcherMap);
		PageOutgoingLinkSearcher pageOutgoingLinkSearcher = pageOutgoingLinkSearcherMap.get(new Locale(targetLang));
		//logger.debug("*\t" + targetLang + "\t" + pageOutgoingLinkSearcher);
		if (pageOutgoingLinkSearcher != null && otherLangPage != null) {
			//logger.debug("searching " + otherLangPage);
			String[] otherLangPages = pageOutgoingLinkSearcher.search(otherLangPage);
			//logger.debug("found " + otherLangPages);
			result = new String[otherLangPages.length];
			for (int j = 0; j < otherLangPages.length; j++) {
				CrossLanguageSearcher crossOtherLanguageSearcher = crossLanguageSearcherMap.get(new Locale(targetLang));
				Map<String, String> crossOtherLangMap = crossOtherLanguageSearcher.search(otherLangPages[j]);
				result[j] = crossOtherLangMap.get(sourceLang);

				//String crossPage = crossLangMap.get(result[i]);
				//logger.debug("*\t" + i + "\t" + j + "\t" + otherLangPages[j] + "\t" + result[j]);

			}
		}

		//}
		return result;
	}

	private double score(String tokenizedform, Set<String> keywordSet, PageFormSearcher pageFormSearcher, String[] subpages, String lang, PageFreqSearcher pageFreqSearcher) {
		double score = 0;
		for (int k = 0; k < subpages.length; k++) {
			//logger.debug("\t" + k + "\t" + subpages[k]);
			if (subpages[k] != null) {
				Matcher matcher = numberPattern.matcher(subpages[k]);

				if (!matcher.matches()) {
					PageFormSearcher.Entry[] subforms = pageFormSearcher.search(subpages[k]);
					int pageFreq = pageFreqSearcher.search(subpages[k]);
					Map<String, Double> formMap = new HashMap<String, Double>();
					double min = 0;
					if (subforms.length > 1 && subforms[0].getFreq() != subforms[subforms.length - 1].getFreq()) {
						min = subforms[subforms.length - 1].getFreq();
					}

					for (int l = 0; l < subforms.length; l++) {
						//logger.debug("\t\t" + l + "\t" + subforms[l]);
						//if (subforms[l].getFreq() > 0.01 && !tokenizedform.equals(subforms[l].getValue())) {
						if (subforms[l].getFreq() > min && !tokenizedform.equals(subforms[l].getValue())) {
							//formSet.add(subforms[l].getValue());
							formMap.put(subforms[l].getValue(), subforms[l].getFreq());

						}
					}
					//logger.debug("\t\t\t\t" + formSet);
					Iterator<String> it = formMap.keySet().iterator();
					for (int l = 0; it.hasNext(); l++) {
						String form = it.next();

						if (keywordSet.contains(form)) {
							Double freq = formMap.get(form);
							//if (freq != null) {
							logger.debug("\t\t\t[" + subpages[k] + "\t" + form + "]\t" + lang + "\t" + sf.format(pageFreq) + "\t" + df.format(freq));
							//score++;

							score += freq / Math.log10(9 + pageFreq);
							//}
						}
					}
				}
			}
		}

		return score;
	}

	public List<Category> enhance(String page, Locale locale, int depth) {
		long begin = System.nanoTime();

		return null;
	}

	@Override
	public String toString() {
		return "PageLinkDisambiguation{" +
				"config=" + config +
				", nf=" + nf +
				", automaticConfiguration=" + automaticConfiguration +
				", pageOutgoingLinkSearcherMap=" + pageOutgoingLinkSearcherMap +
				'}';
	}

}
