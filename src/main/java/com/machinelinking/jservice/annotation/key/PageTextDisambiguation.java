/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key;

import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.utils.lsa.LSI;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.math.Node;
import eu.fbk.twm.utils.analysis.Token;
import eu.fbk.twm.classifier.ContextualSense;
import eu.fbk.twm.index.*;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.twm.classifier.Sense;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 8/19/13
 * Time: 6:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageTextDisambiguation {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageTextDisambiguation</code>.
	 */
	static Logger logger = Logger.getLogger(PageTextDisambiguation.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	DecimalFormat df = new DecimalFormat(".0000");

	DecimalFormat sf = new DecimalFormat("###,###,###");

	private static Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	protected static Pattern numberPattern = Pattern.compile("\\d+");

	private static PageTextDisambiguation ourInstance;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageVectorSearcher> pageVectorSearcherMap;

	Map<Locale, FormPageSearcher> formPageSearcherMap;

	Map<Locale, TypeSearcher> typeSearcherMap;

	Map<Locale, LSI> lsiMap;

	private String[] languages;

	public static PageTextDisambiguation getInstance() {
		if (ourInstance == null) {
			ourInstance = new PageTextDisambiguation();
		}
		return ourInstance;
	}

	private PageTextDisambiguation() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		this.languages = languages;
		pageVectorSearcherMap = new HashMap<Locale, PageVectorSearcher>();
		formPageSearcherMap = new HashMap<Locale, FormPageSearcher>();
		typeSearcherMap = new HashMap<Locale, TypeSearcher>();
		lsiMap = new HashMap<Locale, LSI>();
		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-vector-index") + "...");
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("form-page-index") + "...");
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("type-index") + "...");

			String pageVectorIndexName = resourceMap.get("page-vector-index");
			String formPageIndexName = resourceMap.get("form-page-index");
			String typeIndexName = resourceMap.get("type-index");

			String pageFreqCacheName = resourceMap.get("page-freq.csv");
			String formPageCacheName = resourceMap.get("form-freq");

			int pageMinFreq = config.getInt("page-min-freq");
			//int pageMinFreq = 1000;
			int formMinFreq = config.getInt("form-min-freq");
			//int formMinFreq = 1000;

			try {
				if (pageVectorIndexName != null) {
					PageVectorSearcher pageVectorSearcher = new PageVectorSearcher(pageVectorIndexName);
					pageVectorSearcher.loadCache(pageFreqCacheName, pageMinFreq);
					pageVectorSearcherMap.put(new Locale(languages[i]), pageVectorSearcher);
				}
				else {
					logger.warn(pageVectorIndexName + " not found");
				}

				if (formPageIndexName != null) {
					FormPageSearcher formPageSearcher = new FormPageSearcher(formPageIndexName);
					formPageSearcher.loadCache(formPageCacheName, formMinFreq);
					formPageSearcherMap.put(new Locale(languages[i]), formPageSearcher);
				}
				else {
					logger.warn(pageVectorIndexName + " not found");
				}


				if (typeIndexName != null) {
					TypeSearcher typeSearcher = new TypeSearcher(typeIndexName);
					typeSearcher.loadCache(pageFreqCacheName, pageMinFreq);
					typeSearcherMap.put(new Locale(languages[i]), typeSearcher);
				}
				else {
					logger.warn(pageVectorIndexName + " not found");
				}
			} catch (IOException e) {
				logger.error(e);
			}
			try {

				boolean normalized = false;

				String lsmDirName = automaticConfiguration.getLsModelDir() + languages[i] + File.separator + "current" + File.separator;
				File fileUt = new File(lsmDirName + "X-Ut");
				File fileSk = new File(lsmDirName + "X-S");
				File fileR = new File(lsmDirName + "X-row");
				File fileC = new File(lsmDirName + "X-col");
				File fileDf = new File(lsmDirName + "X-df");
				int dim = 100;

				LSI lsi = new LSI(fileUt, fileSk, fileR, fileC, fileDf, dim, true, normalized);
				lsiMap.put(new Locale(languages[i]), lsi);
			} catch (IOException e) {
				logger.error(e);
			}

		}
	}


	public void link(Keyword[] keywordArray, Token[] tokenArray, Locale locale) throws LanguageNotSupportedException {
		long begin = System.nanoTime();

		PageVectorSearcher pageVectorSearcher = pageVectorSearcherMap.get(locale);
		FormPageSearcher formPageSearcher = formPageSearcherMap.get(locale);
		TypeSearcher typeSearcher = typeSearcherMap.get(locale);
		LSI lsi = lsiMap.get(locale);


		for (int i = 0; i < keywordArray.length; i++) {
			//if (!keywordArray[i].isFiltered() && keywordArray[i].getForm().length() > 2) {
			if (!keywordArray[i].isFiltered()) {
				FormPageSearcher.Entry[] pages = formPageSearcher.search(keywordArray[i].getTokenizedForm());
				//logger.debug(i + "\t'" + keywordArray[i].getTokenizedForm() + "'\t" + keywordArray[i].getForm() + "\t" + pages.length);

				double min = 0;
				if (pages.length > 1 && pages[0].getFreq() != pages[pages.length - 1].getFreq()) {
					min = pages[pages.length - 1].getFreq();
				}

				BOW bow = createBow(tokenArray, keywordArray[i]);
				//logger.debug(bow);
				Node[] bowVector = lsi.mapDocument(bow);
				Node[] lsVector = lsi.mapPseudoDocument(bowVector);
				//logger.debug(i + "\t" + Arrays.toString(bowVector));
				//logger.debug(i + "\t" + Arrays.toString(lsVector));

				Node.normalize(bowVector);
				Node.normalize(lsVector);
				// iterates over the form's senses
				Sense[] senses = new Sense[pages.length];
				for (int j = 0; j < pages.length; j++) {
					//remove the less frequent senses and type inconsistencies
					//double typeKernel = 0.01;
					boolean b = true;
					TypeSearcher.Entry type = typeSearcher.search(pages[j].getValue());

					if (type.getType().equals(TypeSearcher.NAM_LABEL) && Character.isLowerCase(keywordArray[i].getForm().charAt(0))) {
						//typeKernel = 0;
						b = false;
					}
					/*else if (type.getType().equals(TypeSearcher.NOM_LABEL) && Character.isUpperCase(keywordArray[i].getForm().charAt(0))) {
						typeKernel = 0;
					}*/

					if (pages[j].getFreq() > min && b) {
						Node[][] pageVectors = pageVectorSearcher.search(pages[j].getValue());
						//logger.debug(i + "\t" + j + "\t" + Arrays.toString(bowVector));
						//logger.debug(i + "\t" + j + "\t" + Arrays.toString(lsVector));
						double bowKernel = Node.dot(bowVector, pageVectors[PageVectorSearcher.BOW_INDEX]);
						double lsKernel = Node.dot(lsVector, pageVectors[PageVectorSearcher.LS_INDEX]);
						//double kernel = (bowKernel + lsKernel + typeKernel) / 3;
						//double kernel = (bowKernel + typeKernel) / 3;
						double kernel = (bowKernel + lsKernel) / 2;
						//senses[j] = new Sense(pages[j].getValue(), kernel);
						senses[j] = new ContextualSense(pages[j].getValue(), bowKernel, lsKernel, pages[j].getFreq());
						//logger.debug(pages[j].getValue() + "\t" + df.format(pages[j].getFreq()) + "\t" + df.format(bowKernel) + "\t" + df.format(lsKernel) + "\t" + b + "\t" + df.format(kernel));
					}
					else {
						//logger.debug(pages[j].getValue() + "\t" + df.format(pages[j].getFreq()) + "\t" + df.format(0) + "\t" + df.format(0));
						senses[j] = new ContextualSense(pages[j].getValue(), 0, 0, pages[j].getFreq());
					}

				}
				Arrays.sort(senses, new Comparator<Sense>() {
					@Override
					public int compare(Sense sense, Sense sense2) {
						double diff = sense.getCombo() - sense2.getCombo();
						if (diff > 0) {
							return -1;
						}
						else if (diff < 0) {
							return 1;
						}
						return 0;
					}
				});
				if (senses.length > 0) {
					keywordArray[i].setSenseArray(senses);
				}
				else {
					Sense[] senseArray = new Sense[1];
					senseArray[0] = new ContextualSense();
					keywordArray[i].setSenseArray(senseArray);
				}
				//logger.debug(i + "\t" + keywordArray[i].getTokenizedForm() + "\t" + senses[0].getId() + "\t" + senses[0].getProbability());
				//logger.debug(i + "\t" + pages.length + "\t" + Arrays.toString(senses));

			}
		}
		long end = System.nanoTime();
		long time = end - begin;
		logger.info(keywordArray.length + " keywords linked in " + nf.format(time) + " ns");

	}

	private class MySense implements Comparable<MySense> {
		private double bow;

		private double ls;

		private double prior;

		private double combo;

		public String page;

		MySense(String page, double bow, double ls, double prior) {
			this.page = page;
			this.bow = bow;
			this.ls = ls;
			this.prior = prior;
			combo = (bow + ls) / 2;
		}

		public String getPage() {
			return page;
		}

		public double getCombo() {
			return combo;
		}

		public double getBow() {
			return bow;
		}

		public double getLs() {
			return ls;
		}

		public double getPrior() {
			return prior;
		}

		@Override
		public int compareTo(MySense sense) {
			double diff = combo - sense.getCombo();
			if (diff > 0) {
				return -1;
			}
			else if (diff < 0) {
				return 1;
			}

			return 0;
		}

		@Override
		public String toString() {
			return page + "\t" + prior + "\t" + bow + "\t" + ls + "\t" + combo;
		}
	}


	private BOW createBow(Token[] tokenArray, Keyword keyword) {
		BOW bow = new BOW();
		List<NGram> nGramList = keyword.getNGramList();
		int tokenStart = 0;
		int tokenEnd = 0;
		NGram nGram;
		for (int i = 0; i < nGramList.size(); i++) {
			nGram = nGramList.get(i);
			tokenEnd = nGram.getStartIndex();
			for (int j = tokenStart; j < tokenEnd; j++) {
				//logger.debug("L\t" + i + "\t" + j + "\t" + tokenStart + "\t" + tokenEnd + "\t" + tokenArray[j] + "\t" + nGram);
				bow.add(tokenArray[j].getForm().toLowerCase());
			}
			tokenStart = nGram.getEndIndex() + 1;
		}

		for (int j = tokenStart; j < tokenArray.length; j++) {
			//logger.debug("R\t" + "-\t" + j + "\t" + tokenStart + "\t" + tokenEnd + "\t" + tokenArray[j] + "\t" + nGram);
			bow.add(tokenArray[j].getForm().toLowerCase());
		}

		return bow;
	}

	public BOW createKeywordBow(Keyword[] keywordArray, int j) {

		BOW bow = new BOW();
		String tokenizedForm;
		String[] tokenArray;
		for (int i = 0; i < keywordArray.length; i++) {
			//todo: if weight > threashold
			if (!keywordArray[i].isFiltered() && i != j) {
				tokenizedForm = keywordArray[i].getTokenizedForm().toLowerCase();
				//logger.debug("C\t" + i + "/" + j + "\t" + tokenizedForm);
				tokenArray = spacePattern.split(tokenizedForm);
				bow.addAll(tokenArray);
			}
		}
		return bow;
	}

	@Override
	public String toString() {
		return "PageTextDisambiguation{" +
				"config=" + config +
				", nf=" + nf +
				", automaticConfiguration=" + automaticConfiguration +
				", pageVectorSearcherMap=" + pageVectorSearcherMap +
				'}';
	}

}
