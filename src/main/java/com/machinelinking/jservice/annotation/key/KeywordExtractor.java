/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key;

import com.machinelinking.jservice.util.AutomaticConfiguration;
import eu.fbk.twm.utils.analysis.Token;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.index.NGramSearcher;
import eu.fbk.twm.utils.CharacterTable;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;


/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/23/12
 * ServiceTime: 10:07 AM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * todo: do not split quotes
 */
public class KeywordExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>KeywordExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(KeywordExtractor.class.getName());

	private static KeywordExtractor ourInstance = null;

	int nGramLength;

	int minLinkFrequency;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, NGramSearcher> nGramSearcherMap;

	//Map<Locale, Set<String>> stopMap;

	//Map<Locale, FirstNameSearcher> firstNameSearcherMap;

	Map<Locale, Integer> nGramMaxDocumentFrequencyMap;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	Configuration config;

	private static Pattern tabPattern = Pattern.compile("\t");

	private KeywordExtractor() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
			/*Iterator<String> it = config.getKeys();
			while (it.hasNext()) {
				logger.debug(it.next());
			} */

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();
			nGramLength = config.getInt("n-gram-length");
			minLinkFrequency = config.getInt("min-lf");
			logger.info(nGramLength + "-grams extractor");

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
			//initFirstName(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	// when I tried using the max df the performance dropped
	// maybe it's only a matter of thresholds
	private int readMaxDf(String f) {
		logger.debug("reading max df from " + f + "...");
		/*String line;
		try {
			LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
			if ((line = lnr.readLine()) != null) {
				//logger.debug("first line: " + line);
				String[] t = tabPattern.split(line);
				if (t.length == 3) {
					//logger.debug("first line: " + Arrays.toString(t));
					return Integer.parseInt(t[0]);
				}
			}
			lnr.close();
		} catch (IOException e) {
			logger.error(e);
		}   */
		return Integer.MAX_VALUE;
	}

	public static synchronized KeywordExtractor getInstance() {
		if (ourInstance == null) {
			ourInstance = new KeywordExtractor();
		}
		return ourInstance;
	}

	void init(String[] languages) {
		nGramSearcherMap = new HashMap<Locale, NGramSearcher>();
		nGramMaxDocumentFrequencyMap = new HashMap<Locale, Integer>();
		//stopMap = new HashMap<Locale, Set<String>>();

		//String stopSetDir = config.getString("stop-set-dir");
		//if (!stopSetDir.endsWith(File.separator)) {
		//	stopSetDir += File.separator;
		//}

		for (int i = 0; i < languages.length; i++) {

			//Set<String> stopSet = loadStopSet(stopSetDir + languages[i]);
			//stopMap.put(new Locale(languages[i]), stopSet);

			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("ngram-index") + "...");

			String indexName = resourceMap.get("ngram-index");
			String ngramFileName = resourceMap.get("ngram.csv");
			//int nGramMaxDocumentFrequency = config.getInt("n-gram-max-df");
			int nGramMaxDocumentFrequency = readMaxDf(ngramFileName);
			logger.debug("n-gram-max-df " + nf.format(nGramMaxDocumentFrequency) + "(" + languages[i] + ")");
			nGramMaxDocumentFrequencyMap.put(new Locale(languages[i]), nGramMaxDocumentFrequency);
			//int minFreq = config.getInt("form-min-freq");
			int minFreq = config.getInt("ngram-index-form-min-freq");

			logger.debug(i + "\t" + languages[i] + "\t" + indexName + "\t" + minFreq);
			try {
				if (indexName != null) {
					NGramSearcher searcher = new NGramSearcher(indexName, true);
					searcher.loadCache(minFreq);
					nGramSearcherMap.put(new Locale(languages[i]), searcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	/*void initFirstName(String[] languages) {
		nGramSearcherMap = new HashMap<Locale, NGramSearcher>();
		firstNameSearcherMap = new HashMap<Locale, FirstNameSearcher>();


		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("first-name-index") + "...");

			String indexName = resourceMap.get("first-name-index");
			int minFreq = config.getInt("first-name-min-freq");
			logger.debug(i + "\t" + languages[i] + "\t" + indexName + "\t" + minFreq);
			try {
				if (indexName != null) {
					FirstNameSearcher searcher = new FirstNameSearcher(indexName, true);
					searcher.loadCache(minFreq);
					firstNameSearcherMap.put(new Locale(languages[i]), searcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}*/

	private String tokenizedForm(Token[] tokenArray, int start, int end) {
		StringBuilder sb = new StringBuilder();
		sb.append(tokenArray[start].getForm());
		for (int i = start + 1; i <= end; i++) {
			sb.append(CharacterTable.SPACE);
			sb.append(tokenArray[i].getForm());
		}
		return sb.toString();
	}

	public Keyword[] extract(Token[] tokenArray, String text, Locale locale) throws LanguageNotSupportedException {
		long begin = System.nanoTime();
		logger.debug("extracting keyword " + locale + ", " + tokenArray.length + ", " + text.length() + "...");
		Map<String, Keyword> map = new HashMap<String, Keyword>();
		//Set<String > stopSet = stopMap.get(locale);
		NGramSearcher nGramSearcher = nGramSearcherMap.get(locale);
		//FirstNameSearcher firstNameSearcher = firstNameSearcherMap.get(locale);
		if (nGramSearcher == null) {
			throw new LanguageNotSupportedException(locale.getDisplayLanguage(Locale.US) + " not supported yet");
		}
		//todo: replace if use a single constants
		int numberOfDocuments = nGramMaxDocumentFrequencyMap.get(locale);
		NGram ngram = null;
		int start = 0, end = 0;

		Token firstToken = null, lastToken = null;
		int m = 0;
		String form = null, tokenizedForm = null;
		Keyword keyword = null;
		double idf = 0, ilf = 0;
		int lf = 0, df = 0;
		for (int i = 0; i < tokenArray.length; i++) {
			firstToken = tokenArray[i];
			start = firstToken.getStart();
			m = i + nGramLength + 1;
			if (m > tokenArray.length) {
				m = tokenArray.length;
			}
			for (int j = i; j < m; j++) {
				lastToken = tokenArray[j];
				end = lastToken.getEnd();
				//logger.debug(start + "\t" + end);
				form = text.substring(start, end);

				//replaced with the tokenized version!!!!
				//NGramSearcher.Entry entry = searcher.search(form);
				tokenizedForm = tokenizedForm(tokenArray, i, j);

				//logger.debug(i + "/" + j + "\t" + start + "\t" + end + "\t" + form);
				//logger.debug(i + "/" + j + "\t" + start + "\t" + end + "\t" + tokenizedForm);
				//todo: check if I can use multiple searchers (e.g., the one that uses a live update from wikipedia)
				NGramSearcher.Entry entry = nGramSearcher.search(tokenizedForm);
				lf = entry.lf();
				df = entry.df();
				//todo: set the lf threshold in the configuration file
				if (lf > minLinkFrequency && df > 0) {
					idf = Math.log10((double) numberOfDocuments / df);
					ilf = Math.log10(1 + lf);
					//idf = (double) df;
					//ilf = (double) lf;

					//	ilf = Math.log10(1 + lf);
					//ilf = (double) lf / df;
					//logger.debug(idf + "\t" + ilf + "\t" + df + "\t" + lf + "\t" + start + "\t" + end + "\t" + form);
					//ngram = new NGram(start, end, i, j, form, j-i+1);

					ngram = new NGram(start, end, i, j, form);
					keyword = map.get(form);
					if (keyword == null) {
						//todo: form must be tokenized
						//keyword = new Keyword(form);
						keyword = new Keyword(form, tokenizedForm);
						keyword.addNGram(ngram);
						//return Math.log(1 + linkFreq) * Math.log(1 + nGramList.size()) * Math.log((double) numberOfDocuments / idf);
						keyword.setIdf(idf);
						keyword.setLf(ilf);
						map.put(form, keyword);
					}
					else {
						keyword.addNGram(ngram);
					}
					//logger.debug(i + "/" + j + "\t" + keyword);
				}
				/*else
				{
					int firstNameFreq = firstNameSearcher.search(tokenizedForm);
					logger.debug(tokenizedForm + " is a first name (" + firstNameFreq + ")");
				} */

				//todo: handle different entities here (e.g. person names)
			}
		}

		long tend = System.nanoTime();
		long time = tend - begin;
		logger.debug(map.size() + " keyword extracted in " + nf.format(time) + " ns");

		return map.values().

				toArray(new Keyword[map.size()

						]);
	}

	//used by disambiguation only
	public Keyword[] extract(Token[] tokenArray, int i, int j, String text, Locale locale) throws LanguageNotSupportedException {
		long begin = System.nanoTime();
		logger.debug("extracting keywords " + locale + ", " + tokenArray.length + ", " + text.length() + "...");
		NGramSearcher nGramSearcher = nGramSearcherMap.get(locale);
		if (nGramSearcher == null) {
			throw new LanguageNotSupportedException(locale.getDisplayLanguage(Locale.US) + " not supported yet");
		}
		//todo: replace if use a single constants
		int numberOfDocuments = nGramMaxDocumentFrequencyMap.get(locale);
		NGram ngram = null;
		int start = 0, end = 0;
		Token firstToken = null, lastToken = null;
		int m = 0;
		String form = null;
		Keyword keyword = null;
		double idf = 0, ilf = 0;
		int lf = 0, df = 0;

		firstToken = tokenArray[i];
		start = firstToken.getStart();
		lastToken = tokenArray[j];
		end = lastToken.getEnd();
		form = text.substring(start, end);
		String tokenizedForm = tokenizedForm(tokenArray, i, j);

		//logger.debug(i + "/" + j + "\t" + start + "\t" + end + "\t" + form);
		//todo: bug
		NGramSearcher.Entry entry = nGramSearcher.search(tokenizedForm);
		lf = entry.lf();
		df = entry.df();
		//if (df > 0) {
		//	idf = Math.log((double) numberOfDocuments / df);
		//	ilf = Math.log(1 + ((double) lf / df));
		if (lf > minLinkFrequency && df > 0) {
			idf = Math.log10((double) numberOfDocuments / df);
			ilf = Math.log10(1 + lf);

			ngram = new NGram(start, end, i, j, form);
			keyword = new Keyword(form, tokenizedForm);
			keyword.addNGram(ngram);
			keyword.setIdf(idf);
			keyword.setLf(ilf);
			Keyword[] keywords = new Keyword[1];
			keywords[0] = keyword;
			long tend = System.nanoTime();
			long time = tend - begin;
			logger.info(keywords.length + " keyword extracted in " + nf.format(time) + " ns");
			return keywords;
		}
		long tend = System.nanoTime();
		long time = tend - begin;
		logger.info("0 keyword extracted in " + nf.format(time) + " ns");
		return new Keyword[0];
	}

	private Set<String> loadStopSet(String name) {
		logger.info("reading stop words from" + name + "...");
		Set<String> set = new HashSet<String>();

		try {
			LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(name), "UTF-8"));
			String line;
			String[] t;
			int count = 0;
			while ((line = lnr.readLine()) != null) {
				logger.debug(line);
				set.add(line.trim());
			}
			lnr.close();

		} catch (IOException e) {
			logger.error(e);
		}
		logger.info(set.size() + " stop words read from " + name);
		return set;
	}
}
