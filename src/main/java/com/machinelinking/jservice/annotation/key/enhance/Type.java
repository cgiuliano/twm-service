/*
 * Copyright (2012) Machine Typeing srl
 *
 * Machine Typeing reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Typeing.  A license under Machine Typeing's
 * rights in the Program may be available directly from Machine Typeing.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.CharacterTable;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/29/12
 * ServiceTime: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class Type implements Serializable, Comparable<Type> {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Type</code>.
	 */
	static Logger logger = Logger.getLogger(Type.class.getName());

	public static final String ML_TYPE = "http://machinelinking/";


	public static final String WIKIPEDIA = "Wikipedia";

	public static final String DBPEDIA = "DBPedia";

	public static final String AIRPEDIA = "Airpedia";

	public static final String MACHINE_LINKING = "machinelinking";

	public static final String HOME_PAGE = "Home";

	private static final String WIKIPEDIA_ADDRESS = ".wikipedia.org/wiki/";

	private static final String DBPEDIA_ADDRESS = "http://dbpedia.org/ontology/";

	private static final String AIRPEDIA_ADDRESS = ".airpedia.org/page/";

	private static final String MACHINE_LINKING_ADDRESS = "http://machinelinking.com/type/";


	private static final String EN_CODE = "en";

	private static final String HTTP_PREFIX = "http://";

	URL url;

	String resource;

	String label;

	double prob;

	private Type(String label, URL url, String resource, double prob) {
		this.label = label;
		this.url = url;
		this.resource = resource;
		this.prob = prob;
	}

	public static Type getMachineLinkingType(String type, double prob) {
		URL url = null;
		try {
			url = new URL(MACHINE_LINKING_ADDRESS + type);
		} catch (MalformedURLException e) {
			logger.error(e);
		}

		return new Type(type, url, MACHINE_LINKING, prob);
	}


	public static Type getAirpediaType(String type, double prob) {
		URL url = null;
		try {
			url = new URL(DBPEDIA_ADDRESS + type);

		} catch (MalformedURLException e) {
			logger.error(e);
		}

		return new Type(fromCamelCase(type), url, AIRPEDIA, prob);
	}

	public static Type getDBPediaType(String type, double prob) {
		URL url = null;
		try {
			url = new URL(DBPEDIA_ADDRESS + type);

		} catch (MalformedURLException e) {
			logger.error(e);
		}

		return new Type(fromCamelCase(type), url, DBPEDIA, prob);
	}

	static String fromCamelCase(String s) {
		if (s.length() == 0) {
			return s;
		}
		char ch;
		StringBuffer sb = new StringBuffer();
		sb.append(s.charAt(0));
		for (int i = 1; i < s.length(); i++) {
			ch = s.charAt(i);
			if (Character.isUpperCase(ch)) {
				sb.append(CharacterTable.SPACE);
			}
			sb.append(ch);
		}
		return sb.toString();
	}

	public double getProb() {
		return prob;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}


	public URL getURL() {
		return url;
	}

	public void setURL(URL url) {
		this.url = url;
	}


	public int compareTo(Type o) {
		return 0;
	}

	//
	public String toString() {
		StringWriter w = new StringWriter();
		try {
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			g.writeStringField("id", label);
			g.writeStringField("url", url.toString());
			g.writeStringField("resource", resource);
			g.writeEndObject();
			g.close();

		} catch (IOException e) {
			logger.error(e);
		}
		return w.toString();
	} // end toString

}