/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.enhance;

import com.machinelinking.jservice.annotation.key.Keyword;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class LinkEnhancer extends AbstractEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>LinkEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(LinkEnhancer.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static LinkEnhancer ourInstance;

	public static LinkEnhancer getInstance() {
		if (ourInstance == null) {
			ourInstance = new LinkEnhancer();
		}
		return ourInstance;
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		Sense sense;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				//logger.debug(keywordArray[i]);
				sense = keywordArray[i].getSense();
				//logger.debug(sense);
				// Wikipedia link
				keywordArray[i].addLink(Link.getWikipediaLink(sense.getPage(), locale));
				// DBPedia link
				keywordArray[i].addLink(Link.getDBPediaLink(sense.getPage(), locale));
			}

		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");
	}

	public List<Link> enhance(String page, Locale locale) {
		long begin = System.nanoTime();

		List<Link> linkList = new ArrayList<Link>();
		linkList.add(Link.getWikipediaLink(page, locale));
		linkList.add(Link.getDBPediaLink(page, locale));

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug("id enhanced in " + nf.format(time) + " ns");

		return linkList;
	}

}
