package com.machinelinking.jservice.annotation.key.enhance;

import org.apache.log4j.Logger;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/22/13
 * Time: 2:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class Image {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Image</code>.
	 */
	static Logger logger = Logger.getLogger(Image.class.getName());

	public static final String WIKIPEDIA_URL_PREFIX = "http://upload.wikimedia.org/wikipedia/";

	public static final String THUMB_LABEL = "thumb";

	public static final String PX_LABEL = "px-";

	public static final int DEFAULT_IMAGE_SIZE = 100;

	public static final String LOWERCASE_SVG_EXTENSION = ".svg";

	public static final String UPPERCASE_SVG_EXTENSION = ".SVG";

	public static final String PNG_EXTENSION = ".png";

	String image;

	String thumb;

	public Image(String base) {
		this(base, DEFAULT_IMAGE_SIZE);
	}

	public Image(String base, int size) {
		int first = base.indexOf(File.separator);
		int last = base.lastIndexOf(File.separator);
		image = WIKIPEDIA_URL_PREFIX + base;
		thumb = WIKIPEDIA_URL_PREFIX + base.substring(0, first) + File.separator + THUMB_LABEL + base.substring(first, base.length()) + File.separator + size + PX_LABEL + base.substring(last + 1, base.length());
		if (image.endsWith(LOWERCASE_SVG_EXTENSION) || image.endsWith(UPPERCASE_SVG_EXTENSION)) {
			thumb += PNG_EXTENSION;
		}
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	@Override
	public String toString() {
		return "Entry{" +
						"image='" + image + '\'' +
						", thumb='" + thumb + '\'' +
						'}';
	}
}
