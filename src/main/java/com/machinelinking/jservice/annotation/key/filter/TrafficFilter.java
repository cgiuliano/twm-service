/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.index.PageTrafficSearcher;
//import eu.fbk.twm.index.PageWeightedIncomingOutgoingSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/20/13
 * Time: 2:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class TrafficFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TrafficFilter</code>.
	 */
	static Logger logger = Logger.getLogger(TrafficFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	DecimalFormat sf = new DecimalFormat(".0000");

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageTrafficSearcher> pageTrafficSearcherMap;


	private static TrafficFilter ourInstance;

	Configuration config;

	public static TrafficFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new TrafficFilter();
		}
		return ourInstance;
	}

	private TrafficFilter() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		pageTrafficSearcherMap = new HashMap<Locale, PageTrafficSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-traffic-index") + "...");

			String indexName = resourceMap.get("page-traffic-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (indexName != null) {
					PageTrafficSearcher pageTrafficSearcher = new PageTrafficSearcher(indexName);
					//pageTrafficSearcher.loadCache(cacheName, minFreq);
					pageTrafficSearcherMap.put(new Locale(languages[i]), pageTrafficSearcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	/**
	 * keywords must be sorted.
	 *
	 * @param keywordArray
	 * @param locale
	 */
	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		PageTrafficSearcher pageTrafficSearcher = pageTrafficSearcherMap.get(locale);
		String pj = null;

		for (int j = 0; j < keywordArray.length; j++) {
			if (!keywordArray[j].isFiltered() && keywordArray[j].getSense() != null) {
				pj = keywordArray[j].getSense().getPage();;
				try {
					int freq = pageTrafficSearcher.search(pj);
					keywordArray[j].setTraffic(Math.log10(1+freq));
					logger.debug(j + "\t" + pj + "\t" + freq + "\t" + keywordArray[j].getTraffic());
				} catch (Exception e) {
					logger.equals(e);
					logger.equals("error at " + j + "\t" + keywordArray[j]);
				}

			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords traffic stats found in " + nf.format(time) + " ns");

	}
}