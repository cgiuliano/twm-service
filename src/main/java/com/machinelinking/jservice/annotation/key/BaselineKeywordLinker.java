/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key;

import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import eu.fbk.twm.classifier.ContextualSense;
import eu.fbk.twm.classifier.Sense;
import org.apache.log4j.Logger;
import eu.fbk.twm.index.FormPageSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/29/12
 * ServiceTime: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class BaselineKeywordLinker {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>BaselineKeywordLinker</code>.
	 */
	static Logger logger = Logger.getLogger(BaselineKeywordLinker.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static BaselineKeywordLinker ourInstance;

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, FormPageSearcher> formPageSearchMap;

	public static BaselineKeywordLinker getInstance() {
		if (ourInstance == null) {
			ourInstance = new BaselineKeywordLinker();
		}
		return ourInstance;
	}

	private BaselineKeywordLinker() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
			Iterator<String> it = config.getKeys();
			while (it.hasNext()) {
				logger.debug(it.next());
			}

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		formPageSearchMap = new HashMap<Locale, FormPageSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("form-page-index") + "...");

			String indexName = resourceMap.get("form-page-index");
			String cacheName = resourceMap.get("form-freq");
			int minFreq = config.getInt("form-min-freq");
			logger.debug(i + "\t" + languages[i] + "\t" + indexName + "\t" + minFreq);
			try {
				if (indexName != null) {
					FormPageSearcher searcher = new FormPageSearcher(indexName);
					if (cacheName != null) {
						searcher.loadCache(cacheName, minFreq);
					}

					formPageSearchMap.put(new Locale(languages[i]), searcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	//
	public void link(Keyword[] keywordArray, String text, Locale locale) throws LanguageNotSupportedException {
		long begin = System.nanoTime();
		logger.debug("linking " + locale + ", " + keywordArray.length + ", " + text.length() + "...");

		FormPageSearcher searcher = formPageSearchMap.get(locale);
		if (searcher == null) {
			throw new LanguageNotSupportedException(locale.getDisplayLanguage(Locale.US) + " not supported yet");
		}


		String form;
		FormPageSearcher.Entry[] entry;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered()) {
				form = keywordArray[i].form();
				entry = searcher.search(form);

				if (entry.length > 0) {
					// most frequent
					Sense[] senseArray = new Sense[1];
					senseArray[0]=new ContextualSense(entry[0].getValue(), entry[0].getFreq(),0, entry[0].getFreq());
					keywordArray[i].setSenseArray(senseArray);
				}
			}
		}
		long end = System.nanoTime();
		long time = end - begin;
		logger.info(keywordArray.length + " keyword linked in " + nf.format(time) + " ns");

	}


}
