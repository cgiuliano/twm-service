/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.annotation.key.KeywordWeightComparator;
import org.apache.log4j.Logger;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/4/13
 * ServiceTime: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class WeightSortFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WeightSortFilter</code>.
	 */
	static Logger logger = Logger.getLogger(WeightSortFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	KeywordWeightComparator keywordWeightComparator;

	private static WeightSortFilter ourInstance;

	public static WeightSortFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new WeightSortFilter();
		}
		return ourInstance;
	}

	private WeightSortFilter() {
		keywordWeightComparator = new KeywordWeightComparator();
	}


	public void filter(Keyword[] keywordArray) {
		filter(keywordArray, null);
	}

	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();
		//logger.debug("B\t" + Arrays.toString(keywordArray));
		Arrays.sort(keywordArray, keywordWeightComparator);

		//logger.debug("A\t" + Arrays.toString(keywordArray));
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords sorted in " + nf.format(time) + " ns");
	}

}
