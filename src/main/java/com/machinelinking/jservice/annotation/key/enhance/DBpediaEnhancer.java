package com.machinelinking.jservice.annotation.key.enhance;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.index.PageDBpediaClassSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/8/13
 * Time: 8:15 AM
 * To change this template use File | Settings | File Templates.
 */
@Deprecated public class DBpediaEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>DBpediaEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(DBpediaEnhancer.class.getName());

	public static final String UNKNOWN_CLASS = "UNKNOWN";

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageDBpediaClassSearcher> dbpediaClassSearcherMap;

	private static DBpediaEnhancer ourInstance;

	public static DBpediaEnhancer getInstance() {
		if (ourInstance == null) {
			ourInstance = new DBpediaEnhancer();
		}
		return ourInstance;
	}

	private DBpediaEnhancer() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		logger.debug(languages.length);
		dbpediaClassSearcherMap = new HashMap<Locale, PageDBpediaClassSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			//todo: replace airpedia
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("dbpedia-class-index") + "...");

			//todo: replace airpedia
			String indexName = resourceMap.get("dbpedia-class-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (indexName != null) {
					PageDBpediaClassSearcher pageDBpediaClassSearcher = new PageDBpediaClassSearcher(indexName);
					pageDBpediaClassSearcher.loadCache(cacheName, minFreq);
					dbpediaClassSearcherMap.put(new Locale(languages[i]), pageDBpediaClassSearcher);

				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		PageDBpediaClassSearcher pageDBpediaClassSearcher = dbpediaClassSearcherMap.get(locale);
		String page;
		Sense sense;
		String[] entry;
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				sense = keywordArray[i].getSense();
				page = sense.getPage();
				entry = pageDBpediaClassSearcher.search(page);
				//logger.debug(entry.length);
				for (int j = 0; j < entry.length; j++) {
					//logger.debug(keywordArray[i].getForm() + "\t" + entry[j]);

					//todo: verify
					keywordArray[i].addType(Type.getMachineLinkingType(entry[j], 1.0));
					keywordArray[i].addType(Type.getDBPediaType(entry[j], 1.0));
				}
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.info(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");

	}

	public List<Type> enhance(String page, Locale locale) {
		long begin = System.nanoTime();
		PageDBpediaClassSearcher pageDBpediaClassSearcher = dbpediaClassSearcherMap.get(locale);

		List<Type> list = new ArrayList<Type>();
		String[] entry = pageDBpediaClassSearcher.search(page);
		for (int j = 0; j < entry.length; j++) {
			list.add(Type.getDBPediaType(entry[j], 1.0));
		}

		long end = System.nanoTime();
		long time = end - begin;

		logger.info(page + " enhanced in " + nf.format(time) + " ns");
		return list;
	}
}
