package com.machinelinking.jservice.annotation.key;

import org.apache.log4j.Logger;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 8/26/13
 * Time: 12:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeywordWeightComparator implements Comparator<Keyword> {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>KeywordWeightComparator</code>.
	 */
	static Logger logger = Logger.getLogger(KeywordWeightComparator.class.getName());

	public int compare(Keyword k1, Keyword k2) {
		//logger.debug("compare " + k1.getForm() + "." + k1.weight() + "\t" + k2.getForm() + "." + k2.weight() + "\t" + ((int) (k2.weight() - k1.weight())));
		//return (int) (k2.weight() - k1.weight());
		double diff = k2.weight() - k1.weight();
		if (diff < 0) {
			return -1;
		}
		else if (diff > 0) {
			return 1;
		}
		return 0;
	}


}
