package com.machinelinking.jservice.annotation.key.enhance;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import eu.fbk.twm.index.PageAirpediaTypeSearcher;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.utils.ClassResource;
import eu.fbk.twm.utils.WeightedSet;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/8/13
 * Time: 8:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class AirpediaEnhancer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AirpediaEnhancer</code>.
	 */
	static Logger logger = Logger.getLogger(AirpediaEnhancer.class.getName());

	public static final String UNKNOWN_CLASS = "UNKNOWN";

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	//	Map<Locale, PageAirpediaClassSearcher> airpediaClassSearcherMap;
	Map<Locale, PageAirpediaTypeSearcher> airpediaTypeSearcherMap;

	private static AirpediaEnhancer ourInstance;

	public static AirpediaEnhancer getInstance() {
		if (ourInstance == null) {
			ourInstance = new AirpediaEnhancer();
		}
		return ourInstance;
	}

	private AirpediaEnhancer() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		logger.debug(languages.length);
//		airpediaClassSearcherMap = new HashMap<Locale, PageAirpediaClassSearcher>();
		airpediaTypeSearcherMap = new HashMap<Locale, PageAirpediaTypeSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			//todo: replace airpedia
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("page-airpedia2class-index") + "...");

			//todo: replace airpedia
			String indexName = resourceMap.get("page-airpedia2class-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (indexName != null) {
					/*PageDBpediaClassSearcher pageAirpediaClassSearcher = new PageDBpediaClassSearcher(indexName);
					pageAirpediaClassSearcher.loadCache(cacheName, minFreq);
					airpediaClassSearcherMap.put(new Locale(languages[i]), pageAirpediaClassSearcher);*/

//					PageAirpediaClassSearcher airpediaClassSearcher = new PageAirpediaClassSearcher(indexName);
//					airpediaClassSearcher.loadCache(cacheName, minFreq);
//					airpediaClassSearcherMap.put(new Locale(languages[i]), airpediaClassSearcher);

					PageAirpediaTypeSearcher airpediaTypeSearcher = new PageAirpediaTypeSearcher(indexName);
					airpediaTypeSearcher.loadCache(cacheName, minFreq);
					airpediaTypeSearcherMap.put(new Locale(languages[i]), airpediaTypeSearcher);

				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void enhance(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		PageAirpediaTypeSearcher pageAirpediaTypeSearcher = airpediaTypeSearcherMap.get(locale);
		String page;
		Sense sense;
		WeightedSet entry;

		for (Keyword keyword : keywordArray) {
			boolean cleaned = false;

			if (!keyword.isFiltered() && keyword.hasSense()) {
				sense = keyword.getSense();
				page = sense.getPage();
				entry = pageAirpediaTypeSearcher.search(page);

				for (String c : entry.keySet()) {
					ClassResource cr = new ClassResource(c, 3, entry.get(c), 3);
					if (!cleaned) {
						keyword.clearAirpediaList();
						cleaned = true;
					}
					keyword.addAirpediaClass(cr);
				}
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords enhanced in " + nf.format(time) + " ns");

	}

	public List<ClassResource> enhance(String page, Locale locale) {
		long begin = System.nanoTime();
		PageAirpediaTypeSearcher pageAirpediaTypeSearcher = airpediaTypeSearcherMap.get(locale);

		List<ClassResource> list = new ArrayList<ClassResource>();
		WeightedSet entry = pageAirpediaTypeSearcher.search(page);
		for (String c : entry.keySet()) {
			ClassResource cr = new ClassResource(c, 3, entry.get(c), 3);
			list.add(cr);
		}

		long end = System.nanoTime();
		long time = end - begin;

		logger.debug(page + " enhanced in " + nf.format(time) + " ns");
		return list;
	}
}
