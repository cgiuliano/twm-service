package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.StringTable;

import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/26/13
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class LongestNotWorkFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>LongestNotWorkFilter</code>.
	 */
	static Logger logger = Logger.getLogger(LongestNotWorkFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	Set<String> stopwords;

	private static LongestNotWorkFilter ourInstance;

	private static Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	Configuration config;

	public static LongestNotWorkFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new LongestNotWorkFilter();
		}
		return ourInstance;
	}

	private LongestNotWorkFilter() {

		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}

	}

	@Override
	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();

		for (Keyword keyword : keywordArray) {
			if (!keyword.isFiltered()) {
				if (!keyword.hasAirpediaClass("Work")) {
					//logger.trace("Wight before: " + keyword.weight());
					//keyword.setMultiplier(keyword.getMultiplier() * keyword.getMaxNGramLength());

					keyword.setMultiplier(keyword.getMultiplier() * Math.pow(keyword.getMaxNGramLength(), 2));
					//logger.trace("Wight after: " + keyword.weight());
				}

			}
		}

		long end = System.nanoTime();
		long time = end - begin;
//		logger.debug(count + "/" + total + " keywords filtered out in " + nf.format(time) + " ns");
	}

}
