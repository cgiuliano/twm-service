/*
 * Copyright (2013) Machine Linking srl
 *  
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import org.apache.log4j.Logger;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/9/13
 * Time: 12:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class OverlappingFilter extends AbstractFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>OverlappingFilter</code>.
	 */
	static Logger logger = Logger.getLogger(OverlappingFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	KeywordComparator keywordComparator;

	private static OverlappingFilter ourInstance;

	public static OverlappingFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new OverlappingFilter();
		}
		return ourInstance;
	}

	private OverlappingFilter() {
		keywordComparator = new KeywordComparator();
	}

	class KeywordComparator implements Comparator<Keyword> {
		public int compare(Keyword k1, Keyword k2) {
			return (int) (k1.weight() - k2.weight());
		}
	}

	public void filter(Keyword[] keywordArray) {
		filter(keywordArray, null);
	}

	//todo: are sorted?
	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();
		int count = 0, total = 0;
		/*for (int i=0;i<keywordArray.length;i++)
		{
			logger.debug(i + "\t" + keywordArray[i].getForm() + "\t" + keywordArray[i].weight() + "\t" + keywordArray[i].isFiltered());
		}*/
		for (int i = 0; i < keywordArray.length; i++) {
			//logger.debug(i + "\t" + keywordArray[i].getForm() + "\t" + keywordArray[i].weight() + "\t" + keywordArray[i].isFiltered());
			if (!keywordArray[i].isFiltered()) {
				total++;
				for (int j = i + 1; j < keywordArray.length; j++) {
					//logger.debug("\t" + j + "\t" + keywordArray[j].getForm() + "\t" + keywordArray[j].weight() + "\t" + keywordArray[j].isFiltered());
					if (!keywordArray[j].isFiltered()) {
						//logger.debug("B\t " + i + "/" + j + "\t" + keywordArray[i].isFiltered() + "\t" + keywordArray[j].isFiltered());
						if (keywordArray[i].intersects(keywordArray[j])) {
							//logger.debug(keywordArray[i].getForm() + " ^ " + keywordArray[j].getForm());

							// Product/Organisation filter
//							if (keywordArray[i].hasAirpediaClass("Organisation") && keywordArray[j].hasAirpediaClass("Object")) {
//								logger.trace(String.format("%s ORG ---> %s OBJ", keywordArray[i].getSense(), keywordArray[j].getSense()));
//								keywordArray[i].setFiltered(true);
//								continue;
//							}
//							else if (keywordArray[i].hasAirpediaClass("Object") && keywordArray[j].hasAirpediaClass("Organisation")) {
//								logger.trace(String.format("%s OBJ ---> %s ORG", keywordArray[i].getSense(), keywordArray[j].getSense()));
//								keywordArray[j].setFiltered(true);
//								continue;
//							}

							// If they are linked to the same page, keep the biggest one
							if (keywordArray[i].getSense() != null && keywordArray[i].getSense().equals(keywordArray[j].getSense())) {
								logger.debug(keywordArray[i].getForm() + ":" + keywordArray[i].getSense() + " == " + keywordArray[j].getForm() + ":" + keywordArray[j].getSense());
								logger.debug(keywordArray[i].getMaxNGramLength() + " ? " + keywordArray[j].getMaxNGramLength());

								if (keywordArray[i].getMaxNGramLength() > keywordArray[j].getMaxNGramLength()) {
									keywordArray[j].setFiltered(true);
									count++;
								}
								else {
									keywordArray[i].setFiltered(true);
									count++;
								}
							}

							// If they are not linked to the same page...
							else {

								// ... if they have the same weight, keep the biggest one
								//logger.debug(keywordArray[i].getForm() + ":" + keywordArray[i].getSense() + " != " + keywordArray[j].getForm() + ":" + keywordArray[j].getSense());
								if (keywordArray[i].weight() == keywordArray[j].weight()) {
									if (keywordArray[i].getMaxNGramLength() > keywordArray[j].getMaxNGramLength()) {
										keywordArray[j].setFiltered(true);
										count++;
									}
									else {
										keywordArray[i].setFiltered(true);
										count++;
									}

								}

								// ... otherwise keep the one with the highest weight
								else if (keywordArray[i].weight() > keywordArray[j].weight()) {
									keywordArray[j].setFiltered(true);
									count++;
								}
								else {
									keywordArray[i].setFiltered(true);
									count++;
								}
							}


							/*if (keywordArray[i].getSense().equals(keywordArray[j].weight())) {
								logger.warn(keywordArray[i].getForm() + ":" + keywordArray[i].getSense() + " == " + keywordArray[j].getForm() + ":" + keywordArray[j].getSense());
							} else {
								logger.warn(keywordArray[i].getForm() + ":" + keywordArray[i].getSense() + " != " + keywordArray[j].getForm() + ":" + keywordArray[j].getSense());
							}

							if (keywordArray[i].weight() == keywordArray[j].weight()) {
								if (keywordArray[i].getMaxNGramLength() > keywordArray[j].getMaxNGramLength()) {
									keywordArray[j].setFiltered(true);
									count++;
								}
								else {
									keywordArray[i].setFiltered(true);
									count++;
								}

							}
							else if (keywordArray[i].weight() > keywordArray[j].weight()) {
								keywordArray[j].setFiltered(true);
								count++;
							}
							else {
								keywordArray[i].setFiltered(true);
								count++;
							} */
						}
						//logger.debug("A\t " + i + "/" + j + "\t" + keywordArray[i].isFiltered() + "\t" + keywordArray[j].isFiltered());
					}
				}
			}

		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + total + " keywords filtered out in " + nf.format(time) + " ns");
	}


}
