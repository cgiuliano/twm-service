package com.machinelinking.jservice.annotation.key;

import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.analysis.Token;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/27/13
 * Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class NGramExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>NGramExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(NGramExtractor.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static NGramExtractor ourInstance;

	Configuration config;

	private static Pattern tabPattern = Pattern.compile("\t");

	private final HashMap<Locale, Set<String>> stopWordMap;

	int stopWordSize;

	int nGramLength;

	String stopWordDirName;

	public static synchronized NGramExtractor getInstance() {
		if (ourInstance == null) {
			try {
				ourInstance = new NGramExtractor();
			} catch (IOException e) {
				logger.error(e);
			}
		}
		return ourInstance;
	}

	private NGramExtractor() throws IOException {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}

		stopWordSize = config.getInt("stop-words-size");
		stopWordDirName = config.getString("stop-words-dir");
		nGramLength = config.getInt("n-gram-length");
		if (!stopWordDirName.endsWith(File.separator)) {
			stopWordDirName += File.separator;
		}
		logger.info("language model size " + stopWordSize);
		AutomaticConfiguration automaticConfiguration = AutomaticConfiguration.getInstance();

		String[] languages = automaticConfiguration.getLanguages();
		stopWordMap = new HashMap<Locale, Set<String>>();
		logger.info("creating an ngram extractor for " + languages.length + " languages...");
		for (int i = 0; i < languages.length; i++) {

			logger.debug(languages[i]);


			stopWordMap.put(new Locale(languages[i]), loadStopwords(languages[i]));
		}
	}

	private Set<String> loadStopwords(String language) throws IOException {

		File f = new File(stopWordDirName + language);
		logger.debug("reading stopwords from " + f + "...");
		Set<String> set = new HashSet<String>();

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line;

		int count = 0;
		while ((line = lnr.readLine()) != null) {
				set.add(line.trim());
				count++;

			if (count > stopWordSize) {
				break;
			}
		}
		logger.debug(set.size() + " stopwords read from " + f);
		lnr.close();
		return set;
	}

	public List<NGram> extract(Token[] tokenArray, String text, Locale locale) throws LanguageNotSupportedException {
		long tbegin = System.nanoTime();

		Set<String> stopwords = stopWordMap.get(locale);
		if (stopwords == null) {
			throw new LanguageNotSupportedException(locale.getDisplayLanguage(Locale.US) + " not supported yet");
		}

		Token firstToken = null, lastToken = null;
		int m = 0;
		String form = null, tokenizedForm;
		NGram ngram = null;
		int start = 0, end = 0;

		for (int i = 0; i < tokenArray.length; i++) {
			firstToken = tokenArray[i];
			start = firstToken.getStart();
			m = i + nGramLength + 1;
			if (m > tokenArray.length) {
				m = tokenArray.length;
			}
			for (int j = i; j < m; j++) {
				lastToken = tokenArray[j];
				end = lastToken.getEnd();
				//logger.debug(start + "\t" + end);
				//form = text.substring(start, end);

				//replaced with the tokenized version!!!!
				//NGramSearcher.Entry entry = searcher.search(form);
				//tokenizedForm = tokenizedForm(tokenArray, i, j);
				//logger.debug(i + "/" + j + "\t" + start + "\t" + end + "\t" + form);
				//logger.debug(i + "/" + j + "\t" + start + "\t" + end + "\t" + tokenizedForm);
				//	ngram = new NGram(start, end, i, j, form);
			}
		}

		long tend = System.nanoTime();
		long time = tend - tbegin;
		//logger.info(count + "/" + keywordArray.length + " keywords filtered in " + nf.format(time) + " ns");
		return null;
	}

}
