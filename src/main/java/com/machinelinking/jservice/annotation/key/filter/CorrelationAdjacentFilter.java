/*
 * Copyright (2013) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.key.filter;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.index.PageIncomingOutgoingSearcher;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/20/13
 * Time: 2:41 PM
 * To change this template use File | Settings | File Templates.
 */
@Deprecated public class CorrelationAdjacentFilter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CorrelationAdjacentFilter</code>.
	 */
	static Logger logger = Logger.getLogger(CorrelationAdjacentFilter.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	AutomaticConfiguration automaticConfiguration;

	Map<Locale, PageIncomingOutgoingSearcher> pageIncomingOutgoingSearcherMap;

	private static CorrelationAdjacentFilter ourInstance;

	Configuration config;

	public static CorrelationAdjacentFilter getInstance() {
		if (ourInstance == null) {
			ourInstance = new CorrelationAdjacentFilter();
		}
		return ourInstance;
	}

	private CorrelationAdjacentFilter() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	void init(String[] languages) {
		pageIncomingOutgoingSearcherMap = new HashMap<Locale, PageIncomingOutgoingSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			logger.debug("loading " + languages[i] + "\t" + resourceMap.get("incoming-outgoing-index") + "...");

			String indexName = resourceMap.get("incoming-outgoing-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(indexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (indexName != null) {
					PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher = new PageIncomingOutgoingSearcher(indexName);

					pageIncomingOutgoingSearcher.loadCache(cacheName, minFreq);
					//todo: change 1000
					//pageIncomingOutgoingSearcher.loadCache(cacheName, 1000);
					pageIncomingOutgoingSearcherMap.put(new Locale(languages[i]), pageIncomingOutgoingSearcher);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public void filter(Keyword[] keywordArray) {
		filter(keywordArray, null);
	}

	public void filter(Keyword[] keywordArray, Locale locale) {
		long begin = System.nanoTime();
		int count = 0;
		PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher = pageIncomingOutgoingSearcherMap.get(locale);
		String pi, pj = null;
		double[][] matrix = new double[keywordArray.length][keywordArray.length];
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].getSense() != null) {
				pi = keywordArray[i].getSense().getPage();

				double max = 0;
				for (int j = 0; j < i; j++) {
					if (!keywordArray[j].isFiltered() && keywordArray[j].getSense() != null) {
						pj = keywordArray[j].getSense().getPage();
						logger.debug(j + "\t" + pi + "\t" + pj + "\t" + matrix[i][j]);
						if (matrix[i][j] > max) {
							max = matrix[i][j];
						}
					}
				}

				for (int j = i + 1; j < keywordArray.length; j++) {
					if (!keywordArray[j].isFiltered() && keywordArray[j].getSense() != null) {
						pj = keywordArray[j].getSense().getPage();
						if (!pi.equals(pj)) {
							matrix[i][j] = matrix[j][i] = pageIncomingOutgoingSearcher.compare(pi, pj);
							logger.debug(j + "\t" + pi + "\t" + pj + "\t" + matrix[i][j]);
							if (matrix[i][j] > max) {
								max = matrix[i][j];
							}
						}
					}
				}


				if (max > 0.9) {
					logger.info(pi + "\tMAX " + max + " > 0.9");
				}
				else {
					logger.warn("MAX " + max);
				}
			}

		}
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + keywordArray.length + " keyword filtered in " + nf.format(time) + " ns");
	}

	private double[][] buildCorrelationMatrix(Keyword[] keywordArray, Locale locale) {
		PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher = pageIncomingOutgoingSearcherMap.get(locale);
		String pi, pj = null;
		double[][] matrix = new double[keywordArray.length][keywordArray.length];
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].getSense() != null) {
				pi = keywordArray[i].getSense().getPage();

				for (int j = i + 1; j < keywordArray.length; j++) {
					if (!keywordArray[j].isFiltered() && keywordArray[j].getSense() != null) {
						pj = keywordArray[j].getSense().getPage();
						if (!pi.equals(pj)) {
							matrix[i][j] = matrix[j][i] = pageIncomingOutgoingSearcher.compare(pi, pj);
						}
					}
				}
			}
		}
		return matrix;
	}
}
