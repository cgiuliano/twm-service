/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.lang;

import com.machinelinking.jservice.math.Node;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import com.machinelinking.jservice.util.FeatureIndex;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Token;
import eu.fbk.twm.utils.analysis.Tokenizer;
import eu.fbk.twm.utils.GenericFileUtils;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

//import eu.fbk.twm.utils.analysis.HardTokenizer;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/22/12
 * ServiceTime: 5:14 PM
 * To change this template use File | Settings | File Templates.
 * todo: solve the problem with hy, hi,...
 * todo: solve the problem with zh and ja (armenian)
 */
public class LangIdentifier {

    /**
     * Define a static logger variable so that it references the
     * Logger instance named <code>LangIdentifier</code>.
     */
    static Logger logger = Logger.getLogger(LangIdentifier.class.getName());

    private static LangIdentifier ourInstance = null;

    //ExecutorService executor;
    //final static int DEFAULT_NTHREDS = 8;

    protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

    DecimalFormat nf = new DecimalFormat("000,000,000.#");

    FeatureIndex[] featureIndex;

    //private static String[] languages = {"ar", "nl", "de", "ja", "ru", "ca", "en", "el", "no", "es", "zh", "fi", "hu", "po", "sv", "cs", "fr", "it", "pt", "uk", "tr"};
    //todo: sort for frequency at least the first one must be the most frequent (en)
    //private static String[] languages = {"bg", "ca", "cs", "da", "de", "en", "es", "fi", "fr", "it", "nl", "no", "pl", "pt", "ru", "sv", "tr"};
    //private static String[] languages = {"be", "bg", "ca", "cs", "da", "de", "en", "es", "et", "fi", "fr", "hu", "is", "it", "lt", "lv", "nl", "no", "pl", "pt", "ro", "ru", "sk", "sl", "sq", "sr", "sv", "tr", "uk", "id"};
    private static String[] languages;

    Node[][] langVector;

    Configuration config;

    int uniGramSize;

    //private String languageModelsDir;

    private static Pattern tabPattern = Pattern.compile("\t");

    protected int maxTermFrequency;

    public static synchronized LangIdentifier getInstance() {
        if (ourInstance == null) {
            try {
                ourInstance = new LangIdentifier();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return ourInstance;
    }

    private LangIdentifier() throws IOException {
        ConfigurationReader configurationReader = new ConfigurationReader();
        try {
            config = configurationReader.getConfiguration();
        } catch (ConfigurationReaderException e) {
            logger.error(e);
        }
        maxTermFrequency = config.getInt("uni-gram-max-tf");
        uniGramSize = config.getInt("uni-gram-size");
        logger.info("language model size " + uniGramSize);

        AutomaticConfiguration automaticConfiguration = AutomaticConfiguration.getInstance();

        String languageDir = config.getString("language-models-dir");
        logger.info("Folder for language recognition: " + languageDir);

        Map<String, Map<String, String>> resources = new HashMap<>();
        if (languageDir != null) {
            if (!languageDir.endsWith(File.separator)) {
                languageDir += File.separator;
            }
            languageDir += "wikipedia" + File.separator;
            String[] tmpLang = new File(languageDir).list();
            for (int i = 0; i < tmpLang.length; i++) {
                Map<String, String> resourceMap = GenericFileUtils
                        .searchForFilesInTheSameFolder(languageDir + tmpLang[i], "unigram");
                if (resourceMap != null) {
                    resources.put(tmpLang[i], resourceMap);
                }
            }
            languages = new String[resources.keySet().size()];
            int i = 0;
            for (String lang : resources.keySet()) {
                languages[i++] = lang;
            }

        } else {
            languages = automaticConfiguration.getLanguages();
            for (int i = 0; i < languages.length; i++) {
                Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
                if (resourceMap != null) {
                    resources.put(languages[i], resourceMap);
                }
            }
        }

        logger.info("creating a language detector for " + languages.length + " languages...");
        featureIndex = new FeatureIndex[languages.length];
        langVector = new Node[languages.length][];
        for (int i = 0; i < languages.length; i++) {
            Map<String, String> resourceMap = resources.get(languages[i]);
            logger.debug(languages[i] + "\t" + resourceMap);
            logger.debug(resourceMap.get("unigram"));
            String unigramFileName = resourceMap.get("unigram");
            featureIndex[i] = new FeatureIndex(false);
            logger.info("loading " + languages[i] + " (" + i + ") from " + unigramFileName + "...");
            langVector[i] = loadLanguageModel(unigramFileName, i);
        }
    }

    private Node[] loadLanguageModel(String unigramFileName, int i) throws IOException {
        //File f = new File("freqs/freq_" + languages[i] + ".txt");

        //File f = new File(languageModelsDir + languages[i]);
        File f = new File(unigramFileName);
        logger.debug("reading language model from " + f + "...");
        List<Node> vec = new ArrayList<Node>();
        Node node = null;
        LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
        String line;
        String[] t;
        int count = 0;
        while ((line = lnr.readLine()) != null) {
            t = tabPattern.split(line);
            if (t.length == 2) {
                node = new Node();
                node.index = featureIndex[i].add(t[1]);
                //node.value = Double.parseDouble(t[0]) / 114539523 + 1;
                node.value = Double.parseDouble(t[0]) / maxTermFrequency + 1;
                //logger.debug(i + "\t" + t[1] + "\t" + node.index + ":" + node.value);
                vec.add(node);
                count++;
            }

            if (count > uniGramSize) {
                break;
            }
        }
        lnr.close();
        return vec.toArray(new Node[vec.size()]);
    }

    private class Counter {

        int counter;

        public int getCount() {
            return counter;
        }

        Counter(int counter) {
            this.counter = counter;
        }

        void inc() {
            counter++;
        }
    }

    Map<String, Counter> bow(String[] stringArray) {
        Map<String, Counter> map = new HashMap<String, Counter>();
        Counter counter;
        String form;
        for (int i = 0; i < stringArray.length; i++) {
            form = stringArray[i].toLowerCase();
            //logger.debug(i + "\t" + form);
            counter = map.get(form);
            if (counter == null) {
                map.put(form, new Counter(1));
            } else {
                counter.inc();
            }
        }
        return map;
    }

    Map<String, Counter> bow(Token[] tokenArray) {
        Map<String, Counter> map = new HashMap<String, Counter>();
        Counter counter;
        String form;
        for (int i = 0; i < tokenArray.length; i++) {
            form = tokenArray[i].getForm().toLowerCase();
            //logger.debug(i + "\t" + form);
            counter = map.get(form);
            if (counter == null) {
                map.put(form, new Counter(1));
            } else {
                counter.inc();
            }
        }
        return map;
    }

    Node[] map(Map<String, Counter> bow, int k) {
        //logger.debug("mapping " + languages[k] + "...");
        SortedSet<Node> vec = new TreeSet<Node>();

        //Node[] vec = new Node[set.size()];
        Node node = null;
        int j = 0;
        Counter counter;
        String form;
        Iterator<String> it = bow.keySet().iterator();
        for (int i = 0; it.hasNext(); i++)
        //for (int i=0;i<tokenArray.size();i++)
        {
            //form = tokenArray.get(i).getForm();
            form = it.next();
            counter = bow.get(form);
            j = featureIndex[k].getIndex(form);
            if (j != -1) {
                node = new Node();
                node.index = j;
                node.value = counter.getCount();
                //logger.debug(i + "\t" + form + "\t" + node.index + ":" + node.value);
                vec.add(node);
            }
        }

        return vec.toArray(new Node[vec.size()]);
    }

    public Locale detect(String[] stringArray) {
        //long begin = System.nanoTime();

        Locale locale = detectByEncoding(stringArray);
        if (locale != null) {
            return locale;
        }

        //logger.debug("detecting language " + stringArray.length + "...");
        Map<String, Counter> bow = bow(stringArray);
        //todo: use the encoding
        double dot = 0;
        double maxDot = -1;
        int maxLang = -1;
        for (int i = 0; i < langVector.length; i++) {
            Node[] vec = map(bow, i);
            //logger.debug("vec " + Node.toString(vec));
            //logger.debug("lng " + Node.toString(langVector[i]));
            dot = Node.dot(vec, langVector[i]);
            //logger.debug(i + "\t" + languages[i] + "\t" + dot);
            if (dot > maxDot) {
                //logger.debug(i + "\t" + languages[i] + "\t" + dot);
                maxLang = i;
                maxDot = dot;
            }
        }

        //long end = System.nanoTime();
        //long time = end - begin;
        //logger.debug("detected " + languages[maxLang] + " (" + maxDot + ") in " + nf.format(time) + " ns");

        return new Locale(languages[maxLang]);
    }

    public Locale detect(Token[] tokenArray) {
        //long begin = System.nanoTime();

        Locale locale = detectByEncoding(tokenArray);
        if (locale != null) {
            return locale;
        }

        //logger.debug("detecting language " + tokenArray.length + "...");
        Map<String, Counter> bow = bow(tokenArray);
        //todo: use the encoding
        double dot = 0;
        double maxDot = -1;
        int maxLang = -1;
        for (int i = 0; i < langVector.length; i++) {
            Node[] vec = map(bow, i);
            //logger.debug("vec " + Node.toString(vec));
            //logger.debug("lng " + Node.toString(langVector[i]));
            dot = Node.dot(vec, langVector[i]);
            //logger.debug(i + "\t" + languages[i] + "\t" + dot);
            if (dot > maxDot) {
                //logger.debug(i + "\t" + languages[i] + "\t" + dot);
                maxLang = i;
                maxDot = dot;
            }
        }

        //long end = System.nanoTime();
        //long time = end - begin;
        //logger.debug("detected " + languages[maxLang] + " (" + maxDot + ") in " + nf.format(time) + " ns");

        return new Locale(languages[maxLang]);
    }

    private Locale detectByEncoding(String[] stringArray) {
        double avg = average(stringArray);
        //logger.debug("avg " + avg + "\t" + Integer.toHexString((int) avg));
        return detectLocale(avg);
    }

    private Locale detectByEncoding(Token[] tokenArray) {
        double avg = average(tokenArray);
        //logger.debug("avg " + avg + "\t" + Integer.toHexString((int) avg));
        return detectLocale(avg);
    }

    private Locale detectLocale(double avg) {
        /*if (avg >= 0x0400 && avg <= 0x04F9) {
            //CYRILLIC
			return new Locale("ru");
		}
		else */
        if (avg >= 0x060C && avg <= 0x06FE) {
            //arabic
            return new Locale("ar");
        } else if (avg >= 0x0374 && avg <= 0x03F3) {
            //greek
            return new Locale("el");
        } else if (avg >= 0x1F00 && avg <= 0x1FFE) {
            //Greek Extended
            return new Locale("el");
        } else if (avg >= 0xFB13 && avg <= 0xFB17) {
            //armenian
            return new Locale("hy");
        } else if (avg >= 0x0530 && avg <= 0x058F) {
            //armenian
            return new Locale("hy");
        } else if (avg >= 0x0591 && avg <= 0x05F4) {
            //hebrew
            return new Locale("he");
        } else if (avg >= 0x0901 && avg <= 0x03F3) {
            //Standard Hindi, Marathi, and Nepal
            return new Locale("hi");
        } else if (avg >= 0x0981 && avg <= 0x09FA) {
            //bengali
            return new Locale("bn");
        } else if (avg >= 0x0A02 && avg <= 0x0A74) {
            //GURMUKHI - Punjabi language in India
            return new Locale("pa");
        } else if (avg >= 0x0A80 && avg <= 0x0AFF) {
            //Gujarati
            return new Locale("gu");
        } else if (avg >= 0x0B00 && avg <= 0x0B7F) {
            //Oriya language
            return new Locale("or");
        } else if (avg >= 0x0B80 && avg <= 0x0BFF) {
            //Tamil
            return new Locale("ta");
        } else if (avg >= 0x0C00 && avg <= 0x0C7F) {
            //Telugu
            return new Locale("te");
        } else if (avg >= 0x0C82 && avg <= 0x0CEF) {
            //Kannada
            return new Locale("kn");
        } else if (avg >= 0x0B00 && avg <= 0x0D02) {
            //Malayalam
            return new Locale("ml");
        } else if (avg >= 0x0D82 && avg <= 0x0DF4) {
            //SINHALA
            return new Locale("si");
        } else if (avg >= 0x0E01 && avg <= 0x0E5B) {
            //THAI
            return new Locale("th");
        } else if (avg >= 0x0E81 && avg <= 0x0EDD) {
            //LAO
            return new Locale("lo");
        } else if (avg >= 0x0F00 && avg <= 0x0FCF) {
            //Tibetan
            return new Locale("bo");
        } else if (avg >= 0x1000 && avg <= 0x1059) {
            //MYANMAR
            return new Locale("my");
        } else if (avg >= 0x10A0 && avg <= 0x10FB) {
            //GEORGIAN
            return new Locale("ka");
        } else if (avg >= 0xAC00 && avg <= 0xD7AF) {
            //HANGUL
            return new Locale("ko");
        } else if (avg >= 0x1100 && avg <= 0x11FF) {
            //HANGUL
            return new Locale("ko");
        } else if (avg >= 0x3130 && avg <= 0x318F) {
            //HANGUL
            return new Locale("ko");
        } else if (avg >= 0x3200 && avg <= 0x32F) {
            //HANGUL
            return new Locale("ko");
        } else if (avg >= 0xA960 && avg <= 0xA97F) {
            //HANGUL
            return new Locale("ko");
        } else if (avg >= 0xD7B0 && avg <= 0xD7FF) {
            //HANGUL
            return new Locale("ko");
        } else if (avg >= 0xFF00 && avg <= 0xFFEF) {
            //HANGUL
            return new Locale("ko");
        } else if (avg >= 0x1200 && avg <= 0x0E5B) {
            //ETHIOPIC  (ISO 639-2)
            return new Locale("gez");
        } else if (avg >= 0x1300 && avg <= 0x13F4) {
            //Cherokee ISO 639-2	chr
            return new Locale("chr");
        }
        //Missing
        //14: Unified Canadian Aboriginal Syllabics
        //15: ..14 contd..
        //16: ..14 contd.., Ogham, Runic
        else if (avg >= 0x1780 && avg <= 0x17E9) {
            //KHMER
            return new Locale("km");
        } else if (avg >= 0x1800 && avg <= 0x18A9) {
            //MONGOLIAN
            return new Locale("mn");
        }
        //1E: Latin Extended Additional
        //1F: Greek Extended
        //20: General Punctuation, Super/Subscripts, Currency, Combining Diacriticals
        //21: Letterlike Symbols, Number Forms, Arrows
        //22: Mathematical Operators
        //23: Misc. Technical
        //24: Control Pictures, OCR, Enclosed Alphanumerics
        //25: Box Drawing, Block Elements, Geometric Shapes
        //26: Misc Symbols
        //27: Dingbats
        //28: Braille patterns
        else if (avg >= 0x2E80 && avg <= 0x2EF3) {
            //CJK Radicals Supplement
            return new Locale("zh");
        } else if (avg >= 0x3105 && avg <= 0x2FFB) {
            //Kangxi Radicals, Ideographic Description Characters
            return new Locale("zh");
        } else if (avg >= 0x2F00 && avg <= 0x31B7) {
            //Bopomofo, Hangul Compatibility Jamo, Kanbun
            return new Locale("zh");
        }
        //32: Enclosed CJK Letters and Months
        //33: CJK Compatibility
        //34: CJK Unified Ideographs Extension A
        //4D
        //4E: CJK Unified Ideographs
        //9F
        else if (avg >= 0xA000 && avg <= 0xA4C6) {
            //Yi
            return new Locale("ii");
        } else if (avg >= 0x4E00 && avg <= 0x9FBF) {
            //Kanji
            return new Locale("ja");
        } else if (avg >= 0x3040 && avg <= 0x309F) {
            //Hiragana
            return new Locale("ja");
        } else if (avg >= 0x30A0 && avg <= 0x30FF) {
            //Katakana
            return new Locale("ja");
        }

        return null;
    }

    private double average(Token[] tokenArray) {
        int sum = 0;
        int count = 0;
        String form;
        char ch;
        for (int i = 0; i < tokenArray.length; i++) {
            form = tokenArray[i].getForm();
            //logger.debug(i + "\t" + form);
            for (int j = 0; j < form.length(); j++) {
                ch = form.charAt(j);
                if (Character.isLetter(ch)) {
                    sum += ch;
                    count++;
                }

            }
        }
        return (double) (sum / count);
    }

    private double average(String[] stringArray) {
        int sum = 0;
        int count = 0;
        String form;
        char ch;
        for (int i = 0; i < stringArray.length; i++) {
            form = stringArray[i];
            //logger.debug(i + "\t" + form);
            for (int j = 0; j < form.length(); j++) {
                ch = form.charAt(j);
                if (Character.isLetter(ch)) {
                    sum += ch;
                    count++;
                }

            }
        }
        return (double) (sum / count);
    }

    public void interactive() throws Exception {
        InputStreamReader indexReader = null;
        BufferedReader myInput = null;
        long begin = 0, end = 0;
        Tokenizer tokenizer = HardTokenizer.getInstance();

        while (true) {
            System.out.println("\nPlease write a query and type <return> to continue (CTRL C to exit):");
            indexReader = new InputStreamReader(System.in);
            myInput = new BufferedReader(indexReader);
            String query = myInput.readLine().toString();
            begin = System.nanoTime();

            Token[] tokenArray = tokenizer.tokenArray(query);
            LangIdentifier langIdentifier = LangIdentifier.getInstance();
            Locale locale = langIdentifier.detect(tokenArray);
            end = System.nanoTime();
            logger.info(
                    locale + "\t" + locale.getDisplayLanguage(Locale.US) + "\t" + locale.getDisplayCountry() + "\t" + df
                            .format(end - begin));
        }
    }

    public static void main(String[] args) throws Exception {
        // java com.ml.test.net.HttpServerDemo
        String logConfig = System.getProperty("log-config");
        if (logConfig == null) {
            logConfig = "log-config.txt";
        }

        PropertyConfigurator.configure(logConfig);
        //java -cp dist/jservice.jar com.machinelinking.annotation.lang.LangIdentifier
        /*Tokenizer tokenizer = HardTokenizer.getInstance();

		Token[] tokenArray = tokenizer.tokenArray(args[0]);
		LangIdentifier langIdentifier = LangIdentifier.getInstance();
		Locale locale = langIdentifier.detect(tokenArray);
		logger.info(locale + "\t" + locale.getDisplayLanguage(Locale.US) + "\t" + locale.getDisplayCountry()); */
        LangIdentifier langIdentifier = LangIdentifier.getInstance();
        langIdentifier.interactive();

    }
}
