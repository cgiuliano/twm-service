package com.machinelinking.jservice.annotation.topic;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.Sense;
import eu.fbk.twm.utils.WeightedSet;

import java.text.DecimalFormat;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/20/13
 * Time: 6:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopicClassifier {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TopicClassifier</code>.
	 */
	static Logger logger = Logger.getLogger(TopicClassifier.class.getName());
	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	DecimalFormat df = new DecimalFormat("0.000");

	AutomaticConfiguration automaticConfiguration;

	private int maxNumberOfTopicsToReturn;

	private int maxNumberOfKeywords;

	public static final int DEFAULT_MAX_NUMBER_OF_KEYWORDS = 5;

	private static TopicClassifier ourInstance;

	public static TopicClassifier getInstance() {
		if (ourInstance == null) {
			ourInstance = new TopicClassifier();
		}
		return ourInstance;
	}

	private TopicClassifier() {
		logger.debug("creating a topic detector...");
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();
			logger.info("supported languages " + Arrays.toString(languages));

		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}


	public void classify(Keyword[] keywordArray, TopicSet textTopicSet) {

		WeightedSet weightedSet = new WeightedSet();
		long begin = System.nanoTime();
		logger.debug("detecting topic " + keywordArray.length + "...");
		int count = 0;
		int maxIndex = 0;
		if (textTopicSet == null || textTopicSet.size() == 0) {
			return;
		}

		//iterates over the keywords
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered()) {

				TopicSet[] topicSetArray = keywordArray[i].getTopicSetArray();
				Sense[] senseArray = keywordArray[i].getSenseArray();
				//logger.debug(i + "\t" + keywordArray[i].getForm() + "\t" + topicArray.length+"\t"+Arrays.toString(topicArray));
				//logger.debug(i + "\t" + keywordArray[i].getForm() + "\t" + senseArray.length+"\t"+Arrays.toString(senseArray));
				logger.debug(i + "\t" + keywordArray[i].getForm() + "\t" + keywordArray[i].weight());
				//iterates over the senses and topics
				for (int j = 0; j < senseArray.length; j++) {
					TopicSet senseTopicSet = topicSetArray[j];
					double dot = 0;
					if (senseTopicSet != null && senseTopicSet.size() > 0) {
						double dot12 = senseTopicSet.dot(textTopicSet);
						if (dot12 != 0) {
							double dot11 = senseTopicSet.dot(senseTopicSet);
							double dot22 = textTopicSet.dot(textTopicSet);
							dot = dot12 / Math.sqrt(dot11 * dot22);
						}
						logger.debug("\t" + j + "\t" + senseArray[j].getPage() + "\t" + df.format(senseArray[j].getCombo()) + "\t" + df.format(dot) + "\t" + senseTopicSet.topicList());
					} else {
						logger.debug("\t" + j + "\t" + senseArray[j].getPage() + "\t" + df.format(senseArray[j].getCombo()) + "\t" + df.format(dot));
					}

				}
				count++;
			}
		}
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + keywordArray.length + " keywords classified (" + (keywordArray.length - count) + ") in " + nf.format(time) + " ns");
	}
}
