/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.topic;

import eu.fbk.twm.utils.*;
import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.index.CategorySuperCategorySearcher;
import eu.fbk.twm.index.PageCategorySearcher;
import eu.fbk.twm.index.PageNavigationTemplateSearcher;
import eu.fbk.twm.index.PagePortalSearcher;
import eu.fbk.twm.index.util.SetSearcher;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 7/25/13
 * Time: 3:09 PM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * todo: move in machinelinking and create an index page -> top category (offline)
 *
 * @deprecated
 */
@Deprecated
public class PageTopicMultiSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageTopicSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(PageTopicMultiSearcher.class.getName());

	PagePortalSearcher pagePortalSearcher = null;
	PageNavigationTemplateSearcher pageNavigationTemplateSearcher = null;
	PageCategorySearcher pageCategorySearcher = null;
	CategorySuperCategorySearcher categorySuperCategorySearcher = null;

	Properties portalProperties = null;
	Properties navProperties = null;
	Properties suffixProperties = null;
	Properties catProperties = null;

	boolean useCategories = false;
	boolean useNavs = false;
	boolean usePortals = false;
	boolean useSuffixes = false;

	public static final int DEFAULT_DEPTH = 10;
	int maxDepth;

	protected static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);
	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	private void init(String pageCategoryIndexName, String categorySuperCategoryIndexName, String categoryMappingFileName,
					  String pagePortalIndexName, String portalMappingFileName,
					  String pageNavigationTemplateIndexName, String navigationTemplateMappingFileName,
					  String suffixMappingFileName,
					  int maxDepth
	) {
		logger.debug("Files for INIT");
		logger.debug(pageCategoryIndexName);
		logger.debug(categorySuperCategoryIndexName);
		logger.debug(categoryMappingFileName);
		logger.debug(pagePortalIndexName);
		logger.debug(portalMappingFileName);
		logger.debug(pageNavigationTemplateIndexName);
		logger.debug(navigationTemplateMappingFileName);
		logger.debug(suffixMappingFileName);

		if (checkExistance(pageCategoryIndexName, categorySuperCategoryIndexName, categoryMappingFileName)) {
			try {
				catProperties = loadPropertiesFromFile(categoryMappingFileName);
				pageCategorySearcher = new PageCategorySearcher(pageCategoryIndexName);
				categorySuperCategorySearcher = new CategorySuperCategorySearcher(categorySuperCategoryIndexName);
				this.maxDepth = maxDepth;
				if (maxDepth == -1) {
					this.maxDepth = DEFAULT_DEPTH;
				}
				useCategories = true;
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		if (checkExistance(pagePortalIndexName, portalMappingFileName)) {
			try {
				portalProperties = loadPropertiesFromFile(portalMappingFileName);
				pagePortalSearcher = new PagePortalSearcher(pagePortalIndexName);
				usePortals = true;
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		if (checkExistance(pageNavigationTemplateIndexName, navigationTemplateMappingFileName)) {
			try {
				navProperties = loadPropertiesFromFile(navigationTemplateMappingFileName);
				pageNavigationTemplateSearcher = new PageNavigationTemplateSearcher(pageNavigationTemplateIndexName);
				useNavs = true;
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		if (checkExistance(suffixMappingFileName)) {
			try {
				suffixProperties = loadPropertiesFromFile(suffixMappingFileName);
				useSuffixes = true;
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		System.out.println("Constructor [2] loaded");
	}

	public PageTopicMultiSearcher(String baseFolder, Locale lang, String baseMappingFolder, int maxDepth) {

		if (!baseMappingFolder.endsWith(File.separator)) {
			baseMappingFolder += File.separator;
		}
		if (!baseFolder.endsWith(File.separator)) {
			baseFolder += File.separator;
		}

		String folder = baseFolder + lang + File.separator;
		try {
			Map<String, String> folders = GenericFileUtils.searchForFilesInTheSameFolder(
					folder, "page-category-index", "category-super-category-index", "page-portal-index", "page-navigation-index");

			String catMappingFile = baseMappingFolder + lang + "wiki-cat2topic.properties";
			String navMappingFile = baseMappingFolder + lang + "wiki-navtpl2topic.properties";
			String portalMappingFile = baseMappingFolder + lang + "wiki-portal2topic.properties";
			String suffMappingFile = baseMappingFolder + lang + "wiki-suff2topic.properties";

			init(
					folders.get("page-category-index"), folders.get("category-super-category-index"), catMappingFile,
					folders.get("page-portal-index"), portalMappingFile,
					folders.get("page-navigation-index"), navMappingFile,
					suffMappingFile,
					maxDepth
			);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public PageTopicMultiSearcher(String baseFolder, Locale lang, String baseMappingFolder) {
		this(baseFolder, lang, baseMappingFolder, DEFAULT_DEPTH);
	}

	public PageTopicMultiSearcher(
			String pageCategoryIndexName, String categorySuperCategoryIndexName, String categoryMappingFileName,
			String pagePortalIndexName, String portalMappingFileName,
			String pageNavigationTemplateIndexName, String navigationTemplateMappingFileName,
			String suffixMappingFileName
	) {
		this(pageCategoryIndexName, categorySuperCategoryIndexName, categoryMappingFileName, pagePortalIndexName, portalMappingFileName, pageNavigationTemplateIndexName, navigationTemplateMappingFileName, suffixMappingFileName, DEFAULT_DEPTH);
	}

	public PageTopicMultiSearcher(
			String pageCategoryIndexName, String categorySuperCategoryIndexName, String categoryMappingFileName,
			String pagePortalIndexName, String portalMappingFileName,
			String pageNavigationTemplateIndexName, String navigationTemplateMappingFileName,
			String suffixMappingFileName,
			int maxDepth
	) {
		init(pageCategoryIndexName, categorySuperCategoryIndexName, categoryMappingFileName, pagePortalIndexName, portalMappingFileName, pageNavigationTemplateIndexName, navigationTemplateMappingFileName, suffixMappingFileName, maxDepth);
	}

	private boolean checkExistance(String f1, String... files) {
		String[] all = new String[files.length + 1];
		all[0] = f1;
		System.arraycopy(files, 0, all, 1, files.length);

		for (String s : all) {
			if (s == null) {
				return false;
			}
			File tmp = new File(s);
			if (!tmp.exists()) {
				logger.debug("File " + s + " does not exist");
				return false;
			}
		}

		return true;
	}

	public PageTopicMultiSearcher(
			PageCategorySearcher pageCategorySearcher, CategorySuperCategorySearcher categorySuperCategorySearcher, int maxDepth, Properties catProperties,
			PagePortalSearcher pagePortalSearcher, Properties portalProperties,
			PageNavigationTemplateSearcher pageNavigationTemplateSearcher, Properties navProperties,
			Properties suffixProperties
	) {
//		super(pageCategorySearcher, categorySuperCategorySearcher, maxDepth, catProperties);
		this.pageCategorySearcher = pageCategorySearcher;
		this.categorySuperCategorySearcher = categorySuperCategorySearcher;
		this.pagePortalSearcher = pagePortalSearcher;
		this.pageNavigationTemplateSearcher = pageNavigationTemplateSearcher;

		this.portalProperties = portalProperties;
		this.navProperties = navProperties;
		this.suffixProperties = suffixProperties;
		this.catProperties = catProperties;

		useCategories = true;
		useNavs = true;
		usePortals = true;
		useSuffixes = true;

		this.maxDepth = maxDepth;
		System.out.println("Constructor [1] loaded");
	}

	public void search(String page, WeightedSet weightedSet) {
		search(page, weightedSet, 0);
	}

	public WeightedSet search(String page) {
		return search(page, 0);
	}

	private static String normalizePageName(String s) {
		if (s.length() == 0) {
			return s;
		}

		if (Character.isUpperCase(s.charAt(0))) {
			return s;
		}

		return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
	}

	private String tabulator(int l) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < l; i++) {
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
		}
		return sb.toString();
	}

	private void search(String[] categories, WeightedSet textWeightedSet, int depth, String category, Set<String> visitedSet) {

		if (depth > maxDepth) {
			//logger.debug("stop1 " + depth);
			return;
		}

		for (int i = 0; i < categories.length; i++) {
			String normalizedCategory = normalizePageName(categories[i]);
			String label = catProperties.getProperty(normalizedCategory);
			if (label != null) {
				if (label.length() == 0) {
//					System.out.println(tabulator(depth) + "<" + normalizedCategory + ", STOP, " + depth + ">");
				}
				else {
//					System.out.println(tabulator(depth) + "<" + normalizedCategory + ", '" + label + "', " + depth + ">");
					textWeightedSet.add(label, ((double) 1 / depth));
				}
			}
			else {

				if (!visitedSet.contains(normalizedCategory)) {
					visitedSet.add(normalizedCategory);
					try {
						String[] superCategories = categorySuperCategorySearcher.search(normalizedCategory);
						if (superCategories.length > 0) {
//							System.out.println(tabulator(depth) + "{" + normalizedCategory + ", " + depth + ", " + superCategories.length + ", " + Arrays.toString(superCategories) + "}");
							search(superCategories, textWeightedSet, depth + 1, normalizedCategory, visitedSet);
						}

					} catch (Exception e) {
						logger.error(e);
					}
				}
			}
		}
	}

	public WeightedSet search(String page, double weight) {
		WeightedSet ret = new WeightedSet();
		search(page, ret, weight);
		return ret;
	}

	public void search(String page, WeightedSet weightedSet, double weight) {

		logger.debug("PAGE: " + page);

		// Categories
		if (useCategories) {
			logger.debug("Searching categories");
			Set<String> visitedSet = new HashSet<String>();
			String[] categories = pageCategorySearcher.search(page);
			logger.trace("searching page " + page + " (" + weight + ")...");
			logger.trace("categories\t" + categories.length + "\t" + Arrays.toString(categories));
			search(categories, weightedSet, 1, page, visitedSet);
			logger.trace(page + "\t" + weightedSet.size() + "\t" + weightedSet.toSortedMap());
		}

		// Portals
		if (usePortals) {
			logger.debug("Searching portals");
			searchSet(pagePortalSearcher, page, weightedSet);
		}

		// Navigation templates
		if (useNavs) {
			logger.debug("Searching navigation templates");
			searchSet(pageNavigationTemplateSearcher, page, weightedSet);
		}

		// Suffix
		if (useSuffixes) {
			logger.debug("Searching suffix");
			ParsedPageTitle title = new ParsedPageTitle(page);
			if (title.hasSuffix()) {
				String suffix = title.getSuffix();
				if (suffixProperties != null) {
					if (suffixProperties.getProperty(suffix) != null && !suffixProperties.getProperty(suffix).equals("")) {
						String topic = suffixProperties.getProperty(suffix);
						weightedSet.add(topic);
						logger.info("Key: " + suffix + " - Topic: " + topic);
					}
				}
			}
		}

	}

	private void searchSet(SetSearcher s, String page, WeightedSet weightedSet) {
		searchSet(s, page, weightedSet, 1);
	}

	private void searchSet(SetSearcher s, String page, WeightedSet weightedSet, double weight) {
		String[] result = s.search(page);
		HashSet<String> okResults = new HashSet<String>();

		for (int i = 0; i < result.length; i++) {
			if (portalProperties != null) {
				if (portalProperties.getProperty(result[i]) != null && !portalProperties.getProperty(result[i]).equals("")) {
					String topic = portalProperties.getProperty(result[i]);
					okResults.add(topic);
					logger.info("Key: " + result[i] + " - Topic: " + topic);
				}
			}
		}

		for (String topic : okResults) {
			weightedSet.add(topic, weight / okResults.size());
		}
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();

				String page = s[0].replace(' ', CharacterTable.LOW_LINE);
				WeightedSet weightedSet = search(page);

				search(page, weightedSet);

				long end = System.nanoTime();

				logger.debug("*** Results ***");
				weightedSet.normalize();
				Map<Double, List<String>> sortedMap = weightedSet.toSortedMap();
				logger.debug(page + "\t" + sortedMap + "\t" + tf.format(end - begin));

			}
		}
	}

	public static Properties loadPropertiesFromFile(String fileName) throws IOException {

		Properties ret = new Properties();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.startsWith("#")) {
					continue;
				}

				String[] parts = line.split("=");
//				if (parts.length < 2) {
//					continue;
//				}

				String key = parts[0];
				String[] keyParts = key.split("\t");
				key = keyParts[keyParts.length - 1];

				String value = "";

				//todo: predisporre il multi-label
				if (parts.length >= 2) {
					String[] values = parts[1].split(",");
					for (String v : values) {
						if (v.trim().length() > 0) {
							value = v;
							break;
						}
					}
				}
				ret.put(key, value);
			}
			reader.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
//		Properties ret = new Properties();
//		File file = new File(fileName);
//		logger.info("reading category mappings from " + file + "...");
//		ret.load(new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")));
//		logger.debug(ret.size() + " mappings read");
		return ret;
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		CommandLine line = null;
		try {
			options.addOption(OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d"));
			options.addOption(OptionBuilder.withArgName("dir").hasArg().withDescription("model dir").isRequired().withLongOpt("model-dir").create("m"));

			options.addOption(OptionBuilder.withArgName("file").hasArg().withDescription("file from which to read the category mappings").isRequired().withLongOpt("category-mapping-file").create("c"));
			options.addOption(OptionBuilder.withArgName("file").hasArg().withDescription("file from which to read the portal mappings").isRequired().withLongOpt("portal-mapping-file").create("p"));
			options.addOption(OptionBuilder.withArgName("file").hasArg().withDescription("file from which to read the navigation template mappings").isRequired().withLongOpt("navigation-template-mapping-file").create("n"));
			options.addOption(OptionBuilder.withArgName("file").hasArg().withDescription("file from which to read the suffix mappings").isRequired().withLongOpt("suffix-mapping-file").create("s"));

			options.addOption(OptionBuilder.withArgName("int").hasArg().withDescription("recursion depth (default is " + PageTopicMultiSearcher.DEFAULT_DEPTH + ")").withLongOpt("depth").create());
			options.addOption(OptionBuilder.withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t"));
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			CommandLineParser parser = new PosixParser();
			line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/jservice.jar com.machinelinking.annotation.topic.PageTopicSearcher", "\n", options, "\n", true);
			System.exit(1);
		}

		// Start program

		ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("model-dir"), true);

		Properties catProperties = new Properties();
		if (line.hasOption("c")) {
			catProperties = loadPropertiesFromFile(line.getOptionValue("c"));
			logger.debug("Category mappings: " + catProperties.size());
		}

		Properties portalProperties = new Properties();
		if (line.hasOption("p")) {
			portalProperties = loadPropertiesFromFile(line.getOptionValue("p"));
			logger.debug("Portal mappings: " + portalProperties.size());
		}

		Properties navigationProperties = new Properties();
		if (line.hasOption("n")) {
			navigationProperties = loadPropertiesFromFile(line.getOptionValue("n"));
			logger.debug("Navigation template mappings: " + navigationProperties.size());
		}

		Properties suffixProperties = new Properties();
		if (line.hasOption("s")) {
			suffixProperties = loadPropertiesFromFile(line.getOptionValue("s"));
			logger.debug("Suffix mappings: " + suffixProperties.size());
		}

		int depth = DEFAULT_DEPTH;
		if (line.hasOption("depth")) {
			depth = Integer.parseInt(line.getOptionValue("depth"));
		}

		//logger.debug(extractorParameters);

		PageCategorySearcher pageCategorySearcher = new PageCategorySearcher(extractorParameters.getWikipediaPageCategoryIndexName());
		CategorySuperCategorySearcher categorySuperCategorySearcher = new CategorySuperCategorySearcher(extractorParameters.getWikipediaCategorySuperCategoryIndexName());
		PagePortalSearcher pagePortalSearcher = new PagePortalSearcher(extractorParameters.getWikipediaPagePortalIndexName());
		PageNavigationTemplateSearcher pageNavigationTemplateSearcher = new PageNavigationTemplateSearcher(extractorParameters.getWikipediaPageNavigationTemplateIndexName());

		PageTopicMultiSearcher PageTopicSearcher = new PageTopicMultiSearcher(
				pageCategorySearcher, categorySuperCategorySearcher, depth, catProperties,
				pagePortalSearcher, portalProperties,
				pageNavigationTemplateSearcher, navigationProperties,
				suffixProperties
		);

		if (line.hasOption("interactive-mode")) {

			try {
				PageTopicSearcher.interactive();
			} catch (Exception e) {
				logger.error(e);
			}
		}

	}
}
