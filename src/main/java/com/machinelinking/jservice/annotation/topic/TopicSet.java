package com.machinelinking.jservice.annotation.topic;

import org.apache.log4j.Logger;
import eu.fbk.twm.utils.WeightedSet;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/21/13
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopicSet {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TopicSet</code>.
	 */
	static Logger logger = Logger.getLogger(TopicSet.class.getName());

	protected static DecimalFormat nf = new DecimalFormat("0.000");

	private WeightedSet weightedSet;

	public TopicSet() {
		weightedSet = new WeightedSet();
	}

	public TopicSet(WeightedSet weightedSet) {
		this.weightedSet = weightedSet;
	}

	public void add(WeightedSet weightedSet) {
		this.weightedSet.add(weightedSet);
	}

	public void add(TopicSet topicSet) {
		this.weightedSet.add(topicSet.getWeightedSet());
	}

	public WeightedSet getWeightedSet() {
		return weightedSet;
	}

	public List<Topic> topicList() {
		return topicList(3);
	}

	public double dot(TopicSet anotherTopicSet) {
		if (weightedSet.size() == 0 || anotherTopicSet.size() == 0) {
			return 0;
		}

		double dot = 0;
		WeightedSet anotherWeightedSet = anotherTopicSet.getWeightedSet();
		Iterator<String> it = weightedSet.iterator();

		for (int i = 0; it.hasNext(); i++) {
			String topic = it.next();
			double topicScore = weightedSet.get(topic);
			double anotherTopicScore = anotherWeightedSet.get(topic);
			dot += topicScore * anotherTopicScore;
		}
		//logger.debug(dot+"="+topicList() +"*"+anotherTopicSet.topicList());
		return dot;
	}

	public List<Topic> topicList(int maxSize) {
		//logger.debug(weightedSet);
		List<Topic> topicList = new ArrayList<Topic>();
		SortedMap<Double, List<String>> sortedMap = weightedSet.toSortedMap();
		//	logger.debug(sortedMap);
		Iterator<Double> it = sortedMap.keySet().iterator();
		double norm = 0;
		for (int i = 0; it.hasNext() && i < maxSize; i++) {
			Double score = it.next();
			norm += score;
		}

		it = sortedMap.keySet().iterator();
		for (int i = 0; it.hasNext() && i < maxSize; i++) {
			Double score = it.next();
			List<String> list = sortedMap.get(score);
			double noramalizedScore = score / norm;
			//logger.debug(i + "\t" + score + "\t" + noramalizedScore + "\t" + list.size() + "\t" + list);
			for (int j = 0; j < list.size(); j++) {
				topicList.add(new Topic(list.get(j), noramalizedScore));
			}
		}
		return topicList;
	}

	public int size() {
		return weightedSet.size();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		Iterator<String> it = weightedSet.iterator();
		for (int i = 0; it.hasNext(); i++) {
			if (i > 0) {
				sb.append(", ");
			}
			String s = it.next();
			double w = weightedSet.get(s);
			sb.append(s);
			sb.append("=");
			sb.append(nf.format(w));

		}
		sb.append(")");
		return sb.toString();
	}
}

