/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.topic;

import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.index.CategorySuperCategorySearcher;
import eu.fbk.twm.index.PageCategorySearcher;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.twm.utils.WeightedSet;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 7/25/13
 * Time: 3:09 PM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * todo: move in machinelinking and create an index page -> top category (offline)
 *
 * @deprecated
 */
@Deprecated
public class PageTopicSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageTopicSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(PageTopicSearcher.class.getName());

	protected static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	protected static DecimalFormat nf = new DecimalFormat("0.000");

	public static final int DEFAULT_DEPTH = 10;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	PageCategorySearcher pageCategorySearcher;

	CategorySuperCategorySearcher categorySuperCategorySearcher;

	Properties properties;

	int maxDepth;

	public PageTopicSearcher(PageCategorySearcher pageCategorySearcher, CategorySuperCategorySearcher categorySuperCategorySearcher, int maxDepth, Properties properties) {
		this.pageCategorySearcher = pageCategorySearcher;
		this.categorySuperCategorySearcher = categorySuperCategorySearcher;
		this.maxDepth = maxDepth;
		this.properties = properties;
	}

	public PageTopicSearcher(String pageIndex, String categoryIndex, int maxDepth, Properties properties) throws IOException {
		pageCategorySearcher = new PageCategorySearcher(pageIndex);
		categorySuperCategorySearcher = new CategorySuperCategorySearcher(categoryIndex);
		this.properties = properties;
		this.maxDepth = maxDepth;
	}

	public PageTopicSearcher(String pageIndex, String categoryIndex, Properties properties) throws IOException {
		this(pageIndex, categoryIndex, DEFAULT_DEPTH, properties);
	}

	public PageTopicSearcher(String pageIndex, String categoryIndex, String cacheName, int minFreq, Properties properties) throws IOException {
		this(pageIndex, categoryIndex, properties);
		pageCategorySearcher.loadCache(cacheName, minFreq);
		categorySuperCategorySearcher.loadCache(cacheName, minFreq);
	}

	//todo: this must be done when the mapping is created
	public static String normalizePageName(String s) {
		if (s.length() == 0) {
			return s;
		}

		if (Character.isUpperCase(s.charAt(0))) {
			return s;
		}

		return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
	}

	private String tabulator(int l) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < l; i++) {
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
		}
		return sb.toString();
	}

	public void search(String[] categories, WeightedSet textWeightedSet, int depth, String category, Set<String> visitedSet) {
		//logger.debug(category + " searching: " + Arrays.toString(categories) + "\t" + depth + "...");


		if (depth > maxDepth) {
			//logger.debug("stop1 " + depth);
			return;
		}

		for (int i = 0; i < categories.length; i++) {
			//logger.debug(i + "\t" + categories[i]);
			//String label = topMap.get(categories[i]);
			String normalizedCategory = normalizePageName(categories[i]);
			//if (!visitedSet.contains(normalizedCategory)) {
			//visitedSet.add(normalizedCategory);
			//String label = categoryMapping.get(normalizedCategory);
			String label = properties.getProperty(normalizedCategory);
			//logger.debug(i + "\t" + category + "\t" + label);
			//String label = categories[i];
			if (label != null) {
				//logger.debug("<<<" + categories[i] + "\t" + label + "\t" + weight + "\t" + depth + ">>>");

				if (label.length() == 0) {
					//logger.warn("stop category " + normalizedCategory);
					System.out.println(tabulator(depth) + "<" + normalizedCategory + ", STOP, " + depth + ">");
				}
				//else if (!label.equals("Factotum")) {
				else {
					//textWeightedSet.add(label, weight * ((double) 1 / depth));
					//textWeightedSet.add(label, weight);
					//textWeightedSet.add(label, 1);
					System.out.println(tabulator(depth) + "<" + normalizedCategory + ", '" + label + "', " + depth + ">");
					textWeightedSet.add(label, ((double) 1 / depth));
				}
			}
			else {

				if (!visitedSet.contains(normalizedCategory)) {
					visitedSet.add(normalizedCategory);
					try {
						String[] superCategories = categorySuperCategorySearcher.search(normalizedCategory);
						if (superCategories.length > 0) {
							//logger.debug(i + "\t" + depth + "\t" + category + ": " + Arrays.toString(superCategories));
							System.out.println(tabulator(depth) + "{" + normalizedCategory + ", " + depth + ", " + superCategories.length + ", " + Arrays.toString(superCategories) + "}");
							search(superCategories, textWeightedSet, depth + 1, normalizedCategory, visitedSet);
						}

					} catch (Exception e) {
						logger.error(e);
					}
				}
			}
			//}
		}
		//weightedSet.add(prefix);
		//logger.debug("stop2 " + depth);
	}

	public WeightedSet search(String page, double weight) {
		String[] categories = pageCategorySearcher.search(page);

		WeightedSet weightedSet = new WeightedSet();

		long begin = System.nanoTime();
		////logger.debug("searching page " + page + " (" + weight + ")...");
		////logger.debug("categories\t" + categories.length + "\t" + Arrays.toString(categories));
		Set<String> visitedSet = new HashSet<String>();
		search(categories, weightedSet, 1, page, visitedSet);
		long end = System.nanoTime();
		long time = end - begin;
		/////logger.debug(page + "\t" + weightedSet.size() + "\t" + weightedSet.toSortedMap() + "\t" + nf.format(time));
		return weightedSet;
	}

	public void search(String page, WeightedSet weightedSet, double weight) {
		String[] categories = pageCategorySearcher.search(page);
		long begin = System.nanoTime();
		logger.debug("searching page " + page + " (" + weight + ")...");
		logger.debug("categories\t" + categories.length + "\t" + Arrays.toString(categories));
		Set<String> visitedSet = new HashSet<String>();
		search(categories, weightedSet, 1, page, visitedSet);
		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(page + "\t" + weightedSet.size() + "\t" + weightedSet.toSortedMap() + "\t" + nf.format(time));
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				String page = s[0].replace(' ', CharacterTable.LOW_LINE);
				//WeightedSet weightedSet = search(page);
				WeightedSet weightedSet = new WeightedSet();
				search(page, weightedSet, 1);
				long end = System.nanoTime();
				weightedSet.normalize();
				Map<Double, List<String>> sortedMap = weightedSet.toSortedMap();
				logger.debug(page + "\t" + sortedMap + "\t" + tf.format(end - begin));

			}
		}
	}

	@Override
	public String toString() {
		return "PageTopicSearcher{" +
				"properties=" + properties +
				", maxDepth=" + maxDepth +
				'}';
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option modelDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("model-dir").create("m");
			Option categoryMappingDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("directory from which to read the category mapping files").isRequired().withLongOpt("category-mapping-dir").create("c");
			//Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + AbstractWikipediaXmlDumpParser.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			//Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			//Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");
			Option depthOpt = OptionBuilder.withArgName("int").hasArg().withDescription("recursion depth (default is " + PageTopicSearcher.DEFAULT_DEPTH + ")").withLongOpt("depth").create();
			Option interactiveModeOpt = OptionBuilder.withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			logger.debug(options);
			//
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(interactiveModeOpt);
			options.addOption(wikipediaDumpOpt);
			options.addOption(modelDirOpt);
			options.addOption(categoryMappingDirOpt);
			//options.addOption(numThreadOpt);
			//options.addOption(numPageOpt);
			options.addOption(depthOpt);
			//options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}


			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("model-dir"));

			Properties properties = new Properties();
			if (line.hasOption("category-mapping-dir")) {
				String categoryMappingDir = line.getOptionValue("category-mapping-dir");
				if (!categoryMappingDir.endsWith(File.separator)) {
					categoryMappingDir += File.separator;
				}
				File file = new File(categoryMappingDir + extractorParameters.getLang() + ".properties");
				logger.info("reading category mappings from " + file + "...");
				properties.load(new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")));
				logger.debug(properties.size() + " mappings read");
			}

			int depth = DEFAULT_DEPTH;
			if (line.hasOption("depth")) {
				depth = Integer.parseInt(line.getOptionValue("depth"));
			}

			//logger.debug(extractorParameters);

			PageCategorySearcher pageCategorySearcher = new PageCategorySearcher(extractorParameters.getWikipediaPageCategoryIndexName());
			CategorySuperCategorySearcher categorySuperCategorySearcher = new CategorySuperCategorySearcher(extractorParameters.getWikipediaCategorySuperCategoryIndexName());

			PageTopicSearcher PageTopicSearcher = new PageTopicSearcher(pageCategorySearcher, categorySuperCategorySearcher, depth, properties);
			if (line.hasOption("interactive-mode")) {

				try {
					PageTopicSearcher.interactive();
				} catch (Exception e) {
					logger.error(e);
				}
			}

		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/jservice.jar com.machinelinking.annotation.topic.PageTopicSearcher", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
