/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.topic;

import org.apache.log4j.Logger;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.WeightedSet;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/16/13
 * Time: 11:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class Topic {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Topic</code>.
	 */
	static Logger logger = Logger.getLogger(Topic.class.getName());

	private static final String AIRPEDIA_ADDRESS = "http://www.airpedia.org/topic/class/";

	private static Pattern colonPattern = Pattern.compile(":");

	URL url;

	String label;

	double confidence;

	public Topic(String label, double confidence) {
		//String[] s = colonPattern.split(label);
		//label = s[s.length - 1];
		//logger.debug("category label " + label);
		this.label = label.replace(CharacterTable.LOW_LINE, CharacterTable.SPACE);
		try {
			this.url = new URL(AIRPEDIA_ADDRESS + label);
			//logger.debug("category url " + url);
		} catch (MalformedURLException e) {
			logger.equals(e);
		}
		this.confidence = confidence;
		//this.resource = resource;
	}

	public double getConfidence() {
		return confidence;
	}

	public String getLabel() {
		return label;
	}

	public URL getURL() {
		return url;
	}

	public int compareTo(Topic o) {
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Topic)) {
			return false;
		}

		Topic topic = (Topic) o;

		if (label != null ? !label.equals(topic.label) : topic.label != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return label != null ? label.hashCode() : 0;
	}

	@Override
	public String toString() {
		return label+"\t"+confidence;
		/*"Topic{" +
				"confidence=" + confidence +
				", url=" + url +
				", label='" + label + '\'' +
				'}';*/
	}

	/*public String toString() {
		StringWriter w = new StringWriter();
		try {
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createJsonGenerator(w);
			g.writeStartObject();
			g.writeStringField("id", label);
			g.writeStringField("url", url.toString());
			g.writeEndObject();
			g.close();

		} catch (IOException e) {
			logger.error(e);
		}
		return w.toString();
	}  */


	public static List<Topic> createTopicList(WeightedSet textWeightedSet, int maxSize) {
		//logger.debug(textWeightedSet);
		List<Topic> topicList = new ArrayList<Topic>();
		SortedMap<Double, List<String>> sortedMap = textWeightedSet.toSortedMap();
	//	logger.debug(sortedMap);
		Iterator<Double> it = sortedMap.keySet().iterator();
		double norm = 0;
		for (int i = 0; it.hasNext() && i < maxSize; i++) {
			Double score = it.next();
			norm += score;
		}

		it = sortedMap.keySet().iterator();
		for (int i = 0; it.hasNext(); i++) {
			Double score = it.next();
			List<String> list = sortedMap.get(score);
			double noramalizedScore = score / norm;
			//logger.debug(i + "\t" + score + "\t" + noramalizedScore + "\t" + list.size() + "\t" + list);
			for (int j = 0; j < list.size(); j++) {
				topicList.add(new Topic(list.get(j), noramalizedScore));
			}
		}
		return topicList;
	}

	public static Topic[] createTopicArray(WeightedSet textWeightedSet, int maxSize) {
		List<Topic> topicList = createTopicList(textWeightedSet, maxSize);
		Topic[] topicArray = new Topic[topicList.size()];
		return topicList.toArray(topicArray);
	}
}
