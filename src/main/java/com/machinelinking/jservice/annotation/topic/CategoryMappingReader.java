/*
 * Copyright (2012) Machine Linking srl
 *
 * Machine Linking reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced in any
 * form whatsoever except as provided by license without the written
 * consent of Machine Linking.  A license under Machine Linking's
 * rights in the Program may be available directly from Machine Linking.
 */

package com.machinelinking.jservice.annotation.topic;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/25/13
 * Time: 5:51 PM
 * To change this template use File | Settings | File Templates.
 *
 * @deprecated replaced with Properties
 */
@Deprecated public class CategoryMappingReader {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CategoryMappingReader</code>.
	 */
	static Logger logger = Logger.getLogger(CategoryMappingReader.class.getName());

	/*public static Map<String, String> read(File f) throws IOException {
		Map<String, String> map = new HashMap<String, String>();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line;


		while ((line = lnr.readLine()) != null) {

			if (line.length() > 0 && !line.startsWith("#")) {
				String[] s = line.split("=");
				if (s.length == 2) {
					String[] t = s[0].trim().split(":");
					map.put(t[t.length - 1], s[1].trim());
				}


			}
		}
		logger.debug(map);
		return map;
	}   */
}
