package com.machinelinking.jservice.annotation.topic;

import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/19/13
 * Time: 12:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopicDetector {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TopicDetector</code>.
	 */
	static Logger logger = Logger.getLogger(TopicDetector.class.getName());

	public static final int DEFAULT_MAX_NUMBER_OF_TOPICS_TO_RETURN = 3;

	private static TopicDetector ourInstance;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	Configuration config;

	AutomaticConfiguration automaticConfiguration;

	private int maxNumberOfTopicsToReturn;

	private int maxNumberOfKeywords;

	public static final int DEFAULT_MAX_NUMBER_OF_KEYWORDS = 5;

	public static TopicDetector getInstance() {
		if (ourInstance == null) {
			ourInstance = new TopicDetector();
		}
		return ourInstance;
	}

	private TopicDetector() {
		logger.debug("creating a topic detector...");
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();
			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();
			if (config.getProperty("max-number-of-topics-to-return") != null) {
				maxNumberOfTopicsToReturn = config.getInt("max-number-of-topics-to-return");
			}
			else {
				maxNumberOfTopicsToReturn = DEFAULT_MAX_NUMBER_OF_TOPICS_TO_RETURN;
			}

			if (config.getProperty("max-number-of-keywords-per-topic") != null) {
				maxNumberOfKeywords = config.getInt("max-number-of-keywords-per-topic");
			}
			else {
				maxNumberOfKeywords = DEFAULT_MAX_NUMBER_OF_KEYWORDS;
			}

			logger.info("supported languages " + Arrays.toString(languages));

		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	public TopicSet detect(Keyword[] keywordArray) {

		TopicSet textTopicSet = new TopicSet();
		long begin = System.nanoTime();
		logger.debug("detecting topic " + keywordArray.length + "...");
		int count = 0;

		for (int i = 0; i < keywordArray.length && count < 5; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].hasSense()) {
				TopicSet keywordTopicSet = keywordArray[i].getTopic();
				//logger.debug(i+"\t"+ keywordArray[i].getAirpediaClassList());
				if (keywordTopicSet != null && !keywordArray[i].hasAirpediaClass("PopulatedPlace")) {

					//logger.debug(i + "\t" + keywordArray[i].getSense() + "\t" + keywordTopicSet);
					textTopicSet.add(keywordTopicSet);
					count++;
				}
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(count + "/" + keywordArray.length + " keywords considered in " + nf.format(time) + " ns");

		return textTopicSet;
	}
}
