package com.machinelinking.jservice.annotation.topic;

import org.apache.log4j.Logger;
import eu.fbk.twm.classifier.ContextualSense;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/19/13
 * Time: 6:50 PM
 * To change this template use File | Settings | File Templates.
 * @deprecated
 */
@Deprecated public class TopicalSense extends ContextualSense{
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TopicalSense</code>.
	 */
	static Logger logger = Logger.getLogger(TopicalSense.class.getName());
	private String label;
	private double value;

	public TopicalSense(ContextualSense sense, String label, double value) {
		super(sense.getPage(), sense.getPrior(), sense.getBow(), sense.getLs());

		this.label = label;
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public String getLabel() {
		return label;
	}

	@Override
	public double getCombo() {
		return (bow+ls+value)/3;
	}

	@Override
	public String toString() {
		return page + "\t" + prior + "\t" + bow + "\t" + ls + "\t" + value+"\t"+ getCombo();
	}
}
