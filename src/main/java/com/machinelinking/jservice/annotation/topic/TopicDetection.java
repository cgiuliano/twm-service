package com.machinelinking.jservice.annotation.topic;

import com.machinelinking.jservice.util.AutomaticConfiguration;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import eu.fbk.twm.utils.WeightedSet;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/31/12
 * ServiceTime: 5:55 PM
 * To change this template use File | Settings | File Templates.
 *
 * @deprecated
 */
@Deprecated public class TopicDetection {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TopicDetection</code>.
	 */
	static Logger logger = Logger.getLogger(TopicDetection.class.getName());

	public static final int DEFAULT_MAX_NUMBER_OF_TOPICS_TO_RETURN = 10;

	public static final int DEFAULT_MAX_NUMBER_OF_TOPICS_TO_PROCESS = 10;

	public static final int DEFAULT_MAX_NUMBER_OF_KEYWORDS = 5;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	Configuration config;

	Map<Locale, PageTopicSearcher> topCategorySearcherMap;

	AutomaticConfiguration automaticConfiguration;

	private static TopicDetection ourInstance = new TopicDetection();

	private int maxNumberOfTopicsToReturn;

	private int maxNumberOfTopicsToProcess;

	private int maxNumberOfKeywords;

	public static TopicDetection getInstance() {
		return ourInstance;
	}

	private TopicDetection() {
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

			if (config.getProperty("max-number-of-keywords-per-topic") != null) {
				maxNumberOfKeywords = config.getInt("max-number-of-keywords-per-topic");
			}
			else {
				maxNumberOfKeywords = DEFAULT_MAX_NUMBER_OF_KEYWORDS;
			}

			if (config.getProperty("max-number-of-topics-to-return") != null) {
				maxNumberOfTopicsToReturn = config.getInt("max-number-of-topics-to-return");
			}
			else {
				maxNumberOfTopicsToReturn = DEFAULT_MAX_NUMBER_OF_TOPICS_TO_RETURN;
			}

			if (config.getProperty("max-number-of-topics-to-process") != null) {
				maxNumberOfTopicsToProcess = config.getInt("max-number-of-topics-to-process");
			}
			else {
				maxNumberOfTopicsToProcess = DEFAULT_MAX_NUMBER_OF_TOPICS_TO_PROCESS;
			}

			logger.debug(maxNumberOfKeywords + "\t" + maxNumberOfTopicsToReturn);

			automaticConfiguration = AutomaticConfiguration.getInstance();
			String[] languages = automaticConfiguration.getLanguages();

			logger.info("supported languages " + Arrays.toString(languages));
			init(languages);
		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}

	}

	void init(String[] languages) {
		logger.debug(languages.length);
		topCategorySearcherMap = new HashMap<Locale, PageTopicSearcher>();

		for (int i = 0; i < languages.length; i++) {
			Map<String, String> resourceMap = automaticConfiguration.getResources(languages[i]);
			String pageCategoryIndexName = resourceMap.get("page-category-index");
			String categorySuperCategoryIndexName = resourceMap.get("category-super-category-index");
			String cacheName = resourceMap.get("page-freq.csv");
			logger.debug(pageCategoryIndexName + "\t" + categorySuperCategoryIndexName + "\t" + cacheName);
			int minFreq = config.getInt("page-min-freq");

			try {
				if (pageCategoryIndexName != null && categorySuperCategoryIndexName != null) {
					Locale locale = new Locale(languages[i]);

					String dir = config.getString("category-mapping-dir");
					if (!dir.endsWith(File.separator)) {
						dir += File.separator;
					}
					File file = new File(dir + languages[i] + ".properties");
					logger.debug("reading category mappings from " + file + "...");
					//Map<String, String> categoryMapping = CategoryMappingReader.read(file);
					Properties properties = new Properties();
					properties.load(new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")));
					//properties.load(new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")));
					logger.debug(properties.size() + " category mappings read");
					PageTopicSearcher pageTopicSearcher = new PageTopicSearcher(pageCategoryIndexName, categorySuperCategoryIndexName, cacheName, minFreq, properties);
					topCategorySearcherMap.put(locale, pageTopicSearcher);
				}
				else {
					logger.warn(pageCategoryIndexName + " or " + categorySuperCategoryIndexName + " not found");
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public int getMaxNumberOfTopicsToProcess() {
		return maxNumberOfTopicsToProcess;
	}

	public void setMaxNumberOfTopicsToProcess(int maxNumberOfTopicsToProcess) {
		this.maxNumberOfTopicsToProcess = maxNumberOfTopicsToProcess;
	}

	public int getMaxNumberOfKeywords() {
		return maxNumberOfKeywords;
	}

	public void setMaxNumberOfKeywords(int maxNumberOfKeywords) {
		this.maxNumberOfKeywords = maxNumberOfKeywords;
	}

	public int getMaxNumberOfTopicsToReturn() {
		return maxNumberOfTopicsToReturn;
	}

	public void setMaxNumberOfTopicsToReturn(int maxNumberOfTopicsToReturn) {
		this.maxNumberOfTopicsToReturn = maxNumberOfTopicsToReturn;
	}
	/*
	public List<Topic> detect(Keyword[] keywordArray, Locale locale) {
		PageTopicSearcher pageTopicSearcher = topCategorySearcherMap.get(locale);

		long begin = System.nanoTime();
		WeightedSet textWeightedSet = new WeightedSet();
		//todo: using only the n-best?
		//for (int i = 0; i < keywordArray.length; i++) {
		int counter = 0;
		int placeCounter = 0;
		for (int i = 0; i < keywordArray.length && i < maxNumberOfKeywords; i++) {
			if (!keywordArray[i].isFiltered() && keywordArray[i].getSense() != null) {
				logger.debug(i + "\t" + keywordArray[i].getAirpediaClassList());
				double pageWeight = keywordArray[i].weight();
				WeightedSet pageWeightedSet = pageTopicSearcher.search(keywordArray[i].getSense().getPage(), pageWeight);
				List<Topic> pageTopicList = Topic.createTopicList(pageWeightedSet, maxNumberOfTopicsToProcess);
				logger.debug(i + "\t" + keywordArray[i].getSense() + "\t" + pageTopicList);
				keywordArray[i].setTopicList(pageTopicList);
				addTopic(textWeightedSet, pageTopicList, pageWeight);
				//textWeightedSet.add(pageWeightedSet);
				counter++;


				logger.debug("\n\n");
			}
		}

		long end = System.nanoTime();
		long time = end - begin;
		logger.debug(keywordArray.length + " keywords analyzed in " + nf.format(time) + " ns");

		logger.debug("last " + textWeightedSet);
		return Topic.createTopicList(textWeightedSet, maxNumberOfTopicsToReturn);
	}
      */
	private void addTopic(WeightedSet weightedSet, List<Topic> topicList, double pageWeight) {
		logger.debug(weightedSet.size() + "\t" + topicList.size() + "\t" + pageWeight);
		for (int i = 0; i < topicList.size(); i++) {
			Topic topic = topicList.get(i);
			String label = topic.getLabel();
			double topicWeight = pageWeight * topic.getConfidence();
			logger.debug(label + "\t" + pageWeight + " * " + topic.getConfidence() + " = " + topicWeight);
			weightedSet.add(label, topicWeight);
		}
	}
/*	private List<Topic> createTopicList(WeightedSet textWeightedSet, int placeCounter) {
		logger.debug(textWeightedSet);
		List<Topic> topicList = new ArrayList<Topic>();

		SortedMap<Double, List<String>> sortedMap = textWeightedSet.toSortedMap();
		//Integer bestScore = sortedMap.firstKey();
		logger.debug(sortedMap);
		Iterator<Double> it = sortedMap.keySet().iterator();
		for (int i = 0; it.hasNext() && i < MAX_NUMBER_OF_TOPICS_TO_RETURN; i++) {
			Double score = it.next();
			List<String> list = sortedMap.get(score);
			double noramalizedScore = score / textWeightedSet.total();
			logger.debug(i + "\t" + score + "\t" + noramalizedScore + "\t" + list.size() + "\t" + list);
			for (int j = 0; j < list.size(); j++) {
				topicList.add(new Topic(list.get(j), noramalizedScore));
			}
		}

		//todo: this rule is added for...

		//logger.debug(placeCounter + "\t" + sortedMap.size());
		//if (placeCounter > 3 && sortedMap.size() == 0) {
		//	topicList.add(new Topic("Travel", 0.8));
		//}

		//List<String> bestTopic = sortedMap.get(bestScore);
		//double confidence = (double) bestScore / freqSet.total();
		//for (int i = 0; i < bestTopic.size(); i++) {
		//	topicList.add(new Topic(bestTopic.get(i), confidence));
		//}
		return topicList;
	}

	private boolean isPopulatedPlace(List<ClassResource> airpediaClassList) {

		for (int i = 0; i < airpediaClassList.size(); i++) {
			ClassResource classResource = airpediaClassList.get(i);
			//if (classResource.getLabel().equals("PopulatedPlace")) {
			if (classResource.getLabel().equals("Place")) {
				return true;
			}
		}
		return false;
	}

	public String detect(String page, Locale locale) {
		long begin = System.nanoTime();


		long end = System.nanoTime();
		long time = end - begin;

		logger.info(page + " enhanced in " + nf.format(time) + " ns");
		return null;
	} */
}
