package com.machinelinking.jservice.annotation;

import org.apache.log4j.Logger;

import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 8/28/13
 * Time: 9:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class TextAnnotation implements Annotation {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TextAnnotation</code>.
	 */
	static Logger logger = Logger.getLogger(TextAnnotation.class.getName());

	protected String text;

	protected Locale locale;

	//protected Tokenizer tokenizer;

	public TextAnnotation(String text, Locale locale) {
		this.text = text;
		this.locale = locale;
		//Token[] tokenArray = tokenizer.tokenArray(annotateParameter.getText());
	}


}
