package com.machinelinking.jservice.summary;

import com.machinelinking.jservice.analysis.Sentence;
import com.machinelinking.jservice.annotation.key.Keyword;
import com.machinelinking.jservice.annotation.key.NGram;
import com.machinelinking.jservice.util.ConfigurationReader;
import com.machinelinking.jservice.util.ConfigurationReaderException;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 6/6/13
 * Time: 11:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class TextSummarization {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TextSummarization</code>.
	 */
	static Logger logger = Logger.getLogger(TextSummarization.class.getName());

	Configuration config;

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static TextSummarization ourInstance = null;

	public static final int SUM_SCORE = 1;

	public static final int AVERAGE_SCORE = 2;

	public static final int ARG_MAX_SCORE = 3;

	public static synchronized TextSummarization getInstance() {
		if (ourInstance == null) {
			try {
				ourInstance = new TextSummarization();
			} catch (IOException e) {
				logger.error(e);
			}
		}
		return ourInstance;
	}

	private TextSummarization() throws IOException {
		logger.debug("creating text similarity...");
		ConfigurationReader configurationReader = new ConfigurationReader();
		try {
			config = configurationReader.getConfiguration();

		} catch (ConfigurationReaderException e) {
			logger.error(e);
		}
	}

	class WeightedNGramComparator implements Comparator<WeightedNGram> {
		public int compare(WeightedNGram w1, WeightedNGram w2) {
			double diff = w1.getnGram().getStart() - w2.getnGram().getStart();
			if (diff < 0) {
				return -1;
			}
			else if (diff > 0) {
				return 1;
			}
			return 0;
		}
	}

	class PositionSentenceComparator implements Comparator<WeightedSentence> {
		public int compare(WeightedSentence w1, WeightedSentence w2) {
			double diff = w1.getSentence().getStart() - w2.getSentence().getStart();
			if (diff < 0) {
				return -1;
			}
			else if (diff > 0) {
				return 1;
			}
			return 0;
		}
	}

	class WeightedSentenceComparator implements Comparator<WeightedSentence> {
		public int compare(WeightedSentence w1, WeightedSentence w2) {
			double diff = w2.getWeight() - w1.getWeight();
			if (diff < 0) {
				return -1;
			}
			else if (diff > 0) {
				return 1;
			}
			return 0;
		}
	}

	class WeightedNGram {

		private double weight;

		private NGram nGram;

		WeightedNGram(NGram nGram, double weight) {
			this.nGram = nGram;
			this.weight = weight;
		}

		NGram getnGram() {
			return nGram;
		}

		void setnGram(NGram nGram) {
			this.nGram = nGram;
		}

		double getWeight() {
			return weight;
		}

		void setWeight(double weight) {
			this.weight = weight;
		}

		@Override
		public String toString() {
			return weight + "\t" + nGram;
		}
	}

	private SortedSet<WeightedNGram> createWeightedNGramList(Keyword[] keywordArray) {
		SortedSet<WeightedNGram> sortedSet = new TreeSet<WeightedNGram>(new WeightedNGramComparator());
		for (int i = 0; i < keywordArray.length; i++) {
			if (!keywordArray[i].isFiltered()) {
				List<NGram> nGramList = keywordArray[i].getNGramList();
				for (int j = 0; j < nGramList.size(); j++) {
					//logger.debug(j + "\t" + nGramList.get(j) + "\t" + keywordArray[i].weight());
					sortedSet.add(new WeightedNGram(nGramList.get(j), keywordArray[i].weight()));
				}
			}
		}
		//logger.debug(sortedSet);
		return sortedSet;
	}

	public WeightedSentence[] summary(Keyword[] keywordArray, Sentence[] sentenceArray, double ratio) {
		return summary(keywordArray, sentenceArray, ratio, SUM_SCORE);
	}

	public WeightedSentence[] summary(Keyword[] keywordArray, Sentence[] sentenceArray, double ratio, int scoreFunction) {
		//logger.debug("scoreFunction " + scoreFunction);
		//todo: handle sentences with the same score
		SortedSet<WeightedSentence> sentenceSet = new TreeSet<WeightedSentence>(new WeightedSentenceComparator());

		int k = 0, n = 0;
		double score = 0;
		double maxScore = 0;
		Iterator<WeightedNGram> it = createWeightedNGramList(keywordArray).iterator();
		for (int j = 0; it.hasNext(); j++) {
			WeightedNGram weightedNGram = it.next();
			NGram nGram = weightedNGram.getnGram();
			//logger.debug(j + "\t" + weightedNGram.toString());

			//logger.debug("\t" + j + "\t" + nGram.getStart() + "\t" + nGram.getEnd() + "\t" + nGram.getStartIndex() + "\t" + nGram.getEndIndex());
			//logger.debug("\t\t" + sentenceArray[k].getEndIndex());
			if (nGram.getEndIndex() > sentenceArray[k].getEndIndex()) {
				//logger.debug("exit\t<<" + k + ">>\t" + score + "\t" + sentenceArray[k]);
				WeightedSentence weightedSentence = new WeightedSentence(sentenceArray[k]);
				if (scoreFunction == AVERAGE_SCORE) {
					score /= n;
				}
				weightedSentence.setWeight(score);
				sentenceSet.add(weightedSentence);
				k++;
				score = 0;
				maxScore = 0;
			}
			else {
				if (scoreFunction == AVERAGE_SCORE) {
					score += weightedNGram.getWeight();
				}
				else if (scoreFunction == ARG_MAX_SCORE) {

					if (score < weightedNGram.getWeight()) {
						score = weightedNGram.getWeight();
					}
				}
				else {
					score += weightedNGram.getWeight();
				}
				n++;
				//logger.debug("score = " + score);
			}
		}


		//logger.debug("out\t{{" + k + "}}\t" + score + "\t" + sentenceArray[k]);
		WeightedSentence weightedSentence = new WeightedSentence(sentenceArray[k]);
		weightedSentence.setWeight(score);
		sentenceSet.add(weightedSentence);
		//logger.debug("size " + sentenceSet.size());
		//logger.debug("sentenceSet " + sentenceSet);
		int size = (int) Math.round((double) ratio * sentenceSet.size());
		//logger.debug("size " + size + " = " + ratio + " * " + sentenceSet.size());
		//WeightedSentence[] weightedSentenceArray = new WeightedSentence[size];
		SortedSet<WeightedSentence> weightedSentenceSet = new TreeSet<WeightedSentence>(new PositionSentenceComparator());
		//logger.debug(weightedSentenceSet.size());
		double maxWeight = 0;
		Iterator<WeightedSentence> it1 = sentenceSet.iterator();
		for (int m = 0; it1.hasNext() && m < size; m++) {
			WeightedSentence weightedSentence1 = it1.next();
			//WeightedSentence weightedSentence1 = it1.next();
			//logger.debug(m + "\t"+ weightedSentence1);
			//weightedSentenceArray[m] = weightedSentence1;
			//logger.debug(m + "\t" + weightedSentence1);
			weightedSentenceSet.add(weightedSentence1);
			if (weightedSentence1.getWeight() > maxWeight) {
				maxWeight = weightedSentence1.getWeight();
				//logger.debug(m + "\t" + maxWeight);
			}
		}

		//logger.debug(weightedSentenceSet);
		//logger.debug("maxWeight\t" + maxWeight);

		WeightedSentence[] weightedSentenceArray = new WeightedSentence[weightedSentenceSet.size()];
		Iterator<WeightedSentence> it2 = weightedSentenceSet.iterator();
		for (int m = 0; it2.hasNext() && m < weightedSentenceSet.size(); m++) {
			WeightedSentence weightedSentence1 = it2.next();
			weightedSentence1.setWeight(weightedSentence1.getWeight() / maxWeight);
			//logger.debug(m + "\t"+ weightedSentence1);
			weightedSentenceArray[m] = weightedSentence1;
		}
		return weightedSentenceArray;
	}

}
