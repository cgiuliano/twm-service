package com.machinelinking.jservice.summary;

import com.machinelinking.jservice.analysis.Sentence;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 6/6/13
 * Time: 2:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class WeightedSentence {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WeightedSentence</code>.
	 */
	static Logger logger = Logger.getLogger(WeightedSentence.class.getName());

	private Sentence sentence;

	private double weight;

	public WeightedSentence(Sentence sentence) {
		this.sentence = sentence;
	}

	public Sentence getSentence() {
		return sentence;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "WeightedSentence{" +
				"sentence=" + sentence +
				", weight=" + weight +
				'}';
	}
}
