#! /bin/tcsh

setenv CLASSPATH lib/jcore-alone.jar
setenv CLASSPATH ${CLASSPATH}:lib/thewikimachine-alone.jar
setenv CLASSPATH ${CLASSPATH}:lib/threescale-api-2.1.3.jar
setenv CLASSPATH ${CLASSPATH}:dist/jservice-lib.jar
setenv CLASSPATH ${CLASSPATH}:dist/jservice-alone.jar
