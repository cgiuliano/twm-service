#!/bin/bash

echo Intalling deps ...
echo

mvn install:install-file -Dfile=threescale-api-2.1.3.jar -DgroupId=net.threescale.api -DartifactId=threescale -Dversion=2.1.3 -Dpackaging=jar || { echo 'FAILURE'; exit 1; }
echo
echo
echo SUCCESS
